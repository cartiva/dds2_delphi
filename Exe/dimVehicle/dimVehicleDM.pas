unit dimVehicleDM;

interface

uses
  SysUtils, Classes, adsdata, adsfunc, adstable, adscnnct, DB, ADODB, DateUtils,
  IdSMTPBase, IdSMTP, IdMessage,
  Dialogs;

type
  TDM = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure PrepareQuery(query: TDataSet; sql: string='');
    procedure CloseQuery(query: TDataSet);
    function FormatWordToStr(const inVal: Word;
      const OutLength: Integer; const PadChar: Char = #48): String;
    function GetADSSqlTimeStamp(const DateTime: TDateTime): String;
    procedure OpenQuery(query: TDataSet; sql: string);
    procedure ExecuteQuery(query: TDataSet; sql: string);
    procedure SendMail(Subject: string; Body: string='');
    procedure ScrapeCountTable(db2Table, adsTable: string);
    procedure ScrapeCountQuery(db2Q, adsQ: string);
    procedure INPMAST;
  end;

var
  DM: TDM;
  AdoCon: TADOConnection;
  AdsCon: TADSConnection;
  AdoQuery: TAdoQuery;
  AdsQuery: TAdsQuery;
  StartTS: TDateTime;
  EndTS: TDateTime;

resourcestring
  executable = 'dimVehicle';

implementation

constructor TDM.Create(AOwner: TComponent);
begin
  AdoCon := TADOConnection.Create(nil);
  AdsCon := TADSConnection.Create(nil);
  AdoQuery := TADOQuery.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
  AdsCon.ConnectPath := '\\67.135.158.12:6363\Advantage\DDS\DDS.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
  AdoCon.Connected := True;
  AdsCon.Connect;
end;

destructor TDM.Destroy;
begin
  AdoQuery.Close;
  FreeAndNil(AdoQuery);
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  AdoCon.Connected := False;
  FreeAndNil(AdoCon);
  inherited;
end;

procedure TDM.INPMAST;
var
  proc: string;
  dObject: string;
  PassFail: string;

begin
  proc := 'INPMAST';
  dObject := 'dimVehicle';
  try
    try
      OpenQuery(AdsQuery, 'execute procedure checkDependencies(' + QuotedStr(executable) + ',' + QuotedStr(dObject) + ')');
      PassFail := AdsQuery.FieldByName('PassFail').AsString;
      CloseQuery(AdsQuery);
      if PassFail = 'Pass' then
        begin
          ExecuteQuery(AdsQuery, 'execute procedure UpdateNightlyBatch(' + QuotedStr(dObject) + ',True,False)');
          ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ')');
          ExecuteQuery(AdsQuery, 'delete from stgArkonaINPMAST');
          PrepareQuery(AdsQuery, ' insert into stgArkonaINPMAST(' +
              'IMCO#, IMVIN, IMSTK#, IMDOC#, IMSTAT, IMGTRN, IMTYPE, IMFRAN, ' +
              'IMYEAR, IMMAKE, IMMCODE, IMMODL, IMBODY, IMCOLR, IMODOM, IMDINV, ' +
              'IMDSVC, IMDDLV, IMDORD, IMSACT, IMIACT, IMLIC#, IMODOMA, IMKEY, IMPRIC, IMCOST) ' +
              'values(' +
              ':IMCO, :IMVIN, :IMSTK, :IMDOC, :IMSTAT, :IMGTRN, :IMTYPE, :IMFRAN, ' +
              ':IMYEAR, :IMMAKE, :IMMCODE, :IMMODL, :IMBODY, :IMCOLR, :IMODOM, :IMDINV, ' +
              ':IMDSVC, :IMDDLV, :IMDORD, :IMSACT, :IMIACT, :IMLIC, :IMODOMA, :IMKEY, :IMPRIC, :IMCOST)');
          OpenQuery(AdoQuery, 'select ' +
              '  imco#, imvin, imstk#, imdoc#, imstat, ' +
              '  imgtrn, imtype, imfran, imyear, immake, ' +
              '  immcode, immodl, imbody, imcolr, imodom, ' +
              '  case ' +
              '    when imdinv = 0 then cast(''9999-12-31'' as date) ' +
              '    else cast(left(digits(imdinv), 4) || ''-'' || substr(digits(imdinv), 5, 2) || ''-'' || substr(digits(imdinv), 7, 2) as date) ' +
              '  end as imdinv, ' +
              '  case ' +
              '    when imdsvc = 0 or imdsvc = 10101 then cast(''9999-12-31'' as date) ' +
              '    else cast(left(digits(imdsvc), 4) || ''-'' || substr(digits(imdsvc), 5, 2) || ''-'' || substr(digits(imdsvc), 7, 2) as date)  ' +
              '  end as imdsvc, ' +
//              '  case ' +
//              '    when imddlv = 0 then cast(''9999-12-31'' as date) ' +
//              '    else cast(left(digits(imddlv), 4) || ''-'' || substr(digits(imddlv), 5, 2) || ''-'' || substr(digits(imddlv), 7, 2) as date) ' +
//              '  end as imddlv, ' +

              '  case ' +
              '    when imddlv < 19000000 then cast(''9999-12-31'' as date) ' +
              '    else cast(left(digits(imddlv), 4) || ''-'' || substr(digits(imddlv), 5, 2) || ''-'' || substr(digits(imddlv), 7, 2) as date) ' +
              '  end as imddlv, ' +

              '  case ' +
              '    when imdord = 0 then cast(''9999-12-31'' as date) ' +
              '    else cast(left(digits(imdord), 4) || ''-'' || substr(digits(imdord), 5, 2) || ''-'' || substr(digits(imdord), 7, 2) as date) ' +
              '  end as imdord, ' +
              '  imsact, imiact, imlic#, imodoma, imkey, impric, imcost ' +
              'from rydedata.inpmast ' +
//              'where imvin = ''3FA6P0K97ER169771''');
              'where imco# = ''RY1''');

//              'where imco# = ''RY1'' ' +
//              'order by imvin');


          while not AdoQuery.Eof do
          begin
            AdsQuery.ParamByName('imco').AsString := AdoQuery.FieldByName('imco#').AsString;
            AdsQuery.ParamByName('imvin').AsString := AdoQuery.FieldByName('imvin').AsString;
            AdsQuery.ParamByName('imstk').AsString := AdoQuery.FieldByName('imstk#').AsString;
            AdsQuery.ParamByName('imdoc').AsString := AdoQuery.FieldByName('imdoc#').AsString;
            AdsQuery.ParamByName('imstat').AsString := AdoQuery.FieldByName('imstat').AsString;
            AdsQuery.ParamByName('imgtrn').AsString := AdoQuery.FieldByName('imgtrn').AsString;
            AdsQuery.ParamByName('imtype').AsString := AdoQuery.FieldByName('imtype').AsString;
            AdsQuery.ParamByName('imfran').AsString := AdoQuery.FieldByName('imfran').AsString;
            AdsQuery.ParamByName('imyear').AsInteger := AdoQuery.FieldByName('imyear').AsInteger;
            AdsQuery.ParamByName('immake').AsString := AdoQuery.FieldByName('immake').AsString;
            AdsQuery.ParamByName('immcode').AsString := AdoQuery.FieldByName('immcode').AsString;
            AdsQuery.ParamByName('immodl').AsString := AdoQuery.FieldByName('immodl').AsString;
            AdsQuery.ParamByName('imbody').AsString := AdoQuery.FieldByName('imbody').AsString;
            AdsQuery.ParamByName('imcolr').AsString := AdoQuery.FieldByName('imcolr').AsString;
            AdsQuery.ParamByName('imodom').AsInteger := AdoQuery.FieldByName('imodom').AsInteger;
            AdsQuery.ParamByName('imdinv').AsDateTime := AdoQuery.FieldByName('imdinv').AsDateTime;
            AdsQuery.ParamByName('imdsvc').AsDateTime := AdoQuery.FieldByName('imdsvc').AsDateTime;
            AdsQuery.ParamByName('imddlv').AsDateTime := AdoQuery.FieldByName('imddlv').AsDateTime;
            AdsQuery.ParamByName('imdord').AsDateTime := AdoQuery.FieldByName('imdord').AsDateTime;
            AdsQuery.ParamByName('imsact').AsString := AdoQuery.FieldByName('imsact').AsString;
            AdsQuery.ParamByName('imiact').AsString := AdoQuery.FieldByName('imiact').AsString;
            AdsQuery.ParamByName('imlic').AsString := AdoQuery.FieldByName('imlic#').AsString;
            AdsQuery.ParamByName('imodoma').AsString := AdoQuery.FieldByName('imodoma').AsString;
            AdsQuery.ParamByName('imkey').AsInteger := AdoQuery.FieldByName('imkey').AsInteger;
            AdsQuery.ParamByName('impric').AsCurrency := AdoQuery.FieldByName('impric').AsCurrency;
            AdsQuery.ParamByName('imcost').AsCurrency := AdoQuery.FieldByName('imcost').AsCurrency;
//
//            writeln(AdoQuery.FieldByName('imvin').AsString);
//
            AdsQuery.ExecSQL;
            AdoQuery.Next;
          end;
          ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaINPMAST'')');
          ScrapeCountQuery(' rydedata.inpmast where imco# = ''RY1''', 'stgArkonaINPMAST');
          ExecuteQuery(AdsQuery, 'execute procedure xfmDimVehicle()');
          ExecuteQuery(AdsQuery, 'execute procedure UpdateNightlyBatch(' + QuotedStr(dObject) + ',False,True)');
          ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            'null' + ')');
        end
      else
        sendmail(executable + '.' + proc + '.' + dObject + ' failed CheckDependency', '');
    except
      on E: Exception do
      begin
        ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
//
        Raise Exception.Create(proc + '.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;
{$region 'Utilities'}
function TDM.GetADSSqlTimeStamp(const DateTime: TDateTime): String;
var
  ye, mo, da, ho, mi, se, ms: Word;
begin
  DecodeDateTime(DateTime, ye, mo, da, ho, mi, se, ms);
  Result := FormatWordToStr(ye, 4) + '-' +
            FormatWordToStr(mo, 2) + '-' +
            FormatWordToStr(da, 2) + ' ' +
            FormatWordToStr(ho, 2) + ':' +
            FormatWordToStr(mi, 2) + ':' +
            FormatWordToStr(se, 2);
end;
function TDM.FormatWordToStr(const inVal: Word;
  const OutLength: Integer; const PadChar: Char): String;
begin
  Result := IntToStr(inVal);
  if Length(Result) < OutLength then
    Result := StringOfChar(PadChar, OutLength - Length(Result)) + Result;
end;
procedure TDM.ExecuteQuery(query: TDataSet; sql: string);
begin
    if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
    begin
      AdsQuery.AdsConnection := AdsCon;
      CloseQuery(AdsQuery);
      AdsQuery.SQL.Text := sql;
      AdsQuery.ExecSQL;
      CloseQuery(AdsQuery);
    end
    else if query.ClassParent.ClassName = 'TCustomADODataSet' then
    begin
      AdoQuery.Connection := AdoCon;
      CloseQuery(AdoQuery);
      AdoQuery.SQL.Text := sql;
      AdoQuery.ExecSQL;
      CloseQuery(AdoQuery);
    end;
end;
procedure TDM.OpenQuery(query: TDataSet; sql: string);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
    AdsQuery.SQL.Text := sql;
    AdsQuery.Open;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
    AdoQuery.SQL.Text := sql;
    AdoQuery.Open;
  end;
end;
procedure TDM.PrepareQuery(query: TDataSet; sql: string);
begin
  if query.classparent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    if sql <> '' then
      AdsQuery.SQL.Text := sql;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    if sql <> '' then
      AdoQuery.SQL.Text := sql;
  end;
end;
procedure TDM.SendMail(Subject: string; Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
        exit;
      end;
    end;
  finally
    FreeAndNil(Msg);
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;
procedure TDM.CloseQuery(query: TDataSet);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
  end;
end;
procedure TDM.ScrapeCountQuery(db2Q, adsQ: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Q);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsQ);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckQuery between ' + db2Q + '(' + IntToStr(db2Count) + ') and ' + adsQ + '(' + IntToStr(adsCount) + ')  failed');
end;
procedure TDM.ScrapeCountTable(db2Table, adsTable: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  db2Table := 'rydedata.' + db2Table;
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Table);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsTable);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckTable between ' + db2Table + '(' + IntToStr(db2Count) + ') and ' + adsTable + '(' + IntToStr(adsCount) + ')  failed');
end;
{$endregion}
end.


