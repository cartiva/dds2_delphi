unit dimCustomerDM;

interface

uses
  SysUtils, Classes, adsdata, adsfunc, adstable, adscnnct, DB, ADODB, DateUtils,
  IdSMTPBase, IdSMTP, IdMessage,
  Dialogs;

type
  TDM = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Log(job, routine: string; jobStartTS, jobEndTS: TDateTime);
    procedure PrepareQuery(query: TDataSet; sql: string='');
    procedure CloseQuery(query: TDataSet);
    function FormatWordToStr(const inVal: Word;
      const OutLength: Integer; const PadChar: Char = #48): String;
    function GetADSSqlTimeStamp(const DateTime: TDateTime): String;
    procedure OpenQuery(query: TDataSet; sql: string);
    procedure ExecuteQuery(query: TDataSet; sql: string);
    procedure curLoadPrevLoad(tableName: string);
    procedure curLoadIsEmpty(tableName: string);
    procedure SendMail(Subject: string; Body: string='');
    procedure ScrapeCountTable(db2Table, adsTable: string);
    procedure ScrapeCountQuery(db2Q, adsQ: string);
    procedure BOPNAME;
    procedure emailUpdate;
  end;

var
  DM: TDM;
  AdoCon: TADOConnection;
  AdsCon: TADSConnection;
  AdoQuery: TAdoQuery;
  AdsQuery: TAdsQuery;
  LogQuery: TAdsQuery;
  stgErrorLogQuery: TAdsQuery;
  StartTS: TDateTime;
  EndTS: TDateTime;

resourcestring
  executable = 'dimCustomer';

implementation

constructor TDM.Create(AOwner: TComponent);
begin
  AdoCon := TADOConnection.Create(nil);
  AdsCon := TADSConnection.Create(nil);
  AdoQuery := TADOQuery.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  LogQuery := TAdsQuery.Create(nil);
  stgErrorLogQuery := TAdsQuery.Create(nil);
  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
//    AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=odbc0210;Persist Security Info=True;User ID=rydeodbc;Data Source=ArkonaSSL';
//  AdsCon.ConnectPath := '\\jon520:6363\Advantage\DDS\DDS.add';
  AdsCon.ConnectPath := '\\67.135.158.12:6363\Advantage\DDS\DDS.add';
//  AdsCon.ConnectPath := '\\67.135.158.12:6363\DailyCut\DDS\monday\copy\DDS.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
  AdoCon.Connected := True;
  AdsCon.Connect;
  LogQuery.AdsConnection := AdsCon;
  stgErrorLogQuery.AdsConnection := AdsCon;
end;

destructor TDM.Destroy;
begin
  AdoQuery.Close;
  FreeAndNil(AdoQuery);
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  LogQuery.Close;
  FreeAndNil(LogQuery);
  stgErrorLogQuery.Close;
  FreeAndNil(stgErrorLogQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  AdoCon.Connected := False;
  FreeAndNil(AdoCon);
  inherited;
end;


procedure TDM.BOPNAME;
var
  proc: string;
  dObject: string;
  PassFail: string;
begin
  proc := 'BOPNAME';
  dObject := 'dimCustomer';
  try
    try
      DM.OpenQuery(AdsQuery, 'execute procedure checkDependencies(' + QuotedStr(executable) + ',' + QuotedStr(dObject) + ')');
      PassFail := AdsQuery.FieldByName('PassFail').AsString;
      DM.CloseQuery(AdsQuery);
      if PassFail = 'Pass' then
        begin
          DM.ExecuteQuery(AdsQuery, 'execute procedure UpdateNightlyBatch(' + QuotedStr(dObject) + ',True,False)');
          DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
          ExecuteQuery(AdsQuery, 'delete from tmpBOPNAME');
          PrepareQuery(AdsQuery, ' insert into tmpBOPNAME(' +
              'BNCO#, BNKEY, BNTYPE, BNSS#, BNSNAM, BNLNAM, BNFNAM, BNMIDI, ' +
              'BNSALU, BNGENDER, BNLANG, BNADR1, BNADR2, BNCITY, BNCNTY, BNSTCD, ' +
              'BNZIP, BNPHON, BNBPHN, BNBEXT, BNFAX#, BNBDAT, BNDLIC, BNCNTC, ' +
              'BNPCNTC, BNMAIL, BNTAXE, BNCSTS, BNCTYPE, BNPREFPHN, BNCPHON, ' +
              'BNPPHON, BNOPHON, BNOPDESC, BNEMAIL, BNOPTF, BNACBM, BNACBP, ' +
              'BNADBE, BNADR3, BNBEXTA, BNBPHNI, BNCDAT, BNCPHONI, BNEKEY, ' +
              'BNEML2, BNFAX#I, BNOPHONI, BNPHONI, BNPNAM, BNPPHONI, BNPRLN, ' +
              'BNZIPI, BNTOKEN, BNCCID, BNSXFNAM, BNSXLNAM, BNSXADR1, BNOPTF2, ' +
              'BNTAXE2, BNTAXE3, BNTAXE4, BNGSTREG#, BNPSTDAT, BNDLRCD, BNCCCD, ' +
              'BNCSTVER, BNADRVER, BNADRVCUR, BNCRTUSR, BNCRTTS, BNUPDUSR, ' +
              'BNUPDTS, BNCTRYCD, BNDLST, BNOTPTRCD, BNSTATUS) ' +
              'values(' +
              ':BNCO, :BNKEY, :BNTYPE, :BNSS, :BNSNAM, :BNLNAM, :BNFNAM, :BNMIDI, ' +
              ':BNSALU, :BNGENDER, :BNLANG, :BNADR1, :BNADR2, :BNCITY, :BNCNTY, :BNSTCD, ' +
              ':BNZIP, :BNPHON, :BNBPHN, :BNBEXT, :BNFAX, :BNBDAT, :BNDLIC, :BNCNTC, ' +
              ':BNPCNTC, :BNMAIL, :BNTAXE, :BNCSTS, :BNCTYPE, :BNPREFPHN, :BNCPHON, ' +
              ':BNPPHON, :BNOPHON, :BNOPDESC, :BNEMAIL, :BNOPTF, :BNACBM, :BNACBP, ' +
              ':BNADBE, :BNADR3, :BNBEXTA, :BNBPHNI, :BNCDAT, :BNCPHONI, :BNEKEY, ' +
              ':BNEML2, :BNFAXI, :BNOPHONI, :BNPHONI, :BNPNAM, :BNPPHONI, :BNPRLN, ' +
              ':BNZIPI, :BNTOKEN, :BNCCID, :BNSXFNAM, :BNSXLNAM, :BNSXADR1, :BNOPTF2, ' +
              ':BNTAXE2, :BNTAXE3, :BNTAXE4, :BNGSTREG, :BNPSTDAT, :BNDLRCD, :BNCCCD, ' +
              ':BNCSTVER, :BNADRVER, :BNADRVCUR, :BNCRTUSR, :BNCRTTS, :BNUPDUSR, ' +
              ':BNUPDTS, :BNCTRYCD, :BNDLST, :BNOTPTRCD, :BNSTATUS)');
          OpenQuery(AdoQuery, 'select '  +
              'BNCO#, BNKEY, BNTYPE, BNSS#, BNSNAM, BNLNAM, BNFNAM, '  +
              'BNMIDI, BNSALU, BNGENDER, BNLANG, BNADR1, BNADR2, BNCITY, '  +
              'BNCNTY, BNSTCD, BNZIP, BNPHON, BNBPHN, BNBEXT, BNFAX#, '  +
              'case '  +
              '  when BNBDAT < 18000000 then cast(''9999-12-31'' as date) '  +
              '  else cast(left(digits(BNBDAT), 4) || ''-'' || substr(digits(BNBDAT), 5, 2) || ''-'' || substr(digits(BNBDAT), 7, 2) as date) '  +
              'end as BNBDAT, '  +
              'BNDLIC, BNCNTC, BNPCNTC, BNMAIL, BNTAXE, BNCSTS, '  +
              'BNCTYPE, BNPREFPHN, BNCPHON, BNPPHON, BNOPHON, BNOPDESC, '  +
              'BNEMAIL, BNOPTF, BNACBM, BNACBP, BNADBE, BNADR3, BNBEXTA, '  +
              'BNBPHNI, '  +
              'case '  +
              '  when BNCDAT < 18000000 then cast(''9999-12-31'' as date) '  +
              '  else cast(left(digits(BNCDAT), 4) || ''-'' || substr(digits(BNCDAT), 5, 2) || ''-'' || substr(digits(BNCDAT), 7, 2) as date) '  +
              'end as BNCDAT, '  +
              'BNCPHONI, BNEKEY, BNEML2, BNFAX#I, '  +
              'BNOPHONI, BNPHONI, BNPNAM, BNPPHONI, BNPRLN, BNZIPI, '  +
              'BNTOKEN, BNCCID, BNSXFNAM, BNSXLNAM, BNSXADR1, BNOPTF2, '  +
              'BNTAXE2, BNTAXE3, BNTAXE4, BNGSTREG#, BNPSTDAT, BNDLRCD, '  +
              'BNCCCD, BNCSTVER, BNADRVER, BNADRVCUR, BNCRTUSR, '  +
              'CHAR(BNCRTTS) as BNCRTTS, BNUPDUSR, CHAR(BNUPDTS) as BNUPDTS, BNCTRYCD, BNDLST, '  +
            'BNOTPTRCD, BNSTATUS '  +
            'from rydedata.bopname where date(bnupdts) > (select curdate() - 7 day from sysibm.sysdummy1)');
          while not AdoQuery.Eof do
          begin
            AdsQuery.ParamByName('bnco').AsString := AdoQuery.FieldByName('bnco#').AsString;
            AdsQuery.ParamByName('bnkey').AsInteger := AdoQuery.FieldByName('bnkey').AsInteger;
            AdsQuery.ParamByName('bntype').AsString := AdoQuery.FieldByName('bntype').AsString;
            AdsQuery.ParamByName('bnss').AsInteger := AdoQuery.FieldByName('bnss#').AsInteger;
            AdsQuery.ParamByName('bnsnam').AsString := AdoQuery.FieldByName('bnsnam').AsString;
            AdsQuery.ParamByName('bnlnam').AsString := AdoQuery.FieldByName('bnlnam').AsString;
            AdsQuery.ParamByName('bnfnam').AsString := AdoQuery.FieldByName('bnfnam').AsString;
            AdsQuery.ParamByName('bnmidi').AsString := AdoQuery.FieldByName('bnmidi').AsString;
            AdsQuery.ParamByName('bnsalu').AsString := AdoQuery.FieldByName('bnsalu').AsString;
            AdsQuery.ParamByName('bngender').AsString := AdoQuery.FieldByName('bngender').AsString;
            AdsQuery.ParamByName('bnlang').AsString := AdoQuery.FieldByName('bnlang').AsString;
            AdsQuery.ParamByName('bnadr1').AsString := AdoQuery.FieldByName('bnadr1').AsString;
            AdsQuery.ParamByName('bnadr2').AsString := AdoQuery.FieldByName('bnadr2').AsString;
            AdsQuery.ParamByName('bncity').AsString := AdoQuery.FieldByName('bncity').AsString;
            AdsQuery.ParamByName('bncnty').AsString := AdoQuery.FieldByName('bncnty').AsString;
            AdsQuery.ParamByName('bnstcd').AsString := AdoQuery.FieldByName('bnstcd').AsString;
            AdsQuery.ParamByName('bnzip').AsString := AdoQuery.FieldByName('bnzip').AsString;
            AdsQuery.ParamByName('bnphon').AsString := AdoQuery.FieldByName('bnphon').AsString;
            AdsQuery.ParamByName('bnbphn').AsString := AdoQuery.FieldByName('bnbphn').AsString;
            AdsQuery.ParamByName('bnbext').AsString := AdoQuery.FieldByName('bnbext').AsString;
            AdsQuery.ParamByName('bnfax').AsString := AdoQuery.FieldByName('bnfax#').AsString;
            AdsQuery.ParamByName('bnbdat').AsDateTime := AdoQuery.FieldByName('bnbdat').AsDateTime;
            AdsQuery.ParamByName('bndlic').AsString := AdoQuery.FieldByName('bndlic').AsString;
            AdsQuery.ParamByName('bncntc').AsString := AdoQuery.FieldByName('bncntc').AsString;
            AdsQuery.ParamByName('bnpcntc').AsString := AdoQuery.FieldByName('bnpcntc').AsString;
            AdsQuery.ParamByName('bnmail').AsString := AdoQuery.FieldByName('bnmail').AsString;
            AdsQuery.ParamByName('bntaxe').AsString := AdoQuery.FieldByName('bntaxe').AsString;
            AdsQuery.ParamByName('bncsts').AsString := AdoQuery.FieldByName('bncsts').AsString;
            AdsQuery.ParamByName('bnctype').AsString := AdoQuery.FieldByName('bnctype').AsString;
            AdsQuery.ParamByName('bnprefphn').AsString := AdoQuery.FieldByName('bnprefphn').AsString;
            AdsQuery.ParamByName('bncphon').AsString := AdoQuery.FieldByName('bncphon').AsString;
            AdsQuery.ParamByName('bnpphon').AsString := AdoQuery.FieldByName('bnpphon').AsString;
            AdsQuery.ParamByName('bnophon').AsString := AdoQuery.FieldByName('bnophon').AsString;
            AdsQuery.ParamByName('bnopdesc').AsString := AdoQuery.FieldByName('bnopdesc').AsString;
            AdsQuery.ParamByName('bnemail').AsString := AdoQuery.FieldByName('bnemail').AsString;
            AdsQuery.ParamByName('bnoptf').AsString := AdoQuery.FieldByName('bnoptf').AsString;
            AdsQuery.ParamByName('bnacbm').AsString := AdoQuery.FieldByName('bnacbm').AsString;
            AdsQuery.ParamByName('bnacbp').AsString := AdoQuery.FieldByName('bnacbp').AsString;
            AdsQuery.ParamByName('bnadbe').AsString := AdoQuery.FieldByName('bnadbe').AsString;
            AdsQuery.ParamByName('bnadr3').AsString := AdoQuery.FieldByName('bnadr3').AsString;
            AdsQuery.ParamByName('bnbexta').AsString := AdoQuery.FieldByName('bnbexta').AsString;
            AdsQuery.ParamByName('bnbphni').AsString := AdoQuery.FieldByName('bnbphni').AsString;
            AdsQuery.ParamByName('bncdat').AsDateTime := AdoQuery.FieldByName('bncdat').AsDateTime;
            AdsQuery.ParamByName('bncphoni').AsString := AdoQuery.FieldByName('bncphoni').AsString;
            AdsQuery.ParamByName('bnekey').AsInteger := AdoQuery.FieldByName('bnekey').AsInteger;
            AdsQuery.ParamByName('bneml2').AsString := AdoQuery.FieldByName('bneml2').AsString;
            AdsQuery.ParamByName('bnfaxi').AsString := AdoQuery.FieldByName('bnfax#i').AsString;
            AdsQuery.ParamByName('bnophoni').AsString := AdoQuery.FieldByName('bnophoni').AsString;
            AdsQuery.ParamByName('bnphoni').AsString := AdoQuery.FieldByName('bnphoni').AsString;
            AdsQuery.ParamByName('bnpnam').AsString := AdoQuery.FieldByName('bnpnam').AsString;
            AdsQuery.ParamByName('bnpphoni').AsString := AdoQuery.FieldByName('bnpphoni').AsString;
            AdsQuery.ParamByName('bnprln').AsString := AdoQuery.FieldByName('bnprln').AsString;
            AdsQuery.ParamByName('bnzipi').AsString := AdoQuery.FieldByName('bnzipi').AsString;
            AdsQuery.ParamByName('bntoken').AsString := AdoQuery.FieldByName('bntoken').AsString;
            AdsQuery.ParamByName('bnccid').AsString := AdoQuery.FieldByName('bnccid').AsString;
            AdsQuery.ParamByName('bnsxfnam').AsString := AdoQuery.FieldByName('bnsxfnam').AsString;
            AdsQuery.ParamByName('bnsxlnam').AsString := AdoQuery.FieldByName('bnsxlnam').AsString;
            AdsQuery.ParamByName('bnsxadr1').AsString := AdoQuery.FieldByName('bnsxadr1').AsString;
            AdsQuery.ParamByName('bnoptf2').AsString := AdoQuery.FieldByName('bnoptf2').AsString;
            AdsQuery.ParamByName('bntaxe2').AsString := AdoQuery.FieldByName('bntaxe2').AsString;
            AdsQuery.ParamByName('bntaxe3').AsString := AdoQuery.FieldByName('bntaxe3').AsString;
            AdsQuery.ParamByName('bntaxe4').AsString := AdoQuery.FieldByName('bntaxe4').AsString;
            AdsQuery.ParamByName('bngstreg').AsString := AdoQuery.FieldByName('bngstreg#').AsString;
            AdsQuery.ParamByName('bnpstdat').AsInteger := AdoQuery.FieldByName('bnpstdat').AsInteger;
            AdsQuery.ParamByName('bndlrcd').AsInteger := AdoQuery.FieldByName('bndlrcd').AsInteger;
            //Ccase 306
            AdsQuery.ParamByName('bncccd').AsFloat := 0;//AdoQuery.FieldByName('bncccd').AsFloat;
            AdsQuery.ParamByName('bncstver').AsInteger := AdoQuery.FieldByName('bncstver').AsInteger;
            //Ccase 306
            AdsQuery.ParamByName('bnadrver').AsInteger := 0;//AdoQuery.FieldByName('bnadrver').AsInteger;
            AdsQuery.ParamByName('bnadrvcur').AsInteger := AdoQuery.FieldByName('bnadrvcur').AsInteger;
            AdsQuery.ParamByName('bncrtusr').AsInteger := AdoQuery.FieldByName('bncrtusr').AsInteger;
            AdsQuery.ParamByName('bncrtts').AsString := AdoQuery.FieldByName('bncrtts').AsString;
            AdsQuery.ParamByName('bnupdusr').AsInteger := AdoQuery.FieldByName('bnupdusr').AsInteger;
            AdsQuery.ParamByName('bnupdts').AsString := AdoQuery.FieldByName('bnupdts').AsString;
            AdsQuery.ParamByName('bnctrycd').AsString := AdoQuery.FieldByName('bnctrycd').AsString;
            AdsQuery.ParamByName('bndlst').AsString := AdoQuery.FieldByName('bndlst').AsString;
            AdsQuery.ParamByName('bnotptrcd').AsString := AdoQuery.FieldByName('bnotptrcd').AsString;
            AdsQuery.ParamByName('bnstatus').AsString := AdoQuery.FieldByName('bnstatus').AsString;
            AdsQuery.ExecSQL;
            AdoQuery.Next;
          end;
          ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''tmpBOPNAME'')');
          ExecuteQuery(AdsQuery, 'execute procedure stgArkBOPNAME()');
          ExecuteQuery(AdsQuery, 'execute procedure xfmCustDim()');
          DM.ExecuteQuery(AdsQuery, 'execute procedure UpdateNightlyBatch(' + QuotedStr(dObject) + ',False,True)');
          DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
            'null' + ')');
        end
      else
        sendmail(executable + '.' + proc + '.' + dObject + ' failed CheckDependency', '');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('BOPNAME.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDM.emailUpdate;
var
  proc: string;
  dObject: string;
  PassFail: string;
begin
  proc := 'emailUpdate';
  dObject := 'emailUpdate';
  try
    try
      DM.OpenQuery(AdsQuery, 'execute procedure checkDependencies(' + QuotedStr(executable) + ',' + QuotedStr(dObject) + ')');
      PassFail := AdsQuery.FieldByName('PassFail').AsString;
      DM.CloseQuery(AdsQuery);
      if PassFail = 'Pass' then
        begin
          DM.ExecuteQuery(AdsQuery, 'execute procedure UpdateNightlyBatch(' + QuotedStr(dObject) + ',True,False)');
          DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
          ExecuteQuery(AdsQuery, 'delete from tmpDimCustomerEmailUpdate');
          PrepareQuery(AdsQuery, ' insert into tmpDimCustomerEmailUpdate(' +
              'bnkey, name, email, emailValid, email2, email2Valid) ' +
              'values(' +
              ':BNKEY, :BNSNAM, :BNEMAIL, :EMAILVALID, :BNEML2, :EMAIL2VALID)');
          OpenQuery(AdoQuery, 'select bnkey,bnsnam,bnemail,emailValid,bneml2,email2Valid '  +
            'from ( ' +
            '  select bnkey, bnsnam, bnemail, ' +
            '    CASE ' +
            '      WHEN length(TRIM(coalesce(bnemail, ''''))) = 0 THEN 0 ' +
            '      WHEN position(''@'' IN bnemail) = 0 THEN 0 ' +
            '      WHEN position (''.'' IN bnemail) = 0 THEN 0 ' +
            '      WHEN position(''WNG'' IN upper(bnemail)) > 0 THEN 0 ' +
            '      WHEN position(''DNH'' IN upper(bnemail)) > 0 THEN 0 ' +
            '      WHEN position(''NOEMAIL'' IN upper(bnemail)) > 0 THEN 0 ' +
            '      WHEN position(''NONE'' IN upper(bnemail)) > 0 THEN 0 ' +
            '      WHEN position(''DNG'' IN upper(bnemail)) > 0 THEN 0 ' +
            '      ELSE 1 ' +
            '    END as emailValid, ' +
            '    bneml2, ' +
            '    CASE ' +
            '      WHEN length(TRIM(coalesce(bneml2, ''''))) = 0 THEN 0 ' +
            '      WHEN position(''@'' IN bneml2) = 0 THEN 0 ' +
            '      WHEN position (''.'' IN bneml2) = 0 THEN 0 ' +
            '      WHEN position(''WNG'' IN upper(bneml2)) > 0 THEN 0 ' +
            '      WHEN position(''DNH'' IN upper(bneml2)) > 0 THEN 0 ' +
            '      WHEN position(''NOEMAIL'' IN upper(bneml2)) > 0 THEN 0 ' +
            '      WHEN position(''NONE'' IN upper(bneml2)) > 0 THEN 0 ' +
            '      WHEN position(''DNG'' IN upper(bneml2)) > 0 THEN 0 ' +
            '      ELSE 1 ' +
            '    END as email2Valid ' +
            '  from rydedata.bopname ' +
            '  where (bnemail <> '''' or bneml2 <> '''')) x ' +
            'where emailValid = 1 or email2Valid = 1');
          while not AdoQuery.Eof do
          begin
            AdsQuery.ParamByName('bnkey').AsInteger := AdoQuery.FieldByName('bnkey').AsInteger;
            AdsQuery.ParamByName('bnsnam').AsString := AdoQuery.FieldByName('bnsnam').AsString;
            AdsQuery.ParamByName('bnemail').AsString := AdoQuery.FieldByName('bnemail').AsString;
            AdsQuery.ParamByName('emailValid').AsInteger := AdoQuery.FieldByName('emailValid').AsInteger;
            AdsQuery.ParamByName('bneml2').AsString := AdoQuery.FieldByName('bneml2').AsString;
            AdsQuery.ParamByName('email2Valid').AsInteger := AdoQuery.FieldByName('email2Valid').AsInteger;
            AdsQuery.ExecSQL;
            AdoQuery.Next;
          end;
          ExecuteQuery(AdsQuery, 'execute procedure xfmDimCustomerEmailUpdate()');
          DM.ExecuteQuery(AdsQuery, 'execute procedure UpdateNightlyBatch(' + QuotedStr(dObject) + ',False,True)');
          DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
            'null' + ')');
        end
      else
        sendmail(executable + '.' + proc + '.' + dObject + ' failed CheckDependency', '');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('BOPNAME.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;
{$region 'Utilities'}
function TDM.GetADSSqlTimeStamp(const DateTime: TDateTime): String;
var
  ye, mo, da, ho, mi, se, ms: Word;
begin
  DecodeDateTime(DateTime, ye, mo, da, ho, mi, se, ms);
  Result := FormatWordToStr(ye, 4) + '-' +
            FormatWordToStr(mo, 2) + '-' +
            FormatWordToStr(da, 2) + ' ' +
            FormatWordToStr(ho, 2) + ':' +
            FormatWordToStr(mi, 2) + ':' +
            FormatWordToStr(se, 2);
end;

function TDM.FormatWordToStr(const inVal: Word;
  const OutLength: Integer; const PadChar: Char): String;
begin
  Result := IntToStr(inVal);
  if Length(Result) < OutLength then
    Result := StringOfChar(PadChar, OutLength - Length(Result)) + Result;
end;

procedure TDM.curLoadIsEmpty(tableName: string);
var
  curLoadCount: Integer;
begin
  OpenQuery(AdsQuery, 'select count(*) as CurLoad from curLoad' + tableName);
  curLoadCount := AdsQuery.FieldByName('CurLoad').AsInteger;
  CloseQuery(AdsQuery);
  if curLoadCount <> 0 then
    SendMail(tableName + ' failed curLoadIsEmpty');
  Assert(curLoadCount = 0, 'curLoad' + tableName + ' not empty');
end;

procedure TDM.Log(job, routine: string; jobStartTS, jobEndTS: TDateTime);
var
  sql: string;
begin
  PrepareQuery(LogQuery);
  LogQuery.SQL.Add('execute procedure etlJobLogAuditInsert(' + QuotedStr(job) + ', ' +
    QuotedStr(routine) + ', ' +  QuotedStr(GetADSSqlTimeStamp(jobStartTS)) + ', ' +
    QuotedStr(GetADSSqlTimeStamp(jobEndTS)) + ')');
  sql := LogQuery.SQL.Text;
  LogQuery.ExecSQL;
end;



procedure TDM.curLoadPrevLoad(tableName: string);
{ TODO -ojon -crefactor : is this the best way to do this? need these holding tables to be kept to a minimum size,
  don't accumlate deleted records
  12/6/11 appears to be working ok }
begin
  try
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_ZapTable(' + '''' + 'prevLoad' + tableName + '''' + ')');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'prevLoad' + tableName + '''' + ', ' + '''' + 'prevLoad' + tableName + 'x' + '''' +  ', 1, 0)');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'curLoad' + tableName + '''' + ', ' + '''' + 'prevLoad' + tableName + '''' + ', 1, 0)');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'prevLoad' + tableName + 'x' + '''' + ', ' + '''' + 'curLoad' + tableName + '''' + ', 1, 0)');
  except
    on E: Exception do
    begin
      Raise Exception.Create('curLoadPrevLoad.  MESSAGE: ' + E.Message);
    end;
  end;
end;

procedure TDM.ExecuteQuery(query: TDataSet; sql: string);
begin
//  try
    if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
    begin
      AdsQuery.AdsConnection := AdsCon;
      CloseQuery(AdsQuery);
      AdsQuery.SQL.Text := sql;
      AdsQuery.ExecSQL;
      CloseQuery(AdsQuery);
    end
    else if query.ClassParent.ClassName = 'TCustomADODataSet' then
    begin
      AdoQuery.Connection := AdoCon;
      CloseQuery(AdoQuery);
      AdoQuery.SQL.Text := sql;
      AdoQuery.ExecSQL;
      CloseQuery(AdoQuery);
    end;
//  except
//    on E: Exception do
//    begin
//      SendMail('ExecuteQuery failed - no stgErrorLog record entered');
//      exit;
//    end;
//  end;
end;

procedure TDM.OpenQuery(query: TDataSet; sql: string);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
    AdsQuery.SQL.Text := sql;
    AdsQuery.Open;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
    AdoQuery.SQL.Text := sql;
    AdoQuery.Open;
  end;
end;



procedure TDM.PrepareQuery(query: TDataSet; sql: string);
begin
//  if query = 'TAdsExtendedDataSet' then
  if query.classparent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
{ TODO -ojon -cfigure out : don't remember why i commented out close - figure it out and be consistent }
//    AdsQuery.Close;
    if sql <> '' then
      AdsQuery.SQL.Text := sql;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    if sql <> '' then
      AdoQuery.SQL.Text := sql;
  end;
end;



procedure TDM.SendMail(Subject: string; Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
        ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            '''' + 'send mail failed' + '''' + ', ' +
            '''' + 'no sql - line 427' + '''' + ', ' +
            QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        exit;
      end;
    end;
  finally
//    Msg.Free;
    FreeAndNil(Msg);
//    SMTP.Free;
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;


procedure TDM.CloseQuery(query: TDataSet);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
  end;
end;

procedure TDM.ScrapeCountQuery(db2Q, adsQ: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Q);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsQ);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckQuery between ' + db2Q + '(' + IntToStr(db2Count) + ') and ' + adsQ + '(' + IntToStr(adsCount) + ')  failed');
end;

procedure TDM.ScrapeCountTable(db2Table, adsTable: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  db2Table := 'rydedata.' + db2Table;
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Table);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsTable);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckTable between ' + db2Table + '(' + IntToStr(db2Count) + ') and ' + adsTable + '(' + IntToStr(adsCount) + ')  failed');
end;
{$endregion 'Utilities'}
end.


