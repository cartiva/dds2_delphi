unit pdppdetDM;

interface

uses
  SysUtils, Classes, adsdata, adsfunc, adstable, adscnnct, DB, ADODB, DateUtils,
  IdSMTPBase, IdSMTP, IdMessage,
  Dialogs;

type
  TDM = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure PrepareQuery(query: TDataSet; sql: string='');
    procedure CloseQuery(query: TDataSet);
    function FormatWordToStr(const inVal: Word;
      const OutLength: Integer; const PadChar: Char = #48): String;
    function GetADSSqlTimeStamp(const DateTime: TDateTime): String;
    procedure OpenQuery(query: TDataSet; sql: string);
    procedure ExecuteQuery(query: TDataSet; sql: string);
    procedure SendMail(Subject: string; Body: string='');
    procedure ScrapeCountTable(db2Table, adsTable: string);
    procedure ScrapeCountQuery(db2Q, adsQ: string);
    procedure tmpPDPPDET;
  end;

var
  DM: TDM;
  AdoCon: TADOConnection;
  AdsCon: TADSConnection;
  AdoQuery: TAdoQuery;
  AdsQuery: TAdsQuery;
  StartTS: TDateTime;
  EndTS: TDateTime;

resourcestring
  executable = 'PDPPDET';

implementation

constructor TDM.Create(AOwner: TComponent);
begin
  AdoCon := TADOConnection.Create(nil);
  AdsCon := TADSConnection.Create(nil);
  AdoQuery := TADOQuery.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
//  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=Arkona Production';
  AdsCon.ConnectPath := '\\67.135.158.12:6363\Advantage\DDS\DDS.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
  AdoCon.Connected := True;
  AdsCon.Connect;
end;

destructor TDM.Destroy;
begin
  AdoQuery.Close;
  FreeAndNil(AdoQuery);
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  AdoCon.Connected := False;
  FreeAndNil(AdoCon);
  inherited;
end;

procedure TDM.tmpPDPPDET;
var
  proc: string;
  dObject: string;
  PassFail: string;
begin
  proc := 'tmpPDPPDET';
  dObject := 'tmpPDPPDET';
  try
    try
      OpenQuery(AdsQuery, 'execute procedure checkDependencies(' + QuotedStr(executable) + ',' + QuotedStr(dObject) + ')');
      PassFail := AdsQuery.FieldByName('PassFail').AsString;
      CloseQuery(AdsQuery);
      if PassFail = 'Pass' then
        begin
          ExecuteQuery(AdsQuery, 'execute procedure UpdateNightlyBatch(' + QuotedStr(dObject) + ',True,False)');
          ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ')');
          ExecuteQuery(AdsQuery, 'delete from tmpPDPPDET');
          PrepareQuery(AdsQuery, ' insert into tmpPDPPDET(' +
              'PTCO#, PTPKEY, PTLINE, PTLTYP, PTSEQ#, PTCPID, PTCODE, PTSOEP, ' +
              'PTDATE, ' +
              'PTMANF, PTPART, PTPLNG, PTSGRP, PTBINL, PTQTY, PTCOST, ' +
              'PTLIST, PTTRAD, PTFPRC, PTNET, PTBLIST, PTMETH, PTPCT, PTBASE, ' +
              'PTSPCD, PTPOVR, PTGPRC, PTCORE, PTADDP, PTSPPC, PTORSO, PTXCLD, ' +
              'PTCMNT, PTLSTS, PTSVCTYP, PTLPYM, PTPADJ, PTTECH, PTLOPC, PTCRLO, ' +
              'PTLHRS, PTLCHR, PTARLA, PTFAIL, PTPLVL, PTPSAS, PTSLV#, PTSLI#, ' +
              'PTDCPN, PTFACT, PTACODE, PTBAWC, PTDTLC, PTINTA, PTLAUTH, PTNTOT, ' +
              'PTOAMT, PTOHRS, PTPMCS, PTPMRT, PTRPRP, PTTXIN, PTUPSL, PTWCTP, ' +
              'PTVATCODE, PTVATAMT, PTRTNINVF, PTSKLLVL, PTOEMRPLF, PTCSTDIF$, ' +
              'PTOPTSEQ#, PTDISSTRZ, PTDSCLBR$, PTDSCPRT$, PTPICKZNE, PTDISPTY1, ' +
              'PTDISPTY2, PTDISRANK1, PTDISRANK2, PTDSPADATE, PTDSPATIME, PTDSPMDATE, PTDSPMTIME) ' +
              'values(' +
              ':PTCO, :PTPKEY, :PTLINE, :PTLTYP, :PTSEQ, :PTCPID, :PTCODE, :PTSOEP, ' +
              ':PTDATE, :PTMANF, :PTPART, :PTPLNG, :PTSGRP, :PTBINL, :PTQTY, :PTCOST, ' +
              ':PTLIST, :PTTRAD, :PTFPRC, :PTNET, :PTBLIST, :PTMETH, :PTPCT, :PTBASE, ' +
              ':PTSPCD, :PTPOVR, :PTGPRC, :PTCORE, :PTADDP, :PTSPPC, :PTORSO, :PTXCLD, ' +
              ':PTCMNT, :PTLSTS, :PTSVCTYP, :PTLPYM, :PTPADJ, :PTTECH, :PTLOPC, :PTCRLO, ' +
              ':PTLHRS, :PTLCHR, :PTARLA, :PTFAIL, :PTPLVL, :PTPSAS, :PTSLV, :PTSLI, ' +
              ':PTDCPN, :PTFACT, :PTACODE, :PTBAWC, :PTDTLC, :PTINTA, :PTLAUTH, :PTNTOT, ' +
              ':PTOAMT, :PTOHRS, :PTPMCS, :PTPMRT, :PTRPRP, :PTTXIN, :PTUPSL, :PTWCTP, ' +
              ':PTVATCODE, :PTVATAMT, :PTRTNINVF, :PTSKLLVL, :PTOEMRPLF, :PTCSTDIF, ' +
              ':PTOPTSEQ, :PTDISSTRZ, :PTDSCLBR, :PTDSCPRT, :PTPICKZNE, :PTDISPTY1, ' +
              ':PTDISPTY2, :PTDISRANK1, :PTDISRANK2, :PTDSPADATE, :PTDSPATIME, :PTDSPMDATE, :PTDSPMTIME)');
          OpenQuery(AdoQuery, 'select ' +
              'PTCO#, PTPKEY, PTLINE, PTLTYP, PTSEQ#, PTCPID, PTCODE, PTSOEP, ' +
              'case ' +
              '  when PTDATE = 0 then cast(''9999-12-31'' as date) ' +
              '  else cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) ' +
              'end as PTDATE, ' +
              'PTMANF, PTPART, PTPLNG, PTSGRP, PTBINL, PTQTY, PTCOST, ' +
              'PTLIST, PTTRAD, PTFPRC, PTNET, PTBLIST, PTMETH, PTPCT, PTBASE, ' +
              'PTSPCD, PTPOVR, PTGPRC, PTCORE, PTADDP, PTSPPC, PTORSO, PTXCLD, ' +
              'PTCMNT, PTLSTS, PTSVCTYP, PTLPYM, PTPADJ, PTTECH, PTLOPC, PTCRLO, ' +
              'PTLHRS, PTLCHR, PTARLA, PTFAIL, PTPLVL, PTPSAS, PTSLV#, PTSLI#, ' +
              'PTDCPN, PTFACT, PTACODE, PTBAWC, PTDTLC, PTINTA, PTLAUTH, PTNTOT, ' +
              'PTOAMT, PTOHRS, PTPMCS, PTPMRT, PTRPRP, PTTXIN, PTUPSL, PTWCTP, ' +
              'PTVATCODE, PTVATAMT, PTRTNINVF, PTSKLLVL, PTOEMRPLF, PTCSTDIF$, ' +
              'PTOPTSEQ#, PTDISSTRZ, PTDSCLBR$, PTDSCPRT$, PTPICKZNE, PTDISPTY1, ' +
              'PTDISPTY2, PTDISRANK1, PTDISRANK2, PTDSPADATE, PTDSPATIME, PTDSPMDATE, PTDSPMTIME ' +
              'from rydedata.pdppdet a ' +
              'where exists (select 1 from rydedata.pdpphdr where length(trim(ptdoc#)) > 6 and ptpkey = a.ptpkey)');
          while not AdoQuery.Eof do
          begin
            AdsQuery.ParamByName('ptco').AsString := AdoQuery.FieldByName('ptco#').AsString;
            AdsQuery.ParamByName('ptpkey').AsInteger := AdoQuery.FieldByName('ptpkey').AsInteger;
            AdsQuery.ParamByName('ptline').AsInteger := AdoQuery.FieldByName('ptline').AsInteger;
            AdsQuery.ParamByName('ptltyp').AsString := AdoQuery.FieldByName('ptltyp').AsString;
            AdsQuery.ParamByName('ptseq').AsInteger := AdoQuery.FieldByName('ptseq#').AsInteger;
            AdsQuery.ParamByName('ptcpid').AsString := AdoQuery.FieldByName('ptcpid').AsString;
            AdsQuery.ParamByName('ptcode').AsString := AdoQuery.FieldByName('ptcode').AsString;
            AdsQuery.ParamByName('ptsoep').AsString := AdoQuery.FieldByName('ptsoep').AsString;
            AdsQuery.ParamByName('ptdate').AsDateTime := AdoQuery.FieldByName('ptdate').AsDateTime;
            AdsQuery.ParamByName('ptmanf').AsString := AdoQuery.FieldByName('ptmanf').AsString;
            AdsQuery.ParamByName('ptpart').AsString := AdoQuery.FieldByName('ptpart').AsString;
            AdsQuery.ParamByName('ptplng').AsInteger := AdoQuery.FieldByName('ptplng').AsInteger;
            AdsQuery.ParamByName('ptsgrp').AsString := AdoQuery.FieldByName('ptsgrp').AsString;
            AdsQuery.ParamByName('ptbinl').AsString := AdoQuery.FieldByName('ptbinl').AsString;
            AdsQuery.ParamByName('ptqty').AsInteger := AdoQuery.FieldByName('ptqty').AsInteger;
            AdsQuery.ParamByName('ptcost').AsCurrency := AdoQuery.FieldByName('ptcost').AsCurrency;
            AdsQuery.ParamByName('ptlist').AsCurrency := AdoQuery.FieldByName('ptlist').AsCurrency;
            AdsQuery.ParamByName('pttrad').AsCurrency := AdoQuery.FieldByName('pttrad').AsCurrency;
            AdsQuery.ParamByName('ptfprc').AsCurrency := AdoQuery.FieldByName('ptfprc').AsCurrency;
            AdsQuery.ParamByName('ptnet').AsCurrency := AdoQuery.FieldByName('ptnet').AsCurrency;
            AdsQuery.ParamByName('ptblist').AsCurrency := AdoQuery.FieldByName('ptblist').AsCurrency;
            AdsQuery.ParamByName('ptmeth').AsString := AdoQuery.FieldByName('ptmeth').AsString;
            AdsQuery.ParamByName('ptpct').AsInteger := AdoQuery.FieldByName('ptpct').AsInteger;
            AdsQuery.ParamByName('ptbase').AsString := AdoQuery.FieldByName('ptbase').AsString;
            AdsQuery.ParamByName('ptspcd').AsString := AdoQuery.FieldByName('ptspcd').AsString;
            AdsQuery.ParamByName('ptpovr').AsString := AdoQuery.FieldByName('ptpovr').AsString;
            AdsQuery.ParamByName('ptgprc').AsString := AdoQuery.FieldByName('ptgprc').AsString;
            AdsQuery.ParamByName('ptcore').AsString := AdoQuery.FieldByName('ptcore').AsString;
            AdsQuery.ParamByName('ptaddp').AsString := AdoQuery.FieldByName('ptaddp').AsString;
            AdsQuery.ParamByName('ptsppc').AsInteger := AdoQuery.FieldByName('ptsppc').AsInteger;
            AdsQuery.ParamByName('ptorso').AsString := AdoQuery.FieldByName('ptorso').AsString;
            AdsQuery.ParamByName('ptxcld').AsString := AdoQuery.FieldByName('ptxcld').AsString;
            AdsQuery.ParamByName('ptcmnt').AsString := AdoQuery.FieldByName('ptcmnt').AsString;
            AdsQuery.ParamByName('ptlsts').AsString := AdoQuery.FieldByName('ptlsts').AsString;
            AdsQuery.ParamByName('ptsvctyp').AsString := AdoQuery.FieldByName('ptsvctyp').AsString;
            AdsQuery.ParamByName('ptlpym').AsString := AdoQuery.FieldByName('ptlpym').AsString;
            AdsQuery.ParamByName('ptpadj').AsString := AdoQuery.FieldByName('ptpadj').AsString;
            AdsQuery.ParamByName('pttech').AsString := AdoQuery.FieldByName('pttech').AsString;
            AdsQuery.ParamByName('ptlopc').AsString := AdoQuery.FieldByName('ptlopc').AsString;
            AdsQuery.ParamByName('ptcrlo').AsString := AdoQuery.FieldByName('ptcrlo').AsString;
            AdsQuery.ParamByName('ptlhrs').AsFloat := AdoQuery.FieldByName('ptlhrs').AsFloat;
            AdsQuery.ParamByName('ptlchr').AsFloat := AdoQuery.FieldByName('ptlchr').AsFloat;
            AdsQuery.ParamByName('ptarla').AsCurrency := AdoQuery.FieldByName('ptarla').AsCurrency;
            AdsQuery.ParamByName('ptfail').AsString := AdoQuery.FieldByName('ptfail').AsString;
            AdsQuery.ParamByName('ptplvl').AsInteger := AdoQuery.FieldByName('ptplvl').AsInteger;
            AdsQuery.ParamByName('ptpsas').AsInteger := AdoQuery.FieldByName('ptpsas').AsInteger;
            AdsQuery.ParamByName('ptslv').AsString := AdoQuery.FieldByName('ptslv#').AsString;
            AdsQuery.ParamByName('ptsli').AsString := AdoQuery.FieldByName('ptsli#').AsString;
            AdsQuery.ParamByName('ptdcpn').AsInteger := AdoQuery.FieldByName('ptdcpn').AsInteger;
            AdsQuery.ParamByName('ptfact').AsFloat := AdoQuery.FieldByName('ptfact').AsFloat;
            AdsQuery.ParamByName('ptacode').AsString := AdoQuery.FieldByName('ptacode').AsString;
            AdsQuery.ParamByName('ptbawc').AsString := AdoQuery.FieldByName('ptbawc').AsString;
            AdsQuery.ParamByName('ptdtlc').AsFloat := AdoQuery.FieldByName('ptdtlc').AsFloat;
            AdsQuery.ParamByName('ptinta').AsString := AdoQuery.FieldByName('ptinta').AsString;
            AdsQuery.ParamByName('ptlauth').AsString := AdoQuery.FieldByName('ptlauth').AsString;
            AdsQuery.ParamByName('ptntot').AsFloat := AdoQuery.FieldByName('ptntot').AsFloat;
            AdsQuery.ParamByName('ptoamt').AsFloat := AdoQuery.FieldByName('ptoamt').AsFloat;
            AdsQuery.ParamByName('ptohrs').AsFloat := AdoQuery.FieldByName('ptohrs').AsFloat;
            AdsQuery.ParamByName('ptpmcs').AsFloat := AdoQuery.FieldByName('ptpmcs').AsFloat;
            AdsQuery.ParamByName('ptpmrt').AsFloat := AdoQuery.FieldByName('ptpmrt').AsFloat;
            AdsQuery.ParamByName('ptrprp').AsString := AdoQuery.FieldByName('ptrprp').AsString;
            AdsQuery.ParamByName('pttxin').AsString := AdoQuery.FieldByName('pttxin').AsString;
            AdsQuery.ParamByName('ptupsl').AsString := AdoQuery.FieldByName('ptupsl').AsString;
            AdsQuery.ParamByName('ptwctp').AsString := AdoQuery.FieldByName('ptwctp').AsString;
            AdsQuery.ParamByName('ptvatcode').AsString := AdoQuery.FieldByName('ptvatcode').AsString;
            AdsQuery.ParamByName('ptvatamt').AsFloat := AdoQuery.FieldByName('ptvatamt').AsFloat;
            AdsQuery.ParamByName('ptrtninvf').AsString := AdoQuery.FieldByName('ptrtninvf').AsString;
            AdsQuery.ParamByName('ptskllvl').AsString := AdoQuery.FieldByName('ptskllvl').AsString;
            AdsQuery.ParamByName('ptoemrplf').AsString := AdoQuery.FieldByName('ptoemrplf').AsString;
            AdsQuery.ParamByName('ptcstdif').AsFloat := AdoQuery.FieldByName('ptcstdif$').AsFloat;
            AdsQuery.ParamByName('ptoptseq').AsInteger := AdoQuery.FieldByName('ptoptseq#').AsInteger;
            AdsQuery.ParamByName('ptdisstrz').AsString := ''; //AdoQuery.FieldByName('ptdisstrz').AsString;
            AdsQuery.ParamByName('ptdsclbr').AsFloat := AdoQuery.FieldByName('ptdsclbr$').AsFloat;
            AdsQuery.ParamByName('ptdscprt').AsFloat := AdoQuery.FieldByName('ptdscprt$').AsFloat;
            AdsQuery.ParamByName('ptpickzne').AsString := AdoQuery.FieldByName('ptpickzne').AsString;
            AdsQuery.ParamByName('ptdispty1').AsString := AdoQuery.FieldByName('ptdispty1').AsString;
            AdsQuery.ParamByName('ptdispty2').AsString := AdoQuery.FieldByName('ptdispty2').AsString;
            AdsQuery.ParamByName('ptdisrank1').AsInteger := AdoQuery.FieldByName('ptdisrank1').AsInteger;
            AdsQuery.ParamByName('ptdisrank2').AsInteger := AdoQuery.FieldByName('ptdisrank2').AsInteger;
            AdsQuery.ParamByName('ptdspadate').AsString := ''; //AdoQuery.FieldByName('ptdspadate').AsString;
            AdsQuery.ParamByName('ptdspatime').AsString := ''; //AdoQuery.FieldByName('ptdspatime').AsString;
            AdsQuery.ParamByName('ptdspmdate').AsString := ''; //AdoQuery.FieldByName('ptdspmdate').AsString;
            AdsQuery.ParamByName('ptdspmtime').AsString := ''; //AdoQuery.FieldByName('ptdspmtime').AsString;
            AdsQuery.ExecSQL;
            AdoQuery.Next;
          end;
//          ScrapeCountTable('PDPPDET', 'tmpPDPPDET');
          ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''tmpPDPPDET'')');
          ExecuteQuery(AdsQuery, 'execute procedure UpdateNightlyBatch(' + QuotedStr(dObject) + ',False,True)');
          ExecuteQuery(AdsQuery, 'execute procedure stgArkPDPPDET()'); // deletes dups inserts into stg from tmp
          ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            'null' + ')');
        end
      else
        sendmail(executable + '.' + proc + '.' + dObject + ' failed CheckDependency', '');
    except
      on E: Exception do
      begin
        ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
//
        Raise Exception.Create(proc + '.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;
{$region 'Utilities'}
function TDM.GetADSSqlTimeStamp(const DateTime: TDateTime): String;
var
  ye, mo, da, ho, mi, se, ms: Word;
begin
  DecodeDateTime(DateTime, ye, mo, da, ho, mi, se, ms);
  Result := FormatWordToStr(ye, 4) + '-' +
            FormatWordToStr(mo, 2) + '-' +
            FormatWordToStr(da, 2) + ' ' +
            FormatWordToStr(ho, 2) + ':' +
            FormatWordToStr(mi, 2) + ':' +
            FormatWordToStr(se, 2);
end;
function TDM.FormatWordToStr(const inVal: Word;
  const OutLength: Integer; const PadChar: Char): String;
begin
  Result := IntToStr(inVal);
  if Length(Result) < OutLength then
    Result := StringOfChar(PadChar, OutLength - Length(Result)) + Result;
end;
procedure TDM.ExecuteQuery(query: TDataSet; sql: string);
begin
    if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
    begin
      AdsQuery.AdsConnection := AdsCon;
      CloseQuery(AdsQuery);
      AdsQuery.SQL.Text := sql;
      AdsQuery.ExecSQL;
      CloseQuery(AdsQuery);
    end
    else if query.ClassParent.ClassName = 'TCustomADODataSet' then
    begin
      AdoQuery.Connection := AdoCon;
      CloseQuery(AdoQuery);
      AdoQuery.SQL.Text := sql;
      AdoQuery.ExecSQL;
      CloseQuery(AdoQuery);
    end;
end;
procedure TDM.OpenQuery(query: TDataSet; sql: string);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
    AdsQuery.SQL.Text := sql;
    AdsQuery.Open;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
    AdoQuery.SQL.Text := sql;
    AdoQuery.Open;
  end;
end;
procedure TDM.PrepareQuery(query: TDataSet; sql: string);
begin
  if query.classparent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    if sql <> '' then
      AdsQuery.SQL.Text := sql;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    if sql <> '' then
      AdoQuery.SQL.Text := sql;
  end;
end;
procedure TDM.SendMail(Subject: string; Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
        exit;
      end;
    end;
  finally
    FreeAndNil(Msg);
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;
procedure TDM.CloseQuery(query: TDataSet);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
  end;
end;
procedure TDM.ScrapeCountQuery(db2Q, adsQ: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Q);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsQ);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckQuery between ' + db2Q + '(' + IntToStr(db2Count) + ') and ' + adsQ + '(' + IntToStr(adsCount) + ')  failed');
end;
procedure TDM.ScrapeCountTable(db2Table, adsTable: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  db2Table := 'rydedata.' + db2Table;
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Table);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsTable);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckTable between ' + db2Table + '(' + IntToStr(db2Count) + ') and ' + adsTable + '(' + IntToStr(adsCount) + ')  failed');
end;
{$endregion}
end.


