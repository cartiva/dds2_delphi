program ServiceLine;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  ActiveX,
  StrUtils,
  ServiceLineDM in 'ServiceLineDM.pas' {DM: TDataModule};

resourcestring
  executable = 'ServiceLine';

begin
  try
    try
      CoInitialize(nil);
      DM := TDM.Create(nil);
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
        QuotedStr(executable) + ', ' +
        '''' + 'none' + '''' + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
// *** active procs ***//
      DM.stgRO;         // 7
// *** active procs ***//
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        '''' + 'none' + '''' + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          '''' + 'none' + '''' + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        DM.SendMail('ServiceLine.' + leftstr(E.Message, 8),   E.Message);
        exit;
      end;
    end;
  finally
    FreeAndNil(DM);
  end;
end.
