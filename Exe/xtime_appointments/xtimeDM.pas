unit xtimeDM;
(*)
  minimal dependency checking on dims
10/12
  need to add dimVehicle, dimCustomer
(**)
interface

uses
  SysUtils, Classes, adsdata, adsfunc, adstable, adscnnct, DB, ADODB, DateUtils,
  IdSMTPBase, IdSMTP, IdMessage,
  Dialogs;

type
  TDM = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure PrepareQuery(query: TDataSet; sql: string='');
    procedure CloseQuery(query: TDataSet);
    function FormatWordToStr(const inVal: Word;
      const OutLength: Integer; const PadChar: Char = #48): String;
    function GetADSSqlTimeStamp(const DateTime: TDateTime): String;
    procedure OpenQuery(query: TDataSet; sql: string);
    procedure ExecuteQuery(query: TDataSet; sql: string);
    procedure SendMail(Subject: string; Body: string='');
    procedure ext_xtime_appointments;

  end;

var
  DM: TDM;
  AdoCon: TADOConnection;
  AdsCon: TADSConnection;
  AdoQuery: TAdoQuery;
  AdsQuery: TAdsQuery;
  LogQuery: TAdsQuery;
  stgErrorLogQuery: TAdsQuery;
  StartTS: TDateTime;
  EndTS: TDateTime;

resourcestring
  executable = 'xtime_appointments';

implementation

constructor TDM.Create(AOwner: TComponent);
begin
  AdoCon := TADOConnection.Create(nil);
  AdsCon := TADSConnection.Create(nil);
  AdoQuery := TADOQuery.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
//  AdsCon.ConnectPath := '\\10.130.196.83:6363\advantage\rydellservice\rydellservice.add';
  AdsCon.ConnectPath := '\\10.130.190.63:6363\Advantage\NewHondaService\NewHondaService.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva22';
  AdoCon.Connected := True;
  AdsCon.Connect;
end;

destructor TDM.Destroy;
begin
  AdoQuery.Close;
  FreeAndNil(AdoQuery);
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  AdoCon.Connected := False;
  FreeAndNil(AdoCon);
  inherited;
end;

procedure TDM.ext_xtime_appointments;
begin
  try
    try
      ExecuteQuery(AdsQuery, 'delete from ext_xtime_appointments');
      PrepareQuery(AdsQuery, ' insert into ext_xtime_appointments(' +
          'company_number,pending_key,doc_create_timestamp,cust_name,cust_phone_no, ' +
          'vin,promised_date_time,ptline,ptltyp,ptseq,ptcmnt,advisorID, '  +
          'address_1,city,state_code,zip_code,year,make,model) ' +
          'values(' +
          ':company_number,:pending_key,:doc_create_timestamp,:cust_name,:cust_phone_no, ' +
          ':vin,:promised_date_time,:ptline,:ptltyp,:ptseq,:ptcmnt,:advisorID, ' +
          ':address_1,:city,:state_code,:zip_code,:year,:make,:model)');
      OpenQuery(AdoQuery, 'select ' +
          'a.company_number, a.pending_key, ' +
          'varchar_format(a.doc_Create_timestamp, ''YYYY-MM-DD HH24:MI:SS'') as doc_create_timestamp,  ' +
          'a.cust_name, a.cust_phone_no,  ' +
          'vin, promised_date_time,  ' +
          'b.ptline, b.ptltyp, b.ptseq# as ptseq, b.ptcmnt, c.name,  ' +
          'd.address_1, d.city, d.state_code, d.zip_code, ' +
          'e.year, e.make, e.model ' +
          'from rydedata.pdpphdr a  ' +
          'left join rydedata.pdppdet b on a.pending_key = b.ptpkey  ' +
          'left join rydedata.sdpswtr c on a.service_writer_id = c.id ' +
          '  and a.ptco# = c.company_number ' +
          'left join rydedata.bopname d on a.customer_key = d.bopname_record_key ' +
          'left join rydedata.inpmast e on a.vin = e.imvin ' +
          'where a.ptpkey in (  ' +
          '  select ptpkey ' +
          '  from rydedata.pdppdet ' +
          '  where trim(ptdupdate) = ''XT-APTADD'')');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('company_number').AsString := AdoQuery.FieldByName('company_number').AsString;
        AdsQuery.ParamByName('pending_key').AsInteger := AdoQuery.FieldByName('pending_key').AsInteger;
        AdsQuery.ParamByName('doc_create_timestamp').AsString := AdoQuery.FieldByName('doc_create_timestamp').AsString;
        AdsQuery.ParamByName('cust_name').AsString := AdoQuery.FieldByName('cust_name').AsString;
        AdsQuery.ParamByName('cust_phone_no').AsFloat := AdoQuery.FieldByName('cust_phone_no').AsFloat;
        AdsQuery.ParamByName('vin').AsString := AdoQuery.FieldByName('vin').AsString;
        AdsQuery.ParamByName('promised_date_time').AsFloat := AdoQuery.FieldByName('promised_date_time').AsFloat;
        AdsQuery.ParamByName('ptline').AsInteger := AdoQuery.FieldByName('ptline').AsInteger;
        AdsQuery.ParamByName('ptltyp').AsString := AdoQuery.FieldByName('ptltyp').AsString;
        AdsQuery.ParamByName('ptseq').AsInteger := AdoQuery.FieldByName('ptseq').AsInteger;
        AdsQuery.ParamByName('ptcmnt').AsString := AdoQuery.FieldByName('ptcmnt').AsString;
        AdsQuery.ParamByName('advisorId').AsString := AdoQuery.FieldByName('name').AsString;

        AdsQuery.ParamByName('address_1').AsString := AdoQuery.FieldByName('address_1').AsString;
        AdsQuery.ParamByName('city').AsString := AdoQuery.FieldByName('city').AsString;
        AdsQuery.ParamByName('state_code').AsString := AdoQuery.FieldByName('state_code').AsString;
        AdsQuery.ParamByName('zip_code').AsInteger := AdoQuery.FieldByName('zip_code').AsInteger;
        AdsQuery.ParamByName('year').AsInteger := AdoQuery.FieldByName('year').AsInteger;
        AdsQuery.ParamByName('make').AsString := AdoQuery.FieldByName('make').AsString;
        AdsQuery.ParamByName('model').AsString := AdoQuery.FieldByName('model').AsString;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure xfm_xtime_appointments()');
    finally
      CloseQuery(AdsQuery);
      CloseQuery(AdoQuery);
    end;
  except
    on E: Exception do
    begin
      Raise Exception.Create(executable + '.  MESSAGE: ' + E.Message);
      exit;
    end;
  end;
end;

{$region 'Utilities'}
function TDM.GetADSSqlTimeStamp(const DateTime: TDateTime): String;
var
  ye, mo, da, ho, mi, se, ms: Word;
begin
  DecodeDateTime(DateTime, ye, mo, da, ho, mi, se, ms);
  Result := FormatWordToStr(ye, 4) + '-' +
            FormatWordToStr(mo, 2) + '-' +
            FormatWordToStr(da, 2) + ' ' +
            FormatWordToStr(ho, 2) + ':' +
            FormatWordToStr(mi, 2) + ':' +
            FormatWordToStr(se, 2);
end;

function TDM.FormatWordToStr(const inVal: Word;
  const OutLength: Integer; const PadChar: Char): String;
begin
  Result := IntToStr(inVal);
  if Length(Result) < OutLength then
    Result := StringOfChar(PadChar, OutLength - Length(Result)) + Result;
end;





procedure TDM.ExecuteQuery(query: TDataSet; sql: string);
begin
//  try
    if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
    begin
      AdsQuery.AdsConnection := AdsCon;
      CloseQuery(AdsQuery);
      AdsQuery.SQL.Text := sql;
      AdsQuery.ExecSQL;
      CloseQuery(AdsQuery);
    end
    else if query.ClassParent.ClassName = 'TCustomADODataSet' then
    begin
      AdoQuery.Connection := AdoCon;
      CloseQuery(AdoQuery);
      AdoQuery.SQL.Text := sql;
      AdoQuery.ExecSQL;
      CloseQuery(AdoQuery);
    end;
//  except
//    on E: Exception do
//    begin
//      SendMail('ExecuteQuery failed - no stgErrorLog record entered');
//      exit;
//    end;
//  end;
end;

procedure TDM.OpenQuery(query: TDataSet; sql: string);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
    AdsQuery.SQL.Text := sql;
    AdsQuery.Open;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
    AdoQuery.SQL.Text := sql;
    AdoQuery.Open;
  end;
end;



procedure TDM.PrepareQuery(query: TDataSet; sql: string);
begin
//  if query = 'TAdsExtendedDataSet' then
  if query.classparent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
{ TODO -ojon -cfigure out : don't remember why i commented out close - figure it out and be consistent }
//    AdsQuery.Close;
    if sql <> '' then
      AdsQuery.SQL.Text := sql;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    if sql <> '' then
      AdoQuery.SQL.Text := sql;
  end;
end;



procedure TDM.SendMail(Subject: string; Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
        ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            '''' + 'send mail failed' + '''' + ', ' +
            '''' + 'no sql - line 427' + '''' + ', ' +
            QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        exit;
      end;
    end;
  finally
//    Msg.Free;
    FreeAndNil(Msg);
//    SMTP.Free;
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;


procedure TDM.CloseQuery(query: TDataSet);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
  end;
end;


{$endregion}
end.


