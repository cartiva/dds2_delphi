unit dimSalesPersonDM;

interface

uses
  SysUtils, Classes, adsdata, adsfunc, adstable, adscnnct, DB, ADODB, DateUtils,
  IdSMTPBase, IdSMTP, IdMessage,
  Dialogs;

type
  TDM = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Log(job, routine: string; jobStartTS, jobEndTS: TDateTime);
    procedure PrepareQuery(query: TDataSet; sql: string='');
    procedure CloseQuery(query: TDataSet);
    function FormatWordToStr(const inVal: Word;
      const OutLength: Integer; const PadChar: Char = #48): String;
    function GetADSSqlTimeStamp(const DateTime: TDateTime): String;
    procedure OpenQuery(query: TDataSet; sql: string);
    procedure ExecuteQuery(query: TDataSet; sql: string);
    procedure curLoadIsEmpty(tableName: string);
    procedure SendMail(Subject: string; Body: string='');
    procedure ScrapeCountTable(db2Table, adsTable: string);
    procedure ScrapeCountQuery(db2Q, adsQ: string);
    procedure BOPSLSS;        // 6
  end;

var
  DM: TDM;
  AdoCon: TADOConnection;
  AdsCon: TADSConnection;
  AdoQuery: TAdoQuery;
  AdsQuery: TAdsQuery;
  LogQuery: TAdsQuery;
  stgErrorLogQuery: TAdsQuery;
  StartTS: TDateTime;
  EndTS: TDateTime;

resourcestring
  executable = 'dimSalesPerson';

implementation

constructor TDM.Create(AOwner: TComponent);
begin
  AdoCon := TADOConnection.Create(nil);
  AdsCon := TADSConnection.Create(nil);
  AdoQuery := TADOQuery.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  LogQuery := TAdsQuery.Create(nil);
  stgErrorLogQuery := TAdsQuery.Create(nil);
  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
  AdsCon.ConnectPath := '\\67.135.158.12:6363\Advantage\DDS\DDS.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
  AdoCon.Connected := True;
  AdsCon.Connect;
  LogQuery.AdsConnection := AdsCon;
  stgErrorLogQuery.AdsConnection := AdsCon;
end;

destructor TDM.Destroy;
begin
  AdoQuery.Close;
  FreeAndNil(AdoQuery);
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  LogQuery.Close;
  FreeAndNil(LogQuery);
  stgErrorLogQuery.Close;
  FreeAndNil(stgErrorLogQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  AdoCon.Connected := False;
  FreeAndNil(AdoCon);
  inherited;
end;

procedure TDM.BOPSLSS;
var
  proc: string;
  dObject: string;
  PassFail: string;
  missingSalesPerson: integer;
  newSalesPerson: integer;
begin
  proc := 'BOPSLSS';
  dObject := 'dimSalesPerson';
  newSalesPerson := 0;
  try
    try
      DM.OpenQuery(AdsQuery, 'execute procedure checkDependencies(' + QuotedStr(executable) + ',' + QuotedStr(dObject) + ')');
      PassFail := AdsQuery.FieldByName('PassFail').AsString;
      DM.CloseQuery(AdsQuery);
      if PassFail = 'Pass' then
        begin
          ExecuteQuery(AdsQuery, 'execute procedure UpdateNightlyBatch(' + QuotedStr(dObject) + ',True,False)');
          DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
          ExecuteQuery(AdsQuery, 'delete from tmpBOPSLSS');
          PrepareQuery(AdsQuery, ' insert into tmpBOPSLSS(' +
            'BQCO#, BQRTYP, BQKEY, BQSPID, BQSTAT, BQDATE, BQNAME, BQTYPE, ' +
            'BQSTYP, BQPRNT, BQSPLT, BQUNIT, BQUCNT, BQCGRS, BQNGRS, BQGCRT, ' +
            'BQPCRT, BQTCRT, BQHCRT, BQDCRT, BQRCRT, BQICRT, BQSCRT, BQACRT, ' +
            'BQNCRT, BQGBON, BQPBON, BQTBON, BQHBON, BQDBON, BQRBON, BQIBON, ' +
            'BQSBON, BQABON, BQNBON, BQGMIN, BQPMIN, BQTMIN, BQHMIN, BQDMIN, ' +
            'BQRMIN, BQIMIN, BQSMIN, BQAMIN, BQNMIN, BQGMINO, BQPMINO, BQTMINO, ' +
            'BQHMINO, BQDMINO, BQRMINO, BQIMINO, BQSMINO, BQAMINO, BQNMINO, ' +
            'BQGCOM, BQPCOM, BQTCOM, BQHCOM, BQDCOM, BQRCOM, BQICOM, BQSCOM, ' +
            'BQACOM, BQNCOM, BQTOTCOM, BQACCRT, BQACBON, BQACMIN, BQACMINO) ' +
            'values(' +
            ':BQCO, :BQRTYP, :BQKEY, :BQSPID, :BQSTAT, :BQDATE, :BQNAME, :BQTYPE, ' +
            ':BQSTYP, :BQPRNT, :BQSPLT, :BQUNIT, :BQUCNT, :BQCGRS, :BQNGRS, :BQGCRT, ' +
            ':BQPCRT, :BQTCRT, :BQHCRT, :BQDCRT, :BQRCRT, :BQICRT, :BQSCRT, :BQACRT, ' +
            ':BQNCRT, :BQGBON, :BQPBON, :BQTBON, :BQHBON, :BQDBON, :BQRBON, :BQIBON, ' +
            ':BQSBON, :BQABON, :BQNBON, :BQGMIN, :BQPMIN, :BQTMIN, :BQHMIN, :BQDMIN, ' +
            ':BQRMIN, :BQIMIN, :BQSMIN, :BQAMIN, :BQNMIN, :BQGMINO, :BQPMINO, :BQTMINO, ' +
            ':BQHMINO, :BQDMINO, :BQRMINO, :BQIMINO, :BQSMINO, :BQAMINO, :BQNMINO, ' +
            ':BQGCOM, :BQPCOM, :BQTCOM, :BQHCOM, :BQDCOM, :BQRCOM, :BQICOM, :BQSCOM, ' +
            ':BQACOM, :BQNCOM, :BQTOTCOM, :BQACCRT, :BQACBON, :BQACMIN, :BQACMINO)');
          OpenQuery(AdoQuery, 'select ' +
            'BQCO#, BQRTYP, BQKEY, BQSPID, BQSTAT, ' +
            'case ' +
            '  when BQDATE = 0 then cast(''9999-12-31'' as date) ' +
            '  else cast(left(digits(BQDATE), 4) || ''-'' || substr(digits(BQDATE), 5, 2) || ''-'' || substr(digits(BQDATE), 7, 2) as date) ' +
            'end as BQDATE, ' +
            'BQNAME, BQTYPE, BQSTYP, BQPRNT, BQSPLT, ' +
            'BQUNIT, BQUCNT, BQCGRS, BQNGRS, BQGCRT, ' +
            'BQPCRT, BQTCRT, BQHCRT, BQDCRT, BQRCRT, ' +
            'BQICRT, BQSCRT, BQACRT, BQNCRT, BQGBON, ' +
            'BQPBON, BQTBON, BQHBON, BQDBON, BQRBON, ' +
            'BQIBON, BQSBON, BQABON, BQNBON, BQGMIN, ' +
            'BQPMIN, BQTMIN, BQHMIN, BQDMIN, BQRMIN, ' +
            'BQIMIN, BQSMIN, BQAMIN, BQNMIN, BQGMINO, ' +
            'BQPMINO, BQTMINO, BQHMINO, BQDMINO, ' +
            'BQRMINO, BQIMINO, BQSMINO, BQAMINO, ' +
            'BQNMINO, BQGCOM, BQPCOM, BQTCOM, BQHCOM, ' +
            'BQDCOM, BQRCOM, BQICOM, BQSCOM, BQACOM, ' +
            'BQNCOM, BQTOTCOM, BQACCRT, BQACBON, ' +
            'BQACMIN, BQACMINO ' +
            'from rydedata.bopslss ' +
            'where bqco# in (''RY1'',''RY2'') ' +
            '  and ' +
            '    case ' +
            '       when BQDATE = 0 then cast(''9999-12-31'' as date) ' +
            '       else cast(left(digits(BQDATE), 4) || ''-'' || substr(digits(BQDATE), 5, 2) || ''-'' || substr(digits(BQDATE), 7, 2) as date) ' +
            '    end > curdate() - 45 day');
          while not AdoQuery.Eof do
          begin
            AdsQuery.ParamByName('bqco').AsString := AdoQuery.FieldByName('bqco#').AsString;
            AdsQuery.ParamByName('bqrtyp').AsString := AdoQuery.FieldByName('bqrtyp').AsString;
            AdsQuery.ParamByName('bqkey').AsInteger := AdoQuery.FieldByName('bqkey').AsInteger;
            AdsQuery.ParamByName('bqspid').AsString := AdoQuery.FieldByName('bqspid').AsString;
            AdsQuery.ParamByName('bqstat').AsString := AdoQuery.FieldByName('bqstat').AsString;
            AdsQuery.ParamByName('bqdate').AsDateTime := AdoQuery.FieldByName('bqdate').AsDateTime;
            AdsQuery.ParamByName('bqname').AsString := AdoQuery.FieldByName('bqname').AsString;
            AdsQuery.ParamByName('bqtype').AsString := AdoQuery.FieldByName('bqtype').AsString;
            AdsQuery.ParamByName('bqstyp').AsString := AdoQuery.FieldByName('bqstyp').AsString;
            AdsQuery.ParamByName('bqprnt').AsString := AdoQuery.FieldByName('bqprnt').AsString;
            AdsQuery.ParamByName('bqsplt').AsString := AdoQuery.FieldByName('bqsplt').AsString;
            AdsQuery.ParamByName('bqunit').AsString := AdoQuery.FieldByName('bqunit').AsString;
            AdsQuery.ParamByName('bqucnt').AsInteger := AdoQuery.FieldByName('bqucnt').AsInteger;
            AdsQuery.ParamByName('bqcgrs').AsCurrency := AdoQuery.FieldByName('bqcgrs').AsCurrency;
            AdsQuery.ParamByName('bqngrs').AsCurrency := AdoQuery.FieldByName('bqngrs').AsCurrency;
            AdsQuery.ParamByName('bqgcrt').AsFloat := AdoQuery.FieldByName('bqgcrt').AsFloat;
            AdsQuery.ParamByName('bqpcrt').AsFloat := AdoQuery.FieldByName('bqpcrt').AsFloat;
            AdsQuery.ParamByName('bqtcrt').AsFloat := AdoQuery.FieldByName('bqtcrt').AsFloat;
            AdsQuery.ParamByName('bqhcrt').AsFloat := AdoQuery.FieldByName('bqhcrt').AsFloat;
            AdsQuery.ParamByName('bqdcrt').AsFloat := AdoQuery.FieldByName('bqdcrt').AsFloat;
            AdsQuery.ParamByName('bqrcrt').AsFloat := AdoQuery.FieldByName('bqrcrt').AsFloat;
            AdsQuery.ParamByName('bqicrt').AsFloat := AdoQuery.FieldByName('bqicrt').AsFloat;
            AdsQuery.ParamByName('bqscrt').AsFloat := AdoQuery.FieldByName('bqscrt').AsFloat;
            AdsQuery.ParamByName('bqacrt').AsFloat := AdoQuery.FieldByName('bqacrt').AsFloat;
            AdsQuery.ParamByName('bqncrt').AsFloat := AdoQuery.FieldByName('bqncrt').AsFloat;
            AdsQuery.ParamByName('bqgbon').AsFloat := AdoQuery.FieldByName('bqgbon').AsFloat;
            AdsQuery.ParamByName('bqpbon').AsFloat := AdoQuery.FieldByName('bqpbon').AsFloat;
            AdsQuery.ParamByName('bqtbon').AsFloat := AdoQuery.FieldByName('bqtbon').AsFloat;
            AdsQuery.ParamByName('bqhbon').AsFloat := AdoQuery.FieldByName('bqhbon').AsFloat;
            AdsQuery.ParamByName('bqdbon').AsFloat := AdoQuery.FieldByName('bqdbon').AsFloat;
            AdsQuery.ParamByName('bqrbon').AsFloat := AdoQuery.FieldByName('bqrbon').AsFloat;
            AdsQuery.ParamByName('bqibon').AsFloat := AdoQuery.FieldByName('bqibon').AsFloat;
            AdsQuery.ParamByName('bqsbon').AsFloat := AdoQuery.FieldByName('bqsbon').AsFloat;
            AdsQuery.ParamByName('bqabon').AsFloat := AdoQuery.FieldByName('bqabon').AsFloat;
            AdsQuery.ParamByName('bqnbon').AsFloat := AdoQuery.FieldByName('bqnbon').AsFloat;
            AdsQuery.ParamByName('bqgmin').AsFloat := AdoQuery.FieldByName('bqgmin').AsFloat;
            AdsQuery.ParamByName('bqpmin').AsFloat := AdoQuery.FieldByName('bqpmin').AsFloat;
            AdsQuery.ParamByName('bqtmin').AsFloat := AdoQuery.FieldByName('bqtmin').AsFloat;
            AdsQuery.ParamByName('bqhmin').AsFloat := AdoQuery.FieldByName('bqhmin').AsFloat;
            AdsQuery.ParamByName('bqdmin').AsFloat := AdoQuery.FieldByName('bqdmin').AsFloat;
            AdsQuery.ParamByName('bqrmin').AsFloat := AdoQuery.FieldByName('bqrmin').AsFloat;
            AdsQuery.ParamByName('bqimin').AsFloat := AdoQuery.FieldByName('bqimin').AsFloat;
            AdsQuery.ParamByName('bqsmin').AsFloat := AdoQuery.FieldByName('bqsmin').AsFloat;
            AdsQuery.ParamByName('bqamin').AsFloat := AdoQuery.FieldByName('bqamin').AsFloat;
            AdsQuery.ParamByName('bqnmin').AsFloat := AdoQuery.FieldByName('bqnmin').AsFloat;
            AdsQuery.ParamByName('bqgmino').AsFloat := AdoQuery.FieldByName('bqgmino').AsFloat;
            AdsQuery.ParamByName('bqpmino').AsFloat := AdoQuery.FieldByName('bqpmino').AsFloat;
            AdsQuery.ParamByName('bqtmino').AsFloat := AdoQuery.FieldByName('bqtmino').AsFloat;
            AdsQuery.ParamByName('bqhmino').AsFloat := AdoQuery.FieldByName('bqhmino').AsFloat;
            AdsQuery.ParamByName('bqdmino').AsFloat := AdoQuery.FieldByName('bqdmino').AsFloat;
            AdsQuery.ParamByName('bqrmino').AsFloat := AdoQuery.FieldByName('bqrmino').AsFloat;
            AdsQuery.ParamByName('bqimino').AsFloat := AdoQuery.FieldByName('bqimino').AsFloat;
            AdsQuery.ParamByName('bqsmino').AsFloat := AdoQuery.FieldByName('bqsmino').AsFloat;
            AdsQuery.ParamByName('bqamino').AsFloat := AdoQuery.FieldByName('bqamino').AsFloat;
            AdsQuery.ParamByName('bqnmino').AsFloat := AdoQuery.FieldByName('bqnmino').AsFloat;
            AdsQuery.ParamByName('bqgcom').AsFloat := AdoQuery.FieldByName('bqgcom').AsFloat;
            AdsQuery.ParamByName('bqpcom').AsFloat := AdoQuery.FieldByName('bqpcom').AsFloat;
            AdsQuery.ParamByName('bqtcom').AsFloat := AdoQuery.FieldByName('bqtcom').AsFloat;
            AdsQuery.ParamByName('bqhcom').AsFloat := AdoQuery.FieldByName('bqhcom').AsFloat;
            AdsQuery.ParamByName('bqdcom').AsFloat := AdoQuery.FieldByName('bqdcom').AsFloat;
            AdsQuery.ParamByName('bqrcom').AsFloat := AdoQuery.FieldByName('bqrcom').AsFloat;
            AdsQuery.ParamByName('bqicom').AsFloat := AdoQuery.FieldByName('bqicom').AsFloat;
            AdsQuery.ParamByName('bqscom').AsFloat := AdoQuery.FieldByName('bqscom').AsFloat;
            AdsQuery.ParamByName('bqacom').AsFloat := AdoQuery.FieldByName('bqacom').AsFloat;
            AdsQuery.ParamByName('bqncom').AsFloat := AdoQuery.FieldByName('bqncom').AsFloat;
            AdsQuery.ParamByName('bqtotcom').AsFloat := AdoQuery.FieldByName('bqtotcom').AsFloat;
            AdsQuery.ParamByName('bqaccrt').AsFloat := AdoQuery.FieldByName('bqaccrt').AsFloat;
            AdsQuery.ParamByName('bqacbon').AsFloat := AdoQuery.FieldByName('bqacbon').AsFloat;
            AdsQuery.ParamByName('bqacmin').AsFloat := AdoQuery.FieldByName('bqacmin').AsFloat;
            AdsQuery.ParamByName('bqacmino').AsFloat := AdoQuery.FieldByName('bqacmino').AsFloat;
            AdsQuery.ExecSQL;
            AdoQuery.Next;
          end;
          ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''tmpBOPSLSS'')');
          OpenQuery(AdsQuery, 'execute procedure xfmDimSalesPerson()');
          newSalesPerson := AdsQuery.FieldByName('newSalesPerson').AsInteger;
          CloseQuery(AdsQuery);
          if newSalesPerson > 0 then
          begin
            sendmail('*** dimSalesPerson New Salesperson***', 'as indicated by dimSalesPerson');
          end;
          ExecuteQuery(AdsQuery, 'execute procedure UpdateNightlyBatch(' + QuotedStr(dObject) + ',False,True)');
          DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
            'null' + ')');
        end
      else
        sendmail(executable + '.' + proc + '.' + dObject + ' failed CheckDependency', '');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('dimSalesPerson.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

{$region 'utilities'}
//************************Utilities*******************************************//

function TDM.GetADSSqlTimeStamp(const DateTime: TDateTime): String;
var
  ye, mo, da, ho, mi, se, ms: Word;
begin
  DecodeDateTime(DateTime, ye, mo, da, ho, mi, se, ms);
  Result := FormatWordToStr(ye, 4) + '-' +
            FormatWordToStr(mo, 2) + '-' +
            FormatWordToStr(da, 2) + ' ' +
            FormatWordToStr(ho, 2) + ':' +
            FormatWordToStr(mi, 2) + ':' +
            FormatWordToStr(se, 2);
end;

function TDM.FormatWordToStr(const inVal: Word;
  const OutLength: Integer; const PadChar: Char): String;
begin
  Result := IntToStr(inVal);
  if Length(Result) < OutLength then
    Result := StringOfChar(PadChar, OutLength - Length(Result)) + Result;
end;

procedure TDM.curLoadIsEmpty(tableName: string);
var
  curLoadCount: Integer;
begin
  OpenQuery(AdsQuery, 'select count(*) as CurLoad from curLoad' + tableName);
  curLoadCount := AdsQuery.FieldByName('CurLoad').AsInteger;
  CloseQuery(AdsQuery);
  if curLoadCount <> 0 then
    SendMail(tableName + ' failed curLoadIsEmpty');
  Assert(curLoadCount = 0, 'curLoad' + tableName + ' not empty');
end;

procedure TDM.Log(job, routine: string; jobStartTS, jobEndTS: TDateTime);
var
  sql: string;
begin
  PrepareQuery(LogQuery);
  LogQuery.SQL.Add('execute procedure etlJobLogAuditInsert(' + QuotedStr(job) + ', ' +
    QuotedStr(routine) + ', ' +  QuotedStr(GetADSSqlTimeStamp(jobStartTS)) + ', ' +
    QuotedStr(GetADSSqlTimeStamp(jobEndTS)) + ')');
  sql := LogQuery.SQL.Text;
  LogQuery.ExecSQL;
end;

procedure TDM.ExecuteQuery(query: TDataSet; sql: string);
begin
    if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
    begin
      AdsQuery.AdsConnection := AdsCon;
      CloseQuery(AdsQuery);
      AdsQuery.SQL.Text := sql;
      AdsQuery.ExecSQL;
      CloseQuery(AdsQuery);
    end
    else if query.ClassParent.ClassName = 'TCustomADODataSet' then
    begin
      AdoQuery.Connection := AdoCon;
      CloseQuery(AdoQuery);
      AdoQuery.SQL.Text := sql;
      AdoQuery.ExecSQL;
      CloseQuery(AdoQuery);
    end;
end;

procedure TDM.OpenQuery(query: TDataSet; sql: string);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
    AdsQuery.SQL.Text := sql;
    AdsQuery.Open;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
    AdoQuery.SQL.Text := sql;
    AdoQuery.Open;
  end;
end;



procedure TDM.PrepareQuery(query: TDataSet; sql: string);
begin
//  if query = 'TAdsExtendedDataSet' then
  if query.classparent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
{ TODO -ojon -cfigure out : don't remember why i commented out close - figure it out and be consistent }
//    AdsQuery.Close;
    if sql <> '' then
      AdsQuery.SQL.Text := sql;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    if sql <> '' then
      AdoQuery.SQL.Text := sql;
  end;
end;



procedure TDM.SendMail(Subject: string; Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
        ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            '''' + 'send mail failed' + '''' + ', ' +
            '''' + 'no sql - line 427' + '''' + ', ' +
            QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        exit;
      end;
    end;
  finally
//    Msg.Free;
    FreeAndNil(Msg);
//    SMTP.Free;
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;


procedure TDM.CloseQuery(query: TDataSet);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
  end;
end;

procedure TDM.ScrapeCountQuery(db2Q, adsQ: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Q);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsQ);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckQuery between ' + db2Q + '(' + IntToStr(db2Count) + ') and ' + adsQ + '(' + IntToStr(adsCount) + ')  failed');
end;

procedure TDM.ScrapeCountTable(db2Table, adsTable: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  db2Table := 'rydedata.' + db2Table;
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Table);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsTable);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckTable between ' + db2Table + '(' + IntToStr(db2Count) + ') and ' + adsTable + '(' + IntToStr(adsCount) + ')  failed');
end;
{$endregion}

end.


