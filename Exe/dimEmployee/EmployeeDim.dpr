program EmployeeDim;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  ActiveX,
  StrUtils,
  EmpDimDM in 'EmpDimDM.pas' {DM: TDataModule};

resourcestring
  executable = 'EmployeeDim';

begin
  try
    try
      CoInitialize(nil);
      DM := TDM.Create(nil);
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
        QuotedStr(executable) + ', ' +
        '''' + 'none' + '''' + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
//// *** Active Procs *** //
      DM.PYPCLKCTL;               // 1
      DM.PYACTGR;                 // 2
      DM.PYMAST;                  // 3
      DM.PYPRHEAD;                // 4
      DM.xfmEmpDim;               // 5
      DM.maintDimEmployee;        // 8
//// *** Active Procs *** //
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        '''' + 'none' + '''' + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          '''' + 'edwEmployeeDim' + '''' + ', ' +
          '''' + 'no sql - line 136' + '''' + ', ' +
          QuotedStr(E.ClassName + ' ' + E.Message) + ')');

        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          '''' + 'none' + '''' + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.ClassName + ' ' + E.Message) + ')');

        DM.SendMail('edwEmployeeDim Failed.' + leftstr(E.Message, 8),   E.Message);
        exit;
      end;
    end;
//    DM.SendMail('edwEmployeeDim passed');
  finally
    FreeAndNil(DM);
  end;
end.
