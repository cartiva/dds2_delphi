program dimTech;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  ActiveX,
  StrUtils,
  dimTechDM in 'dimTechDM.pas' {DM: TDataModule};

resourcestring
  executable = 'dimTech';

begin
  try
    try
      CoInitialize(nil);
      DM := TDM.Create(nil);
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
        QuotedStr(executable) + ', ' +
        '''' + 'none' + '''' + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
// *** active procs ***//
      DM.SDPTECH;       // 6
// *** active procs ***//
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        '''' + 'none' + '''' + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          '''' + 'none' + '''' + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        DM.SendMail('dimTech.' + leftstr(E.Message, 8),   E.Message);
        exit;
      end;
    end;
//    DM.SendMail('ServiceLine passed');
  finally
    FreeAndNil(DM);
  end;
end.
