unit bopmastDM;

interface

uses
  SysUtils, Classes, adsdata, adsfunc, adstable, adscnnct, DB, ADODB, DateUtils,
  IdSMTPBase, IdSMTP, IdMessage,
  Dialogs;

type
  TDM = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure PrepareQuery(query: TDataSet; sql: string='');
    procedure CloseQuery(query: TDataSet);
    function FormatWordToStr(const inVal: Word;
      const OutLength: Integer; const PadChar: Char = #48): String;
    function GetADSSqlTimeStamp(const DateTime: TDateTime): String;
    procedure OpenQuery(query: TDataSet; sql: string);
    procedure ExecuteQuery(query: TDataSet; sql: string);
    procedure SendMail(Subject: string; Body: string='');
    procedure ScrapeCountTable(db2Table, adsTable: string);
    procedure ScrapeCountQuery(db2Q, adsQ: string);
    procedure extBOPMAST;
  end;

var
  DM: TDM;
  AdoCon: TADOConnection;
  AdsCon: TADSConnection;
  AdoQuery: TAdoQuery;
  AdsQuery: TAdsQuery;
  StartTS: TDateTime;
  EndTS: TDateTime;

resourcestring
  executable = 'BOPMAST';

implementation

constructor TDM.Create(AOwner: TComponent);
begin
  AdoCon := TADOConnection.Create(nil);
  AdsCon := TADSConnection.Create(nil);
  AdoQuery := TADOQuery.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
  AdsCon.ConnectPath := '\\67.135.158.12:6363\Advantage\DDS\DDS.add';
//  AdsCon.ConnectPath := '\\67.135.158.12:6363\DailyCut\DDS\sunday\copy\DDS.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
  AdoCon.Connected := True;
  AdsCon.Connect;
end;

destructor TDM.Destroy;
begin
  AdoQuery.Close;
  FreeAndNil(AdoQuery);
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  AdoCon.Connected := False;
  FreeAndNil(AdoCon);
  inherited;
end;

procedure TDM.extBOPMAST;
var
  proc: string;
  dObject: string;
  PassFail: string;
begin
  proc := 'BOPMAST';
  dObject := 'BOPMAST';
  try
    try
      DM.OpenQuery(AdsQuery, 'execute procedure checkDependencies(' + QuotedStr(executable) + ',' + QuotedStr(dObject) + ')');
      PassFail := AdsQuery.FieldByName('PassFail').AsString;
      DM.CloseQuery(AdsQuery);
      if PassFail = 'Pass' then
        begin
          DM.ExecuteQuery(AdsQuery, 'execute procedure UpdateNightlyBatch(' + QuotedStr(dObject) + ',True,False)');
          DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
          ExecuteQuery(AdsQuery, 'delete from stgArkonaBOPMAST');
          PrepareQuery(AdsQuery, ' insert into stgArkonaBOPMAST(' +
              'BMCO#, BMKEY, BMSTAT, BMTYPE, BMVTYP, BMFRAN, BMWHSL, BMSTK#, ' +
              'BMVIN, BMBUYR, BMCBYR, BMSNAM, BMPRIC, BMDOWN, BMTRAD, BMTACV, ' +
              'BMTDPO, BMAPR, BMTERM, BMITRM, BMDYFP, BMPFRQ, BMPYMT, BMAMTF, ' +
              'BMDTOR, BMDTFP, BMDTLP, BMDTSE, BMTAXG, BMLSRC, BMSSRC, BMISRC, ' +
              'BMGSRC, BMSRVC, BMSVCA, BMSDED, BMSMIL, BMSMOS, BMSCST, BMSCSD, ' +
              'BMSCED, BMSVAC, BMCLPM, BMLVPM, BMAHPM, BMGAPP, BMGCST, BMCLCD, ' +
              'BMAHCD, BMGPCD, BMTAX1, BMTAX2, BMTAX3, BMTAX4, BMTAX5, BMBRAT, ' +
              'BMRHBP, BMTRD#, BMTSK#, BMSVC#, BMCLC#, BMAHC#, BMGAP#, BMFVND, ' +
              'BMPDIC, BMPDAL, BMPDP#, BMPDED, BMPDAN, BMPDAA, BMPDAC, BMPDAS, ' +
              'BMPDAZ, BMPDPN, BMPDCL, BMPDCP, BMPDFT, BMPDCD, BMPDPD, BMPDFD, ' +
              'BMPDVU, BMPDPM, BMATOT, BMFTOT, BMTPAY, BMICHG, BMVCST, BMICST, ' +
              'BMADDC, BMHLDB, BMPACK, BMCGRS, BMINCT, BMRBTE, BMODOM, BMPAMT, ' +
              'BMPBDT, BMPCKF, BMNPPY, BMLRES, BMARES, BMGRES, BMPRES, BMMRES, ' +
              'BMSRES, BMFRES, BMTRES, BMPSLP, BMSLP2, BMDDWN, BMDDDD, BMLOF, ' +
              'BMLOFO, BMEAPR, BMMSRP, BMLPRC, BMCAPC, BMCAPR, BMLAPR, BMLFAC, ' +
              'BMLTRM, BMRESP, BMRESA, BMNETR, BMTDEP, BMMIPY, BMMPYA, BMEMRT, ' +
              'BMEMR2, BMEMCG, BMACQF, BMLPAY, BMLPYM, BMLSTX, BMLCHG, BMCRTX, ' +
              'BMCRT1, BMCRT2, BMCRT3, BMCRT4, BMCRT6, BMSECD, BMDPOS, BMCSHR, ' +
              'BMLFBR, BMITOT, BMCTAX, BMSDOR, BMAFOR, BMFETO, BMCSTX, BMAPYL, ' +
              'BMRAT1, BMRAT2, BMRAT3, BMRAT4, BMRAT6, BMEXT1, BMEXT2, BMEXT3, ' +
              'BMEXT4, BMTOR1, BMTOR2, BMTOR3, BMTOR4, BMTOR6, BMFROR, BMCOVR, ' +
              'BMHOVR, BMTOVR, BMRBDP, BMCSEL, BMCIOP, BMSACT, BMUNWD, BMDOC#, ' +
              'BMBAGE, BMCBAG, BMLCOV, BMHCOV, BMITDT, BMLTXG, BMCBLP, BMDTSV, ' +
              'BMDTTP, BMCCRT, BMRDWN, BMTDWN, BMTCRD, BMGCAP, BMDINCT, BMPRNT, ' +
              'BMNVDR, BMDTAPRV, BMDTCAP, BMEMCB, BMEMPB, BMEMRB, BMMPYB, BMMYAB, ' +
              'BMNTRB, BMRSAB, BMRSPB, B9AMTAX, B9EPAY, B9FPAY, B9FPCODE, B9HTERM, ' +
              'B9LPTAX, B9LTRAD, B9ONETTR, B9ONTEQ, B9PDP#, B9PEAMT, B9PETAX, ' +
              'B9SCTAX, B9THCOV, B9TXBASE, B9WKIP, BMLDTLP, BMRAT5, BMRAT7, ' +
              'BMOLPM, BMOCDT, BMLLOPT, BMRFTX1, BMRFTX2, BMPYXINS, BMPBKR, ' +
              'BMBRRI, BMCRRI, BMBGEN, BMCGEN, BMCICD, BMCIPM, BMLECD, BMLEPM, ' +
              'BMUFTX1, BMUFTX2, BMRESIN, BMDISEL, BMREMIC, BMPRMPCT, BMRAT8, ' +
              'BMDELVDT, BMIMILE, BMIMRT, BMIMCG, BMLPYREM, BMFIGRS, BMHGRS, ' +
              'BMMTHGAPP, BMIMFLAG, BMIMTHLD, BMGAPTAX) ' +
              'values(' +
              ':BMCO, :BMKEY, :BMSTAT, :BMTYPE, :BMVTYP, :BMFRAN, :BMWHSL, ' +
              ':BMSTK, :BMVIN, :BMBUYR, :BMCBYR, :BMSNAM, :BMPRIC, :BMDOWN, ' +
              ':BMTRAD, :BMTACV, :BMTDPO, :BMAPR, :BMTERM, :BMITRM, :BMDYFP, ' +
              ':BMPFRQ, :BMPYMT, :BMAMTF, :BMDTOR, :BMDTFP, :BMDTLP, :BMDTSE, ' +
              ':BMTAXG, :BMLSRC, :BMSSRC, :BMISRC, :BMGSRC, :BMSRVC, :BMSVCA, ' +
              ':BMSDED, :BMSMIL, :BMSMOS, :BMSCST, :BMSCSD, :BMSCED, :BMSVAC, ' +
              ':BMCLPM, :BMLVPM, :BMAHPM, :BMGAPP, :BMGCST, :BMCLCD, :BMAHCD, ' +
              ':BMGPCD, :BMTAX1, :BMTAX2, :BMTAX3, :BMTAX4, :BMTAX5, :BMBRAT, ' +
              ':BMRHBP, :BMTRD, :BMTSK, :BMSVC, :BMCLC, :BMAHC, :BMGAP, :BMFVND, ' +
              ':BMPDIC, :BMPDAL, :BMPDP, :BMPDED, :BMPDAN, :BMPDAA, :BMPDAC, ' +
              ':BMPDAS, :BMPDAZ, :BMPDPN, :BMPDCL, :BMPDCP, :BMPDFT, :BMPDCD, ' +
              ':BMPDPD, :BMPDFD, :BMPDVU, :BMPDPM, :BMATOT, :BMFTOT, :BMTPAY, ' +
              ':BMICHG, :BMVCST, :BMICST, :BMADDC, :BMHLDB, :BMPACK, :BMCGRS, ' +
              ':BMINCT, :BMRBTE, :BMODOM, :BMPAMT, :BMPBDT, :BMPCKF, :BMNPPY, ' +
              ':BMLRES, :BMARES, :BMGRES, :BMPRES, :BMMRES, :BMSRES, :BMFRES, ' +
              ':BMTRES, :BMPSLP, :BMSLP2, :BMDDWN, :BMDDDD, :BMLOF, :BMLOFO, ' +
              ':BMEAPR, :BMMSRP, :BMLPRC, :BMCAPC, :BMCAPR, :BMLAPR, :BMLFAC, ' +
              ':BMLTRM, :BMRESP, :BMRESA, :BMNETR, :BMTDEP, :BMMIPY, :BMMPYA, ' +
              ':BMEMRT, :BMEMR2, :BMEMCG, :BMACQF, :BMLPAY, :BMLPYM, :BMLSTX, ' +
              ':BMLCHG, :BMCRTX, :BMCRT1, :BMCRT2, :BMCRT3, :BMCRT4, :BMCRT6, ' +
              ':BMSECD, :BMDPOS, :BMCSHR, :BMLFBR, :BMITOT, :BMCTAX, :BMSDOR, ' +
              ':BMAFOR, :BMFETO, :BMCSTX, :BMAPYL, :BMRAT1, :BMRAT2, :BMRAT3, ' +
              ':BMRAT4, :BMRAT6, :BMEXT1, :BMEXT2, :BMEXT3, :BMEXT4, :BMTOR1, ' +
              ':BMTOR2, :BMTOR3, :BMTOR4, :BMTOR6, :BMFROR, :BMCOVR, :BMHOVR, ' +
              ':BMTOVR, :BMRBDP, :BMCSEL, :BMCIOP, :BMSACT, :BMUNWD, :BMDOC, ' +
              ':BMBAGE, :BMCBAG, :BMLCOV, :BMHCOV, :BMITDT, :BMLTXG, :BMCBLP, ' +
              ':BMDTSV, :BMDTTP, :BMCCRT, :BMRDWN, :BMTDWN, :BMTCRD, :BMGCAP, ' +
              ':BMDINCT, :BMPRNT, :BMNVDR, :BMDTAPRV, :BMDTCAP, :BMEMCB, ' +
              ':BMEMPB, :BMEMRB, :BMMPYB, :BMMYAB, :BMNTRB, :BMRSAB, :BMRSPB, ' +
              ':B9AMTAX, :B9EPAY, :B9FPAY, :B9FPCODE, :B9HTERM, :B9LPTAX, ' +
              ':B9LTRAD, :B9ONETTR, :B9ONTEQ, :B9PDP, :B9PEAMT, :B9PETAX, ' +
              ':B9SCTAX, :B9THCOV, :B9TXBASE, :B9WKIP, :BMLDTLP, :BMRAT5, ' +
              ':BMRAT7, :BMOLPM, :BMOCDT, :BMLLOPT, :BMRFTX1, :BMRFTX2, ' +
              ':BMPYXINS, :BMPBKR, :BMBRRI, :BMCRRI, :BMBGEN, :BMCGEN, ' +
              ':BMCICD, :BMCIPM, :BMLECD, :BMLEPM, :BMUFTX1, :BMUFTX2, ' +
              ':BMRESIN, :BMDISEL, :BMREMIC, :BMPRMPCT, :BMRAT8, :BMDELVDT, ' +
              ':BMIMILE, :BMIMRT, :BMIMCG, :BMLPYREM, :BMFIGRS, :BMHGRS, ' +
              ':BMMTHGAPP, :BMIMFLAG, :BMIMTHLD, :BMGAPTAX)');
          OpenQuery(AdoQuery, 'select ' +
              '  BMCO#, BMKEY, BMSTAT, BMTYPE, BMVTYP, BMFRAN, ' +
              '  BMWHSL, BMSTK#, BMVIN, BMBUYR, BMCBYR, BMSNAM, ' +
              '  BMPRIC, BMDOWN, BMTRAD, BMTACV, BMTDPO, BMAPR, ' +
              '  BMTERM, BMITRM, BMDYFP, BMPFRQ, BMPYMT, BMAMTF, ' +
              '  case ' +
              '    when BMDTOR = 0 then cast(''9999-12-31'' as date) ' +
              '    else cast(left(digits(BMDTOR), 4) || ''-'' || substr(digits(BMDTOR), 5, 2) || ''-'' || substr(digits(BMDTOR), 7, 2) as date) ' +
              '  end as BMDTOR, ' +
              '  case ' +
              '    when BMDTFP = 0 then cast(''9999-12-31'' as date) ' +
              '    else cast(left(digits(BMDTFP), 4) || ''-'' || substr(digits(BMDTFP), 5, 2) || ''-'' || substr(digits(BMDTFP), 7, 2) as date) ' +
              '  end as BMDTFP, ' +
              '  case ' +
              '    when BMDTLP = 0 then cast(''9999-12-31'' as date) ' +
              '    else cast(left(digits(BMDTLP), 4) || ''-'' || substr(digits(BMDTLP), 5, 2) || ''-'' || substr(digits(BMDTLP), 7, 2) as date) ' +
              '  end as BMDTLP, ' +
              '  case ' +
              '    when BMDTSE = 0 then cast(''9999-12-31'' as date) ' +
              '    else cast(left(digits(BMDTSE), 4) || ''-'' || substr(digits(BMDTSE), 5, 2) || ''-'' || substr(digits(BMDTSE), 7, 2) as date) ' +
              '  end as BMDTSE, ' +
              '  BMTAXG, BMLSRC, BMSSRC, BMISRC, BMGSRC, BMSRVC, ' +
              '  BMSVCA, BMSDED, BMSMIL, BMSMOS, BMSCST, ' +
              '  case ' +
              '    when BMSCSD = 0 then cast(''9999-12-31'' as date) ' +
              '    else cast(left(digits(BMSCSD), 4) || ''-'' || substr(digits(BMSCSD), 5, 2) || ''-'' || substr(digits(BMSCSD), 7, 2) as date) ' +
              '  end as BMSCSD, ' +
              '  case ' +
              '    when BMSCED = 0 then cast(''9999-12-31'' as date) ' +
              '    else cast(left(digits(BMSCED), 4) || ''-'' || substr(digits(BMSCED), 5, 2) || ''-'' || substr(digits(BMSCED), 7, 2) as date) ' +
              '  end as BMSCED, ' +
              '  BMSVAC, BMCLPM, BMLVPM, BMAHPM, BMGAPP, ' +
              '  BMGCST, BMCLCD, BMAHCD, BMGPCD, BMTAX1, ' +
              '  BMTAX2, BMTAX3, BMTAX4, BMTAX5, BMBRAT, ' +
              '  BMRHBP, BMTRD#, BMTSK#, BMSVC#, BMCLC#, ' +
              '  BMAHC#, BMGAP#, BMFVND, BMPDIC, BMPDAL, BMPDP#, ' +
              '  case ' +
              '    when BMPDED = 0 then cast(''9999-12-31'' as date) ' +
              '    else cast(left(digits(BMPDED), 4) || ''-'' || substr(digits(BMPDED), 5, 2) || ''-'' || substr(digits(BMPDED), 7, 2) as date) ' +
              '  end as BMPDED, ' +
              '  BMPDAN, BMPDAA, BMPDAC, BMPDAS, BMPDAZ, ' +
              '  BMPDPN, BMPDCL, BMPDCP, BMPDFT, BMPDCD, ' +
              '  BMPDPD, BMPDFD, BMPDVU, BMPDPM, BMATOT, ' +
              '  BMFTOT, BMTPAY, BMICHG, BMVCST, BMICST, ' +
              '  BMADDC, BMHLDB, BMPACK, BMCGRS, BMINCT, ' +
              '  BMRBTE, BMODOM, BMPAMT, ' +
              '  case ' +
              '    when BMPBDT = 0 then cast(''9999-12-31'' as date) ' +
              '    else cast(left(digits(BMPBDT), 4) || ''-'' || substr(digits(BMPBDT), 5, 2) || ''-'' || substr(digits(BMPBDT), 7, 2) as date) ' +
              '  end as BMPBDT, ' +
              '  BMPCKF, BMNPPY, BMLRES, BMARES, BMGRES, ' +
              '  BMPRES, BMMRES, BMSRES, BMFRES, BMTRES, ' +
              '  BMPSLP, BMSLP2, BMDDWN, ' +
              '  case ' +
              '    when BMDDDD = 0 then cast(''9999-12-31'' as date) ' +
              '    else cast(left(digits(BMDDDD), 4) || ''-'' || substr(digits(BMDDDD), 5, 2) || ''-'' || substr(digits(BMDDDD), 7, 2) as date) ' +
              '  end as BMDDDD, ' +
              '  BMLOF, BMLOFO, BMEAPR, BMMSRP, BMLPRC, ' +
              '  BMCAPC, BMCAPR, BMLAPR, BMLFAC, BMLTRM, ' +
              '  BMRESP, BMRESA, BMNETR, BMTDEP, BMMIPY, ' +
              '  BMMPYA, BMEMRT, BMEMR2, BMEMCG, BMACQF, ' +
              '  BMLPAY, BMLPYM, BMLSTX, BMLCHG, BMCRTX, ' +
              '  BMCRT1, BMCRT2, BMCRT3, BMCRT4, BMCRT6, ' +
              '  BMSECD, BMDPOS, BMCSHR, BMLFBR, BMITOT, ' +
              '  BMCTAX, BMSDOR, BMAFOR, BMFETO, BMCSTX, ' +
              '  BMAPYL, BMRAT1, BMRAT2, BMRAT3, BMRAT4, ' +
              '  BMRAT6, BMEXT1, BMEXT2, BMEXT3, BMEXT4, ' +
              '  BMTOR1, BMTOR2, BMTOR3, BMTOR4, BMTOR6, ' +
              '  BMFROR, BMCOVR, BMHOVR, BMTOVR, BMRBDP, ' +
              '  BMCSEL, BMCIOP, BMSACT, BMUNWD, BMDOC#, ' +
              '  BMBAGE, BMCBAG, BMLCOV, BMHCOV, ' +
              '  case ' +
              '    when BMITDT = 0 then cast(''9999-12-31'' as date) ' +
              '    else cast(left(digits(BMITDT), 4) || ''-'' || substr(digits(BMITDT), 5, 2) || ''-'' || substr(digits(BMITDT), 7, 2) as date) ' +
              '  end as BMITDT, ' +
              '  BMLTXG, BMCBLP, ' +
              '  case ' +
              '    when BMDTSV = 0 then cast(''9999-12-31'' as date) ' +
              '    else cast(left(digits(BMDTSV), 4) || ''-'' || substr(digits(BMDTSV), 5, 2) || ''-'' || substr(digits(BMDTSV), 7, 2) as date) ' +
              '  end as BMDTSV, ' +
              '  case ' +
              '    when BMDTTP = 0 then cast(''9999-12-31'' as date) ' +
              '    else cast(left(digits(BMDTTP), 4) || ''-'' || substr(digits(BMDTTP), 5, 2) || ''-'' || substr(digits(BMDTTP), 7, 2) as date) ' +
              '  end as BMDTTP, ' +
              '  BMCCRT,BMRDWN, BMTDWN, BMTCRD, ' +
              '  BMGCAP, BMDINCT,BMPRNT, BMNVDR, ' +
              '  case ' +
              '     when BMDTAPRV < date(''1899-12-31'') then date(''9999-12-31'') ' +
              '     else BMDTAPRV ' +
              '  end as BMDTAPRV, ' +
              '  case ' +
              '     when BMDTCAP < date(''1899-12-31'') then date(''9999-12-31'') ' +
              '     else BMDTCAP ' +
              '  end as BMDTCAP, ' +
              '  BMEMCB, BMEMPB, BMEMRB, BMMPYB, ' +
              '  BMMYAB, BMNTRB, BMRSAB, BMRSPB, B9AMTAX, ' +
              '  B9EPAY, B9FPAY, B9FPCODE, B9HTERM, B9LPTAX, ' +
              '  B9LTRAD, B9ONETTR, B9ONTEQ, B9PDP#, ' +
              '  B9PEAMT, B9PETAX, B9SCTAX, B9THCOV, ' +
              '  B9TXBASE, B9WKIP, ' +
              '  case ' +
              '    when BMLDTLP = 0 then cast(''9999-12-31'' as date) ' +
              '    else cast(left(digits(BMLDTLP), 4) || ''-'' || substr(digits(BMLDTLP), 5, 2) || ''-'' || substr(digits(BMLDTLP), 7, 2) as date) ' +
              '  end as BMLDTLP, ' +
              '  BMRAT5, BMRAT7, BMOLPM, BMOCDT, BMLLOPT, ' +
              '  BMRFTX1, BMRFTX2, BMPYXINS, BMPBKR, BMBRRI, ' +
              '  BMCRRI, BMBGEN, BMCGEN, BMCICD, BMCIPM, ' +
              '  BMLECD, BMLEPM, BMUFTX1, BMUFTX2, BMRESIN, ' +
              '  BMDISEL, BMREMIC, BMPRMPCT, BMRAT8, BMDELVDT, ' +
              '  BMIMILE, BMIMRT, BMIMCG, BMLPYREM, BMFIGRS, ' +
              '  BMHGRS, BMMTHGAPP, BMIMFLAG, BMIMTHLD, BMGAPTAX ' +
              'from rydedata.bopmast b1');
    //          'inner join ( ' +
    //          '    select bmstk# as stk#, bmvin as vin, max(bmkey) as thekey ' +
    //          '    from rydedata.bopmast ' +
    //          '    where bmstat = ''U'' ' +
    //          '    group by bmstk#, bmvin) b2 on b1.bmstk# = b2.stk# ' +
    //          '  and b1.bmvin = b2.vin ' +
    //          '  and b1.bmkey = b2.thekey');
          while not AdoQuery.Eof do
          begin
            AdsQuery.ParamByName('bmco').AsString := AdoQuery.FieldByName('bmco#').AsString;
            AdsQuery.ParamByName('bmkey').AsInteger := AdoQuery.FieldByName('bmkey').AsInteger;
            AdsQuery.ParamByName('bmstat').AsString := AdoQuery.FieldByName('bmstat').AsString;
            AdsQuery.ParamByName('bmtype').AsString := AdoQuery.FieldByName('bmtype').AsString;
            AdsQuery.ParamByName('bmvtyp').AsString := AdoQuery.FieldByName('bmvtyp').AsString;
            AdsQuery.ParamByName('bmfran').AsString := AdoQuery.FieldByName('bmfran').AsString;
            AdsQuery.ParamByName('bmwhsl').AsString := AdoQuery.FieldByName('bmwhsl').AsString;
            AdsQuery.ParamByName('bmstk').AsString := AdoQuery.FieldByName('bmstk#').AsString;
            AdsQuery.ParamByName('bmvin').AsString := AdoQuery.FieldByName('bmvin').AsString;
            AdsQuery.ParamByName('bmbuyr').AsInteger := AdoQuery.FieldByName('bmbuyr').AsInteger;
            AdsQuery.ParamByName('bmcbyr').AsInteger := AdoQuery.FieldByName('bmcbyr').AsInteger;
            AdsQuery.ParamByName('bmsnam').AsString := AdoQuery.FieldByName('bmsnam').AsString;
            AdsQuery.ParamByName('bmpric').AsCurrency := AdoQuery.FieldByName('bmpric').AsCurrency;
            AdsQuery.ParamByName('bmdown').AsCurrency := AdoQuery.FieldByName('bmdown').AsCurrency;
            AdsQuery.ParamByName('bmtrad').AsCurrency := AdoQuery.FieldByName('bmtrad').AsCurrency;
            AdsQuery.ParamByName('bmtacv').AsCurrency := AdoQuery.FieldByName('bmtacv').AsCurrency;
            AdsQuery.ParamByName('bmtdpo').AsCurrency := AdoQuery.FieldByName('bmtdpo').AsCurrency;
            AdsQuery.ParamByName('bmapr').AsFloat := AdoQuery.FieldByName('bmapr').AsFloat;
            AdsQuery.ParamByName('bmterm').AsInteger := AdoQuery.FieldByName('bmterm').AsInteger;
            AdsQuery.ParamByName('bmitrm').AsInteger := AdoQuery.FieldByName('bmitrm').AsInteger;
            AdsQuery.ParamByName('bmdyfp').AsInteger := AdoQuery.FieldByName('bmdyfp').AsInteger;
            AdsQuery.ParamByName('bmpfrq').AsString := AdoQuery.FieldByName('bmpfrq').AsString;
            AdsQuery.ParamByName('bmpymt').AsCurrency := AdoQuery.FieldByName('bmpymt').AsCurrency;
            AdsQuery.ParamByName('bmamtf').AsCurrency := AdoQuery.FieldByName('bmamtf').AsCurrency;
            AdsQuery.ParamByName('bmdtor').AsDateTime := AdoQuery.FieldByName('bmdtor').AsDateTime;
            AdsQuery.ParamByName('bmdtfp').AsDateTime := AdoQuery.FieldByName('bmdtfp').AsDateTime;
            AdsQuery.ParamByName('bmdtlp').AsDateTime := AdoQuery.FieldByName('bmdtlp').AsDateTime;
            AdsQuery.ParamByName('bmdtse').AsDateTime := AdoQuery.FieldByName('bmdtse').AsDateTime;
            AdsQuery.ParamByName('bmtaxg').AsInteger := AdoQuery.FieldByName('bmtaxg').AsInteger;
            AdsQuery.ParamByName('bmlsrc').AsInteger := AdoQuery.FieldByName('bmlsrc').AsInteger;
            AdsQuery.ParamByName('bmssrc').AsInteger := AdoQuery.FieldByName('bmssrc').AsInteger;
            AdsQuery.ParamByName('bmisrc').AsInteger := AdoQuery.FieldByName('bmisrc').AsInteger;
            AdsQuery.ParamByName('bmgsrc').AsInteger := AdoQuery.FieldByName('bmgsrc').AsInteger;
            AdsQuery.ParamByName('bmsrvc').AsInteger := AdoQuery.FieldByName('bmsrvc').AsInteger;
            AdsQuery.ParamByName('bmsvca').AsCurrency := AdoQuery.FieldByName('bmsvca').AsCurrency;
            AdsQuery.ParamByName('bmsded').AsCurrency := AdoQuery.FieldByName('bmsded').AsCurrency;
            AdsQuery.ParamByName('bmsmil').AsInteger := AdoQuery.FieldByName('bmsmil').AsInteger;
            AdsQuery.ParamByName('bmsmos').AsInteger := AdoQuery.FieldByName('bmsmos').AsInteger;
            AdsQuery.ParamByName('bmscst').AsCurrency := AdoQuery.FieldByName('bmscst').AsCurrency;
            AdsQuery.ParamByName('bmscsd').AsDateTime := AdoQuery.FieldByName('bmscsd').AsDateTime;
            AdsQuery.ParamByName('bmsced').AsDateTime := AdoQuery.FieldByName('bmsced').AsDateTime;
            AdsQuery.ParamByName('bmsvac').AsString := AdoQuery.FieldByName('bmsvac').AsString;
            AdsQuery.ParamByName('bmclpm').AsCurrency := AdoQuery.FieldByName('bmclpm').AsCurrency;
            AdsQuery.ParamByName('bmlvpm').AsCurrency := AdoQuery.FieldByName('bmlvpm').AsCurrency;
            AdsQuery.ParamByName('bmahpm').AsCurrency := AdoQuery.FieldByName('bmahpm').AsCurrency;
            AdsQuery.ParamByName('bmgapp').AsCurrency := AdoQuery.FieldByName('bmgapp').AsCurrency;
            AdsQuery.ParamByName('bmgcst').AsCurrency := AdoQuery.FieldByName('bmgcst').AsCurrency;
            AdsQuery.ParamByName('bmclcd').AsString := AdoQuery.FieldByName('bmclcd').AsString;
            AdsQuery.ParamByName('bmahcd').AsString := AdoQuery.FieldByName('bmahcd').AsString;
            AdsQuery.ParamByName('bmgpcd').AsString := AdoQuery.FieldByName('bmgpcd').AsString;
            AdsQuery.ParamByName('bmtax1').AsCurrency := AdoQuery.FieldByName('bmtax1').AsCurrency;
            AdsQuery.ParamByName('bmtax2').AsCurrency := AdoQuery.FieldByName('bmtax2').AsCurrency;
            AdsQuery.ParamByName('bmtax3').AsCurrency := AdoQuery.FieldByName('bmtax3').AsCurrency;
            AdsQuery.ParamByName('bmtax4').AsCurrency := AdoQuery.FieldByName('bmtax4').AsCurrency;
            AdsQuery.ParamByName('bmtax5').AsCurrency := AdoQuery.FieldByName('bmtax5').AsCurrency;
            AdsQuery.ParamByName('bmbrat').AsFloat := AdoQuery.FieldByName('bmbrat').AsFloat;
            AdsQuery.ParamByName('bmrhbp').AsFloat := AdoQuery.FieldByName('bmrhbp').AsFloat;
            AdsQuery.ParamByName('bmtrd').AsInteger := AdoQuery.FieldByName('bmtrd#').AsInteger;
            AdsQuery.ParamByName('bmtsk').AsString := AdoQuery.FieldByName('bmtsk#').AsString;
            AdsQuery.ParamByName('bmsvc').AsString := AdoQuery.FieldByName('bmsvc#').AsString;
            AdsQuery.ParamByName('bmclc').AsString := AdoQuery.FieldByName('bmclc#').AsString;
            AdsQuery.ParamByName('bmahc').AsString := AdoQuery.FieldByName('bmahc#').AsString;
            AdsQuery.ParamByName('bmgap').AsString := AdoQuery.FieldByName('bmgap#').AsString;
            AdsQuery.ParamByName('bmfvnd').AsString := AdoQuery.FieldByName('bmfvnd').AsString;
            AdsQuery.ParamByName('bmpdic').AsString := AdoQuery.FieldByName('bmpdic').AsString;
            AdsQuery.ParamByName('bmpdal').AsString := AdoQuery.FieldByName('bmpdal').AsString;
            AdsQuery.ParamByName('bmpdp').AsString := AdoQuery.FieldByName('bmpdp#').AsString;
            AdsQuery.ParamByName('bmpded').AsDateTime := AdoQuery.FieldByName('bmpded').AsDateTime;
            AdsQuery.ParamByName('bmpdan').AsString := AdoQuery.FieldByName('bmpdan').AsString;
            AdsQuery.ParamByName('bmpdaa').AsString := AdoQuery.FieldByName('bmpdaa').AsString;
            AdsQuery.ParamByName('bmpdac').AsString := AdoQuery.FieldByName('bmpdac').AsString;
            AdsQuery.ParamByName('bmpdas').AsString := AdoQuery.FieldByName('bmpdas').AsString;
            AdsQuery.ParamByName('bmpdaz').AsString := AdoQuery.FieldByName('bmpdaz').AsString;
            AdsQuery.ParamByName('bmpdpn').AsString := AdoQuery.FieldByName('bmpdpn').AsString;
            AdsQuery.ParamByName('bmpdcl').AsString := AdoQuery.FieldByName('bmpdcl').AsString;
            AdsQuery.ParamByName('bmpdcp').AsString := AdoQuery.FieldByName('bmpdcp').AsString;
            AdsQuery.ParamByName('bmpdft').AsString := AdoQuery.FieldByName('bmpdft').AsString;
            AdsQuery.ParamByName('bmpdcd').AsInteger := AdoQuery.FieldByName('bmpdcd').AsInteger;
            AdsQuery.ParamByName('bmpdpd').AsInteger := AdoQuery.FieldByName('bmpdpd').AsInteger;
            AdsQuery.ParamByName('bmpdfd').AsInteger := AdoQuery.FieldByName('bmpdfd').AsInteger;
            AdsQuery.ParamByName('bmpdvu').AsString := AdoQuery.FieldByName('bmpdvu').AsString;
            AdsQuery.ParamByName('bmpdpm').AsInteger := AdoQuery.FieldByName('bmpdpm').AsInteger;
            AdsQuery.ParamByName('bmatot').AsCurrency := AdoQuery.FieldByName('bmatot').AsCurrency;
            AdsQuery.ParamByName('bmftot').AsCurrency := AdoQuery.FieldByName('bmftot').AsCurrency;
            AdsQuery.ParamByName('bmtpay').AsCurrency := AdoQuery.FieldByName('bmtpay').AsCurrency;
            AdsQuery.ParamByName('bmichg').AsCurrency := AdoQuery.FieldByName('bmichg').AsCurrency;
            AdsQuery.ParamByName('bmvcst').AsCurrency := AdoQuery.FieldByName('bmvcst').AsCurrency;
            AdsQuery.ParamByName('bmicst').AsCurrency := AdoQuery.FieldByName('bmicst').AsCurrency;
            AdsQuery.ParamByName('bmaddc').AsCurrency := AdoQuery.FieldByName('bmaddc').AsCurrency;
            AdsQuery.ParamByName('bmhldb').AsCurrency := AdoQuery.FieldByName('bmhldb').AsCurrency;
            AdsQuery.ParamByName('bmpack').AsCurrency := AdoQuery.FieldByName('bmpack').AsCurrency;
            AdsQuery.ParamByName('bmcgrs').AsCurrency := AdoQuery.FieldByName('bmcgrs').AsCurrency;
            AdsQuery.ParamByName('bminct').AsCurrency := AdoQuery.FieldByName('bminct').AsCurrency;
            AdsQuery.ParamByName('bmrbte').AsCurrency := AdoQuery.FieldByName('bmrbte').AsCurrency;
            AdsQuery.ParamByName('bmodom').AsInteger := AdoQuery.FieldByName('bmodom').AsInteger;
            AdsQuery.ParamByName('bmpamt').AsCurrency := AdoQuery.FieldByName('bmpamt').AsCurrency;
            AdsQuery.ParamByName('bmpbdt').AsDateTime := AdoQuery.FieldByName('bmpbdt').AsDateTime;
            AdsQuery.ParamByName('bmpckf').AsString := AdoQuery.FieldByName('bmpckf').AsString;
            AdsQuery.ParamByName('bmnppy').AsInteger := AdoQuery.FieldByName('bmnppy').AsInteger;
            AdsQuery.ParamByName('bmlres').AsCurrency := AdoQuery.FieldByName('bmlres').AsCurrency;
            AdsQuery.ParamByName('bmares').AsCurrency := AdoQuery.FieldByName('bmares').AsCurrency;
            AdsQuery.ParamByName('bmgres').AsCurrency := AdoQuery.FieldByName('bmgres').AsCurrency;
            AdsQuery.ParamByName('bmpres').AsCurrency := AdoQuery.FieldByName('bmpres').AsCurrency;
            AdsQuery.ParamByName('bmmres').AsCurrency := AdoQuery.FieldByName('bmmres').AsCurrency;
            AdsQuery.ParamByName('bmsres').AsCurrency := AdoQuery.FieldByName('bmsres').AsCurrency;
            AdsQuery.ParamByName('bmfres').AsCurrency := AdoQuery.FieldByName('bmfres').AsCurrency;
            AdsQuery.ParamByName('bmtres').AsCurrency := AdoQuery.FieldByName('bmtres').AsCurrency;
            AdsQuery.ParamByName('bmpslp').AsString := AdoQuery.FieldByName('bmpslp').AsString;
            AdsQuery.ParamByName('bmslp2').AsString := AdoQuery.FieldByName('bmslp2').AsString;
            AdsQuery.ParamByName('bmddwn').AsCurrency := AdoQuery.FieldByName('bmddwn').AsCurrency;
            AdsQuery.ParamByName('bmdddd').AsDateTime := AdoQuery.FieldByName('bmdddd').AsDateTime;
            AdsQuery.ParamByName('bmlof').AsCurrency := AdoQuery.FieldByName('bmlof').AsCurrency;
            AdsQuery.ParamByName('bmlofo').AsString := AdoQuery.FieldByName('bmlofo').AsString;
            AdsQuery.ParamByName('bmeapr').AsFloat := AdoQuery.FieldByName('bmeapr').AsFloat;
            AdsQuery.ParamByName('bmmsrp').AsCurrency := AdoQuery.FieldByName('bmmsrp').AsCurrency;
            AdsQuery.ParamByName('bmlprc').AsCurrency := AdoQuery.FieldByName('bmlprc').AsCurrency;
            AdsQuery.ParamByName('bmcapc').AsCurrency := AdoQuery.FieldByName('bmcapc').AsCurrency;
            AdsQuery.ParamByName('bmcapr').AsCurrency := AdoQuery.FieldByName('bmcapr').AsCurrency;
            AdsQuery.ParamByName('bmlapr').AsInteger := AdoQuery.FieldByName('bmlapr').AsInteger;
            AdsQuery.ParamByName('bmlfac').AsFloat := AdoQuery.FieldByName('bmlfac').AsFloat;
            AdsQuery.ParamByName('bmltrm').AsInteger := AdoQuery.FieldByName('bmltrm').AsInteger;
            AdsQuery.ParamByName('bmresp').AsFloat := AdoQuery.FieldByName('bmresp').AsFloat;
            AdsQuery.ParamByName('bmresa').AsCurrency := AdoQuery.FieldByName('bmresa').AsCurrency;
            AdsQuery.ParamByName('bmnetr').AsCurrency := AdoQuery.FieldByName('bmnetr').AsCurrency;
            AdsQuery.ParamByName('bmtdep').AsCurrency := AdoQuery.FieldByName('bmtdep').AsCurrency;
            AdsQuery.ParamByName('bmmipy').AsInteger := AdoQuery.FieldByName('bmmipy').AsInteger;
            AdsQuery.ParamByName('bmmpya').AsInteger := AdoQuery.FieldByName('bmmpya').AsInteger;
            AdsQuery.ParamByName('bmemrt').AsInteger := AdoQuery.FieldByName('bmemrt').AsInteger;
            AdsQuery.ParamByName('bmemr2').AsInteger := AdoQuery.FieldByName('bmemr2').AsInteger;
            AdsQuery.ParamByName('bmemcg').AsCurrency := AdoQuery.FieldByName('bmemcg').AsCurrency;
            AdsQuery.ParamByName('bmacqf').AsCurrency := AdoQuery.FieldByName('bmacqf').AsCurrency;
            AdsQuery.ParamByName('bmlpay').AsCurrency := AdoQuery.FieldByName('bmlpay').AsCurrency;
            AdsQuery.ParamByName('bmlpym').AsCurrency := AdoQuery.FieldByName('bmlpym').AsCurrency;
            AdsQuery.ParamByName('bmlstx').AsCurrency := AdoQuery.FieldByName('bmlstx').AsCurrency;
            AdsQuery.ParamByName('bmlchg').AsCurrency := AdoQuery.FieldByName('bmlchg').AsCurrency;
            AdsQuery.ParamByName('bmcrtx').AsCurrency := AdoQuery.FieldByName('bmcrtx').AsCurrency;
            AdsQuery.ParamByName('bmcrt1').AsCurrency := AdoQuery.FieldByName('bmcrt1').AsCurrency;
            AdsQuery.ParamByName('bmcrt2').AsCurrency := AdoQuery.FieldByName('bmcrt2').AsCurrency;
            AdsQuery.ParamByName('bmcrt3').AsCurrency := AdoQuery.FieldByName('bmcrt3').AsCurrency;
            AdsQuery.ParamByName('bmcrt4').AsCurrency := AdoQuery.FieldByName('bmcrt4').AsCurrency;
            AdsQuery.ParamByName('bmcrt6').AsCurrency := AdoQuery.FieldByName('bmcrt6').AsCurrency;
            AdsQuery.ParamByName('bmsecd').AsCurrency := AdoQuery.FieldByName('bmsecd').AsCurrency;
            AdsQuery.ParamByName('bmdpos').AsCurrency := AdoQuery.FieldByName('bmdpos').AsCurrency;
            AdsQuery.ParamByName('bmcshr').AsCurrency := AdoQuery.FieldByName('bmcshr').AsCurrency;
            AdsQuery.ParamByName('bmlfbr').AsFloat := AdoQuery.FieldByName('bmlfbr').AsFloat;
            AdsQuery.ParamByName('bmitot').AsCurrency := AdoQuery.FieldByName('bmitot').AsCurrency;
            AdsQuery.ParamByName('bmctax').AsString := AdoQuery.FieldByName('bmctax').AsString;
            AdsQuery.ParamByName('bmsdor').AsString := AdoQuery.FieldByName('bmsdor').AsString;
            AdsQuery.ParamByName('bmafor').AsString := AdoQuery.FieldByName('bmafor').AsString;
            AdsQuery.ParamByName('bmfeto').AsString := AdoQuery.FieldByName('bmfeto').AsString;
            AdsQuery.ParamByName('bmcstx').AsString := AdoQuery.FieldByName('bmcstx').AsString;
            AdsQuery.ParamByName('bmapyl').AsString := AdoQuery.FieldByName('bmapyl').AsString;
            AdsQuery.ParamByName('bmrat1').AsFloat := AdoQuery.FieldByName('bmrat1').AsFloat;
            AdsQuery.ParamByName('bmrat2').AsFloat := AdoQuery.FieldByName('bmrat2').AsFloat;
            AdsQuery.ParamByName('bmrat3').AsFloat := AdoQuery.FieldByName('bmrat3').AsFloat;
            AdsQuery.ParamByName('bmrat4').AsFloat := AdoQuery.FieldByName('bmrat4').AsFloat;
            AdsQuery.ParamByName('bmrat6').AsFloat := AdoQuery.FieldByName('bmrat6').AsFloat;
            AdsQuery.ParamByName('bmext1').AsString := AdoQuery.FieldByName('bmext1').AsString;
            AdsQuery.ParamByName('bmext2').AsString := AdoQuery.FieldByName('bmext2').AsString;
            AdsQuery.ParamByName('bmext3').AsString := AdoQuery.FieldByName('bmext3').AsString;
            AdsQuery.ParamByName('bmext4').AsString := AdoQuery.FieldByName('bmext4').AsString;
            AdsQuery.ParamByName('bmtor1').AsString := AdoQuery.FieldByName('bmtor1').AsString;
            AdsQuery.ParamByName('bmtor2').AsString := AdoQuery.FieldByName('bmtor2').AsString;
            AdsQuery.ParamByName('bmtor3').AsString := AdoQuery.FieldByName('bmtor3').AsString;
            AdsQuery.ParamByName('bmtor4').AsString := AdoQuery.FieldByName('bmtor4').AsString;
            AdsQuery.ParamByName('bmtor6').AsString := AdoQuery.FieldByName('bmtor6').AsString;
            AdsQuery.ParamByName('bmfror').AsString := AdoQuery.FieldByName('bmfror').AsString;
            AdsQuery.ParamByName('bmcovr').AsString := AdoQuery.FieldByName('bmcovr').AsString;
            AdsQuery.ParamByName('bmhovr').AsString := AdoQuery.FieldByName('bmhovr').AsString;
            AdsQuery.ParamByName('bmtovr').AsString := AdoQuery.FieldByName('bmtovr').AsString;
            AdsQuery.ParamByName('bmrbdp').AsString := AdoQuery.FieldByName('bmrbdp').AsString;
            AdsQuery.ParamByName('bmcsel').AsString := AdoQuery.FieldByName('bmcsel').AsString;
            AdsQuery.ParamByName('bmciop').AsString := AdoQuery.FieldByName('bmciop').AsString;
            AdsQuery.ParamByName('bmsact').AsString := AdoQuery.FieldByName('bmsact').AsString;
            AdsQuery.ParamByName('bmunwd').AsString := AdoQuery.FieldByName('bmunwd').AsString;
            AdsQuery.ParamByName('bmdoc').AsString := AdoQuery.FieldByName('bmdoc#').AsString;
            AdsQuery.ParamByName('bmbage').AsInteger := AdoQuery.FieldByName('bmbage').AsInteger;
            AdsQuery.ParamByName('bmcbag').AsInteger := AdoQuery.FieldByName('bmcbag').AsInteger;
            AdsQuery.ParamByName('bmlcov').AsCurrency := AdoQuery.FieldByName('bmlcov').AsCurrency;
            AdsQuery.ParamByName('bmhcov').AsCurrency := AdoQuery.FieldByName('bmhcov').AsCurrency;
            AdsQuery.ParamByName('bmitdt').AsDateTime := AdoQuery.FieldByName('bmitdt').AsDateTime;
            AdsQuery.ParamByName('bmltxg').AsInteger := AdoQuery.FieldByName('bmltxg').AsInteger;
            AdsQuery.ParamByName('bmcblp').AsCurrency := AdoQuery.FieldByName('bmcblp').AsCurrency;
            AdsQuery.ParamByName('bmdtsv').AsDateTime := AdoQuery.FieldByName('bmdtsv').AsDateTime;
            AdsQuery.ParamByName('bmdttp').AsDateTime := AdoQuery.FieldByName('bmdttp').AsDateTime;
            AdsQuery.ParamByName('bmccrt').AsString := AdoQuery.FieldByName('bmccrt').AsString;
            AdsQuery.ParamByName('bmrdwn').AsCurrency := AdoQuery.FieldByName('bmrdwn').AsCurrency;
            AdsQuery.ParamByName('bmtdwn').AsCurrency := AdoQuery.FieldByName('bmtdwn').AsCurrency;
            AdsQuery.ParamByName('bmtcrd').AsCurrency := AdoQuery.FieldByName('bmtcrd').AsCurrency;
            AdsQuery.ParamByName('bmgcap').AsCurrency := AdoQuery.FieldByName('bmgcap').AsCurrency;
            AdsQuery.ParamByName('bmdinct').AsCurrency := AdoQuery.FieldByName('bmdinct').AsCurrency;
            AdsQuery.ParamByName('bmprnt').AsString := AdoQuery.FieldByName('bmprnt').AsString;
            AdsQuery.ParamByName('bmnvdr').AsString := AdoQuery.FieldByName('bmnvdr').AsString;
            AdsQuery.ParamByName('bmdtaprv').AsDateTime := AdoQuery.FieldByName('bmdtaprv').AsDateTime;
            AdsQuery.ParamByName('bmdtcap').AsDateTime := AdoQuery.FieldByName('bmdtcap').AsDateTime;
            AdsQuery.ParamByName('bmemcb').AsCurrency := AdoQuery.FieldByName('bmemcb').AsCurrency;
            AdsQuery.ParamByName('bmempb').AsCurrency := AdoQuery.FieldByName('bmempb').AsCurrency;
            AdsQuery.ParamByName('bmemrb').AsCurrency := AdoQuery.FieldByName('bmemrb').AsCurrency;
            AdsQuery.ParamByName('bmmpyb').AsCurrency := AdoQuery.FieldByName('bmmpyb').AsCurrency;
            AdsQuery.ParamByName('bmmyab').AsCurrency := AdoQuery.FieldByName('bmmyab').AsCurrency;
            AdsQuery.ParamByName('bmntrb').AsCurrency := AdoQuery.FieldByName('bmntrb').AsCurrency;
            AdsQuery.ParamByName('bmrsab').AsCurrency := AdoQuery.FieldByName('bmrsab').AsCurrency;
            AdsQuery.ParamByName('bmrspb').AsCurrency := AdoQuery.FieldByName('bmrspb').AsCurrency;
            AdsQuery.ParamByName('b9amtax').AsCurrency := AdoQuery.FieldByName('b9amtax').AsCurrency;
            AdsQuery.ParamByName('b9epay').AsCurrency := AdoQuery.FieldByName('b9epay').AsCurrency;
            AdsQuery.ParamByName('b9fpay').AsCurrency := AdoQuery.FieldByName('b9fpay').AsCurrency;
            AdsQuery.ParamByName('b9fpcode').AsString := AdoQuery.FieldByName('b9fpcode').AsString;
            AdsQuery.ParamByName('b9hterm').AsInteger := AdoQuery.FieldByName('b9hterm').AsInteger;
            AdsQuery.ParamByName('b9lptax').AsFloat := AdoQuery.FieldByName('b9lptax').AsFloat;
            AdsQuery.ParamByName('b9ltrad').AsString := AdoQuery.FieldByName('b9ltrad').AsString;
            AdsQuery.ParamByName('b9onettr').AsCurrency := AdoQuery.FieldByName('b9onettr').AsCurrency;
            AdsQuery.ParamByName('b9onteq').AsInteger := AdoQuery.FieldByName('b9onteq').AsInteger;
            AdsQuery.ParamByName('b9pdp').AsString := AdoQuery.FieldByName('b9pdp#').AsString;
            AdsQuery.ParamByName('b9peamt').AsCurrency := AdoQuery.FieldByName('b9peamt').AsCurrency;
            AdsQuery.ParamByName('b9petax').AsCurrency := AdoQuery.FieldByName('b9petax').AsCurrency;
            AdsQuery.ParamByName('b9sctax').AsCurrency := AdoQuery.FieldByName('b9sctax').AsCurrency;
            AdsQuery.ParamByName('b9thcov').AsCurrency := AdoQuery.FieldByName('b9thcov').AsCurrency;
            AdsQuery.ParamByName('b9txbase').AsCurrency := AdoQuery.FieldByName('b9txbase').AsCurrency;
            AdsQuery.ParamByName('b9wkip').AsCurrency := AdoQuery.FieldByName('b9wkip').AsCurrency;
            AdsQuery.ParamByName('bmldtlp').AsDateTime := AdoQuery.FieldByName('bmldtlp').AsDateTime;
            AdsQuery.ParamByName('bmrat5').AsFloat := AdoQuery.FieldByName('bmrat5').AsFloat;
            AdsQuery.ParamByName('bmrat7').AsFloat := AdoQuery.FieldByName('bmrat7').AsFloat;
            AdsQuery.ParamByName('bmolpm').AsCurrency := AdoQuery.FieldByName('bmolpm').AsCurrency;
            AdsQuery.ParamByName('bmocdt').AsInteger := AdoQuery.FieldByName('bmocdt').AsInteger;
            AdsQuery.ParamByName('bmllopt').AsString := AdoQuery.FieldByName('bmllopt').AsString;
            AdsQuery.ParamByName('bmrftx1').AsFloat := AdoQuery.FieldByName('bmrftx1').AsFloat;
            AdsQuery.ParamByName('bmrftx2').AsFloat := AdoQuery.FieldByName('bmrftx2').AsFloat;
            AdsQuery.ParamByName('bmpyxins').AsFloat := AdoQuery.FieldByName('bmpyxins').AsFloat;
            AdsQuery.ParamByName('bmpbkr').AsFloat := AdoQuery.FieldByName('bmpbkr').AsFloat;
            AdsQuery.ParamByName('bmbrri').AsString := AdoQuery.FieldByName('bmbrri').AsString;
            AdsQuery.ParamByName('bmcrri').AsString := AdoQuery.FieldByName('bmcrri').AsString;
            AdsQuery.ParamByName('bmbgen').AsString := AdoQuery.FieldByName('bmbgen').AsString;
            AdsQuery.ParamByName('bmcgen').AsString := AdoQuery.FieldByName('bmcgen').AsString;
            AdsQuery.ParamByName('bmcicd').AsString := AdoQuery.FieldByName('bmcicd').AsString;
            AdsQuery.ParamByName('bmcipm').AsFloat := AdoQuery.FieldByName('bmcipm').AsFloat;
            AdsQuery.ParamByName('bmlecd').AsString := AdoQuery.FieldByName('bmlecd').AsString;
            AdsQuery.ParamByName('bmlepm').AsFloat := AdoQuery.FieldByName('bmlepm').AsFloat;
            AdsQuery.ParamByName('bmuftx1').AsFloat := AdoQuery.FieldByName('bmuftx1').AsFloat;
            AdsQuery.ParamByName('bmuftx2').AsFloat := AdoQuery.FieldByName('bmuftx2').AsFloat;
            AdsQuery.ParamByName('bmresin').AsString := AdoQuery.FieldByName('bmresin').AsString;
            AdsQuery.ParamByName('bmdisel').AsString := AdoQuery.FieldByName('bmdisel').AsString;
            AdsQuery.ParamByName('bmremic').AsString := AdoQuery.FieldByName('bmremic').AsString;
            AdsQuery.ParamByName('bmprmpct').AsFloat := AdoQuery.FieldByName('bmprmpct').AsFloat;
            AdsQuery.ParamByName('bmrat8').AsFloat := AdoQuery.FieldByName('bmrat8').AsFloat;
            AdsQuery.ParamByName('bmdelvdt').AsDateTime := AdoQuery.FieldByName('bmdelvdt').AsDateTime;
            AdsQuery.ParamByName('bmimile').AsInteger := AdoQuery.FieldByName('bmimile').AsInteger;
            AdsQuery.ParamByName('bmimrt').AsInteger := AdoQuery.FieldByName('bmimrt').AsInteger;
            AdsQuery.ParamByName('bmimcg').AsFloat := AdoQuery.FieldByName('bmimcg').AsFloat;
            AdsQuery.ParamByName('bmlpyrem').AsFloat := AdoQuery.FieldByName('bmlpyrem').AsFloat;
            AdsQuery.ParamByName('bmfigrs').AsCurrency := AdoQuery.FieldByName('bmfigrs').AsCurrency;
            AdsQuery.ParamByName('bmhgrs').AsCurrency := AdoQuery.FieldByName('bmhgrs').AsCurrency;
            AdsQuery.ParamByName('bmmthgapp').AsCurrency := AdoQuery.FieldByName('bmmthgapp').AsCurrency;
            AdsQuery.ParamByName('bmimflag').AsString := AdoQuery.FieldByName('bmimflag').AsString;
            AdsQuery.ParamByName('bmimthld').AsFloat := AdoQuery.FieldByName('bmimthld').AsFloat;
            AdsQuery.ParamByName('bmgaptax').AsFloat := AdoQuery.FieldByName('bmgaptax').AsFloat;
            AdsQuery.ExecSQL;
            AdoQuery.Next;
          end;
          ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaBOPMAST'')');
          ScrapeCountTable('BOPMAST', 'stgArkonaBOPMAST');



          DM.ExecuteQuery(AdsQuery, 'execute procedure UpdateNightlyBatch(' + QuotedStr(dObject) + ',False,True)');
          DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
            'null' + ')');
        end
      else
        sendmail(executable + '.' + proc + '.' + dObject + ' failed CheckDependency', '');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create(proc + '.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;
{$region 'Utilities'}
function TDM.GetADSSqlTimeStamp(const DateTime: TDateTime): String;
var
  ye, mo, da, ho, mi, se, ms: Word;
begin
  DecodeDateTime(DateTime, ye, mo, da, ho, mi, se, ms);
  Result := FormatWordToStr(ye, 4) + '-' +
            FormatWordToStr(mo, 2) + '-' +
            FormatWordToStr(da, 2) + ' ' +
            FormatWordToStr(ho, 2) + ':' +
            FormatWordToStr(mi, 2) + ':' +
            FormatWordToStr(se, 2);
end;
function TDM.FormatWordToStr(const inVal: Word;
  const OutLength: Integer; const PadChar: Char): String;
begin
  Result := IntToStr(inVal);
  if Length(Result) < OutLength then
    Result := StringOfChar(PadChar, OutLength - Length(Result)) + Result;
end;
procedure TDM.ExecuteQuery(query: TDataSet; sql: string);
begin
    if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
    begin
      AdsQuery.AdsConnection := AdsCon;
      CloseQuery(AdsQuery);
      AdsQuery.SQL.Text := sql;
      AdsQuery.ExecSQL;
      CloseQuery(AdsQuery);
    end
    else if query.ClassParent.ClassName = 'TCustomADODataSet' then
    begin
      AdoQuery.Connection := AdoCon;
      CloseQuery(AdoQuery);
      AdoQuery.SQL.Text := sql;
      AdoQuery.ExecSQL;
      CloseQuery(AdoQuery);
    end;
end;
procedure TDM.OpenQuery(query: TDataSet; sql: string);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
    AdsQuery.SQL.Text := sql;
    AdsQuery.Open;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
    AdoQuery.SQL.Text := sql;
    AdoQuery.Open;
  end;
end;
procedure TDM.PrepareQuery(query: TDataSet; sql: string);
begin
  if query.classparent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    if sql <> '' then
      AdsQuery.SQL.Text := sql;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    if sql <> '' then
      AdoQuery.SQL.Text := sql;
  end;
end;
procedure TDM.SendMail(Subject: string; Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
        exit;
      end;
    end;
  finally
    FreeAndNil(Msg);
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;
procedure TDM.CloseQuery(query: TDataSet);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
  end;
end;
procedure TDM.ScrapeCountQuery(db2Q, adsQ: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Q);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsQ);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckQuery between ' + db2Q + '(' + IntToStr(db2Count) + ') and ' + adsQ + '(' + IntToStr(adsCount) + ')  failed');
end;
procedure TDM.ScrapeCountTable(db2Table, adsTable: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  db2Table := 'rydedata.' + db2Table;
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Table);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsTable);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckTable between ' + db2Table + '(' + IntToStr(db2Count) + ') and ' + adsTable + '(' + IntToStr(adsCount) + ')  failed');
end;
{$endregion}
end.


