

DROP TABLE extRecordings;
CREATE TABLE extRecordings (
  ident numeric (20,0) constraint NOT NULL, 
  recording_time numeric(20,0) constraint NOT NULL, 
  agent_number cichar(4) constraint NOT NULL, 
  device_id cichar(4) constraint NOT NULL, 
  device_alias cichar(4) constraint NOT NULL, 
  filename cichar(100) constraint NOT NULL, 
  ani cichar(20), 
  dnis cichar(20), 
  user1 cichar(20), 
  user2 cichar(20), 
  duration integer constraint NOT NULL, 
  calldirection cichar(1) constraint NOT NULL, 
  audio_size numeric(20,0) constraint NOT NULL, 
  callid cichar(16) constraint NOT NULL,
  constraint pk primary key (ident)) IN database;
  
  
DROP TABLE tmpExtRecordings;
CREATE TABLE tmpExtRecordings ( 
      ident Numeric( 20 ,0 ),
      recording_time Numeric( 20 ,0 ),
      agent_number CIChar( 4 ),
      device_id CIChar( 4 ),
      device_alias CIChar( 4 ),
      filename CIChar( 100 ),
      ani CIChar( 20 ),
      dnis CIChar( 20 ),
      user1 CIChar( 20 ),
      user2 CIChar( 20 ),
      duration Integer,
      calldirection CIChar( 1 ),
      audio_size Numeric( 20 ,0 ),
      callid CIChar( 16 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpExtRecordings',
   'tmpExtRecordings.adi',
   'PK',
   'ident',
   '',
   2051,
   512,
   '' ); 



  
  
drop table callCopyRecordings;  
CREATE TABLE callCopyRecordings (
  ident numeric(20,0) constraint NOT NULL,
  theDate date constraint NOT NULL,
  extension cichar(4) constraint NOT NULL,
  filename cichar(100) constraint NOT NULL,
  guid cichar(15) constraint NOT NULL,
  constraint pk primary key (theDate,extension,guid)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'callCopyRecordings','callCopyRecordings.adi','theDate','theDate',
   '',2,512, '' );     
EXECUTE PROCEDURE sp_CreateIndex90( 
   'callCopyRecordings','callCopyRecordings.adi','extension','extension',
   '',2,512, '' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'callCopyRecordings','callCopyRecordings.adi','guid','guid',
   '',2,512, '' );         
EXECUTE PROCEDURE sp_CreateIndex90( 
   'callCopyRecordings','callCopyRecordings.adi','ident','ident',
   '',2051,512, '' );    