/*
sometimes 11 digits 17017575995, same number could include a +, same number could include 91
so, for ALL phone numbers

call.dialednumber, callerid

include a right(TRIM(),7) version of the phone number
4/28, delphi/ads bombing, think it IS bigint problem
  store AS strings OR numeric(20,0)
  go with numeric
*/ 
DROP TABLE extCall;
Create table extCall (
	id                                numeric(20,0),
	callID                            integer,
	sipCallId                         cichar(32),
	startTime                         timestamp,
	endTime                           timestamp,
	locked                            integer,
	extension                         cichar(16),
	durationSeconds                   integer,
	callType                          integer,
	workgroupCall                     integer,
	dialedNumber                      cichar(32),
  dialedNumberRight7                cichar(7),
	callerId                          cichar(32),
  callerIdRight7                    cichar(7),
  constraint PK primary key (id)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'extCall','extCall.adi','dialedNumber','dialedNumber',
   '',2,512, '' );     
EXECUTE PROCEDURE sp_CreateIndex90( 
   'extCall','extCall.adi','dialedNumberRight7','dialedNumberRight7',
   '',2,512, '' );    
EXECUTE PROCEDURE sp_CreateIndex90( 
   'extCall','extCall.adi','callerId','callerId',
   '',2,512, '' );     
EXECUTE PROCEDURE sp_CreateIndex90( 
   'extCall','extCall.adi','callerIdRight7','callerIdRight7',
   '',2,512, '' );     
EXECUTE PROCEDURE sp_CreateIndex90( 
   'extCall','extCall.adi','callType','callType',
   '',2,512, '' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'extCall','extCall.adi','startTime','startTime',
   '',2,512, '' );     
EXECUTE PROCEDURE sp_CreateIndex90( 
   'extCall','extCall.adi','endTime','endTime',
   '',2,512, '' );     

DROP TABLE extConnect;
Create table extConnect (
	id                                numeric(20,0),    
	partyType                         integer, 
	callTableID                       numeric(20,0), 
	lineID                            numeric(20,0), 
	switchID                          integer, 
	portNumber                        integer, 
	portId                            integer, 
	portName                          cichar(50),
	groupId                           integer, 
	groupName                         cichar(50),
	connectTime                       timestamp,
	disconnectTime                    timestamp,
	connectReason                     integer, 
	disconnectReason                  integer, 
	partyIdFlags                      integer, 
	partyID                           cichar(50),
	partyIdName                       cichar(50),
	partyIdLastName                   cichar(50),
	ctrlPartyIdFlags                  integer, 
	ctrlPartyId                       cichar(50),
	ctrlPartyIdName                   cichar(50),
	ctrlPartyIdLastName               cichar(50),
	mailboxId                         cichar(16),
	talkTimeSeconds                   integer,
	holdTimeSeconds                   integer,
	ringTimeSeconds                   integer,
	durationSeconds                   integer,
  constraint pk primary key (id)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'extConnect','extConnect.adi','callTableId','callTableId',
   '',2,512, '' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'extConnect','extConnect.adi','partyType','partyType',
   '',2,512, '' );    
EXECUTE PROCEDURE sp_CreateIndex90( 
   'extConnect','extConnect.adi','connectReason','connectReason',
   '',2,512, '' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'extConnect','extConnect.adi','disconnectReason','disconnectReason',
   '',2,512, '' );        
            
  
CREATE TABLE extCallType (
  callType integer,
  name cichar(15),
  description cichar(64)) IN database;  
  
CREATE TABLE extConnectReason (
  connectReason integer,
  name cichar(15),
  description cichar(64)) IN database;  
  
CREATE TABLE extDisconnectReason( 
  disconnectReason integer,
  name cichar(15),
  description cichar(64)) IN database;
  
CREATE TABLE extPartyIdFlag (
  partyIdFlag integer,
  name cichar(15),
  description cichar(64)) IN database;  
  
CREATE TABLE extPartyType (
  partyType integer,
  name cichar(15), 
  description cichar(64)) IN database;  
  
  
  
-- 4. COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'extconnect';
@i = (
  SELECT MAX(field_num)
  FROM system.columns
  WHERE parent = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT TRIM(name) + ', '  FROM system.columns WHERE field_num = @j AND parent = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;  

-- parameter list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'extconnect';
@i = (
  SELECT MAX(field_num)
  FROM system.columns
  WHERE parent = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT ':' + replace(TRIM(name), '#', '') + ', '  FROM system.columns WHERE field_num = @j AND parent = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;


-- for code, setting parameters 
DECLARE @table_name string;
@table_name = 'extConnect';
SELECT 'AdsQuery.ParamByName(' + '''' + replace(TRIM(name), '#', '') + '''' + ').' + 
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX'
  END +  ' := AdoQuery.FieldByName('+ '''' + TRIM(name) + '''' + ').' +
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX' 
  END + ';' 
FROM system.columns
WHERE parent = @table_name;
  
    