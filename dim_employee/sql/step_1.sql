﻿drop table if exists employees;
create temp table employees as
select employeekey,storecode,employeenumber,active,fullparttime,pydeptcode,pydept,distcode,distribution,
  name,birthdate,hiredate,termdate,currentrow,rowchangedate,rowfromts,rowthruts,rowchangereason,
  employeekeyfromdate,employeekeythrudate,payrollclasscode,payrollclass,salary,hourlyrate
  payperiodcode,payperiod,managername,lastname,firstname,middlename 
from ads.ext_dds_edwemployeedim  
where termdate > '08/01/2009'
order by employeenumber, employeekey;

-- multiple hire dates  
select *
from employees a
inner join (
  select employeenumber
  from (
    select employeenumber, hiredate
    from employees
    group by employeenumber, hiredate
    having count(*) > 1
    order by employeenumber) w
  group by employeenumber
  having count(*) > 1) b on a.employeenumber = b.employeenumber
order by a.employeenumber, employeekey  

-- all that have rehire/reuse
select *
from employees
where employeenumber in (
  select employeenumber
  from employees
  where rowchangereason = 'Rehire/Reuse')
order by employeenumber, employeekey 

drop table if exists pymast;
create temp table pymast as
select pymast_company_number,pymast_employee_number,active_code,department_code,
  employee_name,employee_first_name,employee_middle_name,employee_last_name,
  address_1,city_name,state_code_address_,zip_code,tel_area_code,telephone_number,
  soc_sec_number,drivers_license_,driver_lic_state_code,birth_date,hire_date,
  org_hire_date,last_raise_date,termination_date,sex,marital_status,payroll_class,
  pay_period,base_salary,base_hrly_rate,alt_salary,alt_hrly_rate,distrib_code
from arkona.ext_pymast;

select pymast_employee_number, count(*)
from (
select a.pymast_employee_number, b.employeenumber
from pymast a
inner join employees b on a.pymast_employee_number = b.employeenumber
) c group by pymast_employee_number

select *
from (
  select a.pymast_employee_number
  from pymast a
  inner join employees b on a.pymast_employee_number = b.employeenumber
  group by a.pymast_employee_number) h
full outer join (
  select employeenumber 
  from employees) i on h.pymast_employee_number = i.employeenumber
where pymast_employee_number is null

select *
from employees
where employeenumber = '132590'

select * from pymast where employee_last_name = 'stinar'


select * 
from employees
where lastname = 'stinar' and firstname = 'cory'











