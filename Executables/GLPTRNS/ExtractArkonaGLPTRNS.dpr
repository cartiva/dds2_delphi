program ExtractArkonaGLPTRNS;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  ActiveX,
  StrUtils,
  glptrnsDM in 'glptrnsDM.pas' {DataModule1: TDataModule};

resourcestring
  executable = 'ExtractArkonaGLPTRNS';
begin
  try
    try
      CoInitialize(nil);
      DM := TDataModule1.Create(nil);
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
        QuotedStr(executable) + ', ' +
        '''' + 'none' + '''' + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
      DM.ExtractGlpdtimNightly;
      DM.CountGLPTRNS;

// to be run manually ONLY
// supply the dates here, one string (ads), one integer (db2)
//DM.SingleDate('04/12/2013', 20130412);
(*)
      DM.ExtractGlptrnsInitial;
      DM.ExtractGlpdtimInitial;
      DM.ExceptionTest;
      DM.SingleDay; // connection to Arkona failed @ 1:05 AM on 5/6, as a result, ExtractArkonaGLPTRNS
                    // did not run, so stgArkonaGLPTRNS is missing all the 5/5 transactions
                    // that would have been generated.  Therefor, need a routine
                    // to do a single days worth of transactions
should be the same as Nightly, only the date is specified rather than generated
i'm leary of fucking with the overly intricate curload/prevload, it's so fucking time dependent
so, pull from ark for the date into a tmpSingleDay table
clean it up, delete from sgtArkonaGLPTRNS where date is the selected date
insert from tmp, zap temp
(**)

      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        '''' + 'none' + '''' + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          '''' + 'stgZapAndReload' + '''' + ', ' +
          '''' + 'no sql - line 136' + '''' + ', ' +
          QuotedStr(E.ClassName + ' ' + E.Message) + ')');

        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          '''' + 'none' + '''' + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.ClassName + ' ' + E.Message) + ')');

        DM.SendMail('GLPTRNS Failed.' + leftstr(E.Message, 8),   E.Message);
        exit;
      end;
    end;
//    DM.SendMail('GLPTRNS passed');
  finally
    FreeAndNil(DM);
  end;
end.
