unit glptrnsDM;
(*)
-   when the module is created, both connections get connected
      should i be checking for connected at the begin of each proc?
-   removed the components from the design surface.
-   error trapping
-   logging
-   refactored out Prepare/Close query, did one of each for Ads and Ado queries
-     how to generalize that for both?
-   took the .Close out of PrepareQuery, added optional sql: string parameter
-   wrapped GLPDTIM/GLPTRNS into one proc, glptrns depencs on glpdtim
-   added pass/fail email
-   where should query.connection be set? currently being set in ExecuteQuery and OpenQuery
--  added CloseQuery to ExecuteQuery
--  12/14 exception loop
(**)
interface

uses
    SysUtils, Classes, adsdata, adsfunc, adstable, adscnnct, DB, ADODB, DateUtils,
    IdSMTPBase, IdSMTP, IdMessage,
    Dialogs;

type
  TDataModule1 = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
// utilities
    procedure Log(job, routine: string; jobStartTS, jobEndTS: TDateTime);
    procedure PrepareQuery(query: TDataSet; sql: string='');
    procedure CloseQuery(query: TDataSet);
    function FormatWordToStr(const inVal: Word;
      const OutLength: Integer; const PadChar: Char = #48): String;
    function GetADSSqlTimeStamp(const DateTime: TDateTime): String;
    procedure OpenQuery(query: TDataSet; sql: string);
    procedure ExecuteQuery(query: TDataSet; sql: string);
    procedure curLoadPrevLoad(tableName: string);
    procedure SendMail(Subject: string; Body: string='');
    procedure ExceptionTest;
    procedure curLoadIsEmpty(tableName: string);
    procedure ScrapeCountQuery(db2Q, adsQ: string);
//
    procedure ExtractGlptrnsInitial;
    procedure ExtractGlpdtimInitial;
    procedure ExtractGlpdtimNightly;
    procedure CountGLPTRNS;
    procedure SingleDate(strDate: string; intDate: integer);
  end;

var
  DM: TDataModule1;
  AdoCon: TADOConnection;
  AdsCon: TADSConnection;
  AdoQuery: TAdoQuery;
  AdsQuery: TAdsQuery;
  LogQuery: TAdsQuery;
  stgErrorLogQuery: TAdsQuery;
  StartTS: TDateTime;
  EndTS: TDateTime;

resourcestring
  executable = 'ExtractArkonaGLPTRNS';

implementation

{$R *.dfm}

{ TDataModule1 }

constructor TDataModule1.Create(AOwner: TComponent);
begin
  AdoCon := TADOConnection.Create(nil);
  AdsCon := TADSConnection.Create(nil);
  AdoQuery := TADOQuery.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  LogQuery := TAdsQuery.Create(nil);
  stgErrorLogQuery := TAdsQuery.Create(nil);
  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
  AdsCon.ConnectPath := '\\67.135.158.12:6363\Advantage\DDS\DDS.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
  AdoCon.Connected := True;
  AdsCon.Connect;
  LogQuery.AdsConnection := AdsCon;
  stgErrorLogQuery.AdsConnection := AdsCon;
end;

destructor TDataModule1.Destroy;
begin
  AdoQuery.Close;
  FreeAndNil(AdoQuery);
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  LogQuery.Close;
  FreeAndNil(LogQuery);
  stgErrorLogQuery.Close;
  FreeAndNil(stgErrorLogQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  AdoCon.Connected := False;
  FreeAndNil(AdoCon);
  inherited;
end;

procedure TDataModule1.ExtractGlpdtimNightly;
var
  maxDate: integer;
  proc: string;
begin
  proc := 'ExtractGlpdtimNightly';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      curLoadIsEmpty('GLPDTIM');
      OpenQuery(AdsQuery, 'select max(gqdate) as maxDate from stgArkonaGLPDTIM');
      maxDate := AdsQuery.FieldByName('maxDate').AsInteger;
      CloseQuery(adsQuery);
      PrepareQuery(AdsQuery,'insert into curLoadGLPDTIM ' +
          '(gqco#, gqtrn#, gquser, gqdate, gqtime, gqfunc) values ' +
          '(:GQCO, :GQTRN, :GQUSER, :GQDATE, :GQTIME, :GQFUNC)');
      OpenQuery(AdoQuery, 'select GQCO#,GQTRN#,GQUSER,GQDATE,GQTIME,GQFUNC ' +
          'from rydedata.glpdtim where ' +
          'gqdate >= ' + IntToStr(maxDate) + ' and gqtime <> 0' + ' and gqtrn# <> 0');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('GQCO').AsString := AdoQuery.FieldByName('GQCO#').AsString;
        AdsQuery.ParamByName('GQTRN').AsInteger := AdoQuery.FieldByName('GQTRN#').AsInteger;
        AdsQuery.ParamByName('GQUSER').AsString := AdoQuery.FieldByName('GQUSER').AsString;
        AdsQuery.ParamByName('GQDATE').AsInteger := AdoQuery.FieldByName('GQDATE').AsInteger;
        AdsQuery.ParamByName('GQTIME').AsInteger := AdoQuery.FieldByName('GQTIME').AsInteger;
        AdsQuery.ParamByName('GQFUNC').AsString := AdoQuery.FieldByName('GQFUNC').AsString;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE stgGlpdtimAddTS()'); // populates the gqTS field
      ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE StgArkGLPDTIM()'); //  insert into stgArkonaGLPTIM
      curLoadPrevLoad('GLPDTIM'); //  config cur/prev
      try // GLPTRNS
        curLoadIsEmpty('GLPTRNS');
        OpenQuery(AdsQuery, 'select max(gqdate) as maxDate from stgArkonaGLPDTIM');
        maxDate := AdsQuery.FieldByName('maxDate').AsInteger;
        CloseQuery(adsQuery);
        PrepareQuery(AdsQuery, 'insert into curLoadGLPTRNS values' +
            '(:GTCO, :GTTRN, :GTSEQ, :GTDTYP, :GTTYPE, :GTPOST, :GTRSTS,' +
            ':GTADJUST, :GTPSEL, :GTJRNL, :GTDATE, :GTRDATE, :GTSDATE,' +
            ':GTACCT, :GTCTL, :GTDOC, :GTRDOC, :GTRDTYP, :GTODOC,' +
            ':GTREF, :GTVND, :GTDESC, :GTTAMT, :GTCOST, :GTCTLO, :GTOCO)');
        OpenQuery(AdoQuery, 'select GTCO#,GTTRN#,GTSEQ#,GTDTYP,GTTYPE,GTPOST, ' +
            'GTRSTS,GTADJUST,GTPSEL,GTJRNL,GTDATE,GTRDATE,GTSDATE,GTACCT, ' +
            'GTCTL#,GTDOC#,GTRDOC#,GTRDTYP,GTODOC#,GTREF#,GTVND#,GTDESC,GTTAMT,GTCOST, ' +
            'GTCTLO,GTOCO# from rydedata.glptrns where gtpost = ''Y''' +
            ' and gttrn# in (select gqtrn# from rydedata.glpdtim where gqtrn# <> 0' +
            ' and gqtime <> 0 and gqdate >= ' + IntToStr(maxDate) + ')');
        while not AdoQuery.eof do
        begin
          AdsQuery.ParamByName('GTCO').AsString := AdoQuery.FieldByName('GTCO#').AsString;
          AdsQuery.ParamByName('GTTRN').AsInteger := AdoQuery.FieldByName('GTTRN#').AsInteger;
          AdsQuery.ParamByName('GTSEQ').AsInteger := AdoQuery.FieldByName('GTSEQ#').AsInteger;
          AdsQuery.ParamByName('GTDTYP').AsString := AdoQuery.FieldByName('GTDTYP').AsString;
          AdsQuery.ParamByName('GTTYPE').AsString := AdoQuery.FieldByName('GTTYPE').AsString;
          AdsQuery.ParamByName('GTPOST').AsString := AdoQuery.FieldByName('GTPOST').AsString;
          AdsQuery.ParamByName('GTRSTS').AsString := AdoQuery.FieldByName('GTRSTS').AsString;
          AdsQuery.ParamByName('GTADJUST').AsString := AdoQuery.FieldByName('GTADJUST').AsString;
          AdsQuery.ParamByName('GTPSEL').AsString := AdoQuery.FieldByName('GTPSEL').AsString;
          AdsQuery.ParamByName('GTJRNL').AsString := AdoQuery.FieldByName('GTJRNL').AsString;
          AdsQuery.ParamByName('GTDATE').AsDateTime := AdoQuery.FieldByName('GTDATE').AsDateTime;
          AdsQuery.ParamByName('GTRDATE').AsDateTime := AdoQuery.FieldByName('GTRDATE').AsDateTime;
          AdsQuery.ParamByName('GTSDATE').AsDateTime := AdoQuery.FieldByName('GTSDATE').AsDateTime;
          AdsQuery.ParamByName('GTACCT').AsString := AdoQuery.FieldByName('GTACCT').AsString;
          AdsQuery.ParamByName('GTCTL').AsString := AdoQuery.FieldByName('GTCTL#').AsString;
          AdsQuery.ParamByName('GTDOC').AsString := AdoQuery.FieldByName('GTDOC#').AsString;
          AdsQuery.ParamByName('GTRDOC').AsString := AdoQuery.FieldByName('GTRDOC#').AsString;
          AdsQuery.ParamByName('GTRDTYP').AsString := AdoQuery.FieldByName('GTRDTYP').AsString;
          AdsQuery.ParamByName('GTODOC').AsString := AdoQuery.FieldByName('GTODOC#').AsString;
          AdsQuery.ParamByName('GTREF').AsString := AdoQuery.FieldByName('GTREF#').AsString;
          AdsQuery.ParamByName('GTVND').AsString := AdoQuery.FieldByName('GTVND#').AsString;
          AdsQuery.ParamByName('GTDESC').AsString := AdoQuery.FieldByName('GTDESC').AsString;
          AdsQuery.ParamByName('GTTAMT').AsCurrency := AdoQuery.FieldByName('GTTAMT').AsCurrency;
          AdsQuery.ParamByName('GTCOST').AsCurrency := AdoQuery.FieldByName('GTCOST').AsCurrency;
          AdsQuery.ParamByName('GTCTLO').AsString := AdoQuery.FieldByName('GTCTLO').AsString;
          AdsQuery.ParamByName('GTOCO').AsString := AdoQuery.FieldByName('GTOCO#').AsString;
          AdsQuery.ExecSQL;
          AdoQuery.Next;
        end;
        ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''curLoadGLPTRNS'')');
        ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE StgArkGLPTRNS()');
        curLoadPrevLoad('GLPTRNS');
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          'null' + ')');
//        SendMail('GLPTRNS passed');
      except
        on E: Exception do
        begin
          DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
            QuotedStr(E.Message) + ')');
          Raise Exception.Create('GLPTRNS.  MESSAGE: ' + E.Message);
        end;
      end;
    except
      on E: Exception do
      begin
        Raise Exception.Create('GLPDTIM.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdoQuery);
    CloseQuery(AdsQuery);
  end;
end;


procedure TDataModule1.ExtractGlpdtimInitial;
var
  i: integer;
  sql: string;

begin
  try
    i := 0;
    PrepareQuery(AdsQuery, 'insert into stgArkonGLDTIM ' +
        '(gqco#, gqtrn#, gquser, gqdate, gqtime, gqfunc) values ' +
        '(:GQCO, :GQTRN, :GQUSER, :GQDATE, :GQTIME, :GQFUNC)');
    OpenQuery(AdoQuery, 'Select * from rydedata.glpdtim where gqtime <> 0 and gqtrn# <> 0');
    while not AdoQuery.EOF do
    begin
      AdsQuery.ParamByName('GQCO').AsString := AdoQuery.FieldByName('GQCO#').AsString;
      AdsQuery.ParamByName('GQTRN').AsInteger := AdoQuery.FieldByName('GQTRN#').AsInteger;
      AdsQuery.ParamByName('GQUSER').AsString := AdoQuery.FieldByName('GQUSER').AsString;
      AdsQuery.ParamByName('GQDATE').AsInteger := AdoQuery.FieldByName('GQDATE').AsInteger;
      AdsQuery.ParamByName('GQTIME').AsInteger := AdoQuery.FieldByName('GQTIME').AsInteger;
      AdsQuery.ParamByName('GQFUNC').AsString := AdoQuery.FieldByName('GQFUNC').AsString;
      sql := AdsQuery.Text;
      AdsQuery.ExecSQL;
      i := i + 1;
      writeln(' record: ' + IntToStr(i));
      AdoQuery.Next;
    end;
    { TODO -ojon -cerror handling : either in the sp or before, need to valid date/time data before converting to a TS }
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE etlGlpdtimTSUpdate()');
  finally
    CloseQuery(AdoQuery);
    CloseQuery(AdsQuery);
  end;

end;


procedure TDataModule1.ExtractGlptrnsInitial;
var
  iMonth: integer;
  iYear: integer;
//  i: integer;
begin
  iMonth := 10;
  iYear := 2011;
//  i := 1;
  try
    AdoQuery.Connection := AdoCon;
    AdsQuery.AdsConnection := AdsCon;
    while imonth = 10 do
    begin
      AdoQuery.Close;
      AdoQuery.SQL.Clear;
//      AdoQuery.SQL.Add('Select * from rydedata.glptrns where gtpost = ''Y'' and year(gtdate) = 2009 and month(gtdate) = 9');
      AdoQuery.SQL.Add('Select * from rydedata.glptrns where gtpost = ''Y'' and year(gtdate) = ' + IntToStr(iYear) + ' and month(gtdate) = ' + IntToStr(iMonth));
      AdoQuery.Open;
      AdsQuery.Close;
      AdsQuery.SQL.Clear;
      AdsQuery.SQL.Add('insert into stgArkonaGlptrns values ');
      AdsQuery.SQL.Add('(:GTCO, :GTTRN, :GTSEQ, :GTDTYP, :GTTYPE, :GTPOST, :GTRSTS, ');
      AdsQuery.SQL.Add(':GTADJUST, :GTPSEL, :GTJRNL, :GTDATE, :GTRDATE, :GTSDATE, ');
      AdsQuery.SQL.Add(':GTACCT, :GTCTL, :GTDOC, :GTRDOC, :GTRDTYP, :GTODOC, ');
      AdsQuery.SQL.Add(':GTREF, :GTVND, :GTDESC, :GTTAMT, :GTCOST, :GTCTLO, :GTOCO)');
      while not AdoQuery.EOF do
      begin
        AdsQuery.ParamByName('GTCO').AsString := AdoQuery.FieldByName('GTCO#').AsString;
        AdsQuery.ParamByName('GTTRN').AsInteger := AdoQuery.FieldByName('GTTRN#').AsInteger;
        AdsQuery.ParamByName('GTSEQ').AsInteger := AdoQuery.FieldByName('GTSEQ#').AsInteger;
        AdsQuery.ParamByName('GTDTYP').AsString := AdoQuery.FieldByName('GTDTYP').AsString;
        AdsQuery.ParamByName('GTTYPE').AsString := AdoQuery.FieldByName('GTTYPE').AsString;
        AdsQuery.ParamByName('GTPOST').AsString := AdoQuery.FieldByName('GTPOST').AsString;
        AdsQuery.ParamByName('GTRSTS').AsString := AdoQuery.FieldByName('GTRSTS').AsString;
        AdsQuery.ParamByName('GTADJUST').AsString := AdoQuery.FieldByName('GTADJUST').AsString;
        AdsQuery.ParamByName('GTPSEL').AsString := AdoQuery.FieldByName('GTPSEL').AsString;
        AdsQuery.ParamByName('GTJRNL').AsString := AdoQuery.FieldByName('GTJRNL').AsString;
        AdsQuery.ParamByName('GTDATE').AsDateTime := AdoQuery.FieldByName('GTDATE').AsDateTime;
        AdsQuery.ParamByName('GTRDATE').AsDateTime := AdoQuery.FieldByName('GTRDATE').AsDateTime;
        AdsQuery.ParamByName('GTSDATE').AsDateTime := AdoQuery.FieldByName('GTSDATE').AsDateTime;
        AdsQuery.ParamByName('GTACCT').AsString := AdoQuery.FieldByName('GTACCT').AsString;
        AdsQuery.ParamByName('GTCTL').AsString := AdoQuery.FieldByName('GTCTL#').AsString;
        AdsQuery.ParamByName('GTDOC').AsString := AdoQuery.FieldByName('GTDOC#').AsString;
        AdsQuery.ParamByName('GTRDOC').AsString := AdoQuery.FieldByName('GTRDOC#').AsString;
        AdsQuery.ParamByName('GTRDTYP').AsString := AdoQuery.FieldByName('GTRDTYP').AsString;
        AdsQuery.ParamByName('GTODOC').AsString := AdoQuery.FieldByName('GTODOC#').AsString;
        AdsQuery.ParamByName('GTREF').AsString := AdoQuery.FieldByName('GTREF#').AsString;
        AdsQuery.ParamByName('GTVND').AsString := AdoQuery.FieldByName('GTVND#').AsString;
        AdsQuery.ParamByName('GTDESC').AsString := AdoQuery.FieldByName('GTDESC').AsString;
        AdsQuery.ParamByName('GTTAMT').AsCurrency := AdoQuery.FieldByName('GTTAMT').AsCurrency;
        AdsQuery.ParamByName('GTCOST').AsCurrency := AdoQuery.FieldByName('GTCOST').AsCurrency;
        AdsQuery.ParamByName('GTCTLO').AsString := AdoQuery.FieldByName('GTCTLO').AsString;
        AdsQuery.ParamByName('GTOCO').AsString := AdoQuery.FieldByName('GTOCO#').AsString;
        AdsQuery.ExecSQL;
//        i := i + 1;
//        writeln(IntToStr(imonth) + '/' + IntToStr(iYear) + ' record: ' + IntToStr(i));
        AdoQuery.Next;
      end;
      imonth := imonth + 1;
//      i := 1;
    end; // while imonth
  finally

  end;
end;


procedure TDataModule1.CountGLPTRNS;
var
  countMonth: integer;
  countYear: integer;
  db2Count: integer;
  adsCount: integer;
  proc: string;
begin
  proc := 'CountGLPTRNS';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      OpenQuery(AdoQuery, 'select month(current date - 1 month) as countMonth, ' +
          'year(current date - 1 month) as countYear from sysibm.sysdummy1');
      countMonth := AdoQuery.FieldByName('countMonth').AsInteger;
      countYear := AdoQuery.FieldByName('countYear').AsInteger;
      CloseQuery(AdoQuery);
      OpenQuery(AdoQuery, 'select count(gttrn#) as db2Count from rydedata.glptrns ' +
          'where gtpost = ''Y'' and year(gtdate) = ' + IntToStr(countYear) +
          ' and month(gtdate) = ' + IntToStr(countMonth));
      db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
      CloseQuery(AdoQuery);
      OpenQuery(AdsQuery, 'SELECT COUNT(*) as adsCount FROM stgArkonaGLPTRNS ' +
          'WHERE year(gtdate) = ' + IntToStr(countYear) +
          ' and month(gtdate) = ' + IntToStr(countMonth));
      adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
      closeQuery(AdsQuery);
      if db2Count <> adsCount then
      begin
        ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''tmpGLPTRNS'')');
        CloseQuery(AdsQuery);
        PrepareQuery(AdsQuery, 'insert into tmpGlptrns values ' +
            '(:GTCO, :GTTRN, :GTSEQ, :GTDTYP, :GTTYPE, :GTPOST, :GTRSTS, ' +
            ':GTADJUST, :GTPSEL, :GTJRNL, :GTDATE, :GTRDATE, :GTSDATE, ' +
            ':GTACCT, :GTCTL, :GTDOC, :GTRDOC, :GTRDTYP, :GTODOC, ' +
            ':GTREF, :GTVND, :GTDESC, :GTTAMT, :GTCOST, :GTCTLO, :GTOCO)');
        OpenQuery(AdoQuery,'Select * from rydedata.glptrns where gtpost = ''Y'' ' +
            'and year(gtdate) = ' + IntToStr(countYear) + ' and month(gtdate) = ' + IntToStr(countMonth));
        while not AdoQuery.EOF do
        begin
          AdsQuery.ParamByName('GTCO').AsString := trim(AdoQuery.FieldByName('GTCO#').AsString);
          AdsQuery.ParamByName('GTTRN').AsInteger := AdoQuery.FieldByName('GTTRN#').AsInteger;
          AdsQuery.ParamByName('GTSEQ').AsInteger := AdoQuery.FieldByName('GTSEQ#').AsInteger;
          AdsQuery.ParamByName('GTDTYP').AsString := trim(AdoQuery.FieldByName('GTDTYP').AsString);
          AdsQuery.ParamByName('GTTYPE').AsString := trim(AdoQuery.FieldByName('GTTYPE').AsString);
          AdsQuery.ParamByName('GTPOST').AsString := trim(AdoQuery.FieldByName('GTPOST').AsString);
          AdsQuery.ParamByName('GTRSTS').AsString := trim(AdoQuery.FieldByName('GTRSTS').AsString);
          AdsQuery.ParamByName('GTADJUST').AsString := trim(AdoQuery.FieldByName('GTADJUST').AsString);
          AdsQuery.ParamByName('GTPSEL').AsString := trim(AdoQuery.FieldByName('GTPSEL').AsString);
          AdsQuery.ParamByName('GTJRNL').AsString := trim(AdoQuery.FieldByName('GTJRNL').AsString);
          AdsQuery.ParamByName('GTDATE').AsDateTime := AdoQuery.FieldByName('GTDATE').AsDateTime;
          AdsQuery.ParamByName('GTRDATE').AsDateTime := AdoQuery.FieldByName('GTRDATE').AsDateTime;
          AdsQuery.ParamByName('GTSDATE').AsDateTime := AdoQuery.FieldByName('GTSDATE').AsDateTime;
          AdsQuery.ParamByName('GTACCT').AsString := trim(AdoQuery.FieldByName('GTACCT').AsString);
          AdsQuery.ParamByName('GTCTL').AsString := trim(AdoQuery.FieldByName('GTCTL#').AsString);
          AdsQuery.ParamByName('GTDOC').AsString := trim(AdoQuery.FieldByName('GTDOC#').AsString);
          AdsQuery.ParamByName('GTRDOC').AsString := trim(AdoQuery.FieldByName('GTRDOC#').AsString);
          AdsQuery.ParamByName('GTRDTYP').AsString := trim(AdoQuery.FieldByName('GTRDTYP').AsString);
          AdsQuery.ParamByName('GTODOC').AsString := trim(AdoQuery.FieldByName('GTODOC#').AsString);
          AdsQuery.ParamByName('GTREF').AsString := trim(AdoQuery.FieldByName('GTREF#').AsString);
          AdsQuery.ParamByName('GTVND').AsString := trim(AdoQuery.FieldByName('GTVND#').AsString);
          AdsQuery.ParamByName('GTDESC').AsString := trim(AdoQuery.FieldByName('GTDESC').AsString);
          AdsQuery.ParamByName('GTTAMT').AsCurrency := AdoQuery.FieldByName('GTTAMT').AsCurrency;
          AdsQuery.ParamByName('GTCOST').AsCurrency := AdoQuery.FieldByName('GTCOST').AsCurrency;
          AdsQuery.ParamByName('GTCTLO').AsString := trim(AdoQuery.FieldByName('GTCTLO').AsString);
          AdsQuery.ParamByName('GTOCO').AsString := trim(AdoQuery.FieldByName('GTOCO#').AsString);
          AdsQuery.ExecSQL;
          AdoQuery.Next;
        end;
        ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''tmpGLPTRNS'')');
        ExecuteQuery(AdsQuery, 'execute procedure stgArkCountGLPTRNS(' + IntToStr(countYear)  + ', ' + IntToStr(countMonth) + ')');
        sendMail('GLPTRNS: reloaded ' + IntToStr(countMonth) + ' / ' + IntToStr(countYear), '');
      end;
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('CountGLPTRNS.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;


procedure TDataModule1.SingleDate(strDate: string; intDate: integer);
var
  proc: string;
begin
  proc := 'Singledate';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
    try
      try // GLPTRNS
        ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''tmpSingleDate'')');
        PrepareQuery(AdsQuery, 'insert into tmpSingleDate values' +
            '(:GTCO, :GTTRN, :GTSEQ, :GTDTYP, :GTTYPE, :GTPOST, :GTRSTS,' +
            ':GTADJUST, :GTPSEL, :GTJRNL, :GTDATE, :GTRDATE, :GTSDATE,' +
            ':GTACCT, :GTCTL, :GTDOC, :GTRDOC, :GTRDTYP, :GTODOC,' +
            ':GTREF, :GTVND, :GTDESC, :GTTAMT, :GTCOST, :GTCTLO, :GTOCO)');
        OpenQuery(AdoQuery, 'select * from rydedata.glptrns where gtpost = ''Y''' +
            ' and gttrn# in (select gqtrn# from rydedata.glpdtim where gqtrn# <> 0' +
            ' and gqtime <> 0 and gqdate = ' + IntToStr(intDate) + ')');

        while not AdoQuery.eof do
        begin
          AdsQuery.ParamByName('GTCO').AsString := AdoQuery.FieldByName('GTCO#').AsString;
          AdsQuery.ParamByName('GTTRN').AsInteger := AdoQuery.FieldByName('GTTRN#').AsInteger;
          AdsQuery.ParamByName('GTSEQ').AsInteger := AdoQuery.FieldByName('GTSEQ#').AsInteger;
          AdsQuery.ParamByName('GTDTYP').AsString := AdoQuery.FieldByName('GTDTYP').AsString;
          AdsQuery.ParamByName('GTTYPE').AsString := AdoQuery.FieldByName('GTTYPE').AsString;
          AdsQuery.ParamByName('GTPOST').AsString := AdoQuery.FieldByName('GTPOST').AsString;
          AdsQuery.ParamByName('GTRSTS').AsString := AdoQuery.FieldByName('GTRSTS').AsString;
          AdsQuery.ParamByName('GTADJUST').AsString := AdoQuery.FieldByName('GTADJUST').AsString;
          AdsQuery.ParamByName('GTPSEL').AsString := AdoQuery.FieldByName('GTPSEL').AsString;
          AdsQuery.ParamByName('GTJRNL').AsString := AdoQuery.FieldByName('GTJRNL').AsString;
          AdsQuery.ParamByName('GTDATE').AsDateTime := AdoQuery.FieldByName('GTDATE').AsDateTime;
          AdsQuery.ParamByName('GTRDATE').AsDateTime := AdoQuery.FieldByName('GTRDATE').AsDateTime;
          AdsQuery.ParamByName('GTSDATE').AsDateTime := AdoQuery.FieldByName('GTSDATE').AsDateTime;
          AdsQuery.ParamByName('GTACCT').AsString := AdoQuery.FieldByName('GTACCT').AsString;
          AdsQuery.ParamByName('GTCTL').AsString := AdoQuery.FieldByName('GTCTL#').AsString;
          AdsQuery.ParamByName('GTDOC').AsString := AdoQuery.FieldByName('GTDOC#').AsString;
          AdsQuery.ParamByName('GTRDOC').AsString := AdoQuery.FieldByName('GTRDOC#').AsString;
          AdsQuery.ParamByName('GTRDTYP').AsString := AdoQuery.FieldByName('GTRDTYP').AsString;
          AdsQuery.ParamByName('GTODOC').AsString := AdoQuery.FieldByName('GTODOC#').AsString;
          AdsQuery.ParamByName('GTREF').AsString := AdoQuery.FieldByName('GTREF#').AsString;
          AdsQuery.ParamByName('GTVND').AsString := AdoQuery.FieldByName('GTVND#').AsString;
          AdsQuery.ParamByName('GTDESC').AsString := AdoQuery.FieldByName('GTDESC').AsString;
          AdsQuery.ParamByName('GTTAMT').AsCurrency := AdoQuery.FieldByName('GTTAMT').AsCurrency;
          AdsQuery.ParamByName('GTCOST').AsCurrency := AdoQuery.FieldByName('GTCOST').AsCurrency;
          AdsQuery.ParamByName('GTCTLO').AsString := AdoQuery.FieldByName('GTCTLO').AsString;
          AdsQuery.ParamByName('GTOCO').AsString := AdoQuery.FieldByName('GTOCO#').AsString;
          AdsQuery.ExecSQL;
          AdoQuery.Next;
        end;
        ScrapeCountQuery(' rydedata.glptrns ' +
            'where gtpost = ''Y''' +
            ' and gttrn# in (select gqtrn# from rydedata.glpdtim where gqtrn# <> 0' +
            ' and gqtime <> 0 and gqdate = ' + IntToStr(intDate) + ')',
            ' tmpSingleDate');
        ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''tmpSingleDate'')');
        ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE StgArkSingleDate(' + QuotedStr(strDate) + ')');
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          'null' + ')');
//        SendMail('GLPTRNS passed');
      except
        on E: Exception do
        begin
          DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
            QuotedStr(executable) + ', ' +
            QuotedStr(proc) + ', ' +
            QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
            QuotedStr(E.Message) + ')');
          Raise Exception.Create('GLPTRNS.  MESSAGE: ' + E.Message);
        end;
      end;
  finally
    CloseQuery(AdoQuery);
    CloseQuery(AdsQuery);
  end;
end;


function TDataModule1.GetADSSqlTimeStamp(const DateTime: TDateTime): String;
var
  ye, mo, da, ho, mi, se, ms: Word;
begin
  DecodeDateTime(DateTime, ye, mo, da, ho, mi, se, ms);
  Result := FormatWordToStr(ye, 4) + '-' +
            FormatWordToStr(mo, 2) + '-' +
            FormatWordToStr(da, 2) + ' ' +
            FormatWordToStr(ho, 2) + ':' +
            FormatWordToStr(mi, 2) + ':' +
            FormatWordToStr(se, 2);
end;

function TDataModule1.FormatWordToStr(const inVal: Word;
  const OutLength: Integer; const PadChar: Char): String;
begin
  Result := IntToStr(inVal);
  if Length(Result) < OutLength then
    Result := StringOfChar(PadChar, OutLength - Length(Result)) + Result;
end;

procedure TDataModule1.curLoadIsEmpty(tableName: string);
var
  curLoadCount: Integer;
begin
  OpenQuery(AdsQuery, 'select count(*) as CurLoad from curLoad' + tableName);
  curLoadCount := AdsQuery.FieldByName('CurLoad').AsInteger;
  CloseQuery(AdsQuery);
  if curLoadCount <> 0 then
    SendMail(tableName + ' failed curLoadIsEmpty', '');
  Assert(curLoadCount = 0, 'curLoad' + tableName + ' not empty');
end;

procedure TDataModule1.Log(job, routine: string; jobStartTS, jobEndTS: TDateTime);
var
  sql: string;
begin
  PrepareQuery(LogQuery);
  LogQuery.SQL.Add('execute procedure etlJobLogAuditInsert(' + QuotedStr(job) + ', ' +
    QuotedStr(routine) + ', ' +  QuotedStr(GetADSSqlTimeStamp(jobStartTS)) + ', ' +
    QuotedStr(GetADSSqlTimeStamp(jobEndTS)) + ')');
  sql := LogQuery.SQL.Text;
  LogQuery.ExecSQL;
end;

procedure TDataModule1.curLoadPrevLoad(tableName: string);
{ TODO -ojon -crefactor : is this the best way to do this? need these holding tables to be kept to a minimum size,
  don't accumlate deleted records
  12/6/11 appears to be working ok }
begin
  try
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_ZapTable(' + '''' + 'prevLoad' + tableName + '''' + ')');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'prevLoad' + tableName + '''' + ', ' + '''' + 'prevLoad' + tableName + 'x' + '''' +  ', 1, 0)');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'curLoad' + tableName + '''' + ', ' + '''' + 'prevLoad' + tableName + '''' + ', 1, 0)');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'prevLoad' + tableName + 'x' + '''' + ', ' + '''' + 'curLoad' + tableName + '''' + ', 1, 0)');
  except
    on E: Exception do
    begin
      Raise Exception.Create('curLoadPrevLoad.  MESSAGE: ' + E.Message);
    end;
  end;
end;

procedure TDataModule1.ExecuteQuery(query: TDataSet; sql: string);
begin
//  try
    if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
    begin
      AdsQuery.AdsConnection := AdsCon;
      CloseQuery(AdsQuery);
      AdsQuery.SQL.Text := sql;
      AdsQuery.ExecSQL;
      CloseQuery(AdsQuery);
    end
    else if query.ClassParent.ClassName = 'TCustomADODataSet' then
    begin
      AdoQuery.Connection := AdoCon;
      CloseQuery(AdoQuery);
      AdoQuery.SQL.Text := sql;
      AdoQuery.ExecSQL;
      CloseQuery(AdoQuery);
    end;
//  except
//    on E: Exception do
//    begin
//      SendMail('ExecuteQuery failed - no stgErrorLog record entered');
//      exit;
//    end;
//  end;
end;

procedure TDataModule1.OpenQuery(query: TDataSet; sql: string);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
    AdsQuery.SQL.Text := sql;
    AdsQuery.Open;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
    AdoQuery.SQL.Text := sql;
    AdoQuery.Open;
  end;
end;

procedure TDataModule1.PrepareQuery(query: TDataSet; sql: string);
begin
//  if query = 'TAdsExtendedDataSet' then
  if query.classparent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
{ TODO -ojon -cfigure out : don't remember why i commented out close - figure it out and be consistent }
//    AdsQuery.Close;
    if sql <> '' then
      AdsQuery.SQL.Text := sql;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    if sql <> '' then
      AdoQuery.SQL.Text := sql;
  end;
end;

procedure TDataModule1.ScrapeCountQuery(db2Q, adsQ: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Q);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsQ);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckQuery between ' + db2Q + '(' + IntToStr(db2Count) + ') and ' + adsQ + '(' + IntToStr(adsCount) + ')  failed');
end;

procedure TDataModule1.SendMail(Subject: string; Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
        ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            '''' + 'send mail failed' + '''' + ', ' +
            '''' + 'no sql - line 427' + '''' + ', ' +
            QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        exit;
      end;
    end;
  finally
//    Msg.Free;
    FreeAndNil(Msg);
//    SMTP.Free;
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;

procedure TDataModule1.CloseQuery(query: TDataSet);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
  end;
end;

procedure TDataModule1.ExceptionTest;
begin
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaPYMAST'')');
      SendMail('wtf1');
    except
      on E: Exception do
      begin
      AdsQuery.AdsConnection := AdsCon;
      CloseQuery(AdsQuery);
      AdsQuery.SQL.Text := 'execute procedure stgErrorLogInsert (' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            '''' + 'PYMAST' + '''' + ', ' +
            '''' + 'no sql - line 136' + '''' + ', ' +
            QuotedStr(E.ClassName + ' ' + E.Message) + ')';
      AdsQuery.ExecSQL;
      CloseQuery(AdsQuery);
        SendMail('ExceptionText Failed');
        exit;
      end;
    end;
    SendMail('wtf2');
  finally
    CloseQuery(AdsQuery);
  end;
end;


end.

