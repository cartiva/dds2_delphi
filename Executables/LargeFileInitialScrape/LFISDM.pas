unit LFISDM;

interface

uses
  SysUtils, Classes, adsdata, adsfunc, adstable, adscnnct, DB, ADODB, DateUtils,
  IdSMTPBase, IdSMTP, IdMessage,
  Dialogs;

type
  TDM = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Log(job, routine: string; jobStartTS, jobEndTS: TDateTime);
    procedure PrepareQuery(query: TDataSet; sql: string='');
    procedure CloseQuery(query: TDataSet);
    function FormatWordToStr(const inVal: Word;
      const OutLength: Integer; const PadChar: Char = #48): String;
    function GetADSSqlTimeStamp(const DateTime: TDateTime): String;
    procedure OpenQuery(query: TDataSet; sql: string);
    procedure ExecuteQuery(query: TDataSet; sql: string);
    procedure curLoadPrevLoad(tableName: string);
    procedure curLoadIsEmpty(tableName: string);
    procedure SendMail(Subject: string; Body: string='');
    procedure ScrapeCountTable(db2Table, adsTable: string);
    procedure ScrapeCountQuery(db2Q, adsQ: string);
    procedure SDPRDETInitial;
    procedure SDPRHDRInitial;
    procedure PDPTDETInitial;
    procedure SDPRTXTInitial;
  end;

var
  DM: TDM;
  AdoCon: TADOConnection;
  AdsCon: TADSConnection;
  AdoQuery: TAdoQuery;
  AdsQuery: TAdsQuery;
  LogQuery: TAdsQuery;
  stgErrorLogQuery: TAdsQuery;
  StartTS: TDateTime;
  EndTS: TDateTime;

implementation

{$R *.dfm}
constructor TDM.Create(AOwner: TComponent);
begin
  AdoCon := TADOConnection.Create(nil);
  AdsCon := TADSConnection.Create(nil);
  AdoQuery := TADOQuery.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  LogQuery := TAdsQuery.Create(nil);
  stgErrorLogQuery := TAdsQuery.Create(nil);
//  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
//    AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=odbc0210;Persist Security Info=True;User ID=rydeodbc;Data Source=ArkonaSSL';
//  AdsCon.ConnectPath := '\\jon520:6363\Advantage\DDS\DDS.add';
  AdsCon.ConnectPath := '\\67.135.158.12:6363\Advantage\DDS\DDS.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
  AdoCon.Connected := True;
  AdsCon.Connect;
  LogQuery.AdsConnection := AdsCon;
  stgErrorLogQuery.AdsConnection := AdsCon;
end;

destructor TDM.Destroy;
begin
  AdoQuery.Close;
  FreeAndNil(AdoQuery);
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  LogQuery.Close;
  FreeAndNil(LogQuery);
  stgErrorLogQuery.Close;
  FreeAndNil(stgErrorLogQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  AdoCon.Connected := False;
  FreeAndNil(AdoCon);
  inherited;
end;

procedure TDM.SDPRDETInitial;
var
  groupQuery: TAdoQuery;
  roGroup: string;
  i: integer;
begin
// shit don't need month, simply loop thru all months for each year
//  monthYearQuery := TAdoQuery.Create(nil);
  groupQuery := TAdoQuery.Create(nil);
  try
    try
(*)
      monthYearQuery.Connection := AdoCon;
      monthYearQuery.SQL.Text := 'select left(digits(min(ptdate)),4) as minYear, ' +
         'left(digits(max(ptdate)),4) as maxYear ' +
         'from RYDEDATA.SDPRDET where ptco# in (''RY1'', ''RY2'',''RY3'') ' +
         'and ptro# <> '''' and ptdate <> 0';
      monthYearQuery.Open;
      Assert(monthYearQuery.RecordCount = 1, 'SDPRDETInitial: monthYearQuery returns more than one record');
      minYear := monthYearQuery.FieldByName('MINYEAR').AsInteger;
      maxYear := monthYearQuery.FieldByName('MAXYEAR').AsInteger;
      monthYearQuery.Close;

      iYear := minYear;
      while iYear <= maxYear do
      begin
        iMonth := 1;
        while iMonth < 13 do
(**)
        begin
          PrepareQuery(AdsQuery, 'insert into stgArkonaSDPRDET values(' +
              ':PTCO, :PTRO, :PTLINE, :PTLTYP, :PTSEQ, :PTCODE, :PTDATE, :PTLPYM, ' +
              ':PTSVCTYP, :PTPADJ, :PTTECH, :PTLOPC, :PTCRLO, :PTLHRS, :PTLAMT, ' +
              ':PTCOST, :PTARTF, :PTFAIL, :PTSLV, :PTSLI, :PTDCPN, :PTDBAS, ' +
              ':PTVATCODE, :PTVATAMT, :PTDISPTY1, :PTDISPTY2, :PTDISRANK1, ' +
              ':PTDISRANK2)');
          groupQuery.Connection := AdoCon;
//          groupQuery.SQL.Text := 'select left(trim(ptro#), 4)as RO from rydedata.sdprdet ' +
//          'where ptco# in (''RY1'',''RY2'',''RY3'') and trim(ptro#) <> '''' and ptdbas <> ''V'' group by left(trim(ptro#), 4)';
// 6/16/2012, redoing initial scrape, exclude pre conversion
          groupQuery.SQL.Text :=
            'select left(trim(ptro#), 4) as RO ' +
            'from rydedata.sdprdet '  +
            'where ptco# in (''RY1'',''RY2'',''RY3'') ' +
            '  and ptdbas <> ''V'' ' +
            '  and length(trim(ptro#)) > 6 ' +
            '  and left(trim(ptro#), 4) not in (''1000'',''1303'',''1545'',''4758'',''4760'',''4762'') ' +
            'group by left(trim(ptro#), 4)';

          groupQuery.Open;
          i := 1;
          while not groupQuery.Eof do
          begin
            roGroup := groupQuery.FieldByName('RO').AsString;
            assert(length(trim(roGroup)) = 4, 'roGroup error');
            writeln('roGroup: ' + roGroup + ' ' + IntToStr(i) + ' of 86');
            i := i + 1;
            OpenQuery(AdoQuery, 'select PTCO#, PTRO#, PTLINE, PTLTYP, PTSEQ#, PTCODE, ' +
                ' case ' +
                '   when ptdate = 0 then cast(''9999-12-31'' as date) ' +
                ' else ' +
                '   cast(left(digits(ptdate), 4) || ''-'' || substr(digits(ptdate), 5, 2) || ''-'' || substr(digits(ptdate), 7, 2) as date) ' +
                ' end as PTDATE, ' +
                ' PTLPYM, PTSVCTYP, PTPADJ, PTTECH, PTLOPC, PTCRLO, PTLHRS, PTLAMT, PTCOST, PTARTF, ' +
                ' PTFAIL, PTSLV#, PTSLI#, PTDCPN, PTDBAS, PTVATCODE, PTVATAMT, PTDISPTY1, PTDISPTY2, ' +
                ' PTDISRANK1, PTDISRANK2 ' +
                ' from RYDEDATA.SDPRDET ' +
                ' where ptco# in (''RY1'', ''RY2'',''RY3'') and ptdbas <> ''V'' and ptro# <> ''''' +
                ' and length(trim(ptro#)) > 6 ' + // exclude pre conversion
                ' and left(trim(ptro#), 4) = ' + roGroup);
            while not AdoQuery.eof do
            begin
              AdsQuery.ParamByName('ptco').AsString := AdoQuery.FieldByName('ptco#').AsString;
              AdsQuery.ParamByName('ptro').AsString := AdoQuery.FieldByName('ptro#').AsString;
              AdsQuery.ParamByName('ptline').AsInteger := AdoQuery.FieldByName('ptline').AsInteger;
              AdsQuery.ParamByName('ptltyp').AsString := AdoQuery.FieldByName('ptltyp').AsString;
              AdsQuery.ParamByName('ptseq').AsInteger := AdoQuery.FieldByName('ptseq#').AsInteger;
              AdsQuery.ParamByName('ptcode').AsString := AdoQuery.FieldByName('ptcode').AsString;
              if  AdoQuery.FieldByName('ptdate').AsString = '12/31/9999' then
                AdsQuery.ParamByName('ptdate').Clear
              else
                AdsQuery.ParamByName('ptdate').AsDateTime := AdoQuery.FieldByName('ptdate').AsDateTime;
              AdsQuery.ParamByName('ptlpym').AsString := AdoQuery.FieldByName('ptlpym').AsString;
              AdsQuery.ParamByName('ptsvctyp').AsString := AdoQuery.FieldByName('ptsvctyp').AsString;
              AdsQuery.ParamByName('ptpadj').AsString := AdoQuery.FieldByName('ptpadj').AsString;
              AdsQuery.ParamByName('pttech').AsString := AdoQuery.FieldByName('pttech').AsString;
              AdsQuery.ParamByName('ptlopc').AsString := AdoQuery.FieldByName('ptlopc').AsString;
              AdsQuery.ParamByName('ptcrlo').AsString := AdoQuery.FieldByName('ptcrlo').AsString;
              AdsQuery.ParamByName('ptlhrs').AsFloat := AdoQuery.FieldByName('ptlhrs').AsFloat;
              AdsQuery.ParamByName('ptlamt').AsCurrency := AdoQuery.FieldByName('ptlamt').AsCurrency;
              AdsQuery.ParamByName('ptcost').AsCurrency := AdoQuery.FieldByName('ptcost').AsCurrency;
              AdsQuery.ParamByName('ptartf').AsString := AdoQuery.FieldByName('ptartf').AsString;
              AdsQuery.ParamByName('ptfail').AsString := AdoQuery.FieldByName('ptfail').AsString;
              AdsQuery.ParamByName('ptslv').AsString := AdoQuery.FieldByName('ptslv#').AsString;
              AdsQuery.ParamByName('ptsli').AsString := AdoQuery.FieldByName('ptsli#').AsString;
              AdsQuery.ParamByName('ptdcpn').AsInteger := AdoQuery.FieldByName('ptdcpn').AsInteger;
              AdsQuery.ParamByName('ptdbas').AsString := AdoQuery.FieldByName('ptdbas').AsString;
              AdsQuery.ParamByName('ptvatcode').AsString := AdoQuery.FieldByName('ptvatcode').AsString;
              AdsQuery.ParamByName('ptvatamt').AsCurrency := AdoQuery.FieldByName('ptvatamt').AsCurrency;
              AdsQuery.ParamByName('ptdispty1').AsString := AdoQuery.FieldByName('ptdispty1').AsString;
              AdsQuery.ParamByName('ptdispty2').AsString := AdoQuery.FieldByName('ptdispty2').AsString;
              AdsQuery.ParamByName('ptdisrank1').AsInteger := AdoQuery.FieldByName('ptdisrank1').AsInteger;
              AdsQuery.ParamByName('ptdisrank2').AsInteger := AdoQuery.FieldByName('ptdisrank2').AsInteger;
              AdsQuery.ExecSQL;
              AdoQuery.Next;
            end; //while not AdoQuery.eof do
            groupQuery.Next;
          end; //while not groupQuery.Eof do
        end;
        ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaSDPRDET'')');
    except
      on E: Exception do
      begin
        ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            '''' + 'SDPRDET' + '''' + ', ' +
            '''' + 'no sql - line 231' + '''' + ', ' +
            QuotedStr(E.ClassName + ' ' + E.Message) + ')');
//        SendMail('SDPRDET Failed');
        exit;
      end;
    end;
  finally
    CloseQuery(groupQuery);
    FreeAndNil(groupQuery);
//    groupQuery.Free;
  end;

end;

procedure TDM.SDPRHDRInitial;
var
  groupQuery: TAdoQuery;
  roGroup: string;
  i: integer;
begin
  groupQuery := TAdoQuery.Create(nil);
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaSDPRHDR'')');
      PrepareQuery(AdsQuery, 'insert into stgArkonaSDPRHDR (' +
          'PTCO#, PTRO#, PTRTYP, PTWRO#, PTSWID, PTRTCH, PTCKEY, PTCNAM, ' +
          'PTARCK, PTPMTH, PTDATE, PTCDAT, PTFCDT, PTVIN, PTTEST, PTSVCO, ' +
          'PTSVCO2, PTDEDA, PTDEDA2, PTWDED, PTFRAN, PTODOM, PTMILO, PTCHK#, ' +
          'PTPO#, PTREC#, PTPTOT, PTLTOT, PTSTOT, PTDEDP, PTSVCT, PTSPOD, ' +
          'PTCPHZ, PTCPST, PTCPST2, PTCPST3, PTCPST4, PTWAST, PTWAST2, ' +
          'PTWAST3, PTWAST4, PTINST, PTINST2, PTINST3, PTINST4, PTSCST,  ' +
          'PTSCST2, PTSCST3, PTSCST4, PTCPSS, PTWASS, PTINSS, PTSCSS, ' +
          'PTHCPN, PTHDSC, PTTCDC, PTPBMF, PTPBDL, PTAUTH, PTTAG#, PTAPDT, ' +
          'PTDTCM, PTDTPI, PTTIIN, PTCREATE, PTDSPTEAM) ' +
          'values(' +
          ':PTCO, :PTRO, :PTRTYP, :PTWRO, :PTSWID, :PTRTCH, :PTCKEY, :PTCNAM, ' +
          ':PTARCK, :PTPMTH, :PTDATE, :PTCDAT, :PTFCDT, :PTVIN, :PTTEST, :PTSVCO, ' +
          ':PTSVCO2, :PTDEDA, :PTDEDA2, :PTWDED, :PTFRAN, :PTODOM, :PTMILO, :PTCHK, ' +
          ':PTPO, :PTREC, :PTPTOT, :PTLTOT, :PTSTOT, :PTDEDP, :PTSVCT, :PTSPOD, ' +
          ':PTCPHZ, :PTCPST, :PTCPST2, :PTCPST3, :PTCPST4, :PTWAST, :PTWAST2, ' +
          ':PTWAST3, :PTWAST4, :PTINST, :PTINST2, :PTINST3, :PTINST4, :PTSCST, ' +
          ':PTSCST2, :PTSCST3, :PTSCST4, :PTCPSS, :PTWASS, :PTINSS, :PTSCSS, ' +
          ':PTHCPN, :PTHDSC, :PTTCDC, :PTPBMF, :PTPBDL, :PTAUTH, :PTTAG, :PTAPDT, ' +
          ':PTDTCM, :PTDTPI, :PTTIIN, :PTCREATE, :PTDSPTEAM)');
      groupQuery.Connection := AdoCon;
      groupQuery.SQL.Text :=  'select left(trim(ptro#), 2)as RO from rydedata.sdprhdr ' +
          'where ptco# in (''RY1'',''RY2'',''RY3'') and trim(ptro#) <> '''' group by left(trim(ptro#), 2)';
      groupQuery.Open;
      i := 1;
      while not groupQuery.Eof do
      begin
        roGroup := groupQuery.FieldByName('RO').AsString;
        assert(length(trim(roGroup)) = 2, 'roGroup error');
        writeln('roGroup: ' + roGroup + ' ' + IntToStr(i) + ' of 7');
        i := i + 1;
        OpenQuery(AdoQuery, 'select PTCO#, PTRO#, PTRTYP, PTWRO#, PTSWID, PTRTCH, PTCKEY, PTCNAM, PTARCK, PTPMTH,' +
            'PTCO#, PTRO#, PTRTYP, PTWRO#, PTSWID, PTRTCH, PTCKEY, PTCNAM, PTARCK, PTPMTH, ' +
            'case ' +
            '  when PTDATE = 0 then cast(''9999-12-31'' as date) ' +
            '  else cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) ' +
            'end as PTDATE, ' +
            'case ' +
            '  when PTCDAT = 0 then cast(''9999-12-31'' as date) ' +
            '  else cast(left(digits(PTCDAT), 4) || ''-'' || substr(digits(PTCDAT), 5, 2) || ''-'' || substr(digits(PTCDAT), 7, 2) as date) ' +
            'end as PTCDAT, ' +
            'case ' +
            '  when PTFCDT = 0 then cast(''9999-12-31'' as date) ' +
            '  else cast(left(digits(PTFCDT), 4) || ''-'' || substr(digits(PTFCDT), 5, 2) || ''-'' || substr(digits(PTFCDT), 7, 2) as date) ' +
            'end as PTFCDT, ' +
            'PTVIN, PTTEST, PTSVCO, PTSVCO2, PTDEDA, PTDEDA2, PTWDED, PTFRAN, PTODOM, PTMILO, ' +
            'PTCHK#, PTPO#, PTREC#, PTPTOT, PTLTOT, PTSTOT, PTDEDP, PTSVCT, PTSPOD, PTCPHZ, ' +
            'PTCPST, PTCPST2, PTCPST3, PTCPST4, PTWAST, PTWAST2, PTWAST3, PTWAST4, PTINST, ' +
            'PTINST2, PTINST3, PTINST4, PTSCST, PTSCST2, PTSCST3, PTSCST4, PTCPSS, PTWASS, ' +
            'PTINSS, PTSCSS, PTHCPN, PTHDSC, PTTCDC, PTPBMF, PTPBDL, PTAUTH, PTTAG#, ' +
            'case ' +
            '  when PTAPDT = 0 then cast(''9999-12-31'' as date) ' +
            '  else cast(left(digits(PTAPDT), 4) || ''-'' || substr(digits(PTAPDT), 5, 2) || ''-'' || substr(digits(PTAPDT), 7, 2) as date) ' +
            'end as PTAPDT,  ' +
            'case ' +
            '  when PTDTCM = 0 then cast(''9999-12-31'' as date) ' +
            '  else cast(left(digits(PTDTCM), 4) || ''-'' || substr(digits(PTDTCM), 5, 2) || ''-'' || substr(digits(PTDTCM), 7, 2) as date) ' +
            'end as PTDTCM, ' +
            'case ' +
            '  when PTDTPI = 0 then cast(''9999-12-31'' as date) ' +
            '  else cast(left(digits(PTDTPI), 4) || ''-'' || substr(digits(PTDTPI), 5, 2) || ''-'' || substr(digits(PTDTPI), 7, 2) as date) ' +
            'end as PTDTPI, ' +
            'PTTIIN, ' +
            'case ' +
            '  when cast(PTCREATE as date) = ''0001-01-01'' then cast(''9999-12-31 00:00:00'' as timestamp) ' +
            '  else PTCREATE ' +
            'end as PTCREATE, ' +
            'PTDSPTEAM ' +
            'from rydedata.sdprhdr ' +
            'where ptco# in (''RY1'',''RY2'',''RY3'') ' +
            'and trim(ptro#) <> '''' ' +
            'and left(trim(ptro#), 2) = ' + roGroup);
        while not AdoQuery.eof do
        begin
          AdsQuery.ParamByName('ptco').AsString := AdoQuery.FieldByName('ptco#').AsString;
          AdsQuery.ParamByName('ptro').AsString := AdoQuery.FieldByName('ptro#').AsString;
          AdsQuery.ParamByName('ptrtyp').AsString := AdoQuery.FieldByName('ptrtyp').AsString;
          AdsQuery.ParamByName('ptwro').AsInteger := AdoQuery.FieldByName('ptwro#').AsInteger;
          AdsQuery.ParamByName('ptswid').AsString := AdoQuery.FieldByName('ptswid').AsString;
          AdsQuery.ParamByName('ptrtch').AsString := AdoQuery.FieldByName('ptrtch').AsString;
          AdsQuery.ParamByName('ptckey').AsInteger := AdoQuery.FieldByName('ptckey').AsInteger;
          AdsQuery.ParamByName('ptcnam').AsString := AdoQuery.FieldByName('ptcnam').AsString;
          AdsQuery.ParamByName('ptarck').AsInteger := AdoQuery.FieldByName('ptarck').AsInteger;
          AdsQuery.ParamByName('ptpmth').AsString := AdoQuery.FieldByName('ptpmth').AsString;
          AdsQuery.ParamByName('ptdate').AsDate := AdoQuery.FieldByName('ptdate').AsDateTime;
          AdsQuery.ParamByName('ptcdat').AsDate := AdoQuery.FieldByName('ptcdat').AsDateTime;
          AdsQuery.ParamByName('ptfcdt').AsDate := AdoQuery.FieldByName('ptfcdt').AsDateTime;
          AdsQuery.ParamByName('ptvin').AsString := AdoQuery.FieldByName('ptvin').AsString;
          AdsQuery.ParamByName('pttest').AsCurrency := AdoQuery.FieldByName('pttest').AsCurrency;
          AdsQuery.ParamByName('ptsvco').AsCurrency := AdoQuery.FieldByName('ptsvco').AsCurrency;
          AdsQuery.ParamByName('ptsvco2').AsCurrency := AdoQuery.FieldByName('ptsvco2').AsCurrency;
          AdsQuery.ParamByName('ptdeda').AsCurrency := AdoQuery.FieldByName('ptdeda').AsCurrency;
          AdsQuery.ParamByName('ptdeda2').AsCurrency := AdoQuery.FieldByName('ptdeda2').AsCurrency;
          AdsQuery.ParamByName('ptwded').AsCurrency := AdoQuery.FieldByName('ptwded').AsCurrency;
          AdsQuery.ParamByName('ptfran').AsString := AdoQuery.FieldByName('ptfran').AsString;
          AdsQuery.ParamByName('ptodom').AsInteger := AdoQuery.FieldByName('ptodom').AsInteger;
          AdsQuery.ParamByName('ptmilo').AsInteger := AdoQuery.FieldByName('ptmilo').AsInteger;
          AdsQuery.ParamByName('ptchk').AsString := AdoQuery.FieldByName('ptchk#').AsString;
          AdsQuery.ParamByName('ptpo').AsString := AdoQuery.FieldByName('ptpo#').AsString;
          AdsQuery.ParamByName('ptrec').AsInteger := AdoQuery.FieldByName('ptrec#').AsInteger;
          AdsQuery.ParamByName('ptptot').AsCurrency := AdoQuery.FieldByName('ptptot').AsCurrency;
          AdsQuery.ParamByName('ptltot').AsCurrency := AdoQuery.FieldByName('ptltot').AsCurrency;
          AdsQuery.ParamByName('ptstot').AsCurrency := AdoQuery.FieldByName('ptstot').AsCurrency;
          AdsQuery.ParamByName('ptdedp').AsCurrency := AdoQuery.FieldByName('ptdedp').AsCurrency;
          AdsQuery.ParamByName('ptsvct').AsCurrency := AdoQuery.FieldByName('ptsvct').AsCurrency;
          AdsQuery.ParamByName('ptspod').AsCurrency := AdoQuery.FieldByName('ptspod').AsCurrency;
          AdsQuery.ParamByName('ptcphz').AsCurrency := AdoQuery.FieldByName('ptcphz').AsCurrency;
          AdsQuery.ParamByName('ptcpst').AsCurrency := AdoQuery.FieldByName('ptcpst').AsCurrency;
          AdsQuery.ParamByName('ptcpst2').AsCurrency := AdoQuery.FieldByName('ptcpst2').AsCurrency;
          AdsQuery.ParamByName('ptcpst3').AsCurrency := AdoQuery.FieldByName('ptcpst3').AsCurrency;
          AdsQuery.ParamByName('ptcpst4').AsCurrency := AdoQuery.FieldByName('ptcpst4').AsCurrency;
          AdsQuery.ParamByName('ptwast').AsCurrency := AdoQuery.FieldByName('ptwast').AsCurrency;
          AdsQuery.ParamByName('ptwast2').AsCurrency := AdoQuery.FieldByName('ptwast2').AsCurrency;
          AdsQuery.ParamByName('ptwast3').AsCurrency := AdoQuery.FieldByName('ptwast3').AsCurrency;
          AdsQuery.ParamByName('ptwast4').AsCurrency := AdoQuery.FieldByName('ptwast4').AsCurrency;
          AdsQuery.ParamByName('ptinst').AsCurrency := AdoQuery.FieldByName('ptinst').AsCurrency;
          AdsQuery.ParamByName('ptinst2').AsCurrency := AdoQuery.FieldByName('ptinst2').AsCurrency;
          AdsQuery.ParamByName('ptinst3').AsCurrency := AdoQuery.FieldByName('ptinst3').AsCurrency;
          AdsQuery.ParamByName('ptinst4').AsCurrency := AdoQuery.FieldByName('ptinst4').AsCurrency;
          AdsQuery.ParamByName('ptscst').AsCurrency := AdoQuery.FieldByName('ptscst').AsCurrency;
          AdsQuery.ParamByName('ptscst2').AsCurrency := AdoQuery.FieldByName('ptscst2').AsCurrency;
          AdsQuery.ParamByName('ptscst3').AsCurrency := AdoQuery.FieldByName('ptscst3').AsCurrency;
          AdsQuery.ParamByName('ptscst4').AsCurrency := AdoQuery.FieldByName('ptscst4').AsCurrency;
          AdsQuery.ParamByName('ptcpss').AsCurrency := AdoQuery.FieldByName('ptcpss').AsCurrency;
          AdsQuery.ParamByName('ptwass').AsCurrency := AdoQuery.FieldByName('ptwass').AsCurrency;
          AdsQuery.ParamByName('ptinss').AsCurrency := AdoQuery.FieldByName('ptinss').AsCurrency;
          AdsQuery.ParamByName('ptscss').AsCurrency := AdoQuery.FieldByName('ptscss').AsCurrency;
          AdsQuery.ParamByName('pthcpn').AsInteger := AdoQuery.FieldByName('pthcpn').AsInteger;
          AdsQuery.ParamByName('pthdsc').AsCurrency := AdoQuery.FieldByName('pthdsc').AsCurrency;
          AdsQuery.ParamByName('pttcdc').AsCurrency := AdoQuery.FieldByName('pttcdc').AsCurrency;
          AdsQuery.ParamByName('ptpbmf').AsCurrency := AdoQuery.FieldByName('ptpbmf').AsCurrency;
          AdsQuery.ParamByName('ptpbdl').AsCurrency := AdoQuery.FieldByName('ptpbdl').AsCurrency;
          AdsQuery.ParamByName('ptauth').AsString := AdoQuery.FieldByName('ptauth').AsString;
          AdsQuery.ParamByName('pttag').AsString := AdoQuery.FieldByName('pttag#').AsString;
          AdsQuery.ParamByName('ptapdt').AsDate := AdoQuery.FieldByName('ptapdt').AsDateTime;
          AdsQuery.ParamByName('ptdtcm').AsDate := AdoQuery.FieldByName('ptdtcm').AsDateTime;
          AdsQuery.ParamByName('ptdtpi').AsDate := AdoQuery.FieldByName('ptdtpi').AsDateTime;
          AdsQuery.ParamByName('pttiin').AsString := AdoQuery.FieldByName('pttiin').AsString;
          AdsQuery.ParamByName('ptcreate').AsString := GetADSSqlTimeStamp(AdoQuery.FieldByName('ptcreate').AsDateTime);
          AdsQuery.ParamByName('ptdspteam').AsString := AdoQuery.FieldByName('ptdspteam').AsString;
          AdsQuery.ExecSQL;
          AdoQuery.Next;
        end;
        groupQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaSDPRHDR'')');
      ScrapeCountQuery(' rydedata.sdprhdr ' +
            'where ptco# in (''RY1'',''RY2'',''RY3'')', 'stgArkonaSDPRHDR');
    except
      on E: Exception do
      begin
        Raise Exception.Create('SDPRHDR.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    groupQuery.Close;
    FreeAndNil(groupQuery);
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;

end;
procedure TDM.SDPRTXTInitial;
var
  groupQuery: TAdoQuery;
  roGroup: string;
  i: integer;
begin
  groupQuery := TAdoQuery.Create(nil);
  try
    try
      ExecuteQuery(AdsQuery, 'delete from stgArkonaSDPRTXT');
      PrepareQuery(AdsQuery, ' insert into stgArkonaSDPRTXT(' +
        'SXCO#, SXRO#, SXLINE, SXLTYP, SXSEQ#, SXCODE, SXTEXT) ' +
        'values(' +
        ':SXCO, :SXRO, :SXLINE, :SXLTYP, :SXSEQ, :SXCODE, :SXTEXT)');
      groupQuery.Connection := AdoCon;
      groupQuery.SQL.Text := 'select left(trim(sxro#),4) as RO ' +
      'from rydedata.sdprtxt ' +
      'where length(trim(sxro#)) in (7,8) ' +
      '  and left(trim(sxro#),1) in (1,2) ' +
      '  and trim(sxro#) in ( ' +
      '    select trim(ptro#) ' +
      '    from rydedata.sdprhdr ' +
      '    where ptdate > 20090800) ' +
      'group by left(trim(sxro#),4)';
      groupQuery.Open;
      i := 1;
      while not groupQuery.Eof do
      begin
        roGroup := groupQuery.FieldByName('RO').AsString;
        writeln('roGroup: ' + roGroup + ' ' + IntToStr(i) + ' of 114');
        i := i + 1;
        OpenQuery(AdoQuery, 'select * from rydedata.sdprtxt ' +
          'where length(trim(sxro#)) in (7,8) ' +
          'and trim(sxro#) in (select trim(ptro#) from rydedata.sdprhdr ' +
          'where ptdate > 20090800) ' +
          'and left(trim(sxro#),4)  = ' + QuotedStr(roGroup));
        while not AdoQuery.Eof do
        begin
          AdsQuery.ParamByName('sxco').AsString := AdoQuery.FieldByName('sxco#').AsString;
          AdsQuery.ParamByName('sxro').AsString := AdoQuery.FieldByName('sxro#').AsString;
          AdsQuery.ParamByName('sxline').AsInteger := AdoQuery.FieldByName('sxline').AsInteger;
          AdsQuery.ParamByName('sxltyp').AsString := AdoQuery.FieldByName('sxltyp').AsString;
          AdsQuery.ParamByName('sxseq').AsInteger := AdoQuery.FieldByName('sxseq#').AsInteger;
          AdsQuery.ParamByName('sxcode').AsString := AdoQuery.FieldByName('sxcode').AsString;
          AdsQuery.ParamByName('sxtext').AsString := AdoQuery.FieldByName('sxtext').AsString;
          AdsQuery.ExecSQL;
          AdoQuery.Next;
        end;
        groupQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaSDPTXT'')');
    except
      on E: Exception do
      begin
        Raise Exception.Create('PDPTDET.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    groupQuery.Close;
    FreeAndNil(groupQuery);
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDM.PDPTDETInitial;
var
  groupQuery: TAdoQuery;
  roGroup: string;
  i: integer;
begin
  groupQuery := TAdoQuery.Create(nil);
  try
    try
      PrepareQuery(AdsQuery, 'insert into stgArkonaPDPTDET (' +
          'PTCO#, PTINV#, PTLINE, PTSEQ#, PTTGRP, PTCODE, PTSOEP, PTDATE, ' +
          'PTCDATE, PTCPID, PTMANF, PTPART, PTSGRP, PTQTY, PTCOST, PTLIST, ' +
          'PTNET, PTEPCDIFF, PTSPCD, PTORSO, PTPOVR, PTGPRC, PTXCLD, ' +
          'PTFPRT, PTRTRN, PTOHAT, PTVATCODE, PTVATAMT) ' +
          'values(' +
          ':PTCO, :PTINV, :PTLINE, :PTSEQ, :PTTGRP, :PTCODE, :PTSOEP, :PTDATE, ' +
          ':PTCDATE, :PTCPID, :PTMANF, :PTPART, :PTSGRP, :PTQTY, :PTCOST, :PTLIST, ' +
          ':PTNET, :PTEPCDIFF, :PTSPCD, :PTORSO, :PTPOVR, :PTGPRC, :PTXCLD, ' +
          ':PTFPRT, :PTRTRN, :PTOHAT, :PTVATCODE, :PTVATAMT)');
      groupQuery.Connection := AdoCon;
      groupQuery.SQL.Text := 'select left(trim(ptinv#), 3)as RO from rydedata.pdptdet ' +
      'where ptco# in (''RY1'',''RY2'',''RY3'') and trim(ptinv#) <> '''' group by left(trim(ptinv#), 3)';
      groupQuery.Open;
      i := 1;
      while not groupQuery.Eof do
      begin
        roGroup := groupQuery.FieldByName('RO').AsString;
        writeln('roGroup: ' + roGroup + ' ' + IntToStr(i) + ' of 58');
        i := i + 1;
        OpenQuery(AdoQuery, 'select PTCO#, PTINV#, PTLINE, PTSEQ#, PTTGRP, PTCODE, PTSOEP, ' +
          'case ' +
          '  when PTDATE = 0 then cast(''9999-12-31'' as date) ' +
          '  else cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) ' +
          'end as PTDATE, ' +
          'case ' +
          '  when PTCDATE = 0 then cast(''9999-12-31'' as date) ' +
          '  else cast(left(digits(PTCDATE), 4) || ''-'' || substr(digits(PTCDATE), 5, 2) || ''-'' || substr(digits(PTCDATE), 7, 2) as date) ' +
          'end as PTCDATE, ' +
          'PTCPID, PTMANF, PTPART, PTSGRP, PTQTY, PTCOST, PTLIST, ' +
          'PTNET, PTEPCDIFF, PTSPCD, PTORSO, PTPOVR, PTGPRC, PTXCLD, ' +
          'PTFPRT, PTRTRN, PTOHAT, PTVATCODE, PTVATAMT ' +
          'from rydedata.pdptdet ' +
          'where ptco# in (''RY1'',''RY2'',''RY3'') and trim(ptinv#) <> ''''' +
          ' and left(trim(ptinv#), 3) = ' + QuotedStr(roGroup));
        while not AdoQuery.Eof do
        begin
          AdsQuery.ParamByName('ptco').AsString := AdoQuery.FieldByName('ptco#').AsString;
          AdsQuery.ParamByName('ptinv').AsString := AdoQuery.FieldByName('ptinv#').AsString;
          AdsQuery.ParamByName('ptline').AsInteger := AdoQuery.FieldByName('ptline').AsInteger;
          AdsQuery.ParamByName('ptseq').AsInteger := AdoQuery.FieldByName('ptseq#').AsInteger;
          AdsQuery.ParamByName('pttgrp').AsString := AdoQuery.FieldByName('pttgrp').AsString;
          AdsQuery.ParamByName('ptcode').AsString := AdoQuery.FieldByName('ptcode').AsString;
          AdsQuery.ParamByName('ptsoep').AsString := AdoQuery.FieldByName('ptsoep').AsString;
          AdsQuery.ParamByName('ptdate').AsDateTime := AdoQuery.FieldByName('ptdate').AsDateTime;
          AdsQuery.ParamByName('ptcdate').AsDateTime := AdoQuery.FieldByName('ptcdate').AsDateTime;
          AdsQuery.ParamByName('ptcpid').AsString := AdoQuery.FieldByName('ptcpid').AsString;
          AdsQuery.ParamByName('ptmanf').AsString := AdoQuery.FieldByName('ptmanf').AsString;
          AdsQuery.ParamByName('ptpart').AsString := AdoQuery.FieldByName('ptpart').AsString;
          AdsQuery.ParamByName('ptsgrp').AsString := AdoQuery.FieldByName('ptsgrp').AsString;
          AdsQuery.ParamByName('ptqty').AsInteger := AdoQuery.FieldByName('ptqty').AsInteger;
          AdsQuery.ParamByName('ptcost').AsCurrency := AdoQuery.FieldByName('ptcost').AsCurrency;
          AdsQuery.ParamByName('ptlist').AsCurrency := AdoQuery.FieldByName('ptlist').AsCurrency;
          AdsQuery.ParamByName('ptnet').AsCurrency := AdoQuery.FieldByName('ptnet').AsCurrency;
          AdsQuery.ParamByName('ptepcdiff').AsCurrency := AdoQuery.FieldByName('ptepcdiff').AsCurrency;
          AdsQuery.ParamByName('ptspcd').AsString := AdoQuery.FieldByName('ptspcd').AsString;
          AdsQuery.ParamByName('ptorso').AsString := AdoQuery.FieldByName('ptorso').AsString;
          AdsQuery.ParamByName('ptpovr').AsString := AdoQuery.FieldByName('ptpovr').AsString;
          AdsQuery.ParamByName('ptgprc').AsString := AdoQuery.FieldByName('ptgprc').AsString;
          AdsQuery.ParamByName('ptxcld').AsString := AdoQuery.FieldByName('ptxcld').AsString;
          AdsQuery.ParamByName('ptfprt').AsString := AdoQuery.FieldByName('ptfprt').AsString;
          AdsQuery.ParamByName('ptrtrn').AsString := AdoQuery.FieldByName('ptrtrn').AsString;
          AdsQuery.ParamByName('ptohat').AsInteger := AdoQuery.FieldByName('ptohat').AsInteger;
          AdsQuery.ParamByName('ptvatcode').AsString := AdoQuery.FieldByName('ptvatcode').AsString;
          AdsQuery.ParamByName('ptvatamt').AsCurrency := AdoQuery.FieldByName('ptvatamt').AsCurrency;
          AdsQuery.ExecSQL;
          AdoQuery.Next;
        end;
        groupQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaPDPTDET'')');
      ScrapeCountQuery(' rydedata.pdptdet ' +
            'where ptco# in (''RY1'',''RY2'',''RY3'') and trim(ptinv#) <> '''')', 'stgArkonaPDPTDET');
    except
      on E: Exception do
      begin
        Raise Exception.Create('PDPTDET.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    groupQuery.Close;
    FreeAndNil(groupQuery);
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;


//************************Utilities*******************************************//

function TDM.GetADSSqlTimeStamp(const DateTime: TDateTime): String;
var
  ye, mo, da, ho, mi, se, ms: Word;
begin
  DecodeDateTime(DateTime, ye, mo, da, ho, mi, se, ms);
  Result := FormatWordToStr(ye, 4) + '-' +
            FormatWordToStr(mo, 2) + '-' +
            FormatWordToStr(da, 2) + ' ' +
            FormatWordToStr(ho, 2) + ':' +
            FormatWordToStr(mi, 2) + ':' +
            FormatWordToStr(se, 2);
end;

function TDM.FormatWordToStr(const inVal: Word;
  const OutLength: Integer; const PadChar: Char): String;
begin
  Result := IntToStr(inVal);
  if Length(Result) < OutLength then
    Result := StringOfChar(PadChar, OutLength - Length(Result)) + Result;
end;

procedure TDM.curLoadIsEmpty(tableName: string);
var
  curLoadCount: Integer;
begin
  OpenQuery(AdsQuery, 'select count(*) as CurLoad from curLoad' + tableName);
  curLoadCount := AdsQuery.FieldByName('CurLoad').AsInteger;
  CloseQuery(AdsQuery);
  if curLoadCount <> 0 then
    SendMail(tableName + ' failed curLoadIsEmpty');
  Assert(curLoadCount = 0, 'curLoad' + tableName + ' not empty');
end;

procedure TDM.Log(job, routine: string; jobStartTS, jobEndTS: TDateTime);
var
  sql: string;
begin
  PrepareQuery(LogQuery);
  LogQuery.SQL.Add('execute procedure etlJobLogAuditInsert(' + QuotedStr(job) + ', ' +
    QuotedStr(routine) + ', ' +  QuotedStr(GetADSSqlTimeStamp(jobStartTS)) + ', ' +
    QuotedStr(GetADSSqlTimeStamp(jobEndTS)) + ')');
  sql := LogQuery.SQL.Text;
  LogQuery.ExecSQL;
end;

procedure TDM.curLoadPrevLoad(tableName: string);
{ TODO -ojon -crefactor : is this the best way to do this? need these holding tables to be kept to a minimum size,
  don't accumlate deleted records
  12/6/11 appears to be working ok }
begin
  try
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_ZapTable(' + '''' + 'prevLoad' + tableName + '''' + ')');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'prevLoad' + tableName + '''' + ', ' + '''' + 'prevLoad' + tableName + 'x' + '''' +  ', 1, 0)');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'curLoad' + tableName + '''' + ', ' + '''' + 'prevLoad' + tableName + '''' + ', 1, 0)');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'prevLoad' + tableName + 'x' + '''' + ', ' + '''' + 'curLoad' + tableName + '''' + ', 1, 0)');
  except
    on E: Exception do
    begin
      Raise Exception.Create('curLoadPrevLoad.  MESSAGE: ' + E.Message);
    end;
  end;
end;

procedure TDM.ExecuteQuery(query: TDataSet; sql: string);
begin
//  try
    if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
    begin
      AdsQuery.AdsConnection := AdsCon;
      CloseQuery(AdsQuery);
      AdsQuery.SQL.Text := sql;
      AdsQuery.ExecSQL;
      CloseQuery(AdsQuery);
    end
    else if query.ClassParent.ClassName = 'TCustomADODataSet' then
    begin
      AdoQuery.Connection := AdoCon;
      CloseQuery(AdoQuery);
      AdoQuery.SQL.Text := sql;
      AdoQuery.ExecSQL;
      CloseQuery(AdoQuery);
    end;
//  except
//    on E: Exception do
//    begin
//      SendMail('ExecuteQuery failed - no stgErrorLog record entered');
//      exit;
//    end;
//  end;
end;

procedure TDM.OpenQuery(query: TDataSet; sql: string);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
    AdsQuery.SQL.Text := sql;
    AdsQuery.Open;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
    AdoQuery.SQL.Text := sql;
    AdoQuery.Open;
  end;
end;



procedure TDM.PrepareQuery(query: TDataSet; sql: string);
begin
//  if query = 'TAdsExtendedDataSet' then
  if query.classparent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
{ TODO -ojon -cfigure out : don't remember why i commented out close - figure it out and be consistent }
//    AdsQuery.Close;
    if sql <> '' then
      AdsQuery.SQL.Text := sql;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    if sql <> '' then
      AdoQuery.SQL.Text := sql;
  end;
end;

procedure TDM.SendMail(Subject: string; Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
        ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            '''' + 'send mail failed' + '''' + ', ' +
            '''' + 'no sql - line 427' + '''' + ', ' +
            QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        exit;
      end;
    end;
  finally
//    Msg.Free;
    FreeAndNil(Msg);
//    SMTP.Free;
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;





procedure TDM.CloseQuery(query: TDataSet);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
  end;
end;

procedure TDM.ScrapeCountQuery(db2Q, adsQ: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Q);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsQ);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckQuery between ' + db2Q + '(' + IntToStr(db2Count) + ') and ' + adsQ + '(' + IntToStr(adsCount) + ')  failed');
end;

procedure TDM.ScrapeCountTable(db2Table, adsTable: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  db2Table := 'rydedata.' + db2Table;
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Table);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsTable);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckTable between ' + db2Table + '(' + IntToStr(db2Count) + ') and ' + adsTable + '(' + IntToStr(adsCount) + ')  failed');
end;



end.
