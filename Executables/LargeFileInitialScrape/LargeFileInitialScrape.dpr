program LargeFileInitialScrape;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  ActiveX,
  StrUtils,
  LFISDM in 'LFISDM.pas' {DM: TDataModule};

begin
  try
    try
      CoInitialize(nil);
      DM := TDM.Create(nil);
//      DM.SDPRDETInitial;  //completed 3/12/12  // rerun 6/15/2012
//      DM.SDPRHDRInitial;  //completed 3/14/12
//      DM.PDPTDETInitial;  //completed 4/22/12  // rerun 7/12/12, rerun 7/17
      DM.SDPRTXTInitial;

    except
      on E: Exception do
      begin
        DM.ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          '''' + 'LargeFileInitialScrape' + '''' + ', ' +
          '''' + 'no sql - line 136' + '''' + ', ' +
          QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        DM.SendMail('LargeFileInitialScrape Failed.' + leftstr(E.Message, 8),   E.Message);
        exit;
      end;
    end;
    DM.SendMail('LargeFileInitialScrape passed');
  finally
    FreeAndNil(DM);
  end;
end.
