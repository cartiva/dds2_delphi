unit SchedulerDetailDM;

interface

uses
    SysUtils, Classes, adsdata, adsfunc, adstable, adscnnct, DB, ADODB, DateUtils,
    IdSMTPBase, IdSMTP, IdMessage,
    Dialogs;

type
  TDataModule1 = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure ExecuteQuery(query: TDataSet; sql: string);
    procedure CloseQuery(query: TDataSet);
    procedure PrepareQuery(query: TDataSet; sql: string='');
    procedure OpenQuery(query: TDataSet; sql: string);
    function GetADSSqlTimeStamp(const DateTime: TDateTime): String;
    function FormatWordToStr(const inVal: Word;
      const OutLength: Integer; const PadChar: Char = #48): String;
    procedure SendMail(Subject: string; Body: string='');
    procedure Appointments;
  end;
var
  DM: TDataModule1;
//  AdoCon: TADOConnection;
  AdsCon: TADSConnection;
  RSAdsCon: TADSConnection;
//  AdoQuery: TAdoQuery;
  AdsQuery: TAdsQuery;
  LogQuery: TAdsQuery;
  stgErrorLogQuery: TAdsQuery;
  StartTS: TDateTime;
  EndTS: TDateTime;

resourcestring
  executable = 'SchedulerDetail';

implementation

{$R *.dfm}

{ TDataModule1 }





constructor TDataModule1.Create(AOwner: TComponent);
begin
  AdsCon := TADSConnection.Create(nil);
  RSAdsCon := TADSConnection.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  LogQuery := TAdsQuery.Create(nil);
  stgErrorLogQuery := TAdsQuery.Create(nil);
  AdsCon.ConnectPath := '\\67.135.158.12:6363\Advantage\DDS\DDS.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
  AdsCon.Connect;
  RSAdsCon.ConnectPath := '\\10.130.196.83:6363\advantage\rydellservice\rydellservice.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva22';
  AdsCon.Connect;
  LogQuery.AdsConnection := AdsCon;
  stgErrorLogQuery.AdsConnection := AdsCon;
end;

destructor TDataModule1.Destroy;
begin
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  LogQuery.Close;
  FreeAndNil(LogQuery);
  stgErrorLogQuery.Close;
  FreeAndNil(stgErrorLogQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  RSAdsCon.Disconnect;
  FreeAndNil(RSAdsCon);
  inherited;
end;

procedure TDataModule1.Appointments;
var
  proc: string;
  RSQuery: TADSQuery;
begin
  proc := 'SchedulerDetailAppointments';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'delete from SchedulerDetailAppointments');
      PrepareQuery(AdsQuery, 'insert into SchedulerDetailAppointments values ' +
          '(:AppointmentID, :CreatedTS, :CreatedBy, :StartTS, :PromiseTS, :Status,' +
          ':RO, :Customer, :JobDescription, :ModelYear, :Make, :Model, :Color, ' +
          ':Vin, :Comments, :TechComments)');
      RSQuery := TADSQuery.Create(nil);
      RSQuery.AdsConnection := RSAdsCon;
      RSQuery.SQL.Clear;
      RSQuery.Close;
      RSQuery.SQL.Text := 'SELECT a.appointmentid,a.created,a.createdbyid,a.starttime, a.promisetime, ' +
        '  a.status,a.ronumber,a.customer, ' +
        '  trim(replace(trim(replace(trim(a.notes),CHAR(13)+char(10),'''')),''Customer States:'','''')) AS squawk, ' +
        '  a.modelyear, a.make, a.model,a.modelcolor, a.vin,a.comments,  a.techcomments ' +
        'FROM appointments a ' +
        'WHERE advisorid = ''Detail'' ' +
        '  AND CAST(starttime AS sql_date) >= curdate()';
      RSQuery.Open;

      while not RSQuery.Eof do
      begin
        AdsQuery.ParamByName('AppointmentID').AsString := RSQuery.FieldByName('AppointmentID').AsString;
        AdsQuery.ParamByName('CreatedTS').AsDateTime := RSQuery.FieldByName('Created').AsDateTime;
        AdsQuery.ParamByName('CreatedBy').AsString := RSQuery.FieldByName('CreatedByID').AsString;
        AdsQuery.ParamByName('StartTS').AsDateTime := RSQuery.FieldByName('StartTime').AsDateTime;
        AdsQuery.ParamByName('PromiseTS').AsDateTime := RSQuery.FieldByName('PromiseTime').AsDateTime;
        AdsQuery.ParamByName('Status').AsString := RSQuery.FieldByName('status').AsString;
        AdsQuery.ParamByName('RO').AsString := RSQuery.FieldByName('ronumber').AsString;
        AdsQuery.ParamByName('Customer').AsString := StringReplace(RSQuery.FieldByName('customer').AsString,'''','',[rfReplaceAll]);
        AdsQuery.ParamByName('JobDescription').AsString := RSQuery.FieldByName('squawk').AsString;
        AdsQuery.ParamByName('ModelYear').AsInteger := RSQuery.FieldByName('modelyear').AsInteger;
        AdsQuery.ParamByName('Make').AsString := RSQuery.FieldByName('make').AsString;
        AdsQuery.ParamByName('Model').AsString := RSQuery.FieldByName('model').AsString;
        AdsQuery.ParamByName('Color').AsString := RSQuery.FieldByName('modelcolor').AsString;
        AdsQuery.ParamByName('Vin').AsString := RSQuery.FieldByName('vin').AsString;
        AdsQuery.ParamByName('Comments').AsString := RSQuery.FieldByName('comments').AsString;
        AdsQuery.ParamByName('TechComments').AsString := RSQuery.FieldByName('techcomments').AsString;
        AdsQuery.ExecSQL;;
        RSQuery.Next;
      end;
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('Appointments.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(RSQuery);
  end;
end;

////////////////////////////////////////////////////////////////////////////////
procedure TDataModule1.ExecuteQuery(query: TDataSet; sql: string);
begin
    if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
    begin
      AdsQuery.AdsConnection := AdsCon;
      CloseQuery(AdsQuery);
      AdsQuery.SQL.Text := sql;
      AdsQuery.ExecSQL;
      CloseQuery(AdsQuery);
    end
    else if query.ClassParent.ClassName = 'TCustomADODataSet' then
    begin
    end;
end;

function TDataModule1.FormatWordToStr(const inVal: Word;
  const OutLength: Integer; const PadChar: Char): String;
begin
  Result := IntToStr(inVal);
  if Length(Result) < OutLength then
    Result := StringOfChar(PadChar, OutLength - Length(Result)) + Result;
end;

function TDataModule1.GetADSSqlTimeStamp(const DateTime: TDateTime): String;
var
  ye, mo, da, ho, mi, se, ms: Word;
begin
  DecodeDateTime(DateTime, ye, mo, da, ho, mi, se, ms);
  Result := FormatWordToStr(ye, 4) + '-' +
            FormatWordToStr(mo, 2) + '-' +
            FormatWordToStr(da, 2) + ' ' +
            FormatWordToStr(ho, 2) + ':' +
            FormatWordToStr(mi, 2) + ':' +
            FormatWordToStr(se, 2);
end;

procedure TDataModule1.OpenQuery(query: TDataSet; sql: string);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
    AdsQuery.SQL.Text := sql;
    AdsQuery.Open;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
  end;
end;


procedure TDataModule1.PrepareQuery(query: TDataSet; sql: string);
begin
  if query.classparent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
{ TODO -ojon -cfigure out : don't remember why i commented out close - figure it out and be consistent }
    if sql <> '' then
      AdsQuery.SQL.Text := sql;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
  end;
end;

procedure TDataModule1.SendMail(Subject, Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
        ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            '''' + 'send mail failed' + '''' + ', ' +
            '''' + 'no sql - line 427' + '''' + ', ' +
            QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        exit;
      end;
    end;
  finally
//    Msg.Free;
    FreeAndNil(Msg);
//    SMTP.Free;
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;

procedure TDataModule1.CloseQuery(query: TDataSet);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
//    AdoQuery.SQL.Clear;
//    AdoQuery.Close;
  end;
end;


end.
