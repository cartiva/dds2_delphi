program todaySchedulerAppointments;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  ActiveX,
  StrUtils,
  todaySchedulerAppointmentsDM in 'todaySchedulerAppointmentsDM.pas' {Datamodule1: TDataModule};

resourcestring
  executable = 'stgRydellService';
begin
  try
    try
//      CoInitialize(nil);
      DM := TDataModule1.Create(nil);
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
        QuotedStr(executable) + ', ' +
        '''' + 'none' + '''' + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
// current procs //
      DM.Appointments;
// current procs //
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        '''' + 'none' + '''' + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          '''' + 'stgRydellService' + '''' + ', ' +
          '''' + 'no sql - line 136' + '''' + ', ' +
          QuotedStr(E.ClassName + ' ' + E.Message) + ')');

        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          '''' + 'none' + '''' + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.ClassName + ' ' + E.Message) + ')');


        DM.SendMail('stgRydellService Failed.' + leftstr(E.Message, 8),   E.Message);
        exit;
      end;
    end;
//    DM.SendMail('stgRydellService passed');
  finally
    FreeAndNil(DM);
  end;
end.
