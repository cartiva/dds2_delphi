unit stgZRDM;

interface

uses
    SysUtils, Classes, adsdata, adsfunc, adstable, adscnnct, DB, ADODB, DateUtils,
    IdSMTPBase, IdSMTP, IdMessage,
    Dialogs;

type
  TDataModule1 = class(TDataModule)
  public
// utilities
    procedure Log(job, routine: string; jobStartTS, jobEndTS: TDateTime);
    procedure PrepareQuery(query: TDataSet; sql: string='');
    procedure CloseQuery(query: TDataSet);
    function FormatWordToStr(const inVal: Word;
      const OutLength: Integer; const PadChar: Char = #48): String;
    function GetADSSqlTimeStamp(const DateTime: TDateTime): String;
    procedure OpenQuery(query: TDataSet; sql: string);
    procedure ExecuteQuery(query: TDataSet; sql: string);
    procedure curLoadPrevLoad(tableName: string);
    procedure curLoadIsEmpty(tableName: string);
    procedure SendMail(Subject: string; Body: string='');
    procedure ScrapeCountTable(db2Table, adsTable: string);
    procedure ScrapeCountQuery(db2Q, adsQ: string);
    procedure ScrapeCount(db2, ads: string);
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure ExtractFramework;
    procedure GLPOPYR;
    procedure FFPXREFDTA;
    procedure FSPagesLines;
    procedure GLPMAST;
    procedure GLPDEPT;
    procedure GLPPOHD;
    procedure PDPTHDR;
    procedure BOPMAST;
    procedure GLPCRHD;
    procedure GLPCRDT;
    procedure PYPCLOCKIN;
    procedure GLPJRND;
    procedure PYHSHDTA;
    procedure PYCNTRL;
    procedure PYDEDUCT;
    procedure SDPXTIM;
    procedure GLPCUST;
//    procedure BOPSLSS;
    procedure PYPCODES;
    procedure PYPCLKL;
    procedure BOPVREF;
    procedure PYPRJOBD;
    procedure INPOPTF;
    procedure INPOPTD;
  end;

var
  DM: TDataModule1;
  AdoCon: TADOConnection;
  AdsCon: TADSConnection;
  AdoQuery: TAdoQuery;
  AdsQuery: TAdsQuery;
  LogQuery: TAdsQuery;
  stgErrorLogQuery: TAdsQuery;
  StartTS: TDateTime;
  EndTS: TDateTime;

resourcestring
  executable = 'stgZapAndReload';

implementation

{$R *.dfm}

{ TDataModule1 }



constructor TDataModule1.Create(AOwner: TComponent);
begin
  AdoCon := TADOConnection.Create(nil);
  AdsCon := TADSConnection.Create(nil);
  AdoQuery := TADOQuery.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  LogQuery := TAdsQuery.Create(nil);
  stgErrorLogQuery := TAdsQuery.Create(nil);
  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=fuckyou5;Persist Security Info=True;User ID=rydejon;Data Source=iSeries System DSN';
//  AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=odbc0210;Persist Security Info=True;User ID=rydeodbc;Data Source=ArkonaSSL';
//  AdsCon.ConnectPath := '\\jon520:6363\Advantage\DDS\DDS.add';
  AdsCon.ConnectPath := '\\67.135.158.12:6363\Advantage\DDS\DDS.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
  AdoCon.Connected := True;
  AdsCon.Connect;
  LogQuery.AdsConnection := AdsCon;
  stgErrorLogQuery.AdsConnection := AdsCon;
end;

destructor TDataModule1.Destroy;
begin
  AdoQuery.Close;
  FreeAndNil(AdoQuery);
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  LogQuery.Close;
  FreeAndNil(LogQuery);
  stgErrorLogQuery.Close;
  FreeAndNil(stgErrorLogQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  AdoCon.Connected := False;
  FreeAndNil(AdoCon);
  inherited;
end;

procedure TDataModule1.GLPOPYR;
var
  proc: string;
begin
  proc := 'GLPOPYR';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
    // zap and reload stgArkonaGlpopyr
      ExecuteQuery(AdsQuery, 'execute procedure sp_zaptable(''stgArkonaGLPOPYR'')');
      PrepareQuery(AdsQuery, 'insert into stgArkonaGlpopyr values ' +
          '(:GOCO, :GOTYPE, :GOYEAR, :GOMN01, :GOMN02, :GOMN03,' +
          ':GOMN04, :GOMN05, :GOMN06, :GOMN07, :GOMN08, :GOMN09,' +
          ':GOMN10, :GOMN11, :GOMN12, :GOMN13)');
      OpenQuery(AdoQuery, 'select goco#,gotype,goyear,gomn01,gomn02,gomn03,gomn04, ' +
          'gomn05,gomn06,gomn07,gomn08,gomn09,gomn10,gomn11,gomn12,gomn13 ' +
          'from rydedata.glpopyr');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('GOCO').AsString := AdoQuery.FieldByName('GOCO#').AsString;
        AdsQuery.ParamByName('GOTYPE').AsString := AdoQuery.FieldByName('GOTYPE').AsString;
        AdsQuery.ParamByName('GOYEAR').AsInteger := AdoQuery.FieldByName('GOYEAR').AsInteger;
        AdsQuery.ParamByName('GOMN01').AsString := AdoQuery.FieldByName('GOMN01').AsString;
        AdsQuery.ParamByName('GOMN02').AsString := AdoQuery.FieldByName('GOMN02').AsString;
        AdsQuery.ParamByName('GOMN03').AsString := AdoQuery.FieldByName('GOMN03').AsString;
        AdsQuery.ParamByName('GOMN04').AsString := AdoQuery.FieldByName('GOMN04').AsString;
        AdsQuery.ParamByName('GOMN05').AsString := AdoQuery.FieldByName('GOMN05').AsString;
        AdsQuery.ParamByName('GOMN06').AsString := AdoQuery.FieldByName('GOMN06').AsString;
        AdsQuery.ParamByName('GOMN07').AsString := AdoQuery.FieldByName('GOMN07').AsString;
        AdsQuery.ParamByName('GOMN08').AsString := AdoQuery.FieldByName('GOMN08').AsString;
        AdsQuery.ParamByName('GOMN09').AsString := AdoQuery.FieldByName('GOMN09').AsString;
        AdsQuery.ParamByName('GOMN10').AsString := AdoQuery.FieldByName('GOMN10').AsString;
        AdsQuery.ParamByName('GOMN11').AsString := AdoQuery.FieldByName('GOMN11').AsString;
        AdsQuery.ParamByName('GOMN12').AsString := AdoQuery.FieldByName('GOMN12').AsString;
        AdsQuery.ParamByName('GOMN13').AsString := AdoQuery.FieldByName('GOMN13').AsString;
        AdsQuery.ExecSQL;;
        AdoQuery.Next;
      end;
      ScrapeCountTable('GLPOPYR', 'stgArkonaGLPOPYR');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('GLPOPYR.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdoQuery);
    CloseQuery(AdsQuery);
  end;
end;

procedure TDataModule1.FFPXREFDTA;
var
  proc: string;
begin
  proc := 'FFPXREFDTA';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaFFPXREFDTA'')');
      PrepareQuery(AdsQuery, 'insert into stgArkonaFFPXREFDTA (' +
          'FXCO#, FXCONSOL, FXGLCO#, FXCODE, FXCYY, FXGACT, FXFACT, FXFAC2, ' +
          'FXFAC3, FXFAC4, FXFAC5, FXFAC6, FXFAC7, FXFAC8, FXFAC9, FXFA10, ' +
          'FXFP01, FXFP02, FXFP03, FXFP04, FXFP05, FXFP06, FXFP07, FXFP08, FXFP09, FXFP10) ' +
          'values (' +
          ':FXCO, :FXCONSOL, :FXGLCO, :FXCODE, :FXCYY, :FXGACT, :FXFACT, :FXFAC2, ' +
          ':FXFAC3, :FXFAC4, :FXFAC5, :FXFAC6, :FXFAC7, :FXFAC8, :FXFAC9, :FXFA10, ' +
          ':FXFP01, :FXFP02, :FXFP03, :FXFP04, :FXFP05, :FXFP06, :FXFP07, :FXFP08, :FXFP09, :FXFP10)');
      OpenQuery(AdoQuery, 'select ' +
          'FXCO#, FXCONSOL, FXGLCO#, FXCODE, FXCYY, FXGACT, FXFACT, FXFAC2, ' +
          'FXFAC3, FXFAC4, FXFAC5, FXFAC6, FXFAC7, FXFAC8, FXFAC9, FXFA10, ' +
          'FXFP01, FXFP02, FXFP03, FXFP04, FXFP05, FXFP06, FXFP07, FXFP08, FXFP09, FXFP10 ' +
          'from rydedata.FFPXREFDTA');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('fxco').AsString := AdoQuery.FieldByName('fxco#').AsString;
        AdsQuery.ParamByName('fxconsol').AsString := AdoQuery.FieldByName('fxconsol').AsString;
        AdsQuery.ParamByName('fxglco').AsString := AdoQuery.FieldByName('fxglco#').AsString;
        AdsQuery.ParamByName('fxcode').AsString := AdoQuery.FieldByName('fxcode').AsString;
        AdsQuery.ParamByName('fxcyy').AsInteger := AdoQuery.FieldByName('fxcyy').AsInteger;
        AdsQuery.ParamByName('fxgact').AsString := AdoQuery.FieldByName('fxgact').AsString;
        AdsQuery.ParamByName('fxfact').AsString := AdoQuery.FieldByName('fxfact').AsString;
        AdsQuery.ParamByName('fxfac2').AsString := AdoQuery.FieldByName('fxfac2').AsString;
        AdsQuery.ParamByName('fxfac3').AsString := AdoQuery.FieldByName('fxfac3').AsString;
        AdsQuery.ParamByName('fxfac4').AsString := AdoQuery.FieldByName('fxfac4').AsString;
        AdsQuery.ParamByName('fxfac5').AsString := AdoQuery.FieldByName('fxfac5').AsString;
        AdsQuery.ParamByName('fxfac6').AsString := AdoQuery.FieldByName('fxfac6').AsString;
        AdsQuery.ParamByName('fxfac7').AsString := AdoQuery.FieldByName('fxfac7').AsString;
        AdsQuery.ParamByName('fxfac8').AsString := AdoQuery.FieldByName('fxfac8').AsString;
        AdsQuery.ParamByName('fxfac9').AsString := AdoQuery.FieldByName('fxfac9').AsString;
        AdsQuery.ParamByName('fxfa10').AsString := AdoQuery.FieldByName('fxfa10').AsString;
        AdsQuery.ParamByName('fxfp01').AsFloat := AdoQuery.FieldByName('fxfp01').AsFloat;
        AdsQuery.ParamByName('fxfp02').AsFloat := AdoQuery.FieldByName('fxfp02').AsFloat;
        AdsQuery.ParamByName('fxfp03').AsFloat := AdoQuery.FieldByName('fxfp03').AsFloat;
        AdsQuery.ParamByName('fxfp04').AsFloat := AdoQuery.FieldByName('fxfp04').AsFloat;
        AdsQuery.ParamByName('fxfp05').AsFloat := AdoQuery.FieldByName('fxfp05').AsFloat;
        AdsQuery.ParamByName('fxfp06').AsFloat := AdoQuery.FieldByName('fxfp06').AsFloat;
        AdsQuery.ParamByName('fxfp07').AsFloat := AdoQuery.FieldByName('fxfp07').AsFloat;
        AdsQuery.ParamByName('fxfp08').AsFloat := AdoQuery.FieldByName('fxfp08').AsFloat;
        AdsQuery.ParamByName('fxfp09').AsFloat := AdoQuery.FieldByName('fxfp09').AsFloat;
        AdsQuery.ParamByName('fxfp10').AsFloat := AdoQuery.FieldByName('fxfp10').AsFloat;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaFFPXREFDTA'')');
      ScrapeCountTable('FFPXREFDTA', 'stgArkonaFFPXREFDTA');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('FFPXREFDTA.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;



procedure TDataModule1.GLPDEPT;
var
  proc: string;
begin
  proc := 'GLPDEPT';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaGLPDEPT'')');
      PrepareQuery(AdsQuery, 'insert into stgArkonaGLPDEPT (' +
          'COMPANY_NUMBER, DEPARTMENT_CODE, DEPT_DESCRIPTION)' +
          'values (' +
          ':COMPANY_NUMBER, :DEPARTMENT_CODE, :DEPT_DESCRIPTION)');
      OpenQuery(AdoQuery, 'select COMPANY_NUMBER, DEPARTMENT_CODE, ' +
          'DEPT_DESCRIPTION from rydedata.GLPDEPT');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('COMPANY_NUMBER').AsString := AdoQuery.FieldByName('COMPANY_NUMBER').AsString;
        AdsQuery.ParamByName('DEPARTMENT_CODE').AsString := AdoQuery.FieldByName('DEPARTMENT_CODE').AsString;
        AdsQuery.ParamByName('DEPT_DESCRIPTION').AsString := AdoQuery.FieldByName('DEPT_DESCRIPTION').AsString;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaGLPDEPT'')');
      ScrapeCountTable('GLPDEPT', 'stgArkonaGLPDEPT');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('GLPDEPT.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;



procedure TDataModule1.GLPMAST;
var
  proc: string;
begin
  proc := 'GLPMAST';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaGLPMAST'')');
      PrepareQuery(AdsQuery, 'insert into stgArkonaGLPMAST (' +
          'GMCO#, GMMODE, GMYEAR, GMACCT, GMTYPE, GMDESC, GMSTYP, GMCTYP, ' +
          'GMDEPT, GMTYPB, GMDBOA, GMCROA, GMOPCT, GMACLR, GMWOAC, GMSCHD, ' +
          'GMRECON, GMCNTU, GMOPT1, GMOPT2, GMOPT3, GMOPT4, GMOPT5, GMATBB, ' +
          'GMAT01, GMAT02, GMAT03, GMAT04, GMAT05, GMAT06, GMAT07, GMAT08, ' +
          'GMAT09, GMAT10, GMAT11, GMAT12, GMAT13, GMUTBB, GMUT01, GMUT02, ' +
          'GMUT03, GMUT04, GMUT05, GMUT06, GMUT07, GMUT08, GMUT09, GMUT10, ' +
          'GMUT11, GMUT12, GMUT13, GMACTIVE) ' +
          'values (' +
          ':GMCO, :GMMODE, :GMYEAR, :GMACCT, :GMTYPE, :GMDESC, :GMSTYP, :GMCTYP, ' +
          ':GMDEPT, :GMTYPB, :GMDBOA, :GMCROA, :GMOPCT, :GMACLR, :GMWOAC, :GMSCHD, ' +
          ':GMRECON, :GMCNTU, :GMOPT1, :GMOPT2, :GMOPT3, :GMOPT4, :GMOPT5, :GMATBB, ' +
          ':GMAT01, :GMAT02, :GMAT03, :GMAT04, :GMAT05, :GMAT06, :GMAT07, :GMAT08, ' +
          ':GMAT09, :GMAT10, :GMAT11, :GMAT12, :GMAT13, :GMUTBB, :GMUT01, :GMUT02, ' +
          ':GMUT03, :GMUT04, :GMUT05, :GMUT06, :GMUT07, :GMUT08, :GMUT09, :GMUT10, ' +
          ':GMUT11, :GMUT12, :GMUT13, :GMACTIVE)');
      OpenQuery(AdoQuery, 'select ' +
          'GMCO#, GMMODE, GMYEAR, GMACCT, GMTYPE, GMDESC, GMSTYP, GMCTYP, ' +
          'GMDEPT, GMTYPB, GMDBOA, GMCROA, GMOPCT, GMACLR, GMWOAC, GMSCHD, ' +
          'GMRECON, GMCNTU, GMOPT1, GMOPT2, GMOPT3, GMOPT4, GMOPT5, GMATBB, ' +
          'GMAT01, GMAT02, GMAT03, GMAT04, GMAT05, GMAT06, GMAT07, GMAT08, ' +
          'GMAT09, GMAT10, GMAT11, GMAT12, GMAT13, GMUTBB, GMUT01, GMUT02, ' +
          'GMUT03, GMUT04, GMUT05, GMUT06, GMUT07, GMUT08, GMUT09, GMUT10, ' +
          'GMUT11, GMUT12, GMUT13, GMACTIVE ' +
          'from rydedata.GLPMAST');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('gmco').AsString := AdoQuery.FieldByName('gmco#').AsString;
        AdsQuery.ParamByName('gmmode').AsString := AdoQuery.FieldByName('gmmode').AsString;
        AdsQuery.ParamByName('gmyear').AsInteger := AdoQuery.FieldByName('gmyear').AsInteger;
        AdsQuery.ParamByName('gmacct').AsString := AdoQuery.FieldByName('gmacct').AsString;
        AdsQuery.ParamByName('gmtype').AsString := AdoQuery.FieldByName('gmtype').AsString;
        AdsQuery.ParamByName('gmdesc').AsString := AdoQuery.FieldByName('gmdesc').AsString;
        AdsQuery.ParamByName('gmstyp').AsString := AdoQuery.FieldByName('gmstyp').AsString;
        AdsQuery.ParamByName('gmctyp').AsString := AdoQuery.FieldByName('gmctyp').AsString;
        AdsQuery.ParamByName('gmdept').AsString := AdoQuery.FieldByName('gmdept').AsString;
        AdsQuery.ParamByName('gmtypb').AsString := AdoQuery.FieldByName('gmtypb').AsString;
        AdsQuery.ParamByName('gmdboa').AsString := AdoQuery.FieldByName('gmdboa').AsString;
        AdsQuery.ParamByName('gmcroa').AsString := AdoQuery.FieldByName('gmcroa').AsString;
        AdsQuery.ParamByName('gmopct').AsFloat := AdoQuery.FieldByName('gmopct').AsFloat;
        AdsQuery.ParamByName('gmaclr').AsCurrency := AdoQuery.FieldByName('gmaclr').AsCurrency;
        AdsQuery.ParamByName('gmwoac').AsString := AdoQuery.FieldByName('gmwoac').AsString;
        AdsQuery.ParamByName('gmschd').AsString := AdoQuery.FieldByName('gmschd').AsString;
        AdsQuery.ParamByName('gmrecon').AsString := AdoQuery.FieldByName('gmrecon').AsString;
        AdsQuery.ParamByName('gmcntu').AsString := AdoQuery.FieldByName('gmcntu').AsString;
        AdsQuery.ParamByName('gmopt1').AsString := AdoQuery.FieldByName('gmopt1').AsString;
        AdsQuery.ParamByName('gmopt2').AsString := AdoQuery.FieldByName('gmopt2').AsString;
        AdsQuery.ParamByName('gmopt3').AsString := AdoQuery.FieldByName('gmopt3').AsString;
        AdsQuery.ParamByName('gmopt4').AsString := AdoQuery.FieldByName('gmopt4').AsString;
        AdsQuery.ParamByName('gmopt5').AsString := AdoQuery.FieldByName('gmopt5').AsString;
        AdsQuery.ParamByName('gmatbb').AsCurrency := AdoQuery.FieldByName('gmatbb').AsCurrency;
        AdsQuery.ParamByName('gmat01').AsCurrency := AdoQuery.FieldByName('gmat01').AsCurrency;
        AdsQuery.ParamByName('gmat02').AsCurrency := AdoQuery.FieldByName('gmat02').AsCurrency;
        AdsQuery.ParamByName('gmat03').AsCurrency := AdoQuery.FieldByName('gmat03').AsCurrency;
        AdsQuery.ParamByName('gmat04').AsCurrency := AdoQuery.FieldByName('gmat04').AsCurrency;
        AdsQuery.ParamByName('gmat05').AsCurrency := AdoQuery.FieldByName('gmat05').AsCurrency;
        AdsQuery.ParamByName('gmat06').AsCurrency := AdoQuery.FieldByName('gmat06').AsCurrency;
        AdsQuery.ParamByName('gmat07').AsCurrency := AdoQuery.FieldByName('gmat07').AsCurrency;
        AdsQuery.ParamByName('gmat08').AsCurrency := AdoQuery.FieldByName('gmat08').AsCurrency;
        AdsQuery.ParamByName('gmat09').AsCurrency := AdoQuery.FieldByName('gmat09').AsCurrency;
        AdsQuery.ParamByName('gmat10').AsCurrency := AdoQuery.FieldByName('gmat10').AsCurrency;
        AdsQuery.ParamByName('gmat11').AsCurrency := AdoQuery.FieldByName('gmat11').AsCurrency;
        AdsQuery.ParamByName('gmat12').AsCurrency := AdoQuery.FieldByName('gmat12').AsCurrency;
        AdsQuery.ParamByName('gmat13').AsCurrency := AdoQuery.FieldByName('gmat13').AsCurrency;
        AdsQuery.ParamByName('gmutbb').AsInteger := AdoQuery.FieldByName('gmutbb').AsInteger;
        AdsQuery.ParamByName('gmut01').AsInteger := AdoQuery.FieldByName('gmut01').AsInteger;
        AdsQuery.ParamByName('gmut02').AsInteger := AdoQuery.FieldByName('gmut02').AsInteger;
        AdsQuery.ParamByName('gmut03').AsInteger := AdoQuery.FieldByName('gmut03').AsInteger;
        AdsQuery.ParamByName('gmut04').AsInteger := AdoQuery.FieldByName('gmut04').AsInteger;
        AdsQuery.ParamByName('gmut05').AsInteger := AdoQuery.FieldByName('gmut05').AsInteger;
        AdsQuery.ParamByName('gmut06').AsInteger := AdoQuery.FieldByName('gmut06').AsInteger;
        AdsQuery.ParamByName('gmut07').AsInteger := AdoQuery.FieldByName('gmut07').AsInteger;
        AdsQuery.ParamByName('gmut08').AsInteger := AdoQuery.FieldByName('gmut08').AsInteger;
        AdsQuery.ParamByName('gmut09').AsInteger := AdoQuery.FieldByName('gmut09').AsInteger;
        AdsQuery.ParamByName('gmut10').AsInteger := AdoQuery.FieldByName('gmut10').AsInteger;
        AdsQuery.ParamByName('gmut11').AsInteger := AdoQuery.FieldByName('gmut11').AsInteger;
        AdsQuery.ParamByName('gmut12').AsInteger := AdoQuery.FieldByName('gmut12').AsInteger;
        AdsQuery.ParamByName('gmut13').AsInteger := AdoQuery.FieldByName('gmut13').AsInteger;
        AdsQuery.ParamByName('gmactive').AsString := AdoQuery.FieldByName('gmactive').AsString;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaGLPMAST'')');
      ScrapeCountTable('GLPMAST', 'stgArkonaGLPMAST');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('GLPMAST.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDataModule1.FSPagesLines;
var
  proc: string;
begin
  proc := 'FSPagesLines';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgFSPagesLines'')');
      PrepareQuery(AdsQuery, 'insert into stgFSPagesLines (' +
          'fxcyy, fxfact, fxgact, fxmpge, fxmlne) ' +
          'values(' +
          ':fxcyy, :fxfact, :fxgact, :fxmpge, :fxmlne)');
      OpenQuery(AdoQuery, 'select fxcyy, fxfact, fxgact, fxmpge, fxmlne ' +
          'from rydedata.ffpxrefdta fs ' +
          'left join eisglobal.sypffxmst sy on fs.fxcyy = sy.fxmcyy ' +
          'and fs.fxfact = sy.fxmact and fs.fxcode = sy.fxmcde ' +
          'where fs.fxconsol = '''' ' +
          '  and fs.fxgact <> ''''');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('fxcyy').AsInteger := AdoQuery.FieldByName('fxcyy').AsInteger;
        AdsQuery.ParamByName('fxfact').AsString := AdoQuery.FieldByName('fxfact').AsString;
        AdsQuery.ParamByName('fxgact').AsString := AdoQuery.FieldByName('fxgact').AsString;
        AdsQuery.ParamByName('fxmpge').AsInteger := AdoQuery.FieldByName('fxmpge').AsInteger;
        AdsQuery.ParamByName('fxmlne').AsFloat := AdoQuery.FieldByName('fxmlne').AsFloat;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgFSPagesLines'')');
      ScrapeCountQuery(' rydedata.ffpxrefdta fs ' +
          'left join eisglobal.sypffxmst sy on fs.fxcyy = sy.fxmcyy ' +
          'and fs.fxfact = sy.fxmact and fs.fxcode = sy.fxmcde ' +
          'where fs.fxconsol = '''' ' +
          '  and fs.fxgact <> ''''', 'stgFSPagesLines');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('stgFSPagesLines.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDataModule1.GLPPOHD;
var
  proc: string;
begin
  proc := 'GLPPOHD';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaGLPPOHD'')');
      PrepareQuery(AdsQuery, ' insert into stgArkonaGLPPOHD(' +
          'GUCO#, GUPO#, GUSTAT, GUTYPE, GUVND#, GUNAME, GUDATE, GUAMT, ' +
          'GUIAMT, GUINV#, GURO#, GUUSER, GUREQUEST, GUVATTOT, GUINVDATE) ' +
          'values(' +
          ':GUCO, :GUPO, :GUSTAT, :GUTYPE, :GUVND, :GUNAME, :GUDATE, :GUAMT, ' +
          ':GUIAMT, :GUINV, :GURO, :GUUSER, :GUREQUEST, :GUVATTOT, :GUINVDATE)');
      OpenQuery(AdoQuery, 'select ' +
          'GUCO#, GUPO#, GUSTAT, GUTYPE, GUVND#, GUNAME, GUDATE, GUAMT, ' +
          'GUIAMT, GUINV#, GURO#, GUUSER, GUREQUEST, GUVATTOT, GUINVDATE ' +
          'from rydedata.glppohd');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('guco').AsString := AdoQuery.FieldByName('guco#').AsString;
        AdsQuery.ParamByName('gupo').AsString := AdoQuery.FieldByName('gupo#').AsString;
        AdsQuery.ParamByName('gustat').AsString := AdoQuery.FieldByName('gustat').AsString;
        AdsQuery.ParamByName('gutype').AsString := AdoQuery.FieldByName('gutype').AsString;
        AdsQuery.ParamByName('guvnd').AsString := AdoQuery.FieldByName('guvnd#').AsString;
        AdsQuery.ParamByName('guname').AsString := AdoQuery.FieldByName('guname').AsString;
        AdsQuery.ParamByName('gudate').AsDateTime := AdoQuery.FieldByName('gudate').AsDateTime;
        AdsQuery.ParamByName('guamt').AsCurrency := AdoQuery.FieldByName('guamt').AsCurrency;
        AdsQuery.ParamByName('guiamt').AsCurrency := AdoQuery.FieldByName('guiamt').AsCurrency;
        AdsQuery.ParamByName('guinv').AsString := AdoQuery.FieldByName('guinv#').AsString;
        AdsQuery.ParamByName('guro').AsString := AdoQuery.FieldByName('guro#').AsString;
        AdsQuery.ParamByName('guuser').AsString := AdoQuery.FieldByName('guuser').AsString;
        AdsQuery.ParamByName('gurequest').AsString := AdoQuery.FieldByName('gurequest').AsString;
        AdsQuery.ParamByName('guvattot').AsCurrency := AdoQuery.FieldByName('guvattot').AsCurrency;
        AdsQuery.ParamByName('guinvdate').AsDateTime := AdoQuery.FieldByName('guinvdate').AsDateTime;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaGLPPOHD'')');
      ScrapeCountTable('GLPPOHD', 'stgArkonaGLPPOHD');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('GLPPOHD.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDataModule1.PDPTHDR;
var
  proc: string;
begin
  proc := 'PDPTHDR';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaPDPTHDR'')');
      PrepareQuery(AdsQuery, ' insert into stgArkonaPDPTHDR(' +
          'PTCO#, PTINV#, PTDTYP, PTTTYP, PTCPID, PTACTP, PTDATE, PTCKEY, ' +
          'PTSKEY, PTPHON, PTSNAM, PTPMTH, PTSTYP, PTPO#, PTREC#, PTPLVL, ' +
          'PTPTOT, PTSHPT, PTSPOD, PTSTAX, PTSTAX2, PTSTAX3, PTSTAX4, ' +
          'PTGROS, PTCREATE, ' +
          'PTCTHOLD, PTAUTH#, PTAUTHID, PTDLVMTH, ' +
          'PTSHIPSEQ, PTTAXGO) ' +
          'values(' +
          ':PTCO, :PTINV, :PTDTYP, :PTTTYP, :PTCPID, :PTACTP, :PTDATE, :PTCKEY, ' +
          ':PTSKEY, :PTPHON, :PTSNAM, :PTPMTH, :PTSTYP, :PTPO, :PTREC, :PTPLVL, ' +
          ':PTPTOT, :PTSHPT, :PTSPOD, :PTSTAX, :PTSTAX2, :PTSTAX3, :PTSTAX4, ' +
          ':PTGROS, :PTCREATE, :PTCTHOLD, :PTAUTH, :PTAUTHID, :PTDLVMTH, ' +
          ':PTSHIPSEQ, :PTTAXGO)');
      OpenQuery(AdoQuery, 'select ' +
          'PTCO#, PTINV#, PTDTYP, PTTTYP, PTCPID, PTACTP, ' +
          'cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) as PTDATE,  ' +
          'PTCKEY,  PTSKEY, PTPHON, PTSNAM, PTPMTH, PTSTYP, PTPO#, PTREC#, PTPLVL, ' +
          'PTPTOT, PTSHPT, PTSPOD, PTSTAX, PTSTAX2, PTSTAX3, PTSTAX4, PTGROS, ' +
          'case ' +
          '  when cast(PTCREATE as date) = ''0001-01-01'' then cast(''9999-12-31 00:00:00'' as timestamp) ' +
          '  else PTCREATE ' +
          'end as PTCREATE, ' +
          'case ' +
          '  when cast(PTCTHOLD as date) = ''0001-01-01'' then cast(''9999-12-31 00:00:00'' as timestamp) ' +
          '  else PTCTHOLD ' +
          'end as PTCTHOLD, ' +
          'PTAUTH#, PTAUTHID, PTDLVMTH, ' +
          'PTSHIPSEQ, PTTAXGO ' +
          'from rydedata.pdpthdr ' +
          'where trim(ptinv#) <> '''' ' +
          '  and ptco# in (''RY1'',''RY2'',''RY3'') ' +
          '  and trim(ptinv#) not in ( ' +
          '    select trim(ptinv#) ' +
          '    from RYDEDATA.PDPTHDR ' +
          '    group by trim(ptinv#) ' +
          '      having count(*) > 1)');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('ptco').AsString := AdoQuery.FieldByName('ptco#').AsString;
        AdsQuery.ParamByName('ptinv').AsString := AdoQuery.FieldByName('ptinv#').AsString;
        AdsQuery.ParamByName('ptdtyp').AsString := AdoQuery.FieldByName('ptdtyp').AsString;
        AdsQuery.ParamByName('ptttyp').AsString := AdoQuery.FieldByName('ptttyp').AsString;
        AdsQuery.ParamByName('ptcpid').AsString := AdoQuery.FieldByName('ptcpid').AsString;
        AdsQuery.ParamByName('ptactp').AsString := AdoQuery.FieldByName('ptactp').AsString;
        AdsQuery.ParamByName('ptdate').AsDateTime := AdoQuery.FieldByName('ptdate').AsDateTime;
        AdsQuery.ParamByName('ptckey').AsInteger := AdoQuery.FieldByName('ptckey').AsInteger;
        AdsQuery.ParamByName('ptskey').AsInteger := AdoQuery.FieldByName('ptskey').AsInteger;
        AdsQuery.ParamByName('ptphon').AsString := AdoQuery.FieldByName('ptphon').AsString;
        AdsQuery.ParamByName('ptsnam').AsString := AdoQuery.FieldByName('ptsnam').AsString;
        AdsQuery.ParamByName('ptpmth').AsString := AdoQuery.FieldByName('ptpmth').AsString;
        AdsQuery.ParamByName('ptstyp').AsString := AdoQuery.FieldByName('ptstyp').AsString;
        AdsQuery.ParamByName('ptpo').AsString := AdoQuery.FieldByName('ptpo#').AsString;
        AdsQuery.ParamByName('ptrec').AsInteger := AdoQuery.FieldByName('ptrec#').AsInteger;
        AdsQuery.ParamByName('ptplvl').AsInteger := AdoQuery.FieldByName('ptplvl').AsInteger;
        AdsQuery.ParamByName('ptptot').AsCurrency := AdoQuery.FieldByName('ptptot').AsCurrency;
        AdsQuery.ParamByName('ptshpt').AsCurrency := AdoQuery.FieldByName('ptshpt').AsCurrency;
        AdsQuery.ParamByName('ptspod').AsCurrency := AdoQuery.FieldByName('ptspod').AsCurrency;
        AdsQuery.ParamByName('ptstax').AsCurrency := AdoQuery.FieldByName('ptstax').AsCurrency;
        AdsQuery.ParamByName('ptstax2').AsCurrency := AdoQuery.FieldByName('ptstax2').AsCurrency;
        AdsQuery.ParamByName('ptstax3').AsCurrency := AdoQuery.FieldByName('ptstax3').AsCurrency;
        AdsQuery.ParamByName('ptstax4').AsCurrency := AdoQuery.FieldByName('ptstax4').AsCurrency;
        AdsQuery.ParamByName('ptgros').AsCurrency := AdoQuery.FieldByName('ptgros').AsCurrency;
        AdsQuery.ParamByName('ptcreate').AsDateTime := AdoQuery.FieldByName('ptcreate').AsDateTime;
        AdsQuery.ParamByName('ptcthold').AsDateTime := AdoQuery.FieldByName('ptcthold').AsDateTime;
        AdsQuery.ParamByName('ptauth').AsString := AdoQuery.FieldByName('ptauth#').AsString;
        AdsQuery.ParamByName('ptauthid').AsString := AdoQuery.FieldByName('ptauthid').AsString;
        AdsQuery.ParamByName('ptdlvmth').AsInteger := AdoQuery.FieldByName('ptdlvmth').AsInteger;
        AdsQuery.ParamByName('ptshipseq').AsInteger := AdoQuery.FieldByName('ptshipseq').AsInteger;
        AdsQuery.ParamByName('pttaxgo').AsInteger := AdoQuery.FieldByName('pttaxgo').AsInteger;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaPDPTHDR'')');
      ScrapeCountQuery(' rydedata.pdpthdr ' +
          'where trim(ptinv#) <> '''' ' +
          '  and ptco# in (''RY1'',''RY2'',''RY3'') ' +
          '  and trim(ptinv#) not in ( ' +
          '    select trim(ptinv#) ' +
          '    from RYDEDATA.PDPTHDR ' +
          '    group by trim(ptinv#) ' +
          '      having count(*) > 1)', ' stgArkonaPDPTHDR');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('PDPTHDR.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;



procedure TDataModule1.BOPMAST;
var
  proc: string;
begin
  proc := 'BOPMAST';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
//      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaBOPMAST'')');
      ExecuteQuery(AdsQuery, 'delete from stgArkonaBOPMAST');
      PrepareQuery(AdsQuery, ' insert into stgArkonaBOPMAST(' +
          'BMCO#, BMKEY, BMSTAT, BMTYPE, BMVTYP, BMFRAN, BMWHSL, BMSTK#, ' +
          'BMVIN, BMBUYR, BMCBYR, BMSNAM, BMPRIC, BMDOWN, BMTRAD, BMTACV, ' +
          'BMTDPO, BMAPR, BMTERM, BMITRM, BMDYFP, BMPFRQ, BMPYMT, BMAMTF, ' +
          'BMDTOR, BMDTFP, BMDTLP, BMDTSE, BMTAXG, BMLSRC, BMSSRC, BMISRC, ' +
          'BMGSRC, BMSRVC, BMSVCA, BMSDED, BMSMIL, BMSMOS, BMSCST, BMSCSD, ' +
          'BMSCED, BMSVAC, BMCLPM, BMLVPM, BMAHPM, BMGAPP, BMGCST, BMCLCD, ' +
          'BMAHCD, BMGPCD, BMTAX1, BMTAX2, BMTAX3, BMTAX4, BMTAX5, BMBRAT, ' +
          'BMRHBP, BMTRD#, BMTSK#, BMSVC#, BMCLC#, BMAHC#, BMGAP#, BMFVND, ' +
          'BMPDIC, BMPDAL, BMPDP#, BMPDED, BMPDAN, BMPDAA, BMPDAC, BMPDAS, ' +
          'BMPDAZ, BMPDPN, BMPDCL, BMPDCP, BMPDFT, BMPDCD, BMPDPD, BMPDFD, ' +
          'BMPDVU, BMPDPM, BMATOT, BMFTOT, BMTPAY, BMICHG, BMVCST, BMICST, ' +
          'BMADDC, BMHLDB, BMPACK, BMCGRS, BMINCT, BMRBTE, BMODOM, BMPAMT, ' +
          'BMPBDT, BMPCKF, BMNPPY, BMLRES, BMARES, BMGRES, BMPRES, BMMRES, ' +
          'BMSRES, BMFRES, BMTRES, BMPSLP, BMSLP2, BMDDWN, BMDDDD, BMLOF, ' +
          'BMLOFO, BMEAPR, BMMSRP, BMLPRC, BMCAPC, BMCAPR, BMLAPR, BMLFAC, ' +
          'BMLTRM, BMRESP, BMRESA, BMNETR, BMTDEP, BMMIPY, BMMPYA, BMEMRT, ' +
          'BMEMR2, BMEMCG, BMACQF, BMLPAY, BMLPYM, BMLSTX, BMLCHG, BMCRTX, ' +
          'BMCRT1, BMCRT2, BMCRT3, BMCRT4, BMCRT6, BMSECD, BMDPOS, BMCSHR, ' +
          'BMLFBR, BMITOT, BMCTAX, BMSDOR, BMAFOR, BMFETO, BMCSTX, BMAPYL, ' +
          'BMRAT1, BMRAT2, BMRAT3, BMRAT4, BMRAT6, BMEXT1, BMEXT2, BMEXT3, ' +
          'BMEXT4, BMTOR1, BMTOR2, BMTOR3, BMTOR4, BMTOR6, BMFROR, BMCOVR, ' +
          'BMHOVR, BMTOVR, BMRBDP, BMCSEL, BMCIOP, BMSACT, BMUNWD, BMDOC#, ' +
          'BMBAGE, BMCBAG, BMLCOV, BMHCOV, BMITDT, BMLTXG, BMCBLP, BMDTSV, ' +
          'BMDTTP, BMCCRT, BMRDWN, BMTDWN, BMTCRD, BMGCAP, BMDINCT, BMPRNT, ' +
          'BMNVDR, BMDTAPRV, BMDTCAP, BMEMCB, BMEMPB, BMEMRB, BMMPYB, BMMYAB, ' +
          'BMNTRB, BMRSAB, BMRSPB, B9AMTAX, B9EPAY, B9FPAY, B9FPCODE, B9HTERM, ' +
          'B9LPTAX, B9LTRAD, B9ONETTR, B9ONTEQ, B9PDP#, B9PEAMT, B9PETAX, ' +
          'B9SCTAX, B9THCOV, B9TXBASE, B9WKIP, BMLDTLP, BMRAT5, BMRAT7, ' +
          'BMOLPM, BMOCDT, BMLLOPT, BMRFTX1, BMRFTX2, BMPYXINS, BMPBKR, ' +
          'BMBRRI, BMCRRI, BMBGEN, BMCGEN, BMCICD, BMCIPM, BMLECD, BMLEPM, ' +
          'BMUFTX1, BMUFTX2, BMRESIN, BMDISEL, BMREMIC, BMPRMPCT, BMRAT8, ' +
          'BMDELVDT, BMIMILE, BMIMRT, BMIMCG, BMLPYREM, BMFIGRS, BMHGRS, ' +
          'BMMTHGAPP, BMIMFLAG, BMIMTHLD, BMGAPTAX) ' +
          'values(' +
          ':BMCO, :BMKEY, :BMSTAT, :BMTYPE, :BMVTYP, :BMFRAN, :BMWHSL, ' +
          ':BMSTK, :BMVIN, :BMBUYR, :BMCBYR, :BMSNAM, :BMPRIC, :BMDOWN, ' +
          ':BMTRAD, :BMTACV, :BMTDPO, :BMAPR, :BMTERM, :BMITRM, :BMDYFP, ' +
          ':BMPFRQ, :BMPYMT, :BMAMTF, :BMDTOR, :BMDTFP, :BMDTLP, :BMDTSE, ' +
          ':BMTAXG, :BMLSRC, :BMSSRC, :BMISRC, :BMGSRC, :BMSRVC, :BMSVCA, ' +
          ':BMSDED, :BMSMIL, :BMSMOS, :BMSCST, :BMSCSD, :BMSCED, :BMSVAC, ' +
          ':BMCLPM, :BMLVPM, :BMAHPM, :BMGAPP, :BMGCST, :BMCLCD, :BMAHCD, ' +
          ':BMGPCD, :BMTAX1, :BMTAX2, :BMTAX3, :BMTAX4, :BMTAX5, :BMBRAT, ' +
          ':BMRHBP, :BMTRD, :BMTSK, :BMSVC, :BMCLC, :BMAHC, :BMGAP, :BMFVND, ' +
          ':BMPDIC, :BMPDAL, :BMPDP, :BMPDED, :BMPDAN, :BMPDAA, :BMPDAC, ' +
          ':BMPDAS, :BMPDAZ, :BMPDPN, :BMPDCL, :BMPDCP, :BMPDFT, :BMPDCD, ' +
          ':BMPDPD, :BMPDFD, :BMPDVU, :BMPDPM, :BMATOT, :BMFTOT, :BMTPAY, ' +
          ':BMICHG, :BMVCST, :BMICST, :BMADDC, :BMHLDB, :BMPACK, :BMCGRS, ' +
          ':BMINCT, :BMRBTE, :BMODOM, :BMPAMT, :BMPBDT, :BMPCKF, :BMNPPY, ' +
          ':BMLRES, :BMARES, :BMGRES, :BMPRES, :BMMRES, :BMSRES, :BMFRES, ' +
          ':BMTRES, :BMPSLP, :BMSLP2, :BMDDWN, :BMDDDD, :BMLOF, :BMLOFO, ' +
          ':BMEAPR, :BMMSRP, :BMLPRC, :BMCAPC, :BMCAPR, :BMLAPR, :BMLFAC, ' +
          ':BMLTRM, :BMRESP, :BMRESA, :BMNETR, :BMTDEP, :BMMIPY, :BMMPYA, ' +
          ':BMEMRT, :BMEMR2, :BMEMCG, :BMACQF, :BMLPAY, :BMLPYM, :BMLSTX, ' +
          ':BMLCHG, :BMCRTX, :BMCRT1, :BMCRT2, :BMCRT3, :BMCRT4, :BMCRT6, ' +
          ':BMSECD, :BMDPOS, :BMCSHR, :BMLFBR, :BMITOT, :BMCTAX, :BMSDOR, ' +
          ':BMAFOR, :BMFETO, :BMCSTX, :BMAPYL, :BMRAT1, :BMRAT2, :BMRAT3, ' +
          ':BMRAT4, :BMRAT6, :BMEXT1, :BMEXT2, :BMEXT3, :BMEXT4, :BMTOR1, ' +
          ':BMTOR2, :BMTOR3, :BMTOR4, :BMTOR6, :BMFROR, :BMCOVR, :BMHOVR, ' +
          ':BMTOVR, :BMRBDP, :BMCSEL, :BMCIOP, :BMSACT, :BMUNWD, :BMDOC, ' +
          ':BMBAGE, :BMCBAG, :BMLCOV, :BMHCOV, :BMITDT, :BMLTXG, :BMCBLP, ' +
          ':BMDTSV, :BMDTTP, :BMCCRT, :BMRDWN, :BMTDWN, :BMTCRD, :BMGCAP, ' +
          ':BMDINCT, :BMPRNT, :BMNVDR, :BMDTAPRV, :BMDTCAP, :BMEMCB, ' +
          ':BMEMPB, :BMEMRB, :BMMPYB, :BMMYAB, :BMNTRB, :BMRSAB, :BMRSPB, ' +
          ':B9AMTAX, :B9EPAY, :B9FPAY, :B9FPCODE, :B9HTERM, :B9LPTAX, ' +
          ':B9LTRAD, :B9ONETTR, :B9ONTEQ, :B9PDP, :B9PEAMT, :B9PETAX, ' +
          ':B9SCTAX, :B9THCOV, :B9TXBASE, :B9WKIP, :BMLDTLP, :BMRAT5, ' +
          ':BMRAT7, :BMOLPM, :BMOCDT, :BMLLOPT, :BMRFTX1, :BMRFTX2, ' +
          ':BMPYXINS, :BMPBKR, :BMBRRI, :BMCRRI, :BMBGEN, :BMCGEN, ' +
          ':BMCICD, :BMCIPM, :BMLECD, :BMLEPM, :BMUFTX1, :BMUFTX2, ' +
          ':BMRESIN, :BMDISEL, :BMREMIC, :BMPRMPCT, :BMRAT8, :BMDELVDT, ' +
          ':BMIMILE, :BMIMRT, :BMIMCG, :BMLPYREM, :BMFIGRS, :BMHGRS, ' +
          ':BMMTHGAPP, :BMIMFLAG, :BMIMTHLD, :BMGAPTAX)');
      OpenQuery(AdoQuery, 'select ' +
          '  BMCO#, BMKEY, BMSTAT, BMTYPE, BMVTYP, BMFRAN, ' +
          '  BMWHSL, BMSTK#, BMVIN, BMBUYR, BMCBYR, BMSNAM, ' +
          '  BMPRIC, BMDOWN, BMTRAD, BMTACV, BMTDPO, BMAPR, ' +
          '  BMTERM, BMITRM, BMDYFP, BMPFRQ, BMPYMT, BMAMTF, ' +
          '  case ' +
          '    when BMDTOR = 0 then cast(''9999-12-31'' as date) ' +
          '    else cast(left(digits(BMDTOR), 4) || ''-'' || substr(digits(BMDTOR), 5, 2) || ''-'' || substr(digits(BMDTOR), 7, 2) as date) ' +
          '  end as BMDTOR, ' +
          '  case ' +
          '    when BMDTFP = 0 then cast(''9999-12-31'' as date) ' +
          '    else cast(left(digits(BMDTFP), 4) || ''-'' || substr(digits(BMDTFP), 5, 2) || ''-'' || substr(digits(BMDTFP), 7, 2) as date) ' +
          '  end as BMDTFP, ' +
          '  case ' +
          '    when BMDTLP = 0 then cast(''9999-12-31'' as date) ' +
          '    else cast(left(digits(BMDTLP), 4) || ''-'' || substr(digits(BMDTLP), 5, 2) || ''-'' || substr(digits(BMDTLP), 7, 2) as date) ' +
          '  end as BMDTLP, ' +
          '  case ' +
          '    when BMDTSE = 0 then cast(''9999-12-31'' as date) ' +
          '    else cast(left(digits(BMDTSE), 4) || ''-'' || substr(digits(BMDTSE), 5, 2) || ''-'' || substr(digits(BMDTSE), 7, 2) as date) ' +
          '  end as BMDTSE, ' +
          '  BMTAXG, BMLSRC, BMSSRC, BMISRC, BMGSRC, BMSRVC, ' +
          '  BMSVCA, BMSDED, BMSMIL, BMSMOS, BMSCST, ' +
          '  case ' +
          '    when BMSCSD = 0 then cast(''9999-12-31'' as date) ' +
          '    else cast(left(digits(BMSCSD), 4) || ''-'' || substr(digits(BMSCSD), 5, 2) || ''-'' || substr(digits(BMSCSD), 7, 2) as date) ' +
          '  end as BMSCSD, ' +
          '  case ' +
          '    when BMSCED = 0 then cast(''9999-12-31'' as date) ' +
          '    else cast(left(digits(BMSCED), 4) || ''-'' || substr(digits(BMSCED), 5, 2) || ''-'' || substr(digits(BMSCED), 7, 2) as date) ' +
          '  end as BMSCED, ' +
          '  BMSVAC, BMCLPM, BMLVPM, BMAHPM, BMGAPP, ' +
          '  BMGCST, BMCLCD, BMAHCD, BMGPCD, BMTAX1, ' +
          '  BMTAX2, BMTAX3, BMTAX4, BMTAX5, BMBRAT, ' +
          '  BMRHBP, BMTRD#, BMTSK#, BMSVC#, BMCLC#, ' +
          '  BMAHC#, BMGAP#, BMFVND, BMPDIC, BMPDAL, BMPDP#, ' +
          '  case ' +
          '    when BMPDED = 0 then cast(''9999-12-31'' as date) ' +
          '    else cast(left(digits(BMPDED), 4) || ''-'' || substr(digits(BMPDED), 5, 2) || ''-'' || substr(digits(BMPDED), 7, 2) as date) ' +
          '  end as BMPDED, ' +
          '  BMPDAN, BMPDAA, BMPDAC, BMPDAS, BMPDAZ, ' +
          '  BMPDPN, BMPDCL, BMPDCP, BMPDFT, BMPDCD, ' +
          '  BMPDPD, BMPDFD, BMPDVU, BMPDPM, BMATOT, ' +
          '  BMFTOT, BMTPAY, BMICHG, BMVCST, BMICST, ' +
          '  BMADDC, BMHLDB, BMPACK, BMCGRS, BMINCT, ' +
          '  BMRBTE, BMODOM, BMPAMT, ' +
          '  case ' +
          '    when BMPBDT = 0 then cast(''9999-12-31'' as date) ' +
          '    else cast(left(digits(BMPBDT), 4) || ''-'' || substr(digits(BMPBDT), 5, 2) || ''-'' || substr(digits(BMPBDT), 7, 2) as date) ' +
          '  end as BMPBDT, ' +
          '  BMPCKF, BMNPPY, BMLRES, BMARES, BMGRES, ' +
          '  BMPRES, BMMRES, BMSRES, BMFRES, BMTRES, ' +
          '  BMPSLP, BMSLP2, BMDDWN, ' +
          '  case ' +
          '    when BMDDDD = 0 then cast(''9999-12-31'' as date) ' +
          '    else cast(left(digits(BMDDDD), 4) || ''-'' || substr(digits(BMDDDD), 5, 2) || ''-'' || substr(digits(BMDDDD), 7, 2) as date) ' +
          '  end as BMDDDD, ' +
          '  BMLOF, BMLOFO, BMEAPR, BMMSRP, BMLPRC, ' +
          '  BMCAPC, BMCAPR, BMLAPR, BMLFAC, BMLTRM, ' +
          '  BMRESP, BMRESA, BMNETR, BMTDEP, BMMIPY, ' +
          '  BMMPYA, BMEMRT, BMEMR2, BMEMCG, BMACQF, ' +
          '  BMLPAY, BMLPYM, BMLSTX, BMLCHG, BMCRTX, ' +
          '  BMCRT1, BMCRT2, BMCRT3, BMCRT4, BMCRT6, ' +
          '  BMSECD, BMDPOS, BMCSHR, BMLFBR, BMITOT, ' +
          '  BMCTAX, BMSDOR, BMAFOR, BMFETO, BMCSTX, ' +
          '  BMAPYL, BMRAT1, BMRAT2, BMRAT3, BMRAT4, ' +
          '  BMRAT6, BMEXT1, BMEXT2, BMEXT3, BMEXT4, ' +
          '  BMTOR1, BMTOR2, BMTOR3, BMTOR4, BMTOR6, ' +
          '  BMFROR, BMCOVR, BMHOVR, BMTOVR, BMRBDP, ' +
          '  BMCSEL, BMCIOP, BMSACT, BMUNWD, BMDOC#, ' +
          '  BMBAGE, BMCBAG, BMLCOV, BMHCOV, ' +
          '  case ' +
          '    when BMITDT = 0 then cast(''9999-12-31'' as date) ' +
          '    else cast(left(digits(BMITDT), 4) || ''-'' || substr(digits(BMITDT), 5, 2) || ''-'' || substr(digits(BMITDT), 7, 2) as date) ' +
          '  end as BMITDT, ' +
          '  BMLTXG, BMCBLP, ' +
          '  case ' +
          '    when BMDTSV = 0 then cast(''9999-12-31'' as date) ' +
          '    else cast(left(digits(BMDTSV), 4) || ''-'' || substr(digits(BMDTSV), 5, 2) || ''-'' || substr(digits(BMDTSV), 7, 2) as date) ' +
          '  end as BMDTSV, ' +
          '  case ' +
          '    when BMDTTP = 0 then cast(''9999-12-31'' as date) ' +
          '    else cast(left(digits(BMDTTP), 4) || ''-'' || substr(digits(BMDTTP), 5, 2) || ''-'' || substr(digits(BMDTTP), 7, 2) as date) ' +
          '  end as BMDTTP, ' +
          '  BMCCRT,BMRDWN, BMTDWN, BMTCRD, ' +
          '  BMGCAP, BMDINCT,BMPRNT, BMNVDR, ' +
          '  case ' +
          '     when BMDTAPRV < date(''1899-12-31'') then date(''9999-12-31'') ' +
          '     else BMDTAPRV ' +
          '  end as BMDTAPRV, ' +
          '  case ' +
          '     when BMDTCAP < date(''1899-12-31'') then date(''9999-12-31'') ' +
          '     else BMDTCAP ' +
          '  end as BMDTCAP, ' +
          '  BMEMCB, BMEMPB, BMEMRB, BMMPYB, ' +
          '  BMMYAB, BMNTRB, BMRSAB, BMRSPB, B9AMTAX, ' +
          '  B9EPAY, B9FPAY, B9FPCODE, B9HTERM, B9LPTAX, ' +
          '  B9LTRAD, B9ONETTR, B9ONTEQ, B9PDP#, ' +
          '  B9PEAMT, B9PETAX, B9SCTAX, B9THCOV, ' +
          '  B9TXBASE, B9WKIP, ' +
          '  case ' +
          '    when BMLDTLP = 0 then cast(''9999-12-31'' as date) ' +
          '    else cast(left(digits(BMLDTLP), 4) || ''-'' || substr(digits(BMLDTLP), 5, 2) || ''-'' || substr(digits(BMLDTLP), 7, 2) as date) ' +
          '  end as BMLDTLP, ' +
          '  BMRAT5, BMRAT7, BMOLPM, BMOCDT, BMLLOPT, ' +
          '  BMRFTX1, BMRFTX2, BMPYXINS, BMPBKR, BMBRRI, ' +
          '  BMCRRI, BMBGEN, BMCGEN, BMCICD, BMCIPM, ' +
          '  BMLECD, BMLEPM, BMUFTX1, BMUFTX2, BMRESIN, ' +
          '  BMDISEL, BMREMIC, BMPRMPCT, BMRAT8, BMDELVDT, ' +
          '  BMIMILE, BMIMRT, BMIMCG, BMLPYREM, BMFIGRS, ' +
          '  BMHGRS, BMMTHGAPP, BMIMFLAG, BMIMTHLD, BMGAPTAX ' +
          'from rydedata.bopmast b1 ');
//          'inner join ( ' +
//          '    select bmstk# as stk#, bmvin as vin, max(bmkey) as thekey ' +
//          '    from rydedata.bopmast ' +
//          '    where bmstat = ''U'' ' +
//          '    group by bmstk#, bmvin) b2 on b1.bmstk# = b2.stk# ' +
//          '  and b1.bmvin = b2.vin ' +
//          '  and b1.bmkey = b2.thekey');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('bmco').AsString := AdoQuery.FieldByName('bmco#').AsString;
        AdsQuery.ParamByName('bmkey').AsInteger := AdoQuery.FieldByName('bmkey').AsInteger;
        AdsQuery.ParamByName('bmstat').AsString := AdoQuery.FieldByName('bmstat').AsString;
        AdsQuery.ParamByName('bmtype').AsString := AdoQuery.FieldByName('bmtype').AsString;
        AdsQuery.ParamByName('bmvtyp').AsString := AdoQuery.FieldByName('bmvtyp').AsString;
        AdsQuery.ParamByName('bmfran').AsString := AdoQuery.FieldByName('bmfran').AsString;
        AdsQuery.ParamByName('bmwhsl').AsString := AdoQuery.FieldByName('bmwhsl').AsString;
        AdsQuery.ParamByName('bmstk').AsString := AdoQuery.FieldByName('bmstk#').AsString;
        AdsQuery.ParamByName('bmvin').AsString := AdoQuery.FieldByName('bmvin').AsString;
        AdsQuery.ParamByName('bmbuyr').AsInteger := AdoQuery.FieldByName('bmbuyr').AsInteger;
        AdsQuery.ParamByName('bmcbyr').AsInteger := AdoQuery.FieldByName('bmcbyr').AsInteger;
        AdsQuery.ParamByName('bmsnam').AsString := AdoQuery.FieldByName('bmsnam').AsString;
        AdsQuery.ParamByName('bmpric').AsCurrency := AdoQuery.FieldByName('bmpric').AsCurrency;
        AdsQuery.ParamByName('bmdown').AsCurrency := AdoQuery.FieldByName('bmdown').AsCurrency;
        AdsQuery.ParamByName('bmtrad').AsCurrency := AdoQuery.FieldByName('bmtrad').AsCurrency;
        AdsQuery.ParamByName('bmtacv').AsCurrency := AdoQuery.FieldByName('bmtacv').AsCurrency;
        AdsQuery.ParamByName('bmtdpo').AsCurrency := AdoQuery.FieldByName('bmtdpo').AsCurrency;
        AdsQuery.ParamByName('bmapr').AsFloat := AdoQuery.FieldByName('bmapr').AsFloat;
        AdsQuery.ParamByName('bmterm').AsInteger := AdoQuery.FieldByName('bmterm').AsInteger;
        AdsQuery.ParamByName('bmitrm').AsInteger := AdoQuery.FieldByName('bmitrm').AsInteger;
        AdsQuery.ParamByName('bmdyfp').AsInteger := AdoQuery.FieldByName('bmdyfp').AsInteger;
        AdsQuery.ParamByName('bmpfrq').AsString := AdoQuery.FieldByName('bmpfrq').AsString;
        AdsQuery.ParamByName('bmpymt').AsCurrency := AdoQuery.FieldByName('bmpymt').AsCurrency;
        AdsQuery.ParamByName('bmamtf').AsCurrency := AdoQuery.FieldByName('bmamtf').AsCurrency;
        AdsQuery.ParamByName('bmdtor').AsDateTime := AdoQuery.FieldByName('bmdtor').AsDateTime;
        AdsQuery.ParamByName('bmdtfp').AsDateTime := AdoQuery.FieldByName('bmdtfp').AsDateTime;
        AdsQuery.ParamByName('bmdtlp').AsDateTime := AdoQuery.FieldByName('bmdtlp').AsDateTime;
        AdsQuery.ParamByName('bmdtse').AsDateTime := AdoQuery.FieldByName('bmdtse').AsDateTime;
        AdsQuery.ParamByName('bmtaxg').AsInteger := AdoQuery.FieldByName('bmtaxg').AsInteger;
        AdsQuery.ParamByName('bmlsrc').AsInteger := AdoQuery.FieldByName('bmlsrc').AsInteger;
        AdsQuery.ParamByName('bmssrc').AsInteger := AdoQuery.FieldByName('bmssrc').AsInteger;
        AdsQuery.ParamByName('bmisrc').AsInteger := AdoQuery.FieldByName('bmisrc').AsInteger;
        AdsQuery.ParamByName('bmgsrc').AsInteger := AdoQuery.FieldByName('bmgsrc').AsInteger;
        AdsQuery.ParamByName('bmsrvc').AsInteger := AdoQuery.FieldByName('bmsrvc').AsInteger;
        AdsQuery.ParamByName('bmsvca').AsCurrency := AdoQuery.FieldByName('bmsvca').AsCurrency;
        AdsQuery.ParamByName('bmsded').AsCurrency := AdoQuery.FieldByName('bmsded').AsCurrency;
        AdsQuery.ParamByName('bmsmil').AsInteger := AdoQuery.FieldByName('bmsmil').AsInteger;
        AdsQuery.ParamByName('bmsmos').AsInteger := AdoQuery.FieldByName('bmsmos').AsInteger;
        AdsQuery.ParamByName('bmscst').AsCurrency := AdoQuery.FieldByName('bmscst').AsCurrency;
        AdsQuery.ParamByName('bmscsd').AsDateTime := AdoQuery.FieldByName('bmscsd').AsDateTime;
        AdsQuery.ParamByName('bmsced').AsDateTime := AdoQuery.FieldByName('bmsced').AsDateTime;
        AdsQuery.ParamByName('bmsvac').AsString := AdoQuery.FieldByName('bmsvac').AsString;
        AdsQuery.ParamByName('bmclpm').AsCurrency := AdoQuery.FieldByName('bmclpm').AsCurrency;
        AdsQuery.ParamByName('bmlvpm').AsCurrency := AdoQuery.FieldByName('bmlvpm').AsCurrency;
        AdsQuery.ParamByName('bmahpm').AsCurrency := AdoQuery.FieldByName('bmahpm').AsCurrency;
        AdsQuery.ParamByName('bmgapp').AsCurrency := AdoQuery.FieldByName('bmgapp').AsCurrency;
        AdsQuery.ParamByName('bmgcst').AsCurrency := AdoQuery.FieldByName('bmgcst').AsCurrency;
        AdsQuery.ParamByName('bmclcd').AsString := AdoQuery.FieldByName('bmclcd').AsString;
        AdsQuery.ParamByName('bmahcd').AsString := AdoQuery.FieldByName('bmahcd').AsString;
        AdsQuery.ParamByName('bmgpcd').AsString := AdoQuery.FieldByName('bmgpcd').AsString;
        AdsQuery.ParamByName('bmtax1').AsCurrency := AdoQuery.FieldByName('bmtax1').AsCurrency;
        AdsQuery.ParamByName('bmtax2').AsCurrency := AdoQuery.FieldByName('bmtax2').AsCurrency;
        AdsQuery.ParamByName('bmtax3').AsCurrency := AdoQuery.FieldByName('bmtax3').AsCurrency;
        AdsQuery.ParamByName('bmtax4').AsCurrency := AdoQuery.FieldByName('bmtax4').AsCurrency;
        AdsQuery.ParamByName('bmtax5').AsCurrency := AdoQuery.FieldByName('bmtax5').AsCurrency;
        AdsQuery.ParamByName('bmbrat').AsFloat := AdoQuery.FieldByName('bmbrat').AsFloat;
        AdsQuery.ParamByName('bmrhbp').AsFloat := AdoQuery.FieldByName('bmrhbp').AsFloat;
        AdsQuery.ParamByName('bmtrd').AsInteger := AdoQuery.FieldByName('bmtrd#').AsInteger;
        AdsQuery.ParamByName('bmtsk').AsString := AdoQuery.FieldByName('bmtsk#').AsString;
        AdsQuery.ParamByName('bmsvc').AsString := AdoQuery.FieldByName('bmsvc#').AsString;
        AdsQuery.ParamByName('bmclc').AsString := AdoQuery.FieldByName('bmclc#').AsString;
        AdsQuery.ParamByName('bmahc').AsString := AdoQuery.FieldByName('bmahc#').AsString;
        AdsQuery.ParamByName('bmgap').AsString := AdoQuery.FieldByName('bmgap#').AsString;
        AdsQuery.ParamByName('bmfvnd').AsString := AdoQuery.FieldByName('bmfvnd').AsString;
        AdsQuery.ParamByName('bmpdic').AsString := AdoQuery.FieldByName('bmpdic').AsString;
        AdsQuery.ParamByName('bmpdal').AsString := AdoQuery.FieldByName('bmpdal').AsString;
        AdsQuery.ParamByName('bmpdp').AsString := AdoQuery.FieldByName('bmpdp#').AsString;
        AdsQuery.ParamByName('bmpded').AsDateTime := AdoQuery.FieldByName('bmpded').AsDateTime;
        AdsQuery.ParamByName('bmpdan').AsString := AdoQuery.FieldByName('bmpdan').AsString;
        AdsQuery.ParamByName('bmpdaa').AsString := AdoQuery.FieldByName('bmpdaa').AsString;
        AdsQuery.ParamByName('bmpdac').AsString := AdoQuery.FieldByName('bmpdac').AsString;
        AdsQuery.ParamByName('bmpdas').AsString := AdoQuery.FieldByName('bmpdas').AsString;
        AdsQuery.ParamByName('bmpdaz').AsString := AdoQuery.FieldByName('bmpdaz').AsString;
        AdsQuery.ParamByName('bmpdpn').AsString := AdoQuery.FieldByName('bmpdpn').AsString;
        AdsQuery.ParamByName('bmpdcl').AsString := AdoQuery.FieldByName('bmpdcl').AsString;
        AdsQuery.ParamByName('bmpdcp').AsString := AdoQuery.FieldByName('bmpdcp').AsString;
        AdsQuery.ParamByName('bmpdft').AsString := AdoQuery.FieldByName('bmpdft').AsString;
        AdsQuery.ParamByName('bmpdcd').AsInteger := AdoQuery.FieldByName('bmpdcd').AsInteger;
        AdsQuery.ParamByName('bmpdpd').AsInteger := AdoQuery.FieldByName('bmpdpd').AsInteger;
        AdsQuery.ParamByName('bmpdfd').AsInteger := AdoQuery.FieldByName('bmpdfd').AsInteger;
        AdsQuery.ParamByName('bmpdvu').AsString := AdoQuery.FieldByName('bmpdvu').AsString;
        AdsQuery.ParamByName('bmpdpm').AsInteger := AdoQuery.FieldByName('bmpdpm').AsInteger;
        AdsQuery.ParamByName('bmatot').AsCurrency := AdoQuery.FieldByName('bmatot').AsCurrency;
        AdsQuery.ParamByName('bmftot').AsCurrency := AdoQuery.FieldByName('bmftot').AsCurrency;
        AdsQuery.ParamByName('bmtpay').AsCurrency := AdoQuery.FieldByName('bmtpay').AsCurrency;
        AdsQuery.ParamByName('bmichg').AsCurrency := AdoQuery.FieldByName('bmichg').AsCurrency;
        AdsQuery.ParamByName('bmvcst').AsCurrency := AdoQuery.FieldByName('bmvcst').AsCurrency;
        AdsQuery.ParamByName('bmicst').AsCurrency := AdoQuery.FieldByName('bmicst').AsCurrency;
        AdsQuery.ParamByName('bmaddc').AsCurrency := AdoQuery.FieldByName('bmaddc').AsCurrency;
        AdsQuery.ParamByName('bmhldb').AsCurrency := AdoQuery.FieldByName('bmhldb').AsCurrency;
        AdsQuery.ParamByName('bmpack').AsCurrency := AdoQuery.FieldByName('bmpack').AsCurrency;
        AdsQuery.ParamByName('bmcgrs').AsCurrency := AdoQuery.FieldByName('bmcgrs').AsCurrency;
        AdsQuery.ParamByName('bminct').AsCurrency := AdoQuery.FieldByName('bminct').AsCurrency;
        AdsQuery.ParamByName('bmrbte').AsCurrency := AdoQuery.FieldByName('bmrbte').AsCurrency;
        AdsQuery.ParamByName('bmodom').AsInteger := AdoQuery.FieldByName('bmodom').AsInteger;
        AdsQuery.ParamByName('bmpamt').AsCurrency := AdoQuery.FieldByName('bmpamt').AsCurrency;
        AdsQuery.ParamByName('bmpbdt').AsDateTime := AdoQuery.FieldByName('bmpbdt').AsDateTime;
        AdsQuery.ParamByName('bmpckf').AsString := AdoQuery.FieldByName('bmpckf').AsString;
        AdsQuery.ParamByName('bmnppy').AsInteger := AdoQuery.FieldByName('bmnppy').AsInteger;
        AdsQuery.ParamByName('bmlres').AsCurrency := AdoQuery.FieldByName('bmlres').AsCurrency;
        AdsQuery.ParamByName('bmares').AsCurrency := AdoQuery.FieldByName('bmares').AsCurrency;
        AdsQuery.ParamByName('bmgres').AsCurrency := AdoQuery.FieldByName('bmgres').AsCurrency;
        AdsQuery.ParamByName('bmpres').AsCurrency := AdoQuery.FieldByName('bmpres').AsCurrency;
        AdsQuery.ParamByName('bmmres').AsCurrency := AdoQuery.FieldByName('bmmres').AsCurrency;
        AdsQuery.ParamByName('bmsres').AsCurrency := AdoQuery.FieldByName('bmsres').AsCurrency;
        AdsQuery.ParamByName('bmfres').AsCurrency := AdoQuery.FieldByName('bmfres').AsCurrency;
        AdsQuery.ParamByName('bmtres').AsCurrency := AdoQuery.FieldByName('bmtres').AsCurrency;
        AdsQuery.ParamByName('bmpslp').AsString := AdoQuery.FieldByName('bmpslp').AsString;
        AdsQuery.ParamByName('bmslp2').AsString := AdoQuery.FieldByName('bmslp2').AsString;
        AdsQuery.ParamByName('bmddwn').AsCurrency := AdoQuery.FieldByName('bmddwn').AsCurrency;
        AdsQuery.ParamByName('bmdddd').AsDateTime := AdoQuery.FieldByName('bmdddd').AsDateTime;
        AdsQuery.ParamByName('bmlof').AsCurrency := AdoQuery.FieldByName('bmlof').AsCurrency;
        AdsQuery.ParamByName('bmlofo').AsString := AdoQuery.FieldByName('bmlofo').AsString;
        AdsQuery.ParamByName('bmeapr').AsFloat := AdoQuery.FieldByName('bmeapr').AsFloat;
        AdsQuery.ParamByName('bmmsrp').AsCurrency := AdoQuery.FieldByName('bmmsrp').AsCurrency;
        AdsQuery.ParamByName('bmlprc').AsCurrency := AdoQuery.FieldByName('bmlprc').AsCurrency;
        AdsQuery.ParamByName('bmcapc').AsCurrency := AdoQuery.FieldByName('bmcapc').AsCurrency;
        AdsQuery.ParamByName('bmcapr').AsCurrency := AdoQuery.FieldByName('bmcapr').AsCurrency;
        AdsQuery.ParamByName('bmlapr').AsInteger := AdoQuery.FieldByName('bmlapr').AsInteger;
        AdsQuery.ParamByName('bmlfac').AsFloat := AdoQuery.FieldByName('bmlfac').AsFloat;
        AdsQuery.ParamByName('bmltrm').AsInteger := AdoQuery.FieldByName('bmltrm').AsInteger;
        AdsQuery.ParamByName('bmresp').AsFloat := AdoQuery.FieldByName('bmresp').AsFloat;
        AdsQuery.ParamByName('bmresa').AsCurrency := AdoQuery.FieldByName('bmresa').AsCurrency;
        AdsQuery.ParamByName('bmnetr').AsCurrency := AdoQuery.FieldByName('bmnetr').AsCurrency;
        AdsQuery.ParamByName('bmtdep').AsCurrency := AdoQuery.FieldByName('bmtdep').AsCurrency;
        AdsQuery.ParamByName('bmmipy').AsInteger := AdoQuery.FieldByName('bmmipy').AsInteger;
        AdsQuery.ParamByName('bmmpya').AsInteger := AdoQuery.FieldByName('bmmpya').AsInteger;
        AdsQuery.ParamByName('bmemrt').AsInteger := AdoQuery.FieldByName('bmemrt').AsInteger;
        AdsQuery.ParamByName('bmemr2').AsInteger := AdoQuery.FieldByName('bmemr2').AsInteger;
        AdsQuery.ParamByName('bmemcg').AsCurrency := AdoQuery.FieldByName('bmemcg').AsCurrency;
        AdsQuery.ParamByName('bmacqf').AsCurrency := AdoQuery.FieldByName('bmacqf').AsCurrency;
        AdsQuery.ParamByName('bmlpay').AsCurrency := AdoQuery.FieldByName('bmlpay').AsCurrency;
        AdsQuery.ParamByName('bmlpym').AsCurrency := AdoQuery.FieldByName('bmlpym').AsCurrency;
        AdsQuery.ParamByName('bmlstx').AsCurrency := AdoQuery.FieldByName('bmlstx').AsCurrency;
        AdsQuery.ParamByName('bmlchg').AsCurrency := AdoQuery.FieldByName('bmlchg').AsCurrency;
        AdsQuery.ParamByName('bmcrtx').AsCurrency := AdoQuery.FieldByName('bmcrtx').AsCurrency;
        AdsQuery.ParamByName('bmcrt1').AsCurrency := AdoQuery.FieldByName('bmcrt1').AsCurrency;
        AdsQuery.ParamByName('bmcrt2').AsCurrency := AdoQuery.FieldByName('bmcrt2').AsCurrency;
        AdsQuery.ParamByName('bmcrt3').AsCurrency := AdoQuery.FieldByName('bmcrt3').AsCurrency;
        AdsQuery.ParamByName('bmcrt4').AsCurrency := AdoQuery.FieldByName('bmcrt4').AsCurrency;
        AdsQuery.ParamByName('bmcrt6').AsCurrency := AdoQuery.FieldByName('bmcrt6').AsCurrency;
        AdsQuery.ParamByName('bmsecd').AsCurrency := AdoQuery.FieldByName('bmsecd').AsCurrency;
        AdsQuery.ParamByName('bmdpos').AsCurrency := AdoQuery.FieldByName('bmdpos').AsCurrency;
        AdsQuery.ParamByName('bmcshr').AsCurrency := AdoQuery.FieldByName('bmcshr').AsCurrency;
        AdsQuery.ParamByName('bmlfbr').AsFloat := AdoQuery.FieldByName('bmlfbr').AsFloat;
        AdsQuery.ParamByName('bmitot').AsCurrency := AdoQuery.FieldByName('bmitot').AsCurrency;
        AdsQuery.ParamByName('bmctax').AsString := AdoQuery.FieldByName('bmctax').AsString;
        AdsQuery.ParamByName('bmsdor').AsString := AdoQuery.FieldByName('bmsdor').AsString;
        AdsQuery.ParamByName('bmafor').AsString := AdoQuery.FieldByName('bmafor').AsString;
        AdsQuery.ParamByName('bmfeto').AsString := AdoQuery.FieldByName('bmfeto').AsString;
        AdsQuery.ParamByName('bmcstx').AsString := AdoQuery.FieldByName('bmcstx').AsString;
        AdsQuery.ParamByName('bmapyl').AsString := AdoQuery.FieldByName('bmapyl').AsString;
        AdsQuery.ParamByName('bmrat1').AsFloat := AdoQuery.FieldByName('bmrat1').AsFloat;
        AdsQuery.ParamByName('bmrat2').AsFloat := AdoQuery.FieldByName('bmrat2').AsFloat;
        AdsQuery.ParamByName('bmrat3').AsFloat := AdoQuery.FieldByName('bmrat3').AsFloat;
        AdsQuery.ParamByName('bmrat4').AsFloat := AdoQuery.FieldByName('bmrat4').AsFloat;
        AdsQuery.ParamByName('bmrat6').AsFloat := AdoQuery.FieldByName('bmrat6').AsFloat;
        AdsQuery.ParamByName('bmext1').AsString := AdoQuery.FieldByName('bmext1').AsString;
        AdsQuery.ParamByName('bmext2').AsString := AdoQuery.FieldByName('bmext2').AsString;
        AdsQuery.ParamByName('bmext3').AsString := AdoQuery.FieldByName('bmext3').AsString;
        AdsQuery.ParamByName('bmext4').AsString := AdoQuery.FieldByName('bmext4').AsString;
        AdsQuery.ParamByName('bmtor1').AsString := AdoQuery.FieldByName('bmtor1').AsString;
        AdsQuery.ParamByName('bmtor2').AsString := AdoQuery.FieldByName('bmtor2').AsString;
        AdsQuery.ParamByName('bmtor3').AsString := AdoQuery.FieldByName('bmtor3').AsString;
        AdsQuery.ParamByName('bmtor4').AsString := AdoQuery.FieldByName('bmtor4').AsString;
        AdsQuery.ParamByName('bmtor6').AsString := AdoQuery.FieldByName('bmtor6').AsString;
        AdsQuery.ParamByName('bmfror').AsString := AdoQuery.FieldByName('bmfror').AsString;
        AdsQuery.ParamByName('bmcovr').AsString := AdoQuery.FieldByName('bmcovr').AsString;
        AdsQuery.ParamByName('bmhovr').AsString := AdoQuery.FieldByName('bmhovr').AsString;
        AdsQuery.ParamByName('bmtovr').AsString := AdoQuery.FieldByName('bmtovr').AsString;
        AdsQuery.ParamByName('bmrbdp').AsString := AdoQuery.FieldByName('bmrbdp').AsString;
        AdsQuery.ParamByName('bmcsel').AsString := AdoQuery.FieldByName('bmcsel').AsString;
        AdsQuery.ParamByName('bmciop').AsString := AdoQuery.FieldByName('bmciop').AsString;
        AdsQuery.ParamByName('bmsact').AsString := AdoQuery.FieldByName('bmsact').AsString;
        AdsQuery.ParamByName('bmunwd').AsString := AdoQuery.FieldByName('bmunwd').AsString;
        AdsQuery.ParamByName('bmdoc').AsString := AdoQuery.FieldByName('bmdoc#').AsString;
        AdsQuery.ParamByName('bmbage').AsInteger := AdoQuery.FieldByName('bmbage').AsInteger;
        AdsQuery.ParamByName('bmcbag').AsInteger := AdoQuery.FieldByName('bmcbag').AsInteger;
        AdsQuery.ParamByName('bmlcov').AsCurrency := AdoQuery.FieldByName('bmlcov').AsCurrency;
        AdsQuery.ParamByName('bmhcov').AsCurrency := AdoQuery.FieldByName('bmhcov').AsCurrency;
        AdsQuery.ParamByName('bmitdt').AsDateTime := AdoQuery.FieldByName('bmitdt').AsDateTime;
        AdsQuery.ParamByName('bmltxg').AsInteger := AdoQuery.FieldByName('bmltxg').AsInteger;
        AdsQuery.ParamByName('bmcblp').AsCurrency := AdoQuery.FieldByName('bmcblp').AsCurrency;
        AdsQuery.ParamByName('bmdtsv').AsDateTime := AdoQuery.FieldByName('bmdtsv').AsDateTime;
        AdsQuery.ParamByName('bmdttp').AsDateTime := AdoQuery.FieldByName('bmdttp').AsDateTime;
        AdsQuery.ParamByName('bmccrt').AsString := AdoQuery.FieldByName('bmccrt').AsString;
        AdsQuery.ParamByName('bmrdwn').AsCurrency := AdoQuery.FieldByName('bmrdwn').AsCurrency;
        AdsQuery.ParamByName('bmtdwn').AsCurrency := AdoQuery.FieldByName('bmtdwn').AsCurrency;
        AdsQuery.ParamByName('bmtcrd').AsCurrency := AdoQuery.FieldByName('bmtcrd').AsCurrency;
        AdsQuery.ParamByName('bmgcap').AsCurrency := AdoQuery.FieldByName('bmgcap').AsCurrency;
        AdsQuery.ParamByName('bmdinct').AsCurrency := AdoQuery.FieldByName('bmdinct').AsCurrency;
        AdsQuery.ParamByName('bmprnt').AsString := AdoQuery.FieldByName('bmprnt').AsString;
        AdsQuery.ParamByName('bmnvdr').AsString := AdoQuery.FieldByName('bmnvdr').AsString;
        AdsQuery.ParamByName('bmdtaprv').AsDateTime := AdoQuery.FieldByName('bmdtaprv').AsDateTime;
        AdsQuery.ParamByName('bmdtcap').AsDateTime := AdoQuery.FieldByName('bmdtcap').AsDateTime;
        AdsQuery.ParamByName('bmemcb').AsCurrency := AdoQuery.FieldByName('bmemcb').AsCurrency;
        AdsQuery.ParamByName('bmempb').AsCurrency := AdoQuery.FieldByName('bmempb').AsCurrency;
        AdsQuery.ParamByName('bmemrb').AsCurrency := AdoQuery.FieldByName('bmemrb').AsCurrency;
        AdsQuery.ParamByName('bmmpyb').AsCurrency := AdoQuery.FieldByName('bmmpyb').AsCurrency;
        AdsQuery.ParamByName('bmmyab').AsCurrency := AdoQuery.FieldByName('bmmyab').AsCurrency;
        AdsQuery.ParamByName('bmntrb').AsCurrency := AdoQuery.FieldByName('bmntrb').AsCurrency;
        AdsQuery.ParamByName('bmrsab').AsCurrency := AdoQuery.FieldByName('bmrsab').AsCurrency;
        AdsQuery.ParamByName('bmrspb').AsCurrency := AdoQuery.FieldByName('bmrspb').AsCurrency;
        AdsQuery.ParamByName('b9amtax').AsCurrency := AdoQuery.FieldByName('b9amtax').AsCurrency;
        AdsQuery.ParamByName('b9epay').AsCurrency := AdoQuery.FieldByName('b9epay').AsCurrency;
        AdsQuery.ParamByName('b9fpay').AsCurrency := AdoQuery.FieldByName('b9fpay').AsCurrency;
        AdsQuery.ParamByName('b9fpcode').AsString := AdoQuery.FieldByName('b9fpcode').AsString;
        AdsQuery.ParamByName('b9hterm').AsInteger := AdoQuery.FieldByName('b9hterm').AsInteger;
        AdsQuery.ParamByName('b9lptax').AsFloat := AdoQuery.FieldByName('b9lptax').AsFloat;
        AdsQuery.ParamByName('b9ltrad').AsString := AdoQuery.FieldByName('b9ltrad').AsString;
        AdsQuery.ParamByName('b9onettr').AsCurrency := AdoQuery.FieldByName('b9onettr').AsCurrency;
        AdsQuery.ParamByName('b9onteq').AsInteger := AdoQuery.FieldByName('b9onteq').AsInteger;
        AdsQuery.ParamByName('b9pdp').AsString := AdoQuery.FieldByName('b9pdp#').AsString;
        AdsQuery.ParamByName('b9peamt').AsCurrency := AdoQuery.FieldByName('b9peamt').AsCurrency;
        AdsQuery.ParamByName('b9petax').AsCurrency := AdoQuery.FieldByName('b9petax').AsCurrency;
        AdsQuery.ParamByName('b9sctax').AsCurrency := AdoQuery.FieldByName('b9sctax').AsCurrency;
        AdsQuery.ParamByName('b9thcov').AsCurrency := AdoQuery.FieldByName('b9thcov').AsCurrency;
        AdsQuery.ParamByName('b9txbase').AsCurrency := AdoQuery.FieldByName('b9txbase').AsCurrency;
        AdsQuery.ParamByName('b9wkip').AsCurrency := AdoQuery.FieldByName('b9wkip').AsCurrency;
        AdsQuery.ParamByName('bmldtlp').AsDateTime := AdoQuery.FieldByName('bmldtlp').AsDateTime;
        AdsQuery.ParamByName('bmrat5').AsFloat := AdoQuery.FieldByName('bmrat5').AsFloat;
        AdsQuery.ParamByName('bmrat7').AsFloat := AdoQuery.FieldByName('bmrat7').AsFloat;
        AdsQuery.ParamByName('bmolpm').AsCurrency := AdoQuery.FieldByName('bmolpm').AsCurrency;
        AdsQuery.ParamByName('bmocdt').AsInteger := AdoQuery.FieldByName('bmocdt').AsInteger;
        AdsQuery.ParamByName('bmllopt').AsString := AdoQuery.FieldByName('bmllopt').AsString;
        AdsQuery.ParamByName('bmrftx1').AsFloat := AdoQuery.FieldByName('bmrftx1').AsFloat;
        AdsQuery.ParamByName('bmrftx2').AsFloat := AdoQuery.FieldByName('bmrftx2').AsFloat;
        AdsQuery.ParamByName('bmpyxins').AsFloat := AdoQuery.FieldByName('bmpyxins').AsFloat;
        AdsQuery.ParamByName('bmpbkr').AsFloat := AdoQuery.FieldByName('bmpbkr').AsFloat;
        AdsQuery.ParamByName('bmbrri').AsString := AdoQuery.FieldByName('bmbrri').AsString;
        AdsQuery.ParamByName('bmcrri').AsString := AdoQuery.FieldByName('bmcrri').AsString;
        AdsQuery.ParamByName('bmbgen').AsString := AdoQuery.FieldByName('bmbgen').AsString;
        AdsQuery.ParamByName('bmcgen').AsString := AdoQuery.FieldByName('bmcgen').AsString;
        AdsQuery.ParamByName('bmcicd').AsString := AdoQuery.FieldByName('bmcicd').AsString;
        AdsQuery.ParamByName('bmcipm').AsFloat := AdoQuery.FieldByName('bmcipm').AsFloat;
        AdsQuery.ParamByName('bmlecd').AsString := AdoQuery.FieldByName('bmlecd').AsString;
        AdsQuery.ParamByName('bmlepm').AsFloat := AdoQuery.FieldByName('bmlepm').AsFloat;
        AdsQuery.ParamByName('bmuftx1').AsFloat := AdoQuery.FieldByName('bmuftx1').AsFloat;
        AdsQuery.ParamByName('bmuftx2').AsFloat := AdoQuery.FieldByName('bmuftx2').AsFloat;
        AdsQuery.ParamByName('bmresin').AsString := AdoQuery.FieldByName('bmresin').AsString;
        AdsQuery.ParamByName('bmdisel').AsString := AdoQuery.FieldByName('bmdisel').AsString;
        AdsQuery.ParamByName('bmremic').AsString := AdoQuery.FieldByName('bmremic').AsString;
        AdsQuery.ParamByName('bmprmpct').AsFloat := AdoQuery.FieldByName('bmprmpct').AsFloat;
        AdsQuery.ParamByName('bmrat8').AsFloat := AdoQuery.FieldByName('bmrat8').AsFloat;
        AdsQuery.ParamByName('bmdelvdt').AsDateTime := AdoQuery.FieldByName('bmdelvdt').AsDateTime;
        AdsQuery.ParamByName('bmimile').AsInteger := AdoQuery.FieldByName('bmimile').AsInteger;
        AdsQuery.ParamByName('bmimrt').AsInteger := AdoQuery.FieldByName('bmimrt').AsInteger;
        AdsQuery.ParamByName('bmimcg').AsFloat := AdoQuery.FieldByName('bmimcg').AsFloat;
        AdsQuery.ParamByName('bmlpyrem').AsFloat := AdoQuery.FieldByName('bmlpyrem').AsFloat;
        AdsQuery.ParamByName('bmfigrs').AsCurrency := AdoQuery.FieldByName('bmfigrs').AsCurrency;
        AdsQuery.ParamByName('bmhgrs').AsCurrency := AdoQuery.FieldByName('bmhgrs').AsCurrency;
        AdsQuery.ParamByName('bmmthgapp').AsCurrency := AdoQuery.FieldByName('bmmthgapp').AsCurrency;
        AdsQuery.ParamByName('bmimflag').AsString := AdoQuery.FieldByName('bmimflag').AsString;
        AdsQuery.ParamByName('bmimthld').AsFloat := AdoQuery.FieldByName('bmimthld').AsFloat;
        AdsQuery.ParamByName('bmgaptax').AsFloat := AdoQuery.FieldByName('bmgaptax').AsFloat;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaBOPMAST'')');
      ScrapeCountTable('BOPMAST', 'stgArkonaBOPMAST');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('BOPMAST.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;


procedure TDataModule1.INPOPTD;
var
  proc: string;
begin
  proc := 'INPOPTD';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
//      ExecuteQuery(AdsQuery, 'insert into xxxxxtempxxxxxx select * from xxxxxxstgxxxxxx');
      ExecuteQuery(AdsQuery, 'delete from stgArkonaINPOPTD');
      PrepareQuery(AdsQuery, ' insert into stgArkonaINPOPTD(' +
          'IDCO#, IDSEQ#, IDDESC, IDTYPE, IDREQE, IDRETD, IDREQU, IDSCHD, IDDVLN, IDDVLU, IDADDC, IDCTL#, IDCTGL) ' +
          'values(' +
          ':IDCO, :IDSEQ, :IDDESC, :IDTYPE, :IDREQE, :IDRETD, :IDREQU, :IDSCHD, :IDDVLN, :IDDVLU, :IDADDC, :IDCTL, :IDCTGL)');
      OpenQuery(AdoQuery, 'select ' +
          'IDCO#, IDSEQ#, IDDESC, IDTYPE, IDREQE, IDRETD, IDREQU, IDSCHD, ' +
          'IDDVLN, IDDVLU, IDADDC, IDCTL#, IDCTGL ' +
          'from rydedata.inpoptd');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('idco').AsString := AdoQuery.FieldByName('idco#').AsString;
        AdsQuery.ParamByName('idseq').AsInteger := AdoQuery.FieldByName('idseq#').AsInteger;
        AdsQuery.ParamByName('iddesc').AsString := AdoQuery.FieldByName('iddesc').AsString;
        AdsQuery.ParamByName('idtype').AsString := AdoQuery.FieldByName('idtype').AsString;
        AdsQuery.ParamByName('idreqe').AsString := AdoQuery.FieldByName('idreqe').AsString;
        AdsQuery.ParamByName('idretd').AsString := AdoQuery.FieldByName('idretd').AsString;
        AdsQuery.ParamByName('idrequ').AsString := AdoQuery.FieldByName('idrequ').AsString;
        AdsQuery.ParamByName('idschd').AsString := AdoQuery.FieldByName('idschd').AsString;
        AdsQuery.ParamByName('iddvln').AsFloat := AdoQuery.FieldByName('iddvln').AsFloat;
        AdsQuery.ParamByName('iddvlu').AsFloat := AdoQuery.FieldByName('iddvlu').AsFloat;
        AdsQuery.ParamByName('idaddc').AsString := AdoQuery.FieldByName('idaddc').AsString;
        AdsQuery.ParamByName('idctl').AsString := AdoQuery.FieldByName('idctl#').AsString;
        AdsQuery.ParamByName('idctgl').AsString := AdoQuery.FieldByName('idctgl').AsString;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaINPOPTD'')');
//      ScrapeCount('xxxxxxxxxxxxdb2xxxxxxxxxxxx', 'xxxxxxxxxxxxadsxxxxxxxxxxxx');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('INPOPTD.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDataModule1.INPOPTF;
var
  proc: string;
begin
  proc := 'INPOPTF';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
//      ExecuteQuery(AdsQuery, 'insert into xxxxxtempxxxxxx select * from xxxxxxstgxxxxxx');
      ExecuteQuery(AdsQuery, 'delete from stgArkonaINPOPTF');
      PrepareQuery(AdsQuery, ' insert into stgArkonaINPOPTF(' +
          'IOCO#, IOVIN, IOSEQ#, IOTYPE, IOFVAL, IONVAL, IODATE, IOADDC) ' +
          'values(' +
          ':IOCO, :IOVIN, :IOSEQ, :IOTYPE, :IOFVAL, :IONVAL, :IODATE, :IOADDC)');
      OpenQuery(AdoQuery, 'select IOCO#, IOVIN, IOSEQ#, IOTYPE, IOFVAL, IONVAL, ' +
          'IODATE, IOADDC from rydedata.inpoptf where ionval <> 0');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('ioco').AsString := AdoQuery.FieldByName('ioco#').AsString;
        AdsQuery.ParamByName('iovin').AsString := AdoQuery.FieldByName('iovin').AsString;
        AdsQuery.ParamByName('ioseq').AsInteger := AdoQuery.FieldByName('ioseq#').AsInteger;
        AdsQuery.ParamByName('iotype').AsString := AdoQuery.FieldByName('iotype').AsString;
        AdsQuery.ParamByName('iofval').AsString := AdoQuery.FieldByName('iofval').AsString;
        AdsQuery.ParamByName('ionval').AsFloat := AdoQuery.FieldByName('ionval').AsFloat;
        AdsQuery.ParamByName('iodate').AsFloat := AdoQuery.FieldByName('iodate').AsFloat;
        AdsQuery.ParamByName('ioaddc').AsString := AdoQuery.FieldByName('ioaddc').AsString;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaINPOPTF'')');
//      ScrapeCount('xxxxxxxxxxxxdb2xxxxxxxxxxxx', 'xxxxxxxxxxxxadsxxxxxxxxxxxx');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('INPOPTF.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDataModule1.GLPCRHD;
var
  proc: string;
begin
  proc := 'GLPCRHD';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaGLPCRHD'')');
      PrepareQuery(AdsQuery, ' insert into stgArkonaGLPCRHD(' +
          'GNCO#, GNREC#, GNTYPE, GNCKEY, GNDATE, GNRAMT, GNTRN#, GNUSER) ' +
          'values(' +
          ':GNCO, :GNREC, :GNTYPE, :GNCKEY, :GNDATE, :GNRAMT, :GNTRN, :GNUSER)');
      OpenQuery(AdoQuery, 'select ' +
          'GNCO#, GNREC#, GNTYPE, GNCKEY, GNDATE, ' +
          'GNRAMT, GNTRN#, GNUSER ' +
          'from rydedata.glpcrhd');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('gnco').AsString := AdoQuery.FieldByName('gnco#').AsString;
        AdsQuery.ParamByName('gnrec').AsString := AdoQuery.FieldByName('gnrec#').AsString;
        AdsQuery.ParamByName('gntype').AsString := AdoQuery.FieldByName('gntype').AsString;
        AdsQuery.ParamByName('gnckey').AsInteger := AdoQuery.FieldByName('gnckey').AsInteger;
        AdsQuery.ParamByName('gndate').AsDateTime := AdoQuery.FieldByName('gndate').AsDateTime;
        AdsQuery.ParamByName('gnramt').AsCurrency := AdoQuery.FieldByName('gnramt').AsCurrency;
        AdsQuery.ParamByName('gntrn').AsInteger := AdoQuery.FieldByName('gntrn#').AsInteger;
        AdsQuery.ParamByName('gnuser').AsString := AdoQuery.FieldByName('gnuser').AsString;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaGLPCRHD'')');
      ScrapeCountTable('GLPCRHD', 'stgArkonaGLPCRHD');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('GLPCRHD.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;



procedure TDataModule1.GLPCRDT;
var
  proc: string;
begin
  proc := 'GLPCRDT';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaGLPCRDT'')');
      PrepareQuery(AdsQuery, ' insert into stgArkonaGLPCRDT(' +
          'GTCO#, GTREC#, GTRSEQ#, GTDATE, GTCTL#, GTREF#, GTACCT, GTDESC, GTTAMT) ' +
          'values(' +
          ':GTCO, :GTREC, :GTRSEQ, :GTDATE, :GTCTL, :GTREF, :GTACCT, :GTDESC, :GTTAMT)');
      OpenQuery(AdoQuery, 'select ' +
          'GTCO#, GTREC#, GTRSEQ#, GTDATE, GTCTL#, GTREF#, GTACCT, GTDESC, GTTAMT ' +
          'from rydedata.glpcrdt');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('gtco').AsString := AdoQuery.FieldByName('gtco#').AsString;
        AdsQuery.ParamByName('gtrec').AsString := AdoQuery.FieldByName('gtrec#').AsString;
        AdsQuery.ParamByName('gtrseq').AsInteger := AdoQuery.FieldByName('gtrseq#').AsInteger;
        AdsQuery.ParamByName('gtdate').AsDateTime := AdoQuery.FieldByName('gtdate').AsDateTime;
        AdsQuery.ParamByName('gtctl').AsString := AdoQuery.FieldByName('gtctl#').AsString;
        AdsQuery.ParamByName('gtref').AsString := AdoQuery.FieldByName('gtref#').AsString;
        AdsQuery.ParamByName('gtacct').AsString := AdoQuery.FieldByName('gtacct').AsString;
        AdsQuery.ParamByName('gtdesc').AsString := AdoQuery.FieldByName('gtdesc').AsString;
        AdsQuery.ParamByName('gttamt').AsCurrency := AdoQuery.FieldByName('gttamt').AsCurrency;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaGLPCRDT'')');
      ScrapeCountTable('GLPCRDT', 'stgArkonaGLPCRDT');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('GLPCRDT.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;



procedure TDataModule1.PYPCLKL;
var
  proc: string;
begin
  proc := 'PYPCLKL';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
//      ExecuteQuery(AdsQuery, 'insert into xxxxxtempxxxxxx select * from xxxxxxstgxxxxxx');
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaPYPCLKL'')');
      PrepareQuery(AdsQuery, ' insert into stgArkonaPYPCLKL(' +
          'YLCCO#, YLEMP#, YLCLKIND, YLCLKINT, YLCLKOUTD, YLCLKOUTT, YLCODE, YLUSER, YLWSID, YLCHGC, YLCHGD, YLCHGT) ' +
          'values(' +
          ':YLCCO, :YLEMP, :YLCLKIND, :YLCLKINT, :YLCLKOUTD, :YLCLKOUTT, :YLCODE, :YLUSER, :YLWSID, :YLCHGC, :YLCHGD, :YLCHGT)');
      OpenQuery(AdoQuery, 'select ' +
        'ylcco#, ylemp#, ylclkind, ' +
        'case when ylclkint = ''24:00:00'' then ''23:59:59'' else ylclkint end as ylclkint, ' +
        'ylclkoutd, ' +
        'case when ylclkoutt = ''24:00:00'' then ''23:59:59'' else ylclkoutt end as ylclkoutt, ' +
        'ylcode, yluser, ylwsid, ylchgc, ylchgd, ylchgt  ' +
        'from rydedata.pypclkl');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('ylcco').AsString := AdoQuery.FieldByName('ylcco#').AsString;
        AdsQuery.ParamByName('ylemp').AsString := AdoQuery.FieldByName('ylemp#').AsString;
        AdsQuery.ParamByName('ylclkind').AsDateTime := AdoQuery.FieldByName('ylclkind').AsDateTime;
        AdsQuery.ParamByName('ylclkint').AsDateTime := AdoQuery.FieldByName('ylclkint').AsDateTime;
        AdsQuery.ParamByName('ylclkoutd').AsDateTime := AdoQuery.FieldByName('ylclkoutd').AsDateTime;
        AdsQuery.ParamByName('ylclkoutt').AsDateTime := AdoQuery.FieldByName('ylclkoutt').AsDateTime;
        AdsQuery.ParamByName('ylcode').AsString := AdoQuery.FieldByName('ylcode').AsString;
        AdsQuery.ParamByName('yluser').AsString := AdoQuery.FieldByName('yluser').AsString;
        AdsQuery.ParamByName('ylwsid').AsString := AdoQuery.FieldByName('ylwsid').AsString;
        AdsQuery.ParamByName('ylchgc').AsString := AdoQuery.FieldByName('ylchgc').AsString;
        AdsQuery.ParamByName('ylchgd').AsDateTime := AdoQuery.FieldByName('ylchgd').AsDateTime;
        AdsQuery.ParamByName('ylchgt').AsDateTime := AdoQuery.FieldByName('ylchgt').AsDateTime;

        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaPYPCLKL'')');
      ScrapeCountTable('pypclkl', 'stgArkonaPYPCLKL');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('pypclkl.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDataModule1.PYPCLOCKIN;
var
  startDate: string;
  proc: string;
begin
  proc := 'PYPCLOCKIN';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''tmpPYPCLOCKIN'')');
      OpenQuery(AdsQuery, 'select max(thedate) - 28 as theDate ' +
          'from day where dayofweek = 1 and thedate <= curdate()');
      startDate := AdsQuery.FieldByName('theDate').AsString;
      CloseQuery(AdsQuery);
      PrepareQuery(AdsQuery, ' insert into tmpPYPCLOCKIN(' +
          'YICO#, YIEMP#, YICLKIND, YICLKINT, YICLKOUTD, YICLKOUTT, YICODE) ' +
          'values(' +
          ':YICO, :YIEMP, :YICLKIND, :YICLKINT, :YICLKOUTD, :YICLKOUTT, :YICODE)');
      OpenQuery(AdoQuery, 'select ' +
          'yico#, yiemp#, yiclkind, ' +
          '  case when yiclkint = ''24:00:00'' then ''23:59:59'' else yiclkint end as yiclkint, ' +
          '  yiclkoutd, ' +
          '  max(case when yiclkoutt = ''24:00:00'' then ''23:59:59'' else yiclkoutt end) as yiclkoutt, ' +
          '  yicode ' +
          'from rydedata.pypclockin ' +
          'where yicode <> ''666'' ' +
          '  and yiclkind >= ' + QuotedStr(startDate) +
          ' group by yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yicode');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('yico').AsString := AdoQuery.FieldByName('yico#').AsString;
        AdsQuery.ParamByName('yiemp').AsString := AdoQuery.FieldByName('yiemp#').AsString;
        AdsQuery.ParamByName('yiclkind').AsDate := AdoQuery.FieldByName('yiclkind').AsDateTime;
        AdsQuery.ParamByName('yiclkint').AsTime := AdoQuery.FieldByName('yiclkint').AsDateTime;
        AdsQuery.ParamByName('yiclkoutd').AsDate := AdoQuery.FieldByName('yiclkoutd').AsDateTime;
        AdsQuery.ParamByName('yiclkoutt').AsTime := AdoQuery.FieldByName('yiclkoutt').AsDateTime;
        AdsQuery.ParamByName('yicode').AsString := AdoQuery.FieldByName('yicode').AsString;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''tmpPYPCLOCKIN'')');
      ScrapeCount('( select yiemp# from rydedata.pypclockin ' +
          'where yicode <> ''666'' ' +
          '  and yiclkind >= ' + QuotedStr(startDate) +
          ' group by yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yicode) x',
          'tmpPYPCLOCKIN');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
// ok, tmpPYPCLOCKIN is loaded
      ExecuteQuery(AdsQuery, 'execute procedure stgArkPYPCLOCKIN()');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('PYPCLOCKIN.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;



procedure TDataModule1.GLPJRND;
var
  proc: string;
begin
  proc := 'GLPJRND';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaGLPJRND'')');
      PrepareQuery(AdsQuery, ' insert into stgArkonaGLPJRND(' +
          'GJCO#, GJJRNL, GJDESC, GJCTLP, GJDOCP, GJOVRP, GJTDES, GJCSTR, ' +
          'GJCRPC, GJCSAU, GJCASU, GJUDLC, GJUDLP, GJALST, GJPBTC, GJRCWO, GJCSTC) ' +
          'values(' +
          ':GJCO, :GJJRNL, :GJDESC, :GJCTLP, :GJDOCP, :GJOVRP, :GJTDES, :GJCSTR, ' +
          ':GJCRPC, :GJCSAU, :GJCASU, :GJUDLC, :GJUDLP, :GJALST, :GJPBTC, :GJRCWO, :GJCSTC)');
      OpenQuery(AdoQuery, 'select ' +
          'GJCO#, GJJRNL, GJDESC, GJCTLP, GJDOCP, GJOVRP, GJTDES, GJCSTR, ' +
          'GJCRPC, GJCSAU, GJCASU, GJUDLC, GJUDLP, GJALST, GJPBTC, GJRCWO, GJCSTC ' +
      'from rydedata.GLPJRND');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('gjco').AsString := AdoQuery.FieldByName('gjco#').AsString;
        AdsQuery.ParamByName('gjjrnl').AsString := AdoQuery.FieldByName('gjjrnl').AsString;
        AdsQuery.ParamByName('gjdesc').AsString := AdoQuery.FieldByName('gjdesc').AsString;
        AdsQuery.ParamByName('gjctlp').AsString := AdoQuery.FieldByName('gjctlp').AsString;
        AdsQuery.ParamByName('gjdocp').AsString := AdoQuery.FieldByName('gjdocp').AsString;
        AdsQuery.ParamByName('gjovrp').AsString := AdoQuery.FieldByName('gjovrp').AsString;
        AdsQuery.ParamByName('gjtdes').AsString := AdoQuery.FieldByName('gjtdes').AsString;
        AdsQuery.ParamByName('gjcstr').AsString := AdoQuery.FieldByName('gjcstr').AsString;
        AdsQuery.ParamByName('gjcrpc').AsFloat := AdoQuery.FieldByName('gjcrpc').AsFloat;
        AdsQuery.ParamByName('gjcsau').AsString := AdoQuery.FieldByName('gjcsau').AsString;
        AdsQuery.ParamByName('gjcasu').AsString := AdoQuery.FieldByName('gjcasu').AsString;
        AdsQuery.ParamByName('gjudlc').AsString := AdoQuery.FieldByName('gjudlc').AsString;
        AdsQuery.ParamByName('gjudlp').AsString := AdoQuery.FieldByName('gjudlp').AsString;
        AdsQuery.ParamByName('gjalst').AsString := AdoQuery.FieldByName('gjalst').AsString;
        AdsQuery.ParamByName('gjpbtc').AsString := AdoQuery.FieldByName('gjpbtc').AsString;
        AdsQuery.ParamByName('gjrcwo').AsString := AdoQuery.FieldByName('gjrcwo').AsString;
        AdsQuery.ParamByName('gjcstc').AsString := AdoQuery.FieldByName('gjcstc').AsString;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaGLPJRND'')');
      ScrapeCount('RYDEDATA.GLPJRND', 'stgArkonaGLPJRND');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('GLPJRND.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDataModule1.PYHSHDTA;
var
//  i: integer;
  proc: string;
begin
  proc := 'PYHSHDTA';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
//  i := 1;
  try
    try
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaPYHSHDTA'')');
      PrepareQuery(AdsQuery, ' insert into stgArkonaPYHSHDTA(' +
          'YHDCO#, YPBCYY, YPBNUM, YHDEMP, YHDSEQ, YHDREF, YHDECC, ' +
          'YHDEYY, YHDEMM, YHDEDD, YHDCCC, YHDCYY, YHDCMM, YHDCDD, ' +
          'YHDBSP, YHDTGP, YHDTGA, YHCFED, YHCSSE, YHCSSM, YHCMDE, ' +
          'YHCMDM, YHCEIC, YHCST, YHCSDE, YHCSDM, YHCCNT, YHCCTY, ' +
          'YHCFUC, YHCSUC, YHCCMP, YHCRTE, YHCRTM, YHDOTA, YHDDED, ' +
          'YHCOPY, YHCNET, YHCTAX, YHAFED, YHASS, YHAFUC, YHAST, ' +
          'YHACN, YHAMU, YHASUC, YHACM, YHDVAC, YHDHOL, YHDSCK, ' +
          'YHAVAC, YHAHOL, YHASCK, YHDHRS, YHDOTH, YHDAHR, YHVACD, ' +
          'YHHOLD, YHSCKD, YHXVAC, YHXSCK, YHXHOL, YHXFED, YHXSS, ' +
          'YHXSIT, YHXCNT, YHXMU, YHHDTE, YHMARI, YHPPER, YHCLAS, ' +
          'YHEXSS, YHFDEX, YHFDAD, YHEEIC, YHECLS, YHSTEX, YHSTFX, ' +
          'YHSTAD, YHWKCD, YHCWFL, YHSTTX, YHCNTX, YHMUTX, YHSTCD, ' +
          'YHCNCD, YHMUCD, YHSALY, YHRATE, YHSALA, YHRATA, YHRETE, ' +
          'YHRETP, YHRFIX, YMDVYR, YMDHYR, YMDSYR, YMNAME, YMDEPT, ' +
          'YMSECG, YMSS#, YMDIST, YMAR#, YHVOID, YHVCYY, YHVNUM, ' +
          'YHVSEQ, YHTYPE, YHACTY, YHASDIW, YHAOTR, YHDIRD, YHFTAP, ' +
          'YHOLCS, YHSTAP, YHXFUT, YHXOVR, YHXRTE, YHXRTM, YHXSDE, ' +
          'YHXSDM, YHXSUT, YHCSUE, YHCST2, YHCCN2, YHCCT2, YHFDST, ' +
          'YHSTST, YHCNST, YHMUST, YHSTS2, YHCNS2, YHMUS2, YHSTT2, ' +
          'YHCNT2, YHMUT2, YHSDST, YHWKST, YHRPT1, YHRPT2, YHEEOC, ' +
          'YHOTEX, YHHGHC, YHTMP01, YHTMP02, YHTMP03, YHTMP04, YHTMP05, ' +
          'YHTMP06, YHTMP07, YHTMP08, YHTMP09) ' +
          'values(' +
          ':YHDCO, :YPBCYY, :YPBNUM, :YHDEMP, :YHDSEQ, :YHDREF, :YHDECC, ' +
          ':YHDEYY, :YHDEMM, :YHDEDD, :YHDCCC, :YHDCYY, :YHDCMM, :YHDCDD, ' +
          ':YHDBSP, :YHDTGP, :YHDTGA, :YHCFED, :YHCSSE, :YHCSSM, :YHCMDE, ' +
          ':YHCMDM, :YHCEIC, :YHCST, :YHCSDE, :YHCSDM, :YHCCNT, :YHCCTY, ' +
          ':YHCFUC, :YHCSUC, :YHCCMP, :YHCRTE, :YHCRTM, :YHDOTA, :YHDDED, ' +
          ':YHCOPY, :YHCNET, :YHCTAX, :YHAFED, :YHASS, :YHAFUC, :YHAST, ' +
          ':YHACN, :YHAMU, :YHASUC, :YHACM, :YHDVAC, :YHDHOL, :YHDSCK, ' +
          ':YHAVAC, :YHAHOL, :YHASCK, :YHDHRS, :YHDOTH, :YHDAHR, :YHVACD, ' +
          ':YHHOLD, :YHSCKD, :YHXVAC, :YHXSCK, :YHXHOL, :YHXFED, :YHXSS, ' +
          ':YHXSIT, :YHXCNT, :YHXMU, :YHHDTE, :YHMARI, :YHPPER, :YHCLAS, ' +
          ':YHEXSS, :YHFDEX, :YHFDAD, :YHEEIC, :YHECLS, :YHSTEX, :YHSTFX, ' +
          ':YHSTAD, :YHWKCD, :YHCWFL, :YHSTTX, :YHCNTX, :YHMUTX, :YHSTCD, ' +
          ':YHCNCD, :YHMUCD, :YHSALY, :YHRATE, :YHSALA, :YHRATA, :YHRETE, ' +
          ':YHRETP, :YHRFIX, :YMDVYR, :YMDHYR, :YMDSYR, :YMNAME, :YMDEPT, ' +
          ':YMSECG, :YMSS, :YMDIST, :YMAR, :YHVOID, :YHVCYY, :YHVNUM, ' +
          ':YHVSEQ, :YHTYPE, :YHACTY, :YHASDIW, :YHAOTR, :YHDIRD, :YHFTAP, ' +
          ':YHOLCS, :YHSTAP, :YHXFUT, :YHXOVR, :YHXRTE, :YHXRTM, :YHXSDE, ' +
          ':YHXSDM, :YHXSUT, :YHCSUE, :YHCST2, :YHCCN2, :YHCCT2, :YHFDST, ' +
          ':YHSTST, :YHCNST, :YHMUST, :YHSTS2, :YHCNS2, :YHMUS2, :YHSTT2, ' +
          ':YHCNT2, :YHMUT2, :YHSDST, :YHWKST, :YHRPT1, :YHRPT2, :YHEEOC, ' +
          ':YHOTEX, :YHHGHC, :YHTMP01, :YHTMP02, :YHTMP03, :YHTMP04, :YHTMP05, ' +
          ':YHTMP06, :YHTMP07, :YHTMP08, :YHTMP09)');
//      OpenQuery(AdoQuery, 'select * from rydedata.pyhshdta where yhvoid <> ''V''');
      OpenQuery(AdoQuery, 'select ' +
          'YHDCO#, YPBCYY, YPBNUM, YHDEMP, YHDSEQ, YHDREF, YHDECC, ' +
          'YHDEYY, YHDEMM, YHDEDD, YHDCCC, YHDCYY, YHDCMM, YHDCDD, ' +
          'YHDBSP, YHDTGP, YHDTGA, YHCFED, YHCSSE, YHCSSM, YHCMDE, ' +
          'YHCMDM, YHCEIC, YHCST, YHCSDE, YHCSDM, YHCCNT, YHCCTY, ' +
          'YHCFUC, YHCSUC, YHCCMP, YHCRTE, YHCRTM, YHDOTA, YHDDED, ' +
          'YHCOPY, YHCNET, YHCTAX, YHAFED, YHASS, YHAFUC, YHAST, ' +
          'YHACN, YHAMU, YHASUC, YHACM, YHDVAC, YHDHOL, YHDSCK, ' +
          'YHAVAC, YHAHOL, YHASCK, YHDHRS, YHDOTH, YHDAHR, YHVACD, ' +
          'YHHOLD, YHSCKD, YHXVAC, YHXSCK, YHXHOL, YHXFED, YHXSS, ' +
          'YHXSIT, YHXCNT, YHXMU, YHHDTE, YHMARI, YHPPER, YHCLAS, ' +
          'YHEXSS, YHFDEX, YHFDAD, YHEEIC, YHECLS, YHSTEX, YHSTFX, ' +
          'YHSTAD, YHWKCD, YHCWFL, YHSTTX, YHCNTX, YHMUTX, YHSTCD, ' +
          'YHCNCD, YHMUCD, YHSALY, YHRATE, YHSALA, YHRATA, YHRETE, ' +
          'YHRETP, YHRFIX, YMDVYR, YMDHYR, YMDSYR, YMNAME, YMDEPT, ' +
          'YMSECG, YMSS#, YMDIST, YMAR#, YHVOID, YHVCYY, YHVNUM, ' +
          'YHVSEQ, YHTYPE, YHACTY, YHASDIW, YHAOTR, YHDIRD, YHFTAP, ' +
          'YHOLCS, YHSTAP, YHXFUT, YHXOVR, YHXRTE, YHXRTM, YHXSDE, ' +
          'YHXSDM, YHXSUT, YHCSUE, YHCST2, YHCCN2, YHCCT2, YHFDST, ' +
          'YHSTST, YHCNST, YHMUST, YHSTS2, YHCNS2, YHMUS2, YHSTT2, ' +
          'YHCNT2, YHMUT2, YHSDST, YHWKST, YHRPT1, YHRPT2, YHEEOC, ' +
          'YHOTEX, YHHGHC, YHTMP01, YHTMP02, YHTMP03, YHTMP04, YHTMP05, ' +
          'YHTMP06, YHTMP07, YHTMP08, YHTMP09 '  +
          'from rydedata.pyhshdta');
      while not AdoQuery.Eof do
      begin

        AdsQuery.ParamByName('yhdco').AsString := AdoQuery.FieldByName('yhdco#').AsString;
        AdsQuery.ParamByName('ypbcyy').AsInteger := AdoQuery.FieldByName('ypbcyy').AsInteger;
        AdsQuery.ParamByName('ypbnum').AsInteger := AdoQuery.FieldByName('ypbnum').AsInteger;
        AdsQuery.ParamByName('yhdemp').AsString := AdoQuery.FieldByName('yhdemp').AsString;
        AdsQuery.ParamByName('yhdseq').AsString := AdoQuery.FieldByName('yhdseq').AsString;
        AdsQuery.ParamByName('yhdref').AsString := AdoQuery.FieldByName('yhdref').AsString;
        AdsQuery.ParamByName('yhdecc').AsInteger := AdoQuery.FieldByName('yhdecc').AsInteger;
        AdsQuery.ParamByName('yhdeyy').AsInteger := AdoQuery.FieldByName('yhdeyy').AsInteger;
        AdsQuery.ParamByName('yhdemm').AsInteger := AdoQuery.FieldByName('yhdemm').AsInteger;
        AdsQuery.ParamByName('yhdedd').AsInteger := AdoQuery.FieldByName('yhdedd').AsInteger;
        AdsQuery.ParamByName('yhdccc').AsInteger := AdoQuery.FieldByName('yhdccc').AsInteger;
        AdsQuery.ParamByName('yhdcyy').AsInteger := AdoQuery.FieldByName('yhdcyy').AsInteger;
        AdsQuery.ParamByName('yhdcmm').AsInteger := AdoQuery.FieldByName('yhdcmm').AsInteger;
        AdsQuery.ParamByName('yhdcdd').AsInteger := AdoQuery.FieldByName('yhdcdd').AsInteger;
        AdsQuery.ParamByName('yhdbsp').AsCurrency := AdoQuery.FieldByName('yhdbsp').AsCurrency;
        AdsQuery.ParamByName('yhdtgp').AsCurrency := AdoQuery.FieldByName('yhdtgp').AsCurrency;
        AdsQuery.ParamByName('yhdtga').AsCurrency := AdoQuery.FieldByName('yhdtga').AsCurrency;
        AdsQuery.ParamByName('yhcfed').AsCurrency := AdoQuery.FieldByName('yhcfed').AsCurrency;
        AdsQuery.ParamByName('yhcsse').AsCurrency := AdoQuery.FieldByName('yhcsse').AsCurrency;
        AdsQuery.ParamByName('yhcssm').AsCurrency := AdoQuery.FieldByName('yhcssm').AsCurrency;
        AdsQuery.ParamByName('yhcmde').AsCurrency := AdoQuery.FieldByName('yhcmde').AsCurrency;
        AdsQuery.ParamByName('yhcmdm').AsCurrency := AdoQuery.FieldByName('yhcmdm').AsCurrency;
        AdsQuery.ParamByName('yhceic').AsCurrency := AdoQuery.FieldByName('yhceic').AsCurrency;
        AdsQuery.ParamByName('yhcst').AsCurrency := AdoQuery.FieldByName('yhcst').AsCurrency;
        AdsQuery.ParamByName('yhcsde').AsCurrency := AdoQuery.FieldByName('yhcsde').AsCurrency;
        AdsQuery.ParamByName('yhcsdm').AsCurrency := AdoQuery.FieldByName('yhcsdm').AsCurrency;
        AdsQuery.ParamByName('yhccnt').AsCurrency := AdoQuery.FieldByName('yhccnt').AsCurrency;
        AdsQuery.ParamByName('yhccty').AsCurrency := AdoQuery.FieldByName('yhccty').AsCurrency;
        AdsQuery.ParamByName('yhcfuc').AsCurrency := AdoQuery.FieldByName('yhcfuc').AsCurrency;
        AdsQuery.ParamByName('yhcsuc').AsCurrency := AdoQuery.FieldByName('yhcsuc').AsCurrency;
        AdsQuery.ParamByName('yhccmp').AsCurrency := AdoQuery.FieldByName('yhccmp').AsCurrency;
        AdsQuery.ParamByName('yhcrte').AsCurrency := AdoQuery.FieldByName('yhcrte').AsCurrency;
        AdsQuery.ParamByName('yhcrtm').AsCurrency := AdoQuery.FieldByName('yhcrtm').AsCurrency;
        AdsQuery.ParamByName('yhdota').AsCurrency := AdoQuery.FieldByName('yhdota').AsCurrency;
        AdsQuery.ParamByName('yhdded').AsCurrency := AdoQuery.FieldByName('yhdded').AsCurrency;
        AdsQuery.ParamByName('yhcopy').AsCurrency := AdoQuery.FieldByName('yhcopy').AsCurrency;
        AdsQuery.ParamByName('yhcnet').AsCurrency := AdoQuery.FieldByName('yhcnet').AsCurrency;
        AdsQuery.ParamByName('yhctax').AsCurrency := AdoQuery.FieldByName('yhctax').AsCurrency;
        AdsQuery.ParamByName('yhafed').AsCurrency := AdoQuery.FieldByName('yhafed').AsCurrency;
        AdsQuery.ParamByName('yhass').AsCurrency := AdoQuery.FieldByName('yhass').AsCurrency;
        AdsQuery.ParamByName('yhafuc').AsCurrency := AdoQuery.FieldByName('yhafuc').AsCurrency;
        AdsQuery.ParamByName('yhast').AsCurrency := AdoQuery.FieldByName('yhast').AsCurrency;
        AdsQuery.ParamByName('yhacn').AsCurrency := AdoQuery.FieldByName('yhacn').AsCurrency;
        AdsQuery.ParamByName('yhamu').AsCurrency := AdoQuery.FieldByName('yhamu').AsCurrency;
        AdsQuery.ParamByName('yhasuc').AsCurrency := AdoQuery.FieldByName('yhasuc').AsCurrency;
        AdsQuery.ParamByName('yhacm').AsCurrency := AdoQuery.FieldByName('yhacm').AsCurrency;
        AdsQuery.ParamByName('yhdvac').AsFloat := AdoQuery.FieldByName('yhdvac').AsFloat;
        AdsQuery.ParamByName('yhdhol').AsFloat := AdoQuery.FieldByName('yhdhol').AsFloat;
        AdsQuery.ParamByName('yhdsck').AsFloat := AdoQuery.FieldByName('yhdsck').AsFloat;
        AdsQuery.ParamByName('yhavac').AsFloat := AdoQuery.FieldByName('yhavac').AsFloat;
        AdsQuery.ParamByName('yhahol').AsFloat := AdoQuery.FieldByName('yhahol').AsFloat;
        AdsQuery.ParamByName('yhasck').AsFloat := AdoQuery.FieldByName('yhasck').AsFloat;
        AdsQuery.ParamByName('yhdhrs').AsFloat := AdoQuery.FieldByName('yhdhrs').AsFloat;
        AdsQuery.ParamByName('yhdoth').AsFloat := AdoQuery.FieldByName('yhdoth').AsFloat;
        AdsQuery.ParamByName('yhdahr').AsFloat := AdoQuery.FieldByName('yhdahr').AsFloat;
        AdsQuery.ParamByName('yhvacd').AsFloat := AdoQuery.FieldByName('yhvacd').AsFloat;
        AdsQuery.ParamByName('yhhold').AsFloat := AdoQuery.FieldByName('yhhold').AsFloat;
        AdsQuery.ParamByName('yhsckd').AsFloat := AdoQuery.FieldByName('yhsckd').AsFloat;
        AdsQuery.ParamByName('yhxvac').AsString := AdoQuery.FieldByName('yhxvac').AsString;
        AdsQuery.ParamByName('yhxsck').AsString := AdoQuery.FieldByName('yhxsck').AsString;
        AdsQuery.ParamByName('yhxhol').AsString := AdoQuery.FieldByName('yhxhol').AsString;
        AdsQuery.ParamByName('yhxfed').AsString := AdoQuery.FieldByName('yhxfed').AsString;
        AdsQuery.ParamByName('yhxss').AsString := AdoQuery.FieldByName('yhxss').AsString;
        AdsQuery.ParamByName('yhxsit').AsString := AdoQuery.FieldByName('yhxsit').AsString;
        AdsQuery.ParamByName('yhxcnt').AsString := AdoQuery.FieldByName('yhxcnt').AsString;
        AdsQuery.ParamByName('yhxmu').AsString := AdoQuery.FieldByName('yhxmu').AsString;
        AdsQuery.ParamByName('yhhdte').AsInteger := AdoQuery.FieldByName('yhhdte').AsInteger;
        AdsQuery.ParamByName('yhmari').AsString := AdoQuery.FieldByName('yhmari').AsString;
        AdsQuery.ParamByName('yhpper').AsString := AdoQuery.FieldByName('yhpper').AsString;
        AdsQuery.ParamByName('yhclas').AsString := AdoQuery.FieldByName('yhclas').AsString;
        AdsQuery.ParamByName('yhexss').AsString := AdoQuery.FieldByName('yhexss').AsString;
        AdsQuery.ParamByName('yhfdex').AsInteger := AdoQuery.FieldByName('yhfdex').AsInteger;
        AdsQuery.ParamByName('yhfdad').AsCurrency := AdoQuery.FieldByName('yhfdad').AsCurrency;
        AdsQuery.ParamByName('yheeic').AsString := AdoQuery.FieldByName('yheeic').AsString;
        AdsQuery.ParamByName('yhecls').AsString := AdoQuery.FieldByName('yhecls').AsString;
        AdsQuery.ParamByName('yhstex').AsInteger := AdoQuery.FieldByName('yhstex').AsInteger;
        AdsQuery.ParamByName('yhstfx').AsCurrency := AdoQuery.FieldByName('yhstfx').AsCurrency;
        AdsQuery.ParamByName('yhstad').AsCurrency := AdoQuery.FieldByName('yhstad').AsCurrency;
        AdsQuery.ParamByName('yhwkcd').AsCurrency := AdoQuery.FieldByName('yhwkcd').AsCurrency;
        AdsQuery.ParamByName('yhcwfl').AsString := AdoQuery.FieldByName('yhcwfl').AsString;
        AdsQuery.ParamByName('yhsttx').AsString := AdoQuery.FieldByName('yhsttx').AsString;
        AdsQuery.ParamByName('yhcntx').AsString := AdoQuery.FieldByName('yhcntx').AsString;
        AdsQuery.ParamByName('yhmutx').AsString := AdoQuery.FieldByName('yhmutx').AsString;
        AdsQuery.ParamByName('yhstcd').AsString := AdoQuery.FieldByName('yhstcd').AsString;
        AdsQuery.ParamByName('yhcncd').AsString := AdoQuery.FieldByName('yhcncd').AsString;
        AdsQuery.ParamByName('yhmucd').AsString := AdoQuery.FieldByName('yhmucd').AsString;
        AdsQuery.ParamByName('yhsaly').AsCurrency := AdoQuery.FieldByName('yhsaly').AsCurrency;
        AdsQuery.ParamByName('yhrate').AsCurrency := AdoQuery.FieldByName('yhrate').AsCurrency;
        AdsQuery.ParamByName('yhsala').AsCurrency := AdoQuery.FieldByName('yhsala').AsCurrency;
        AdsQuery.ParamByName('yhrata').AsCurrency := AdoQuery.FieldByName('yhrata').AsCurrency;
        AdsQuery.ParamByName('yhrete').AsString := AdoQuery.FieldByName('yhrete').AsString;
        AdsQuery.ParamByName('yhretp').AsCurrency := AdoQuery.FieldByName('yhretp').AsCurrency;
        AdsQuery.ParamByName('yhrfix').AsCurrency := AdoQuery.FieldByName('yhrfix').AsCurrency;
        AdsQuery.ParamByName('ymdvyr').AsInteger := AdoQuery.FieldByName('ymdvyr').AsInteger;
        AdsQuery.ParamByName('ymdhyr').AsInteger := AdoQuery.FieldByName('ymdhyr').AsInteger;
        AdsQuery.ParamByName('ymdsyr').AsInteger := AdoQuery.FieldByName('ymdsyr').AsInteger;
        AdsQuery.ParamByName('ymname').AsString := AdoQuery.FieldByName('ymname').AsString;
        AdsQuery.ParamByName('ymdept').AsString := AdoQuery.FieldByName('ymdept').AsString;
        AdsQuery.ParamByName('ymsecg').AsInteger := AdoQuery.FieldByName('ymsecg').AsInteger;
        AdsQuery.ParamByName('ymss').AsInteger := AdoQuery.FieldByName('ymss#').AsInteger;
        AdsQuery.ParamByName('ymdist').AsString := AdoQuery.FieldByName('ymdist').AsString;
        AdsQuery.ParamByName('ymar').AsString := AdoQuery.FieldByName('ymar#').AsString;
        AdsQuery.ParamByName('yhvoid').AsString := AdoQuery.FieldByName('yhvoid').AsString;
        AdsQuery.ParamByName('yhvcyy').AsInteger := AdoQuery.FieldByName('yhvcyy').AsInteger;
        AdsQuery.ParamByName('yhvnum').AsInteger := AdoQuery.FieldByName('yhvnum').AsInteger;
        AdsQuery.ParamByName('yhvseq').AsString := AdoQuery.FieldByName('yhvseq').AsString;
        AdsQuery.ParamByName('yhtype').AsString := AdoQuery.FieldByName('yhtype').AsString;
        AdsQuery.ParamByName('yhacty').AsCurrency := AdoQuery.FieldByName('yhacty').AsCurrency;
        AdsQuery.ParamByName('yhasdiw').AsCurrency := AdoQuery.FieldByName('yhasdiw').AsCurrency;
        AdsQuery.ParamByName('yhaotr').AsCurrency := AdoQuery.FieldByName('yhaotr').AsCurrency;
        AdsQuery.ParamByName('yhdird').AsString := AdoQuery.FieldByName('yhdird').AsString;
        AdsQuery.ParamByName('yhftap').AsFloat := AdoQuery.FieldByName('yhftap').AsFloat;
        AdsQuery.ParamByName('yholcs').AsString := AdoQuery.FieldByName('yholcs').AsString;
        AdsQuery.ParamByName('yhstap').AsFloat := AdoQuery.FieldByName('yhstap').AsFloat;
        AdsQuery.ParamByName('yhxfut').AsString := AdoQuery.FieldByName('yhxfut').AsString;
        AdsQuery.ParamByName('yhxovr').AsString := AdoQuery.FieldByName('yhxovr').AsString;
        AdsQuery.ParamByName('yhxrte').AsString := AdoQuery.FieldByName('yhxrte').AsString;
        AdsQuery.ParamByName('yhxrtm').AsString := AdoQuery.FieldByName('yhxrtm').AsString;
        AdsQuery.ParamByName('yhxsde').AsString := AdoQuery.FieldByName('yhxsde').AsString;
        AdsQuery.ParamByName('yhxsdm').AsString := AdoQuery.FieldByName('yhxsdm').AsString;
        AdsQuery.ParamByName('yhxsut').AsString := AdoQuery.FieldByName('yhxsut').AsString;
        AdsQuery.ParamByName('yhcsue').AsFloat := AdoQuery.FieldByName('yhcsue').AsFloat;
        AdsQuery.ParamByName('yhcst2').AsFloat := AdoQuery.FieldByName('yhcst2').AsFloat;
        AdsQuery.ParamByName('yhccn2').AsFloat := AdoQuery.FieldByName('yhccn2').AsFloat;
        AdsQuery.ParamByName('yhcct2').AsFloat := AdoQuery.FieldByName('yhcct2').AsFloat;
        AdsQuery.ParamByName('yhfdst').AsString := AdoQuery.FieldByName('yhfdst').AsString;
        AdsQuery.ParamByName('yhstst').AsString := AdoQuery.FieldByName('yhstst').AsString;
        AdsQuery.ParamByName('yhcnst').AsString := AdoQuery.FieldByName('yhcnst').AsString;
        AdsQuery.ParamByName('yhmust').AsString := AdoQuery.FieldByName('yhmust').AsString;
        AdsQuery.ParamByName('yhsts2').AsString := AdoQuery.FieldByName('yhsts2').AsString;
        AdsQuery.ParamByName('yhcns2').AsString := AdoQuery.FieldByName('yhcns2').AsString;
        AdsQuery.ParamByName('yhmus2').AsString := AdoQuery.FieldByName('yhmus2').AsString;
        AdsQuery.ParamByName('yhstt2').AsString := AdoQuery.FieldByName('yhstt2').AsString;
        AdsQuery.ParamByName('yhcnt2').AsString := AdoQuery.FieldByName('yhcnt2').AsString;
        AdsQuery.ParamByName('yhmut2').AsString := AdoQuery.FieldByName('yhmut2').AsString;
        AdsQuery.ParamByName('yhsdst').AsString := AdoQuery.FieldByName('yhsdst').AsString;
        AdsQuery.ParamByName('yhwkst').AsString := AdoQuery.FieldByName('yhwkst').AsString;
        AdsQuery.ParamByName('yhrpt1').AsString := AdoQuery.FieldByName('yhrpt1').AsString;
        AdsQuery.ParamByName('yhrpt2').AsString := AdoQuery.FieldByName('yhrpt2').AsString;
        AdsQuery.ParamByName('yheeoc').AsString := AdoQuery.FieldByName('yheeoc').AsString;
        AdsQuery.ParamByName('yhotex').AsString := AdoQuery.FieldByName('yhotex').AsString;
        AdsQuery.ParamByName('yhhghc').AsString := AdoQuery.FieldByName('yhhghc').AsString;
        AdsQuery.ParamByName('yhtmp01').AsString := AdoQuery.FieldByName('yhtmp01').AsString;
        AdsQuery.ParamByName('yhtmp02').AsString := AdoQuery.FieldByName('yhtmp02').AsString;
        AdsQuery.ParamByName('yhtmp03').AsString := AdoQuery.FieldByName('yhtmp03').AsString;
        AdsQuery.ParamByName('yhtmp04').AsString := AdoQuery.FieldByName('yhtmp04').AsString;
        AdsQuery.ParamByName('yhtmp05').AsString := AdoQuery.FieldByName('yhtmp05').AsString;
        AdsQuery.ParamByName('yhtmp06').AsString := AdoQuery.FieldByName('yhtmp06').AsString;
        AdsQuery.ParamByName('yhtmp07').AsString := AdoQuery.FieldByName('yhtmp07').AsString;
        AdsQuery.ParamByName('yhtmp08').AsFloat := AdoQuery.FieldByName('yhtmp08').AsFloat;
        AdsQuery.ParamByName('yhtmp09').AsFloat := AdoQuery.FieldByName('yhtmp09').AsFloat;

        AdsQuery.ExecSQL;
//        WriteLn(i);
//        i := i + 1;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaPYHSHDTA'')');
      ScrapeCountQuery(' rydedata.pyhshdta', ' stgArkonaPYHSHDTA');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('PYHSHDTA.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDataModule1.PYCNTRL;
var
  proc: string;
begin
  proc := 'PYCNTRL';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
//      ExecuteQuery(AdsQuery, 'insert into xxxxxtempxxxxxx select * from xxxxxxstgxxxxxx');
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''tmpPYCNTRL'')');
      PrepareQuery(AdsQuery, ' insert into tmpPYCNTRL(' +
          'YCO#, YCYY, YPDTE, YCKDTE, YCHK#, YFED#, YFICL, YFICEP, YFICMP, ' +
          'YFICEA, YFICMA, YMEDL, YMEDEP, YMEDMP, YMEDEA, YMEDMA, YFUCL, ' +
          'YFUCP, YFUCMA, YBANK, YPAYAC, YFXACT, YRETAC, YARAC, YAVAC, ' +
          'YAHOL, YASCK, YVACR, YHOLR, YSCKR, YVACOP, YHOLOP, YSCKOP, ' +
          'YVACAC, YHOLAC, YSCKAC, YVACSH, YHOLSH, YSCKSH, YVACCD, ' +
          'YHOLCD, YSCKCD, YVACDM, YHOLDM, YSCKDM, YVACMX, YHOLMX, ' +
          'YSCKMX, YRETP, YRETAP, Y401K, YCAFCD, YPAYYR, YGLYR, YGLMO, ' +
          'YQTRUP, YPNCK, YHRDAY, YDWEEK, YASRC, YRETL, YMSID, YRETMP, ' +
          'YRETMY, YHLDSW, YRETAG, YHOLDA, YGLLIB, YGLPGM, YGLFIL, ' +
          'YALTPO, YPNUM, YFLAG0, YFLAG1, YFLAG2, YFLAG3, YFLAG4, ' +
          'YFLAG5, YFLAG6, YFLAG7, YFLAG8, YFLAG9, YGLDDC, YRETMP1, ' +
          'YRETMP2, YRETP1, YRETP2, YSCKCS, YVACCS, Y401L, Y401S) ' +
          'values(' +
          ':YCO, :YCYY, :YPDTE, :YCKDTE, :YCHK, :YFED, :YFICL, :YFICEP, :YFICMP, ' +
          ':YFICEA, :YFICMA, :YMEDL, :YMEDEP, :YMEDMP, :YMEDEA, :YMEDMA, :YFUCL, ' +
          ':YFUCP, :YFUCMA, :YBANK, :YPAYAC, :YFXACT, :YRETAC, :YARAC, :YAVAC, ' +
          ':YAHOL, :YASCK, :YVACR, :YHOLR, :YSCKR, :YVACOP, :YHOLOP, :YSCKOP, ' +
          ':YVACAC, :YHOLAC, :YSCKAC, :YVACSH, :YHOLSH, :YSCKSH, :YVACCD, ' +
          ':YHOLCD, :YSCKCD, :YVACDM, :YHOLDM, :YSCKDM, :YVACMX, :YHOLMX, ' +
          ':YSCKMX, :YRETP, :YRETAP, :Y401K, :YCAFCD, :YPAYYR, :YGLYR, :YGLMO, ' +
          ':YQTRUP, :YPNCK, :YHRDAY, :YDWEEK, :YASRC, :YRETL, :YMSID, :YRETMP, ' +
          ':YRETMY, :YHLDSW, :YRETAG, :YHOLDA, :YGLLIB, :YGLPGM, :YGLFIL, ' +
          ':YALTPO, :YPNUM, :YFLAG0, :YFLAG1, :YFLAG2, :YFLAG3, :YFLAG4, ' +
          ':YFLAG5, :YFLAG6, :YFLAG7, :YFLAG8, :YFLAG9, :YGLDDC, :YRETMP1, ' +
          ':YRETMP2, :YRETP1, :YRETP2, :YSCKCS, :YVACCS, :Y401L, :Y401S)');
      OpenQuery(AdoQuery, 'select ' +
          'YCO#, YCYY, YPDTE, YCKDTE, YCHK#, YFED#, YFICL, YFICEP, YFICMP, ' +
          'YFICEA, YFICMA, YMEDL, YMEDEP, YMEDMP, YMEDEA, YMEDMA, YFUCL, ' +
          'YFUCP, YFUCMA, YBANK, YPAYAC, YFXACT, YRETAC, YARAC, YAVAC, ' +
          'YAHOL, YASCK, YVACR, YHOLR, YSCKR, YVACOP, YHOLOP, YSCKOP, ' +
          'YVACAC, YHOLAC, YSCKAC, YVACSH, YHOLSH, YSCKSH, YVACCD, ' +
          'YHOLCD, YSCKCD, YVACDM, YHOLDM, YSCKDM, YVACMX, YHOLMX, ' +
          'YSCKMX, YRETP, YRETAP, Y401K, YCAFCD, YPAYYR, YGLYR, YGLMO, ' +
          'YQTRUP, YPNCK, YHRDAY, YDWEEK, YASRC, YRETL, YMSID, YRETMP, ' +
          'YRETMY, YHLDSW, YRETAG, YHOLDA, YGLLIB, YGLPGM, YGLFIL, ' +
          'YALTPO, YPNUM, YFLAG0, YFLAG1, YFLAG2, YFLAG3, YFLAG4, ' +
          'YFLAG5, YFLAG6, YFLAG7, YFLAG8, YFLAG9, YGLDDC, YRETMP1, ' +
          'YRETMP2, YRETP1, YRETP2, YSCKCS, YVACCS, Y401L, Y401S ' +
          'from rydedata.pycntrl');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('yco').AsString := AdoQuery.FieldByName('yco#').AsString;
        AdsQuery.ParamByName('ycyy').AsInteger := AdoQuery.FieldByName('ycyy').AsInteger;
        AdsQuery.ParamByName('ypdte').AsInteger := AdoQuery.FieldByName('ypdte').AsInteger;
        AdsQuery.ParamByName('yckdte').AsInteger := AdoQuery.FieldByName('yckdte').AsInteger;
        AdsQuery.ParamByName('ychk').AsInteger := AdoQuery.FieldByName('ychk#').AsInteger;
        AdsQuery.ParamByName('yfed').AsString := AdoQuery.FieldByName('yfed#').AsString;
        AdsQuery.ParamByName('yficl').AsInteger := AdoQuery.FieldByName('yficl').AsInteger;
        AdsQuery.ParamByName('yficep').AsFloat := AdoQuery.FieldByName('yficep').AsFloat;
        AdsQuery.ParamByName('yficmp').AsFloat := AdoQuery.FieldByName('yficmp').AsFloat;
        AdsQuery.ParamByName('yficea').AsString := AdoQuery.FieldByName('yficea').AsString;
        AdsQuery.ParamByName('yficma').AsString := AdoQuery.FieldByName('yficma').AsString;
        AdsQuery.ParamByName('ymedl').AsInteger := AdoQuery.FieldByName('ymedl').AsInteger;
        AdsQuery.ParamByName('ymedep').AsFloat := AdoQuery.FieldByName('ymedep').AsFloat;
        AdsQuery.ParamByName('ymedmp').AsFloat := AdoQuery.FieldByName('ymedmp').AsFloat;
        AdsQuery.ParamByName('ymedea').AsString := AdoQuery.FieldByName('ymedea').AsString;
        AdsQuery.ParamByName('ymedma').AsString := AdoQuery.FieldByName('ymedma').AsString;
        AdsQuery.ParamByName('yfucl').AsInteger := AdoQuery.FieldByName('yfucl').AsInteger;
        AdsQuery.ParamByName('yfucp').AsFloat := AdoQuery.FieldByName('yfucp').AsFloat;
        AdsQuery.ParamByName('yfucma').AsString := AdoQuery.FieldByName('yfucma').AsString;
        AdsQuery.ParamByName('ybank').AsString := AdoQuery.FieldByName('ybank').AsString;
        AdsQuery.ParamByName('ypayac').AsString := AdoQuery.FieldByName('ypayac').AsString;
        AdsQuery.ParamByName('yfxact').AsString := AdoQuery.FieldByName('yfxact').AsString;
        AdsQuery.ParamByName('yretac').AsString := AdoQuery.FieldByName('yretac').AsString;
        AdsQuery.ParamByName('yarac').AsString := AdoQuery.FieldByName('yarac').AsString;
        AdsQuery.ParamByName('yavac').AsString := AdoQuery.FieldByName('yavac').AsString;
        AdsQuery.ParamByName('yahol').AsString := AdoQuery.FieldByName('yahol').AsString;
        AdsQuery.ParamByName('yasck').AsString := AdoQuery.FieldByName('yasck').AsString;
        AdsQuery.ParamByName('yvacr').AsInteger := AdoQuery.FieldByName('yvacr').AsInteger;
        AdsQuery.ParamByName('yholr').AsInteger := AdoQuery.FieldByName('yholr').AsInteger;
        AdsQuery.ParamByName('ysckr').AsInteger := AdoQuery.FieldByName('ysckr').AsInteger;
        AdsQuery.ParamByName('yvacop').AsString := AdoQuery.FieldByName('yvacop').AsString;
        AdsQuery.ParamByName('yholop').AsString := AdoQuery.FieldByName('yholop').AsString;
        AdsQuery.ParamByName('ysckop').AsString := AdoQuery.FieldByName('ysckop').AsString;
        AdsQuery.ParamByName('yvacac').AsString := AdoQuery.FieldByName('yvacac').AsString;
        AdsQuery.ParamByName('yholac').AsString := AdoQuery.FieldByName('yholac').AsString;
        AdsQuery.ParamByName('ysckac').AsString := AdoQuery.FieldByName('ysckac').AsString;
        AdsQuery.ParamByName('yvacsh').AsString := AdoQuery.FieldByName('yvacsh').AsString;
        AdsQuery.ParamByName('yholsh').AsString := AdoQuery.FieldByName('yholsh').AsString;
        AdsQuery.ParamByName('yscksh').AsString := AdoQuery.FieldByName('yscksh').AsString;
        AdsQuery.ParamByName('yvaccd').AsString := AdoQuery.FieldByName('yvaccd').AsString;
        AdsQuery.ParamByName('yholcd').AsString := AdoQuery.FieldByName('yholcd').AsString;
        AdsQuery.ParamByName('ysckcd').AsString := AdoQuery.FieldByName('ysckcd').AsString;
        AdsQuery.ParamByName('yvacdm').AsInteger := AdoQuery.FieldByName('yvacdm').AsInteger;
        AdsQuery.ParamByName('yholdm').AsInteger := AdoQuery.FieldByName('yholdm').AsInteger;
        AdsQuery.ParamByName('ysckdm').AsInteger := AdoQuery.FieldByName('ysckdm').AsInteger;
        AdsQuery.ParamByName('yvacmx').AsInteger := AdoQuery.FieldByName('yvacmx').AsInteger;
        AdsQuery.ParamByName('yholmx').AsInteger := AdoQuery.FieldByName('yholmx').AsInteger;
        AdsQuery.ParamByName('ysckmx').AsInteger := AdoQuery.FieldByName('ysckmx').AsInteger;
        AdsQuery.ParamByName('yretp').AsFloat := AdoQuery.FieldByName('yretp').AsFloat;
        AdsQuery.ParamByName('yretap').AsFloat := AdoQuery.FieldByName('yretap').AsFloat;
        AdsQuery.ParamByName('y401k').AsString := AdoQuery.FieldByName('y401k').AsString;
        AdsQuery.ParamByName('ycafcd').AsString := AdoQuery.FieldByName('ycafcd').AsString;
        AdsQuery.ParamByName('ypayyr').AsInteger := AdoQuery.FieldByName('ypayyr').AsInteger;
        AdsQuery.ParamByName('yglyr').AsInteger := AdoQuery.FieldByName('yglyr').AsInteger;
        AdsQuery.ParamByName('yglmo').AsInteger := AdoQuery.FieldByName('yglmo').AsInteger;
        AdsQuery.ParamByName('yqtrup').AsInteger := AdoQuery.FieldByName('yqtrup').AsInteger;
        AdsQuery.ParamByName('ypnck').AsString := AdoQuery.FieldByName('ypnck').AsString;
        AdsQuery.ParamByName('yhrday').AsFloat := AdoQuery.FieldByName('yhrday').AsFloat;
        AdsQuery.ParamByName('ydweek').AsInteger := AdoQuery.FieldByName('ydweek').AsInteger;
        AdsQuery.ParamByName('yasrc').AsString := AdoQuery.FieldByName('yasrc').AsString;
        AdsQuery.ParamByName('yretl').AsInteger := AdoQuery.FieldByName('yretl').AsInteger;
        AdsQuery.ParamByName('ymsid').AsString := AdoQuery.FieldByName('ymsid').AsString;
        AdsQuery.ParamByName('yretmp').AsFloat := AdoQuery.FieldByName('yretmp').AsFloat;
        AdsQuery.ParamByName('yretmy').AsString := AdoQuery.FieldByName('yretmy').AsString;
        AdsQuery.ParamByName('yhldsw').AsString := AdoQuery.FieldByName('yhldsw').AsString;
        AdsQuery.ParamByName('yretag').AsString := AdoQuery.FieldByName('yretag').AsString;
        AdsQuery.ParamByName('yholda').AsString := AdoQuery.FieldByName('yholda').AsString;
        AdsQuery.ParamByName('ygllib').AsString := AdoQuery.FieldByName('ygllib').AsString;
        AdsQuery.ParamByName('yglpgm').AsString := AdoQuery.FieldByName('yglpgm').AsString;
        AdsQuery.ParamByName('yglfil').AsString := AdoQuery.FieldByName('yglfil').AsString;
        AdsQuery.ParamByName('yaltpo').AsString := AdoQuery.FieldByName('yaltpo').AsString;
        AdsQuery.ParamByName('ypnum').AsInteger := AdoQuery.FieldByName('ypnum').AsInteger;
        AdsQuery.ParamByName('yflag0').AsString := AdoQuery.FieldByName('yflag0').AsString;
        AdsQuery.ParamByName('yflag1').AsString := AdoQuery.FieldByName('yflag1').AsString;
        AdsQuery.ParamByName('yflag2').AsString := AdoQuery.FieldByName('yflag2').AsString;
        AdsQuery.ParamByName('yflag3').AsString := AdoQuery.FieldByName('yflag3').AsString;
        AdsQuery.ParamByName('yflag4').AsString := AdoQuery.FieldByName('yflag4').AsString;
        AdsQuery.ParamByName('yflag5').AsString := AdoQuery.FieldByName('yflag5').AsString;
        AdsQuery.ParamByName('yflag6').AsString := AdoQuery.FieldByName('yflag6').AsString;
        AdsQuery.ParamByName('yflag7').AsString := AdoQuery.FieldByName('yflag7').AsString;
        AdsQuery.ParamByName('yflag8').AsString := AdoQuery.FieldByName('yflag8').AsString;
        AdsQuery.ParamByName('yflag9').AsString := AdoQuery.FieldByName('yflag9').AsString;
        AdsQuery.ParamByName('yglddc').AsString := AdoQuery.FieldByName('yglddc').AsString;
        AdsQuery.ParamByName('yretmp1').AsFloat := AdoQuery.FieldByName('yretmp1').AsFloat;
        AdsQuery.ParamByName('yretmp2').AsFloat := AdoQuery.FieldByName('yretmp2').AsFloat;
        AdsQuery.ParamByName('yretp1').AsFloat := AdoQuery.FieldByName('yretp1').AsFloat;
        AdsQuery.ParamByName('yretp2').AsFloat := AdoQuery.FieldByName('yretp2').AsFloat;
        AdsQuery.ParamByName('ysckcs').AsString := AdoQuery.FieldByName('ysckcs').AsString;
        AdsQuery.ParamByName('yvaccs').AsString := AdoQuery.FieldByName('yvaccs').AsString;
        AdsQuery.ParamByName('y401l').AsInteger := AdoQuery.FieldByName('y401l').AsInteger;
        AdsQuery.ParamByName('y401s').AsInteger := AdoQuery.FieldByName('y401s').AsInteger;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''tmpPYCNTRL'')');
      ScrapeCountTable('PYCNTRL', 'tmpPYCNTRL');
      ExecuteQuery(AdsQuery, 'execute procedure stgArkPYCNTRL()');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('PYCNTRL.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDataModule1.PYDEDUCT;
var
  proc: string;
begin
  proc := 'PYDEDUCT';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
//      ExecuteQuery(AdsQuery, 'insert into xxxxxtempxxxxxx select * from xxxxxxstgxxxxxx');
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaPYDEDUCT'')');
      PrepareQuery(AdsQuery, ' insert into stgArkonaPYDEDUCT(' +
          'COMPANY_NUMBER, EMPLOYEE_NUMBER, CODE_TYPE, DED_PAY_CODE, SEQUENCE, ' +
          'FIXED_DED_AMT, VARIABLE_AMOUNT, FIXED_DED_PCT, DED_LIMIT, DED_FREQ, ' +
          'TOTAL, TAXING_UNIT_ID_NUMBER, TAXING_UNIT_TYPE, BANK_ROUTING_, ' +
          'BANK_ACCOUNT, CODE_BEGINNING_DATE, CODE_ENDING_DATE, ' +
          'FIXED_DED_AMT_2, VARIABLE_AMOUNT_2, FIXED_DED_PCT_2, ' +
          'EMP_LEVEL_CTL_, PLAN_POLICY_OF_GARN, FUTURE_1, FUTURE_2, YEAR_FOR_USE, DED_PRIORITY_SEQ)' +
          'values(' +
          ':COMPANY_NUMBER, :EMPLOYEE_NUMBER, :CODE_TYPE, :DED_PAY_CODE, :SEQUENCE, ' +
          ':FIXED_DED_AMT, :VARIABLE_AMOUNT, :FIXED_DED_PCT, :DED_LIMIT, :DED_FREQ, ' +
          ':TOTAL, :TAXING_UNIT_ID_NUMBER, :TAXING_UNIT_TYPE, :BANK_ROUTING_, ' +
          ':BANK_ACCOUNT, :CODE_BEGINNING_DATE, :CODE_ENDING_DATE, ' +
          ':FIXED_DED_AMT_2, :VARIABLE_AMOUNT_2, :FIXED_DED_PCT_2, ' +
          ':EMP_LEVEL_CTL_, :PLAN_POLICY_OF_GARN, :FUTURE_1, :FUTURE_2, :YEAR_FOR_USE, :DED_PRIORITY_SEQ)');
      OpenQuery(AdoQuery, 'select ' +
          'COMPANY_NUMBER, EMPLOYEE_NUMBER, CODE_TYPE, DED_PAY_CODE, SEQUENCE, ' +
          'FIXED_DED_AMT, VARIABLE_AMOUNT, FIXED_DED_PCT, DED_LIMIT, DED_FREQ, ' +
          'TOTAL, TAXING_UNIT_ID_NUMBER, TAXING_UNIT_TYPE, BANK_ROUTING_, ' +
          'BANK_ACCOUNT, CODE_BEGINNING_DATE, CODE_ENDING_DATE, ' +
          'FIXED_DED_AMT_2, VARIABLE_AMOUNT_2, FIXED_DED_PCT_2, ' +
          'EMP_LEVEL_CTL_, PLAN_POLICY_OF_GARN, FUTURE_1, FUTURE_2, ' +
          'YEAR_FOR_USE, DED_PRIORITY_SEQ ' +
          'from rydedata.pydeduct');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('COMPANY_NUMBER').AsString := AdoQuery.FieldByName('COMPANY_NUMBER').AsString;
        AdsQuery.ParamByName('EMPLOYEE_NUMBER').AsString := AdoQuery.FieldByName('EMPLOYEE_NUMBER').AsString;
        AdsQuery.ParamByName('CODE_TYPE').AsString := AdoQuery.FieldByName('CODE_TYPE').AsString;
        AdsQuery.ParamByName('DED_PAY_CODE').AsString := AdoQuery.FieldByName('DED_PAY_CODE').AsString;
        AdsQuery.ParamByName('SEQUENCE').AsInteger := AdoQuery.FieldByName('SEQUENCE').AsInteger;
        AdsQuery.ParamByName('FIXED_DED_AMT').AsFloat := AdoQuery.FieldByName('FIXED_DED_AMT').AsFloat;
        AdsQuery.ParamByName('VARIABLE_AMOUNT').AsString := AdoQuery.FieldByName('VARIABLE_AMOUNT').AsString;
        AdsQuery.ParamByName('FIXED_DED_PCT').AsString := AdoQuery.FieldByName('FIXED_DED_PCT').AsString;
        AdsQuery.ParamByName('DED_LIMIT').AsFloat := AdoQuery.FieldByName('DED_LIMIT').AsFloat;
        AdsQuery.ParamByName('DED_FREQ').AsString := AdoQuery.FieldByName('DED_FREQ').AsString;
        AdsQuery.ParamByName('TOTAL').AsFloat := AdoQuery.FieldByName('TOTAL').AsFloat;
        AdsQuery.ParamByName('TAXING_UNIT_ID_NUMBER').AsString := AdoQuery.FieldByName('TAXING_UNIT_ID_NUMBER').AsString;
        AdsQuery.ParamByName('TAXING_UNIT_TYPE').AsString := AdoQuery.FieldByName('TAXING_UNIT_TYPE').AsString;
        AdsQuery.ParamByName('BANK_ROUTING_').AsInteger := AdoQuery.FieldByName('BANK_ROUTING_').AsInteger;
        AdsQuery.ParamByName('BANK_ACCOUNT').AsInteger := AdoQuery.FieldByName('BANK_ACCOUNT').AsInteger;
        AdsQuery.ParamByName('CODE_BEGINNING_DATE').AsInteger := AdoQuery.FieldByName('CODE_BEGINNING_DATE').AsInteger;
        AdsQuery.ParamByName('CODE_ENDING_DATE').AsInteger := AdoQuery.FieldByName('CODE_ENDING_DATE').AsInteger;
        AdsQuery.ParamByName('FIXED_DED_AMT_2').AsFloat := AdoQuery.FieldByName('FIXED_DED_AMT_2').AsFloat;
        AdsQuery.ParamByName('VARIABLE_AMOUNT_2').AsString := AdoQuery.FieldByName('VARIABLE_AMOUNT_2').AsString;
        AdsQuery.ParamByName('FIXED_DED_PCT_2').AsString := AdoQuery.FieldByName('FIXED_DED_PCT_2').AsString;
        AdsQuery.ParamByName('EMP_LEVEL_CTL_').AsString := AdoQuery.FieldByName('EMP_LEVEL_CTL_').AsString;
        AdsQuery.ParamByName('PLAN_POLICY_OF_GARN').AsString := AdoQuery.FieldByName('PLAN_POLICY_OF_GARN').AsString;
        AdsQuery.ParamByName('FUTURE_1').AsString := AdoQuery.FieldByName('FUTURE_1').AsString;
        AdsQuery.ParamByName('FUTURE_2').AsString := AdoQuery.FieldByName('FUTURE_2').AsString;
        AdsQuery.ParamByName('YEAR_FOR_USE').AsInteger := AdoQuery.FieldByName('YEAR_FOR_USE').AsInteger;
        AdsQuery.ParamByName('DED_PRIORITY_SEQ').AsInteger := AdoQuery.FieldByName('DED_PRIORITY_SEQ').AsInteger;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaPYDEDUCT'')');
      ScrapeCountTable('PYDEDUCT', 'stgArkonaPYDEDUCT');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('PYDEDUCT.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;


procedure TDataModule1.SDPXTIM;
var
  proc: string;
begin
  proc := 'SDPXTIM';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
//      ExecuteQuery(AdsQuery, 'insert into xxxxxtempxxxxxx select * from xxxxxxstgxxxxxx');
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaSDPXTIM'')');
      PrepareQuery(AdsQuery, ' insert into stgArkonaSDPXTIM(' +
          'PTCO#, PTPKEY, PTRO#, PTLINE, PTDATE, PTCOST, PTLPYM, PTTECH, PTLHRS) ' +
          'values(' +
          ':PTCO, :PTPKEY, :PTRO, :PTLINE, :PTDATE, :PTCOST, :PTLPYM, :PTTECH, :PTLHRS)');
      OpenQuery(AdoQuery, 'select ' +
          'ptco#, ptpkey, ptro#, ptline, ' +
          'case ' +
          '  when PTDATE = 0 then cast(''9999-12-31'' as date) ' +
          '  else cast(left(digits(PTDATE), 4) || ''-'' || substr(digits(PTDATE), 5, 2) || ''-'' || substr(digits(PTDATE), 7, 2) as date) ' +
          'end as PTDATE, ' +
          'ptcost, ptlpym, pttech, ptlhrs ' +
          'from rydedata.sdpxtim');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('ptco').AsString := AdoQuery.FieldByName('ptco#').AsString;
        AdsQuery.ParamByName('ptpkey').AsInteger := AdoQuery.FieldByName('ptpkey').AsInteger;
        AdsQuery.ParamByName('ptro').AsString := AdoQuery.FieldByName('ptro#').AsString;
        AdsQuery.ParamByName('ptline').AsInteger := AdoQuery.FieldByName('ptline').AsInteger;
        AdsQuery.ParamByName('ptdate').AsDateTime := AdoQuery.FieldByName('ptdate').AsDateTime;
        AdsQuery.ParamByName('ptcost').AsCurrency := AdoQuery.FieldByName('ptcost').AsCurrency;
        AdsQuery.ParamByName('ptlpym').AsString := AdoQuery.FieldByName('ptlpym').AsString;
        AdsQuery.ParamByName('pttech').AsString := AdoQuery.FieldByName('pttech').AsString;
        AdsQuery.ParamByName('ptlhrs').AsFloat := AdoQuery.FieldByName('ptlhrs').AsFloat;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaSDPXTIM'')');
      ScrapeCountTable('SDPXTIM', 'stgArkonaSDPXTIM');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('SDPXTIM.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDataModule1.GLPCUST;
var
  proc: string;
begin
  proc := 'GLPCUST';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
//      ExecuteQuery(AdsQuery, 'insert into xxxxxtempxxxxxx select * from xxxxxxstgxxxxxx');
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaGLPCUST'')');
      PrepareQuery(AdsQuery, ' insert into stgArkonaGLPCUST(' +
          'GCCO#, GCKEY, GCACTIVE, GCCUS#, GCVND#, GCSNAM, GCBCNT, GCBAD1, ' +
          'GCBAD2, GCBCTY, GCBSTC, GCBZIP, GCBPHN, GCBEXT, GCBFAX, GCPCNT, ' +
          'GCPAD1, GCPAD2, GCPCTY, GCPSTC, GCPZIP, GCPPHN, GCPEXT, GCPFAX, ' +
          'GCFTID, GCSS#, GCPORQ, GCCTYP, GCVTYP, GCRPTM, GCPPTM, GCTAXG, ' +
          'GCCLMT, GCFCHG, GCCORP, GCCKSD, GCCDAT, GCOPO#, GCCUSAC, GCLIDT, ' +
          'GCLIVA, GCLPDT, GCLPYM, GCLSDT, GCLSBA, GCTRN#, GCDSPC, GCDACCT, ' +
          'GCNAM2, GCBAD3, GCBZIPI, GCBPHNI, GCBEXTI, GCBFAXI, GCPAD3, ' +
          'GCPZIPI, GCPPHNI, GCPEXTI, GCPFAXI, GCBNK#, GCROUT, GCWRK1) ' +
          'values(' +
          ':GCCO, :GCKEY, :GCACTIVE, :GCCUS, :GCVND, :GCSNAM, :GCBCNT, :GCBAD1, ' +
          ':GCBAD2, :GCBCTY, :GCBSTC, :GCBZIP, :GCBPHN, :GCBEXT, :GCBFAX, :GCPCNT, ' +
          ':GCPAD1, :GCPAD2, :GCPCTY, :GCPSTC, :GCPZIP, :GCPPHN, :GCPEXT, :GCPFAX, ' +
          ':GCFTID, :GCSS, :GCPORQ, :GCCTYP, :GCVTYP, :GCRPTM, :GCPPTM, :GCTAXG, ' +
          ':GCCLMT, :GCFCHG, :GCCORP, :GCCKSD, :GCCDAT, :GCOPO, :GCCUSAC, :GCLIDT, ' +
          ':GCLIVA, :GCLPDT, :GCLPYM, :GCLSDT, :GCLSBA, :GCTRN, :GCDSPC, :GCDACCT, ' +
          ':GCNAM2, :GCBAD3, :GCBZIPI, :GCBPHNI, :GCBEXTI, :GCBFAXI, :GCPAD3, ' +
          ':GCPZIPI, :GCPPHNI, :GCPEXTI, :GCPFAXI, :GCBNK, :GCROUT, :GCWRK1)');
      OpenQuery(AdoQuery, 'select ' +
          'GCCO#, GCKEY, GCACTIVE, GCCUS#, GCVND#, GCSNAM, GCBCNT, GCBAD1, ' +
          'GCBAD2, GCBCTY, GCBSTC, GCBZIP, GCBPHN, GCBEXT, GCBFAX, GCPCNT, ' +
          'GCPAD1, GCPAD2, GCPCTY, GCPSTC, GCPZIP, GCPPHN, GCPEXT, GCPFAX, ' +
          'GCFTID, GCSS#, GCPORQ, GCCTYP, GCVTYP, GCRPTM, GCPPTM, GCTAXG, ' +
          'GCCLMT, GCFCHG, GCCORP, GCCKSD, GCCDAT, GCOPO#, GCCUSAC, GCLIDT, ' +
          'GCLIVA, ' +
//          'GCLPDT, ' +
          'case ' +
          '  when GCLPDT < date(''1899-12-31'') then date(''9999-12-31'') ' +
          '  else GCLPDT ' +
          'end as GCLPDT, ' +
          'GCLPYM, GCLSDT, GCLSBA, GCTRN#, GCDSPC, GCDACCT, ' +
          'GCNAM2, GCBAD3, GCBZIPI, GCBPHNI, GCBEXTI, GCBFAXI, GCPAD3, ' +
          'GCPZIPI, GCPPHNI, GCPEXTI, GCPFAXI, GCBNK#, GCROUT, GCWRK1 ' +
          'from rydedata.glpcust');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('gcco').AsString := AdoQuery.FieldByName('gcco#').AsString;
        AdsQuery.ParamByName('gckey').AsInteger := AdoQuery.FieldByName('gckey').AsInteger;
        AdsQuery.ParamByName('gcactive').AsString := AdoQuery.FieldByName('gcactive').AsString;
        AdsQuery.ParamByName('gccus').AsString := AdoQuery.FieldByName('gccus#').AsString;
        AdsQuery.ParamByName('gcvnd').AsString := AdoQuery.FieldByName('gcvnd#').AsString;
        AdsQuery.ParamByName('gcsnam').AsString := AdoQuery.FieldByName('gcsnam').AsString;
        AdsQuery.ParamByName('gcbcnt').AsString := AdoQuery.FieldByName('gcbcnt').AsString;
        AdsQuery.ParamByName('gcbad1').AsString := AdoQuery.FieldByName('gcbad1').AsString;
        AdsQuery.ParamByName('gcbad2').AsString := AdoQuery.FieldByName('gcbad2').AsString;
        AdsQuery.ParamByName('gcbcty').AsString := AdoQuery.FieldByName('gcbcty').AsString;
        AdsQuery.ParamByName('gcbstc').AsString := AdoQuery.FieldByName('gcbstc').AsString;
        AdsQuery.ParamByName('gcbzip').AsString := AdoQuery.FieldByName('gcbzip').AsString;
        AdsQuery.ParamByName('gcbphn').AsString := AdoQuery.FieldByName('gcbphn').AsString;
        AdsQuery.ParamByName('gcbext').AsString := AdoQuery.FieldByName('gcbext').AsString;
        AdsQuery.ParamByName('gcbfax').AsString := AdoQuery.FieldByName('gcbfax').AsString;
        AdsQuery.ParamByName('gcpcnt').AsString := AdoQuery.FieldByName('gcpcnt').AsString;
        AdsQuery.ParamByName('gcpad1').AsString := AdoQuery.FieldByName('gcpad1').AsString;
        AdsQuery.ParamByName('gcpad2').AsString := AdoQuery.FieldByName('gcpad2').AsString;
        AdsQuery.ParamByName('gcpcty').AsString := AdoQuery.FieldByName('gcpcty').AsString;
        AdsQuery.ParamByName('gcpstc').AsString := AdoQuery.FieldByName('gcpstc').AsString;
        AdsQuery.ParamByName('gcpzip').AsString := AdoQuery.FieldByName('gcpzip').AsString;
        AdsQuery.ParamByName('gcpphn').AsString := AdoQuery.FieldByName('gcpphn').AsString;
        AdsQuery.ParamByName('gcpext').AsString := AdoQuery.FieldByName('gcpext').AsString;
        AdsQuery.ParamByName('gcpfax').AsString := AdoQuery.FieldByName('gcpfax').AsString;
        AdsQuery.ParamByName('gcftid').AsString := AdoQuery.FieldByName('gcftid').AsString;
        AdsQuery.ParamByName('gcss').AsString := AdoQuery.FieldByName('gcss#').AsString;
        AdsQuery.ParamByName('gcporq').AsString := AdoQuery.FieldByName('gcporq').AsString;
        AdsQuery.ParamByName('gcctyp').AsInteger := AdoQuery.FieldByName('gcctyp').AsInteger;
        AdsQuery.ParamByName('gcvtyp').AsInteger := AdoQuery.FieldByName('gcvtyp').AsInteger;
        AdsQuery.ParamByName('gcrptm').AsInteger := AdoQuery.FieldByName('gcrptm').AsInteger;
        AdsQuery.ParamByName('gcpptm').AsInteger := AdoQuery.FieldByName('gcpptm').AsInteger;
        AdsQuery.ParamByName('gctaxg').AsInteger := AdoQuery.FieldByName('gctaxg').AsInteger;
        AdsQuery.ParamByName('gcclmt').AsCurrency := AdoQuery.FieldByName('gcclmt').AsCurrency;
        AdsQuery.ParamByName('gcfchg').AsString := AdoQuery.FieldByName('gcfchg').AsString;
        AdsQuery.ParamByName('gccorp').AsString := AdoQuery.FieldByName('gccorp').AsString;
        AdsQuery.ParamByName('gccksd').AsString := AdoQuery.FieldByName('gccksd').AsString;
        AdsQuery.ParamByName('gccdat').AsDateTime := AdoQuery.FieldByName('gccdat').AsDateTime;
        AdsQuery.ParamByName('gcopo').AsString := AdoQuery.FieldByName('gcopo#').AsString;
        AdsQuery.ParamByName('gccusac').AsString := AdoQuery.FieldByName('gccusac').AsString;
        AdsQuery.ParamByName('gclidt').AsDateTime := AdoQuery.FieldByName('gclidt').AsDateTime;
        AdsQuery.ParamByName('gcliva').AsCurrency := AdoQuery.FieldByName('gcliva').AsCurrency;
        AdsQuery.ParamByName('gclpdt').AsDateTime := AdoQuery.FieldByName('gclpdt').AsDateTime;
        AdsQuery.ParamByName('gclpym').AsCurrency := AdoQuery.FieldByName('gclpym').AsCurrency;
        AdsQuery.ParamByName('gclsdt').AsDateTime := AdoQuery.FieldByName('gclsdt').AsDateTime;
        AdsQuery.ParamByName('gclsba').AsCurrency := AdoQuery.FieldByName('gclsba').AsCurrency;
        AdsQuery.ParamByName('gctrn').AsInteger := AdoQuery.FieldByName('gctrn#').AsInteger;
        AdsQuery.ParamByName('gcdspc').AsString := AdoQuery.FieldByName('gcdspc').AsString;
        AdsQuery.ParamByName('gcdacct').AsString := AdoQuery.FieldByName('gcdacct').AsString;
        AdsQuery.ParamByName('gcnam2').AsString := AdoQuery.FieldByName('gcnam2').AsString;
        AdsQuery.ParamByName('gcbad3').AsString := AdoQuery.FieldByName('gcbad3').AsString;
        AdsQuery.ParamByName('gcbzipi').AsString := AdoQuery.FieldByName('gcbzipi').AsString;
        AdsQuery.ParamByName('gcbphni').AsString := AdoQuery.FieldByName('gcbphni').AsString;
        AdsQuery.ParamByName('gcbexti').AsString := AdoQuery.FieldByName('gcbexti').AsString;
        AdsQuery.ParamByName('gcbfaxi').AsString := AdoQuery.FieldByName('gcbfaxi').AsString;
        AdsQuery.ParamByName('gcpad3').AsString := AdoQuery.FieldByName('gcpad3').AsString;
        AdsQuery.ParamByName('gcpzipi').AsString := AdoQuery.FieldByName('gcpzipi').AsString;
        AdsQuery.ParamByName('gcpphni').AsString := AdoQuery.FieldByName('gcpphni').AsString;
        AdsQuery.ParamByName('gcpexti').AsString := AdoQuery.FieldByName('gcpexti').AsString;
        AdsQuery.ParamByName('gcpfaxi').AsString := AdoQuery.FieldByName('gcpfaxi').AsString;
        AdsQuery.ParamByName('gcbnk').AsString := AdoQuery.FieldByName('gcbnk#').AsString;
        AdsQuery.ParamByName('gcrout').AsString := AdoQuery.FieldByName('gcrout').AsString;
        AdsQuery.ParamByName('gcwrk1').AsString := AdoQuery.FieldByName('gcwrk1').AsString;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaGLPCUST'')');
      ScrapeCountTable('GLPCUST', 'stgArkonaGLPCUST');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('GLPCUST.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

//procedure TDataModule1.BOPSLSS;
//var
//  proc: string;
//begin
//  proc := 'BOPSLSS';
//  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
//    QuotedStr(executable) + ', ' +
//    QuotedStr(proc) + ', ' +
//    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
//  try
//    try
////      ExecuteQuery(AdsQuery, 'insert into xxxxxtempxxxxxx select * from xxxxxxstgxxxxxx');
//      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaBOPSLSS'')');
//      PrepareQuery(AdsQuery, ' insert into stgArkonaBOPSLSS(' +
//          'BQCO#, BQRTYP, BQKEY, BQSPID, BQSTAT, BQDATE, BQNAME, BQTYPE, ' +
//          'BQSTYP, BQPRNT, BQSPLT, BQUNIT, BQUCNT, BQCGRS, BQNGRS, BQGCRT, ' +
//          'BQPCRT, BQTCRT, BQHCRT, BQDCRT, BQRCRT, BQICRT, BQSCRT, BQACRT, ' +
//          'BQNCRT, BQGBON, BQPBON, BQTBON, BQHBON, BQDBON, BQRBON, BQIBON, ' +
//          'BQSBON, BQABON, BQNBON, BQGMIN, BQPMIN, BQTMIN, BQHMIN, BQDMIN, ' +
//          'BQRMIN, BQIMIN, BQSMIN, BQAMIN, BQNMIN, BQGMINO, BQPMINO, BQTMINO, ' +
//          'BQHMINO, BQDMINO, BQRMINO, BQIMINO, BQSMINO, BQAMINO, BQNMINO, ' +
//          'BQGCOM, BQPCOM, BQTCOM, BQHCOM, BQDCOM, BQRCOM, BQICOM, BQSCOM, ' +
//          'BQACOM, BQNCOM, BQTOTCOM, BQACCRT, BQACBON, BQACMIN, BQACMINO) ' +
//          'values(' +
//          ':BQCO, :BQRTYP, :BQKEY, :BQSPID, :BQSTAT, :BQDATE, :BQNAME, :BQTYPE, ' +
//          ':BQSTYP, :BQPRNT, :BQSPLT, :BQUNIT, :BQUCNT, :BQCGRS, :BQNGRS, :BQGCRT, ' +
//          ':BQPCRT, :BQTCRT, :BQHCRT, :BQDCRT, :BQRCRT, :BQICRT, :BQSCRT, :BQACRT, ' +
//          ':BQNCRT, :BQGBON, :BQPBON, :BQTBON, :BQHBON, :BQDBON, :BQRBON, :BQIBON, ' +
//          ':BQSBON, :BQABON, :BQNBON, :BQGMIN, :BQPMIN, :BQTMIN, :BQHMIN, :BQDMIN, ' +
//          ':BQRMIN, :BQIMIN, :BQSMIN, :BQAMIN, :BQNMIN, :BQGMINO, :BQPMINO, :BQTMINO, ' +
//          ':BQHMINO, :BQDMINO, :BQRMINO, :BQIMINO, :BQSMINO, :BQAMINO, :BQNMINO, ' +
//          ':BQGCOM, :BQPCOM, :BQTCOM, :BQHCOM, :BQDCOM, :BQRCOM, :BQICOM, :BQSCOM, ' +
//          ':BQACOM, :BQNCOM, :BQTOTCOM, :BQACCRT, :BQACBON, :BQACMIN, :BQACMINO)');
//      OpenQuery(AdoQuery, 'select ' +
//          'BQCO#, BQRTYP, BQKEY, BQSPID, BQSTAT, ' +
//          'case ' +
//          '  when BQDATE = 0 then cast(''9999-12-31'' as date) ' +
//          '  else cast(left(digits(BQDATE), 4) || ''-'' || substr(digits(BQDATE), 5, 2) || ''-'' || substr(digits(BQDATE), 7, 2) as date) ' +
//          'end as BQDATE, ' +
//          'BQNAME, BQTYPE, BQSTYP, BQPRNT, BQSPLT, ' +
//          'BQUNIT, BQUCNT, BQCGRS, BQNGRS, BQGCRT, ' +
//          'BQPCRT, BQTCRT, BQHCRT, BQDCRT, BQRCRT, ' +
//          'BQICRT, BQSCRT, BQACRT, BQNCRT, BQGBON, ' +
//          'BQPBON, BQTBON, BQHBON, BQDBON, BQRBON, ' +
//          'BQIBON, BQSBON, BQABON, BQNBON, BQGMIN, ' +
//          'BQPMIN, BQTMIN, BQHMIN, BQDMIN, BQRMIN, ' +
//          'BQIMIN, BQSMIN, BQAMIN, BQNMIN, BQGMINO, ' +
//          'BQPMINO, BQTMINO, BQHMINO, BQDMINO, ' +
//          'BQRMINO, BQIMINO, BQSMINO, BQAMINO, ' +
//          'BQNMINO, BQGCOM, BQPCOM, BQTCOM, BQHCOM, ' +
//          'BQDCOM, BQRCOM, BQICOM, BQSCOM, BQACOM, ' +
//          'BQNCOM, BQTOTCOM, BQACCRT, BQACBON, ' +
//          'BQACMIN, BQACMINO ' +
//          'from rydedata.bopslss');
//      while not AdoQuery.Eof do
//      begin
//        AdsQuery.ParamByName('bqco').AsString := AdoQuery.FieldByName('bqco#').AsString;
//        AdsQuery.ParamByName('bqrtyp').AsString := AdoQuery.FieldByName('bqrtyp').AsString;
//        AdsQuery.ParamByName('bqkey').AsInteger := AdoQuery.FieldByName('bqkey').AsInteger;
//        AdsQuery.ParamByName('bqspid').AsString := AdoQuery.FieldByName('bqspid').AsString;
//        AdsQuery.ParamByName('bqstat').AsString := AdoQuery.FieldByName('bqstat').AsString;
//        AdsQuery.ParamByName('bqdate').AsDateTime := AdoQuery.FieldByName('bqdate').AsDateTime;
//        AdsQuery.ParamByName('bqname').AsString := AdoQuery.FieldByName('bqname').AsString;
//        AdsQuery.ParamByName('bqtype').AsString := AdoQuery.FieldByName('bqtype').AsString;
//        AdsQuery.ParamByName('bqstyp').AsString := AdoQuery.FieldByName('bqstyp').AsString;
//        AdsQuery.ParamByName('bqprnt').AsString := AdoQuery.FieldByName('bqprnt').AsString;
//        AdsQuery.ParamByName('bqsplt').AsString := AdoQuery.FieldByName('bqsplt').AsString;
//        AdsQuery.ParamByName('bqunit').AsString := AdoQuery.FieldByName('bqunit').AsString;
//        AdsQuery.ParamByName('bqucnt').AsInteger := AdoQuery.FieldByName('bqucnt').AsInteger;
//        AdsQuery.ParamByName('bqcgrs').AsCurrency := AdoQuery.FieldByName('bqcgrs').AsCurrency;
//        AdsQuery.ParamByName('bqngrs').AsCurrency := AdoQuery.FieldByName('bqngrs').AsCurrency;
//        AdsQuery.ParamByName('bqgcrt').AsFloat := AdoQuery.FieldByName('bqgcrt').AsFloat;
//        AdsQuery.ParamByName('bqpcrt').AsFloat := AdoQuery.FieldByName('bqpcrt').AsFloat;
//        AdsQuery.ParamByName('bqtcrt').AsFloat := AdoQuery.FieldByName('bqtcrt').AsFloat;
//        AdsQuery.ParamByName('bqhcrt').AsFloat := AdoQuery.FieldByName('bqhcrt').AsFloat;
//        AdsQuery.ParamByName('bqdcrt').AsFloat := AdoQuery.FieldByName('bqdcrt').AsFloat;
//        AdsQuery.ParamByName('bqrcrt').AsFloat := AdoQuery.FieldByName('bqrcrt').AsFloat;
//        AdsQuery.ParamByName('bqicrt').AsFloat := AdoQuery.FieldByName('bqicrt').AsFloat;
//        AdsQuery.ParamByName('bqscrt').AsFloat := AdoQuery.FieldByName('bqscrt').AsFloat;
//        AdsQuery.ParamByName('bqacrt').AsFloat := AdoQuery.FieldByName('bqacrt').AsFloat;
//        AdsQuery.ParamByName('bqncrt').AsFloat := AdoQuery.FieldByName('bqncrt').AsFloat;
//        AdsQuery.ParamByName('bqgbon').AsFloat := AdoQuery.FieldByName('bqgbon').AsFloat;
//        AdsQuery.ParamByName('bqpbon').AsFloat := AdoQuery.FieldByName('bqpbon').AsFloat;
//        AdsQuery.ParamByName('bqtbon').AsFloat := AdoQuery.FieldByName('bqtbon').AsFloat;
//        AdsQuery.ParamByName('bqhbon').AsFloat := AdoQuery.FieldByName('bqhbon').AsFloat;
//        AdsQuery.ParamByName('bqdbon').AsFloat := AdoQuery.FieldByName('bqdbon').AsFloat;
//        AdsQuery.ParamByName('bqrbon').AsFloat := AdoQuery.FieldByName('bqrbon').AsFloat;
//        AdsQuery.ParamByName('bqibon').AsFloat := AdoQuery.FieldByName('bqibon').AsFloat;
//        AdsQuery.ParamByName('bqsbon').AsFloat := AdoQuery.FieldByName('bqsbon').AsFloat;
//        AdsQuery.ParamByName('bqabon').AsFloat := AdoQuery.FieldByName('bqabon').AsFloat;
//        AdsQuery.ParamByName('bqnbon').AsFloat := AdoQuery.FieldByName('bqnbon').AsFloat;
//        AdsQuery.ParamByName('bqgmin').AsFloat := AdoQuery.FieldByName('bqgmin').AsFloat;
//        AdsQuery.ParamByName('bqpmin').AsFloat := AdoQuery.FieldByName('bqpmin').AsFloat;
//        AdsQuery.ParamByName('bqtmin').AsFloat := AdoQuery.FieldByName('bqtmin').AsFloat;
//        AdsQuery.ParamByName('bqhmin').AsFloat := AdoQuery.FieldByName('bqhmin').AsFloat;
//        AdsQuery.ParamByName('bqdmin').AsFloat := AdoQuery.FieldByName('bqdmin').AsFloat;
//        AdsQuery.ParamByName('bqrmin').AsFloat := AdoQuery.FieldByName('bqrmin').AsFloat;
//        AdsQuery.ParamByName('bqimin').AsFloat := AdoQuery.FieldByName('bqimin').AsFloat;
//        AdsQuery.ParamByName('bqsmin').AsFloat := AdoQuery.FieldByName('bqsmin').AsFloat;
//        AdsQuery.ParamByName('bqamin').AsFloat := AdoQuery.FieldByName('bqamin').AsFloat;
//        AdsQuery.ParamByName('bqnmin').AsFloat := AdoQuery.FieldByName('bqnmin').AsFloat;
//        AdsQuery.ParamByName('bqgmino').AsFloat := AdoQuery.FieldByName('bqgmino').AsFloat;
//        AdsQuery.ParamByName('bqpmino').AsFloat := AdoQuery.FieldByName('bqpmino').AsFloat;
//        AdsQuery.ParamByName('bqtmino').AsFloat := AdoQuery.FieldByName('bqtmino').AsFloat;
//        AdsQuery.ParamByName('bqhmino').AsFloat := AdoQuery.FieldByName('bqhmino').AsFloat;
//        AdsQuery.ParamByName('bqdmino').AsFloat := AdoQuery.FieldByName('bqdmino').AsFloat;
//        AdsQuery.ParamByName('bqrmino').AsFloat := AdoQuery.FieldByName('bqrmino').AsFloat;
//        AdsQuery.ParamByName('bqimino').AsFloat := AdoQuery.FieldByName('bqimino').AsFloat;
//        AdsQuery.ParamByName('bqsmino').AsFloat := AdoQuery.FieldByName('bqsmino').AsFloat;
//        AdsQuery.ParamByName('bqamino').AsFloat := AdoQuery.FieldByName('bqamino').AsFloat;
//        AdsQuery.ParamByName('bqnmino').AsFloat := AdoQuery.FieldByName('bqnmino').AsFloat;
//        AdsQuery.ParamByName('bqgcom').AsFloat := AdoQuery.FieldByName('bqgcom').AsFloat;
//        AdsQuery.ParamByName('bqpcom').AsFloat := AdoQuery.FieldByName('bqpcom').AsFloat;
//        AdsQuery.ParamByName('bqtcom').AsFloat := AdoQuery.FieldByName('bqtcom').AsFloat;
//        AdsQuery.ParamByName('bqhcom').AsFloat := AdoQuery.FieldByName('bqhcom').AsFloat;
//        AdsQuery.ParamByName('bqdcom').AsFloat := AdoQuery.FieldByName('bqdcom').AsFloat;
//        AdsQuery.ParamByName('bqrcom').AsFloat := AdoQuery.FieldByName('bqrcom').AsFloat;
//        AdsQuery.ParamByName('bqicom').AsFloat := AdoQuery.FieldByName('bqicom').AsFloat;
//        AdsQuery.ParamByName('bqscom').AsFloat := AdoQuery.FieldByName('bqscom').AsFloat;
//        AdsQuery.ParamByName('bqacom').AsFloat := AdoQuery.FieldByName('bqacom').AsFloat;
//        AdsQuery.ParamByName('bqncom').AsFloat := AdoQuery.FieldByName('bqncom').AsFloat;
//        AdsQuery.ParamByName('bqtotcom').AsFloat := AdoQuery.FieldByName('bqtotcom').AsFloat;
//        AdsQuery.ParamByName('bqaccrt').AsFloat := AdoQuery.FieldByName('bqaccrt').AsFloat;
//        AdsQuery.ParamByName('bqacbon').AsFloat := AdoQuery.FieldByName('bqacbon').AsFloat;
//        AdsQuery.ParamByName('bqacmin').AsFloat := AdoQuery.FieldByName('bqacmin').AsFloat;
//        AdsQuery.ParamByName('bqacmino').AsFloat := AdoQuery.FieldByName('bqacmino').AsFloat;
//        AdsQuery.ExecSQL;
//        AdoQuery.Next;
//      end;
//      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaBOPSLSS'')');
//      ScrapeCountTable('BOPSLSS', 'stgArkonaBOPSLSS');
//      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
//        QuotedStr(executable) + ', ' +
//        QuotedStr(proc) + ', ' +
//        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
//        'null' + ')');
//    except
//      on E: Exception do
//      begin
//        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
//          QuotedStr(executable) + ', ' +
//          QuotedStr(proc) + ', ' +
//          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
//          QuotedStr(E.Message) + ')');
//        Raise Exception.Create('BOPSLSS.  MESSAGE: ' + E.Message);
//      end;
//    end;
//  finally
//    CloseQuery(AdsQuery);
//    CloseQuery(AdoQuery);
//  end;
//end;

procedure TDataModule1.BOPVREF;
var
  proc: string;
begin
  proc := 'bopvref';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
//      ExecuteQuery(AdsQuery, 'insert into xxxxxtempxxxxxx select * from xxxxxxstgxxxxxx');
//      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaBOPVREF'')');
      executeQuery(AdsQuery, 'delete from stgArkonaBOPVREF');
      PrepareQuery(AdsQuery, ' insert into stgArkonaBOPVREF(' +
          'BVCO#, BVKEY, BVVIN, BVSDAT, BVEDAT, BVTYPE, BVSLSP, BVDLR#, BVLHLD) ' +
          'values(' +
          ':BVCO, :BVKEY, :BVVIN, :BVSDAT, :BVEDAT, :BVTYPE, :BVSLSP, :BVDLR, :BVLHLD)');
      OpenQuery(AdoQuery, 'select ' +
        'bvco#, bvkey, bvvin, ' +
        'case ' +
        '  when bvsdat = 0 then cast(''9999-12-31'' as date) ' +
        '  else cast(left(digits(bvsdat), 4) || ''-'' || substr(digits(bvsdat), 5, 2) || ''-'' || substr(digits(bvsdat), 7, 2) as date) ' +
        'end as bvsdat, ' +
        'case ' +
        '  when bvedat = 0 then cast(''9999-12-31'' as date) ' +
        '  else cast(left(digits(bvedat), 4) || ''-'' || substr(digits(bvedat), 5, 2) || ''-'' || substr(digits(bvedat), 7, 2) as date) ' +
        'end as bvedat, ' +
        'bvtype, bvslsp, bvdlr#, bvlhld ' +
        'from RYDEDATA.BOPVREF');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('bvco').AsString := AdoQuery.FieldByName('bvco#').AsString;
        AdsQuery.ParamByName('bvkey').AsInteger := AdoQuery.FieldByName('bvkey').AsInteger;
        AdsQuery.ParamByName('bvvin').AsString := AdoQuery.FieldByName('bvvin').AsString;
        AdsQuery.ParamByName('bvsdat').AsDateTime := AdoQuery.FieldByName('bvsdat').AsDateTime;
        AdsQuery.ParamByName('bvedat').AsDateTime := AdoQuery.FieldByName('bvedat').AsDateTime;
        AdsQuery.ParamByName('bvtype').AsString := AdoQuery.FieldByName('bvtype').AsString;
        AdsQuery.ParamByName('bvslsp').AsString := AdoQuery.FieldByName('bvslsp').AsString;
        AdsQuery.ParamByName('bvdlr').AsString := AdoQuery.FieldByName('bvdlr#').AsString;
        AdsQuery.ParamByName('bvlhld').AsString := AdoQuery.FieldByName('bvlhld').AsString;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaBOPVREF'')');
//      ScrapeCount('xxxxxxxxxxxxdb2xxxxxxxxxxxx', 'xxxxxxxxxxxxadsxxxxxxxxxxxx');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('bopvref.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDataModule1.PYPCODES;
var
  proc: string;
begin
  proc := 'PYPCODES';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
//      ExecuteQuery(AdsQuery, 'insert into xxxxxtempxxxxxx select * from xxxxxxstgxxxxxx');
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaPYPCODES'')');
      PrepareQuery(AdsQuery, ' insert into stgArkonaPYPCODES(' +
          'YTDCO#, YTDTYP, YTDDCD, YTDDSC, YTDACT, YTDEX1, YTDEX2, YTDEX3, ' +
          'YTDEX4, YTDEX5, YTDEX6, YTDEX7, YTDEX8, YTDEX9, YTDEX10, YTDEX11, ' +
          'YTDEXF, YTDPAH, YTDFRB, YTDEXG, YTDINP, YTDCTL, YTDPTN, YTDW2C, ' +
          'YTDW2B, YTDDIS, YTDSEQ, YTD3RD, YTDEXP, YTDATT, YTDDLK, YTDGRN, ' +
          'YTDDEF, YTD401, YTDOTP) ' +
          'values(' +
          ':YTDCO, :YTDTYP, :YTDDCD, :YTDDSC, :YTDACT, :YTDEX1, :YTDEX2, :YTDEX3, ' +
          ':YTDEX4, :YTDEX5, :YTDEX6, :YTDEX7, :YTDEX8, :YTDEX9, :YTDEX10, :YTDEX11, ' +
          ':YTDEXF, :YTDPAH, :YTDFRB, :YTDEXG, :YTDINP, :YTDCTL, :YTDPTN, :YTDW2C, ' +
          ':YTDW2B, :YTDDIS, :YTDSEQ, :YTD3RD, :YTDEXP, :YTDATT, :YTDDLK, :YTDGRN, ' +
          ':YTDDEF, :YTD401, :YTDOTP)');
      OpenQuery(AdoQuery, 'select ' +
          'YTDCO#, YTDTYP, YTDDCD, YTDDSC, YTDACT, YTDEX1, YTDEX2, YTDEX3, ' +
          'YTDEX4, YTDEX5, YTDEX6, YTDEX7, YTDEX8, YTDEX9, YTDEX10, YTDEX11, ' +
          'YTDEXF, YTDPAH, YTDFRB, YTDEXG, YTDINP, YTDCTL, YTDPTN, YTDW2C, ' +
          'YTDW2B, YTDDIS, YTDSEQ, YTD3RD, YTDEXP, YTDATT, YTDDLK, YTDGRN, ' +
          'YTDDEF, YTD401, YTDOTP ' +
      'from rydedata.pypcodes');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('ytdco').AsString := AdoQuery.FieldByName('ytdco#').AsString;
        AdsQuery.ParamByName('ytdtyp').AsString := AdoQuery.FieldByName('ytdtyp').AsString;
        AdsQuery.ParamByName('ytddcd').AsString := AdoQuery.FieldByName('ytddcd').AsString;
        AdsQuery.ParamByName('ytddsc').AsString := AdoQuery.FieldByName('ytddsc').AsString;
        AdsQuery.ParamByName('ytdact').AsString := AdoQuery.FieldByName('ytdact').AsString;
        AdsQuery.ParamByName('ytdex1').AsString := AdoQuery.FieldByName('ytdex1').AsString;
        AdsQuery.ParamByName('ytdex2').AsString := AdoQuery.FieldByName('ytdex2').AsString;
        AdsQuery.ParamByName('ytdex3').AsString := AdoQuery.FieldByName('ytdex3').AsString;
        AdsQuery.ParamByName('ytdex4').AsString := AdoQuery.FieldByName('ytdex4').AsString;
        AdsQuery.ParamByName('ytdex5').AsString := AdoQuery.FieldByName('ytdex5').AsString;
        AdsQuery.ParamByName('ytdex6').AsString := AdoQuery.FieldByName('ytdex6').AsString;
        AdsQuery.ParamByName('ytdex7').AsString := AdoQuery.FieldByName('ytdex7').AsString;
        AdsQuery.ParamByName('ytdex8').AsString := AdoQuery.FieldByName('ytdex8').AsString;
        AdsQuery.ParamByName('ytdex9').AsString := AdoQuery.FieldByName('ytdex9').AsString;
        AdsQuery.ParamByName('ytdex10').AsString := AdoQuery.FieldByName('ytdex10').AsString;
        AdsQuery.ParamByName('ytdex11').AsString := AdoQuery.FieldByName('ytdex11').AsString;
        AdsQuery.ParamByName('ytdexf').AsString := AdoQuery.FieldByName('ytdexf').AsString;
        AdsQuery.ParamByName('ytdpah').AsString := AdoQuery.FieldByName('ytdpah').AsString;
        AdsQuery.ParamByName('ytdfrb').AsString := AdoQuery.FieldByName('ytdfrb').AsString;
        AdsQuery.ParamByName('ytdexg').AsString := AdoQuery.FieldByName('ytdexg').AsString;
        AdsQuery.ParamByName('ytdinp').AsString := AdoQuery.FieldByName('ytdinp').AsString;
        AdsQuery.ParamByName('ytdctl').AsString := AdoQuery.FieldByName('ytdctl').AsString;
        AdsQuery.ParamByName('ytdptn').AsString := AdoQuery.FieldByName('ytdptn').AsString;
        AdsQuery.ParamByName('ytdw2c').AsString := AdoQuery.FieldByName('ytdw2c').AsString;
        AdsQuery.ParamByName('ytdw2b').AsString := AdoQuery.FieldByName('ytdw2b').AsString;
        AdsQuery.ParamByName('ytddis').AsString := AdoQuery.FieldByName('ytddis').AsString;
        AdsQuery.ParamByName('ytdseq').AsInteger := AdoQuery.FieldByName('ytdseq').AsInteger;
        AdsQuery.ParamByName('ytd3rd').AsString := AdoQuery.FieldByName('ytd3rd').AsString;
        AdsQuery.ParamByName('ytdexp').AsString := AdoQuery.FieldByName('ytdexp').AsString;
        AdsQuery.ParamByName('ytdatt').AsString := AdoQuery.FieldByName('ytdatt').AsString;
        AdsQuery.ParamByName('ytddlk').AsString := AdoQuery.FieldByName('ytddlk').AsString;
        AdsQuery.ParamByName('ytdgrn').AsString := AdoQuery.FieldByName('ytdgrn').AsString;
        AdsQuery.ParamByName('ytddef').AsString := AdoQuery.FieldByName('ytddef').AsString;
        AdsQuery.ParamByName('ytd401').AsString := AdoQuery.FieldByName('ytd401').AsString;
        AdsQuery.ParamByName('ytdotp').AsString := AdoQuery.FieldByName('ytdotp').AsString;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaPYPCODES'')');
      ScrapeCountTable('PYPCODES', 'stgArkonaPYPCODES');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('PYPCODES.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDataModule1.PYPRJOBD;
var
  proc: string;
begin
  proc := 'PYPRJOBD';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
//      ExecuteQuery(AdsQuery, 'insert into xxxxxtempxxxxxx select * from xxxxxxstgxxxxxx');
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''stgArkonaPYPRJOBD'')');
      PrepareQuery(AdsQuery, ' insert into stgArkonaPYPRJOBD(' +
          'YRCO#, YRJOBD, YRJOBL, YRSEQ, YRPER, YRTEXT) ' +
          'values(' +
          ':YRCO, :YRJOBD, :YRJOBL, :YRSEQ, :YRPER, :YRTEXT)');
      OpenQuery(AdoQuery, 'select YRCO#, YRJOBD, YRJOBL, YRSEQ, YRPER, YRTEXT ' +
          'from rydedata.pyprjobd');
      while not AdoQuery.Eof do
      begin
        AdsQuery.ParamByName('yrco').AsString := AdoQuery.FieldByName('yrco#').AsString;
        AdsQuery.ParamByName('yrjobd').AsString := AdoQuery.FieldByName('yrjobd').AsString;
        AdsQuery.ParamByName('yrjobl').AsString := AdoQuery.FieldByName('yrjobl').AsString;
        AdsQuery.ParamByName('yrseq').AsInteger := AdoQuery.FieldByName('yrseq').AsInteger;
        AdsQuery.ParamByName('yrper').AsString := AdoQuery.FieldByName('yrper').AsString;
        AdsQuery.ParamByName('yrtext').AsString := AdoQuery.FieldByName('yrtext').AsString;
        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''stgArkonaPYPRJOBD'')');
//      ScrapeCount('xxxxxxxxxxxxdb2xxxxxxxxxxxx', 'xxxxxxxxxxxxadsxxxxxxxxxxxx');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('PYPRJOBD.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

procedure TDataModule1.ExtractFramework;
var
  proc: string;
begin
  proc := '**************ExtractFramework***********';
  DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
    QuotedStr(executable) + ', ' +
    QuotedStr(proc) + ', ' +
    QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
  try
    try
      ExecuteQuery(AdsQuery, 'insert into xxxxxtempxxxxxx select * from xxxxxxstgxxxxxx');
      ExecuteQuery(AdsQuery, 'execute procedure sp_ZapTable(''xxxxxxadsxxxxxx'')');
      PrepareQuery(AdsQuery, ' insert into xxxxxxadsxxxxxx(' +
          ') ' +
          'values(' +
          ')');
      OpenQuery(AdoQuery, '');
      while not AdoQuery.Eof do
      begin

        AdsQuery.ExecSQL;
        AdoQuery.Next;
      end;
      ExecuteQuery(AdsQuery, 'execute procedure stgTrimCharacterFields(''xxxxxadsxxxxxxx'')');
      ScrapeCount('xxxxxxxxxxxxdb2xxxxxxxxxxxx', 'xxxxxxxxxxxxadsxxxxxxxxxxxx');
      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
        QuotedStr(executable) + ', ' +
        QuotedStr(proc) + ', ' +
        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
        'null' + ')');
    except
      on E: Exception do
      begin
        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
          QuotedStr(executable) + ', ' +
          QuotedStr(proc) + ', ' +
          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
          QuotedStr(E.Message) + ')');
        Raise Exception.Create('xxxxxdb2xxxxxxx.  MESSAGE: ' + E.Message);
      end;
    end;
  finally
    CloseQuery(AdsQuery);
    CloseQuery(AdoQuery);
  end;
end;

//************************Utilities*******************************************//

function TDataModule1.GetADSSqlTimeStamp(const DateTime: TDateTime): String;
var
  ye, mo, da, ho, mi, se, ms: Word;
begin
  DecodeDateTime(DateTime, ye, mo, da, ho, mi, se, ms);
  Result := FormatWordToStr(ye, 4) + '-' +
            FormatWordToStr(mo, 2) + '-' +
            FormatWordToStr(da, 2) + ' ' +
            FormatWordToStr(ho, 2) + ':' +
            FormatWordToStr(mi, 2) + ':' +
            FormatWordToStr(se, 2);
end;

function TDataModule1.FormatWordToStr(const inVal: Word;
  const OutLength: Integer; const PadChar: Char): String;
begin
  Result := IntToStr(inVal);
  if Length(Result) < OutLength then
    Result := StringOfChar(PadChar, OutLength - Length(Result)) + Result;
end;

procedure TDataModule1.curLoadIsEmpty(tableName: string);
var
  curLoadCount: Integer;
begin
  OpenQuery(AdsQuery, 'select count(*) as CurLoad from curLoad' + tableName);
  curLoadCount := AdsQuery.FieldByName('CurLoad').AsInteger;
  CloseQuery(AdsQuery);
  if curLoadCount <> 0 then
    SendMail(tableName + ' failed curLoadIsEmpty');
  Assert(curLoadCount = 0, 'curLoad' + tableName + ' not empty');
end;

procedure TDataModule1.Log(job, routine: string; jobStartTS, jobEndTS: TDateTime);
var
  sql: string;
begin
  PrepareQuery(LogQuery);
  LogQuery.SQL.Add('execute procedure etlJobLogAuditInsert(' + QuotedStr(job) + ', ' +
    QuotedStr(routine) + ', ' +  QuotedStr(GetADSSqlTimeStamp(jobStartTS)) + ', ' +
    QuotedStr(GetADSSqlTimeStamp(jobEndTS)) + ')');
  sql := LogQuery.SQL.Text;
  LogQuery.ExecSQL;
end;

procedure TDataModule1.curLoadPrevLoad(tableName: string);
{ TODO -ojon -crefactor : is this the best way to do this? need these holding tables to be kept to a minimum size,
  don't accumlate deleted records
  12/6/11 appears to be working ok }
begin
  try
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_ZapTable(' + '''' + 'prevLoad' + tableName + '''' + ')');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'prevLoad' + tableName + '''' + ', ' + '''' + 'prevLoad' + tableName + 'x' + '''' +  ', 1, 0)');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'curLoad' + tableName + '''' + ', ' + '''' + 'prevLoad' + tableName + '''' + ', 1, 0)');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'prevLoad' + tableName + 'x' + '''' + ', ' + '''' + 'curLoad' + tableName + '''' + ', 1, 0)');
  except
    on E: Exception do
    begin
      Raise Exception.Create('curLoadPrevLoad.  MESSAGE: ' + E.Message);
    end;
  end;
end;

procedure TDataModule1.ExecuteQuery(query: TDataSet; sql: string);
begin
//  try
    if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
    begin
      AdsQuery.AdsConnection := AdsCon;
      CloseQuery(AdsQuery);
      AdsQuery.SQL.Text := sql;
      AdsQuery.ExecSQL;
      CloseQuery(AdsQuery);
    end
    else if query.ClassParent.ClassName = 'TCustomADODataSet' then
    begin
      AdoQuery.Connection := AdoCon;
      CloseQuery(AdoQuery);
      AdoQuery.SQL.Text := sql;
      AdoQuery.ExecSQL;
      CloseQuery(AdoQuery);
    end;
end;


procedure TDataModule1.OpenQuery(query: TDataSet; sql: string);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
    AdsQuery.SQL.Text := sql;
    AdsQuery.Open;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
    AdoQuery.SQL.Text := sql;
    AdoQuery.Open;
  end;
end;



procedure TDataModule1.PrepareQuery(query: TDataSet; sql: string);
begin
//  if query = 'TAdsExtendedDataSet' then
  if query.classparent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
{ TODO -ojon -cfigure out : don't remember why i commented out close - figure it out and be consistent }
//    AdsQuery.Close;
    if sql <> '' then
      AdsQuery.SQL.Text := sql;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    if sql <> '' then
      AdoQuery.SQL.Text := sql;
  end;
end;


procedure TDataModule1.SendMail(Subject: string; Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
        ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            '''' + 'send mail failed' + '''' + ', ' +
            '''' + 'no sql - line 427' + '''' + ', ' +
            QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        exit;
      end;
    end;
  finally
//    Msg.Free;
    FreeAndNil(Msg);
//    SMTP.Free;
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;






procedure TDataModule1.CloseQuery(query: TDataSet);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
  end;
end;

procedure TDataModule1.ScrapeCount(db2, ads: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + ads);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckQuery between ' + db2 + '(' + IntToStr(db2Count) + ') and ' + ads + '(' + IntToStr(adsCount) + ')  failed');
end;

procedure TDataModule1.ScrapeCountQuery(db2Q, adsQ: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Q);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsQ);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckQuery between ' + db2Q + '(' + IntToStr(db2Count) + ') and ' + adsQ + '(' + IntToStr(adsCount) + ')  failed');
end;

procedure TDataModule1.ScrapeCountTable(db2Table, adsTable: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  db2Table := 'rydedata.' + db2Table;
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Table);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsTable);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckTable between ' + db2Table + '(' + IntToStr(db2Count) + ') and ' + adsTable + '(' + IntToStr(adsCount) + ')  failed');
end;
end.
